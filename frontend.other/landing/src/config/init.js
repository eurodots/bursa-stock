// console.log( process.env )

export const ENV = process.env.NODE_ENV || 'development';
export const isProduction = ENV === 'production';

// export const apiDomain = isProduction ? 'https://bursadex.com' : 'http://127.0.0.1:8000';
export const apiDomain = isProduction ? 'http://bursadex.com' : 'http://127.0.0.1:8000';
// export const apiDomain = isProduction ? 'http://bursadex.com:8888' : 'http://bursadex.com:8888';
export const apiEndpoint = `${apiDomain}/api`



export const confSite = {
    name: 'StockScript.com',
    version: 'beta 1.0',
    slogan: 'Decentralized trading with Smart Contract',
    links: {
        facebook: 'http://facebook.com/...',
        bitcointalk: 'http://bitcointalk.com/...',
    },
    donations: {
        ETH: {
            'name': 'Ethereum',
            'wallet': '0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0',
        },
        BTC: {
            'name': 'BitCoin',
            'wallet': '0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0',
        },
    }
}
