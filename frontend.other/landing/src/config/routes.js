import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from '../containers/Home';
import Demo from '../containers/Demo';
import Docs from '../containers/Docs';
import Tokens from '../containers/Tokens';
import Support from '../containers/Support';
import Pricing from '../containers/Pricing';

import UserStocks from '../containers/User/Stocks';
import UserStocksAdd from '../containers/User/StocksAdd';

import NotFoundPage from '../containers/NotFoundPage';

import AdminComponent from '../containers/Auth/Admin'
import LoginComponent from '../containers/Auth/Login'
import Verify from '../containers/Auth/Verify'


import { userIsAuthenticatedRedir, userIsNotAuthenticatedRedir, userIsAdminRedir,
         userIsAuthenticated, userIsNotAuthenticated } from '../auth'


// Need to apply the hocs here to avoid applying them inside the render method
const Login = userIsNotAuthenticatedRedir(LoginComponent)
const Admin = userIsAuthenticatedRedir(userIsAdminRedir(AdminComponent))

const ProtectedUserStocks = userIsAuthenticatedRedir(UserStocks)
const ProtectedUserStocksAdd = userIsAuthenticatedRedir(UserStocksAdd)


export default (
  <Switch>
    <Route exact path="/" component={Home}/>
    <Route exact path="/p/demo" component={Demo}/>
    <Route exact path="/p/docs" component={Docs}/>
    <Route exact path="/p/pricing" component={Pricing}/>
    <Route exact path="/p/tokens" component={Tokens}/>
    <Route exact path="/p/support" component={Support}/>

    <Route exact path="/p/login" component={Login}/>
    <Route exact path="/p/signup" component={Login}/>
    <Route path="/p/login/reset/:password_reset_uid/:password_reset_token" component={Login}/>
    <Route path="/p/verify/:token" component={Verify}/>

    <Route exact path="/p/user/stocks" component={ProtectedUserStocks}/>
    <Route exact path="/p/user/stocks/add" component={ProtectedUserStocksAdd}/>
    <Route path="/p/user/stocks/add/:tariff" component={ProtectedUserStocksAdd}/>

    <Route component={NotFoundPage} />

  </Switch>
);
