import cookielib from 'react-cookies';
import axios from 'axios';
const _ = require('lodash');

import {apiDomain, apiEndpoint, confSite} from '../config/init'



// PAGE TITLE FUNC()
export function pageTitle(title=false) {
    if(!title) {
        document.title = confSite.name
    } else {
        document.title = `${title} — ${confSite.name}`
    }
    return true
}



export function apiContactForm(request) {
    const url = `${apiEndpoint}/support/contactform/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
	}).catch((error) => {
		// console.log(error);
        return error.response
	})

	return data
}

export function apiTokenAuth() {

    const url = `${apiEndpoint}/accounts/user-profile/`
    console.log(url)

    const data = axios.get(url).then((response) => {
        return response
    }).catch((error) => {
        console.log(error);
        return error.response
    })

    return data;

}


export function apiPasswordSubmit(uid, token, request) {
    const url = `${apiEndpoint}/accounts/reset/${uid}/${token}/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
	}).catch((error) => {
		// console.log(error);
        return error.response
	})

	return data
}


export function apiPasswordReset(request) {
    const url = `${apiEndpoint}/accounts/password_reset/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
	}).catch((error) => {
		// console.log(error);
        return error.response
	})

	return data
}

export function apiSignupVerify(token) {
    const url = `${apiEndpoint}/accounts/verify/${token}/`
    console.log(url)

	const data = axios.get(url).then((response) => {
		return response
	}).catch((error) => {
		// console.log(error);
        return error.response
	})

	return data
}

export function apiSignup(request) {
    const url = `${apiEndpoint}/accounts/register/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
	}).catch((error) => {
		// console.log(error);
        return error.response
	})

	return data
}

export function apiSignin(request) {
    const url = `${apiEndpoint}/accounts/login/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
	}).catch((error) => {
		// console.log(error);
        return error.response
	})

	return data
}


export function apiPages(folder=false) {
    const url = `${apiEndpoint}/pages/${folder}/`
    console.log(url)

	const data = axios.get(url).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

export function apiFaviconsList() {
    const url = `${apiEndpoint}/coins_favicons/`
    console.log(url)

	const data = axios.get(url).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

export function apiUserStocksList() {
    let request = {
        'user_token': cookie('get','token')
    }
    const url = `${apiEndpoint}/user/stocks/list/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

export function apiUserStocksDelete(id) {

    let request = {
        'user_token': cookie('get','token'),
        'stock_id': id,
    }
    const url = `${apiEndpoint}/user/stocks/delete/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

export function apiUserStocksAddData() {
    const url = `${apiEndpoint}/user/stocks/add/`
    console.log(url)

	const data = axios.get(url).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

export function apiUserStocksAddSubmit(postData) {
    const url = `${apiEndpoint}/user/stocks/add/`
    console.log(url)

	const data = axios.post(url, postData).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}


export function apiLinkPreview(postData) {
    const url = `${apiEndpoint}/linkpreview/`
    console.log(url)

	const data = axios.post(url, postData).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}




export function cookie(type, payload) {
    switch (type) {
    	case 'get':
            let value = cookielib.load(payload)
            if(typeof(value) === 'undefined' || value === 'undefined') {
                value = false
            }
    		return value
        case 'set':
            cookielib.save(payload[0], payload[1], {path: '/'})
            return true
        case 'delete':
            cookielib.remove(payload, {path: '/'})
            return true
    	default:
    		return false
    }
}


// Token for API Authorization
function applyAxiosHeader() {
    axios.defaults.headers.common['Authorization'] = `Token ${cookie('get', 'token')}`;
    axios.defaults.xsrfCookieName = 'csrftoken';
    axios.defaults.xsrfHeaderName = 'X-CSRFToken';
} applyAxiosHeader()
