import React from 'react';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';

import theme from './theme.scss'

import axios, { post } from 'axios';
class SimpleReactFileUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state ={
      file: null,
      button_disabled: false,
      file_src: false,
      error: false,
    }
    // this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.fileUpload = this.fileUpload.bind(this)
  }
  // onFormSubmit(e){
  //   e.preventDefault() // Stop form submit
  //   this.fileUpload(this.state.file).then((response)=>{
  //     console.log(response.data);
  //   })
  // }
  onChange(e) {

    this.setState({
        file: e.target.files[0],
        button_disabled: true,
    }, () => {
        this.fileUpload(this.state.file).then((response)=>{

          let d = response.data
          if(d.error) {
              this.setState({error: response.data.errors.file[0]}, () => {})
          } else {
              this.setState({file_src: d.file_src})
          }

          this.setState({button_disabled: false}, () => {})
        })
    })

  }
  fileUpload(file){

    const url = 'http://127.0.0.1:8000/api/icosignup_favicon_upload/';
    const formData = new FormData();
    formData.append('name',this.props.faviconName)
    formData.append('file',file)
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    return post(url, formData,config)
  }

  removeFavicon = () => {
      this.setState({file_src: false}, () => {})
  }

  renderFaviconUploader() {
      const { button_disabled, file_src, error } = this.state

      let init = {
          id: 'token_favicon'
      }

      if(file_src) {
          return (
              <div className={theme.fileuploadPreview}>
                  <div data-el="close">
                      <Icon data-link onClick={this.removeFavicon}>close</Icon>
                  </div>
                  <div data-el="image">
                      <img src={file_src} />
                  </div>
              </div>
          )
      }
      return (
          <ul className={theme.fileuploadAction}>
              <li>
                  <input
                      accept="image/*"
                      id={init.id}
                      multiple={false}
                      type="file"
                      onChange={this.onChange}
                      style={{display: 'none'}}
                    />
                    <label htmlFor={init.id}>
                      <Button variant="raised" component="span" disabled={button_disabled} data-nowrap>
                        Upload favicon
                      </Button>
                    </label>
              </li>
              <li>
                  {error ? <span data-color="danger">{error}</span> : 'Max file size: 2 MB'}
              </li>
        </ul>
      )
  }

  render() {
    return (
      <div className={theme.fileuploadWrapper}>
          {this.renderFaviconUploader()}
      </div>
   )
  }
}

export default SimpleReactFileUpload
