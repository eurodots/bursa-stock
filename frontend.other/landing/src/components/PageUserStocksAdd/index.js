import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import Typography from 'material-ui/Typography';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import Input, { InputLabel } from 'material-ui/Input';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Icon from 'material-ui/Icon';

import {CopyToClipboard} from 'react-copy-to-clipboard';
import LinksPreviewer from '../LinksPreviewer'
import FaviconUploader from './FaviconUploader'

import BlockForm from '../BlockForm'
import FormButton from '../BlockForm/FormButton'
import Preloader from '../Preloader'

import {apiDomain} from '../../config/init'
import {apiUserStocksAddSubmit, apiUserStocksAddData} from '../../utils/functions'

import theme from './theme.scss'



const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
      maxWidth: 250,
    },
  },
};

const styles = theme => ({
  // root: {
  //   // display: 'flex',
  //   // flexWrap: 'wrap',
  // },
  formControl: {
    // margin: theme.spacing.unit,
    minWidth: 120,
    width: '100%',
  },
  textField: {
    // marginLeft: theme.spacing.unit,
    // marginRight: theme.spacing.unit,
    // margin: theme.spacing.unit,
    width: '100%',
  },
  selectField: {
      width: '100%',
  },
});

class PageUserStocksAdd extends React.Component {

    state = {
        data_categories: [],
        data_industries: [],
        data_countries: [],
        data_platforms: [],
        data_statuses: [],
        data_tariffs: [],
        data_categories_active: false,
        data_industries_active: false,
        data_countries_active: false,
        data_platforms_active: false,
        data_statuses_active: false,
        data_tariffs_active: false,

        // token_category: [],
        // token_industry: [],
        // token_country: '',
        // token_platform: '',
        // token_status: '',
        //
        // token_name: '',
        // token_ticker: '',
        // token_address: '',
        // token_description: '',
        // token_links: [],

        token_tariff: '',
        token_category: [1],
        token_industry: [1],
        token_country: 'RU',
        token_platform: 3,
        token_status: 0,

        token_wallet: '0x0DE0BcB0703FF8f1AEb8c892EdbE692683Bd8F30',
        token_domain: 'stockscript.com',
        token_name: 'My token name',
        token_ticker: 'OMG',
        token_address: '0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0',
        token_description: 'Nice token',
        token_links: [],

        errors: {
            token_category: false,
            token_industry: false,
            token_country: false,
            token_platform: false,
            token_status: false,
            token_tariff: false,
            // owner_email: false,
            token_wallet: false,
            token_domain: false,
            token_name: false,
            token_ticker: false,
            token_address: false,
            token_description: false,
        },



        form_disabled: false,
        submit_response: false,
        widget_copied: false,
        favicon_random_name: false,
        form_tariff: 'free',
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     if(this.state.data_categories.length > 0) {
    //         return false
    //     }
    //     return true
    // }


    componentDidMount() {

        const {match} = this.props
        let load_tariff = match.params.tariff ? match.params.tariff : false
        if(load_tariff == 'pro' || load_tariff == 'enterprise') {
            load_tariff = load_tariff
        } else {
            load_tariff = 'free'
        }
        this.setState({
            form_tariff: load_tariff,
            token_tariff: load_tariff,
        }, () => {})

        apiUserStocksAddData().then((response) => {
            let d = response.data
            this.setState({
                data_tariffs: d.tariffs,
                data_categories: d.categories,
                data_countries: d.countries,
                data_industries: d.industries,
                data_platforms: d.platforms,
                data_statuses: d.statuses,
            }, () => {})
            // console.log(this.state)
        })

        this.faviconRandomId()
    }


    faviconRandomId() {
        let rand = Math.floor(100000000 + Math.random() * 900000000)
        this.setState({favicon_random_name: rand})
    }


    formSubmit = (event) => {
        event.preventDefault()
        const {
            token_tariff,
            token_category,
            token_industry,
            token_country,
            token_platform,
            token_status,
            token_wallet,
            token_domain,
            token_name,
            token_ticker,
            token_address,
            token_description,
            token_links,

            favicon_random_name
        } = this.state

        let categories = token_category.join(',')
            categories = `[${categories}]`

        let industries = token_industry.join(',')
            industries = `[${industries}]`

        let links = token_links.join('||')
            links = `${links}`

        const joinData = {
            user_token: this.props.user.data.token,
            token_tariff: token_tariff,
            token_category: categories,
            token_industry: industries,
            token_country: token_country,
            token_platform: token_platform,
            token_status: token_status,
            token_wallet: token_wallet,
            token_domain: token_domain,
            token_name: token_name,
            token_ticker: token_ticker,
            token_address: token_address,
            token_description: token_description,
            token_name: token_name,
            token_links: links,

            favicon_name: favicon_random_name,
        }

        console.log( joinData )

        this.setState({form_disabled: true})

        apiUserStocksAddSubmit(joinData).then((response) => {
            // console.log('-----response------')
            // console.log(response)

            const {errors} = this.state
            let d = response.data
            if(d.error) {
                let newState = Object.assign({}, errors);
                _.keys(newState).map((item, index) => {
                    newState[item] = false
                })
                _.keys(d.errors).map((item, index) => {
                    newState[item] = d.errors[item][0];
                })
                this.setState({errors: newState}, () => {});
                console.log(d.errors)
            } else {
                this.setState({submit_response: d})
                this.props.history.push(`/p/user/stocks`)
            }
            this.setState({form_disabled: false}, () => {});
            window.scrollTo(0,0);
        })
    }

    handleChangeSelect = (event, active, status) => {
        this.setState({
            [event.target.name]: event.target.value,
            [active]: status,
        });
    };
    handleChangeSelectOpen = (active, status) => {
        this.setState({[active]: status})
    }


    handleChangeInput = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    renderTextfield(init) {
        const { classes } = this.props;
        const {errors, form_disabled} = this.state;
        const error_id = `${init.state}-error`

        return (
            <div>
                <FormControl className={classes.formControl} error={errors[init.state] ? true : false} aria-describedby={error_id}>
                    <InputLabel htmlFor={init.state}>{init.label}</InputLabel>
                    <Input
                        id={init.state}
                        value={this.state[init.state]}
                        onChange={this.handleChangeInput(init.state)}
                        multiline={init.multiline}
                        type={init.type}
                        margin="none"
                        className={classes.textField}
                        disabled={form_disabled}
                        />
                    <FormHelperText id={error_id}>{errors[init.state]}</FormHelperText>
                </FormControl>
            </div>
        )
    }

    renderSelectSingle(init) {
        const {classes} = this.props
        const {errors, form_disabled} = this.state;
        const error_id = `${init.id}-error`
        const list = this.state[init.state]

        return (
            <FormControl className={classes.formControl} error={errors[init.id] ? true : false} aria-describedby={error_id}>
              <InputLabel htmlFor={init.id}>{init.label}</InputLabel>
              <Select
                open={this.state[init.active]}
                value={this.state[init.id]}
                onChange={(event) => this.handleChangeSelect(event, init.active, false)}
                onOpen={() => this.handleChangeSelectOpen(init.active, true)}
                onClose={() => this.handleChangeSelectOpen(init.active, false)}
                inputProps={{
                  name: init.id,
                  id: init.id,
                }}
                className={classes.selectField}
                disabled={form_disabled}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {list.map((item, index) => {
                    return (
                        <MenuItem key={index} value={item.id}>{item.value}</MenuItem>
                    )
                })}
              </Select>
              <FormHelperText id={error_id}>{errors[init.id]}</FormHelperText>
            </FormControl>
        )
    }

    renderSelectMultiple(init) {
        const {classes} = this.props
        const {errors, form_disabled} = this.state;
        const error_id = `${init.id}-error`
        const list = this.state[init.state]

        let openLimiter = true
        if(this.state[init.id].length >= 1) {
            openLimiter = false
        }

        return (
            <FormControl className={classes.formControl} error={errors[init.id] ? true : false} aria-describedby={error_id}>
              <InputLabel htmlFor={init.id}>{init.label}</InputLabel>
              <Select
                multiple
                open={this.state[init.active]}
                value={this.state[init.id]}
                onChange={(event) => this.handleChangeSelect(event, init.active, openLimiter)}
                onOpen={() => this.handleChangeSelectOpen(init.active, true)}
                onClose={() => this.handleChangeSelectOpen(init.active, false)}
                input={<Input id={init.id} name={init.id} />}
                MenuProps={MenuProps}
                className={classes.selectField}
                disabled={form_disabled}
              >
                {list.map((item, index) => {
                    return (
                        <MenuItem
                          key={index}
                          value={item.id}
                        >
                          {item.value}
                        </MenuItem>
                    )
                })}
              </Select>
              <FormHelperText id={error_id}>{errors[init.id]}</FormHelperText>
            </FormControl>
        )
    }



    // style={{
    //     fontWeight:
    //       this.state[state_id].indexOf(item.value) === -1
    //         ? theme.typography.fontWeightRegular
    //         : theme.typography.fontWeightMedium,
    //   }}


    onSaveLinks = (links_arr) => {
        this.setState({token_links: links_arr}, () => {
            console.log(this.state.token_links)
        });
    }



	render() {
        const {classes} = this.props
        const {form_tariff, data_categories, form_disabled, submit_response, widget_copied, favicon_random_name} = this.state


        if(submit_response) {
            return (
                <div data-el="block" className={theme.responseWrapper}>
                    <div data-el="inner">
                        <Typography variant="title" gutterBottom>
                            Success
                        </Typography>
                        <code>{submit_response.widget}</code>

                            <CopyToClipboard text={submit_response.widget}
                              onCopy={() => this.setState({widget_copied: true})}>

                              {widget_copied
                              ?
                                  <Button variant="raised" size="small" aria-label="Copied">
                                    <Icon>check</Icon>
                                    <span data-el="btn-label">Copied</span>
                                  </Button>
                              :
                                  <Button variant="raised" color="primary" size="small" aria-label="Copy">
                                    <Icon>content_copy</Icon>
                                    <span data-el="btn-label">Copy</span>
                                  </Button>
                              }
                            </CopyToClipboard>
                    </div>
                </div>
            )
        }

        if(data_categories.length == 0) {
            return (
                <BlockForm bgid={13} title="Adding new stock">
                    <Preloader />
                </BlockForm>
            )
        }

        let form_title = `Tariff «${form_tariff}»`

		return (
            <BlockForm bgid={13} title={form_title}>
                <form onSubmit={(event) => this.formSubmit(event)} className={theme.formWrapper}>
                    <ul>
                        <li>
                            {this.renderSelectSingle({
                                state: 'data_tariffs',
                                id: 'token_tariff',
                                active: 'data_tariffs_active',
                                label: 'Tariff',
                            })}
                        </li>
                        <li>
                            {this.renderTextfield({
                                state: 'token_domain',
                                label: 'Domain',
                                type: 'text',
                                multiline: false
                            })}
                        </li>
                        <li>
                            {this.renderTextfield({
                                state: 'token_wallet',
                                label: 'ETH wallet for money incoming',
                                type: 'text',
                                multiline: false
                            })}
                        </li>
                        <li>
                            {this.renderTextfield({
                                state: 'token_name',
                                label: 'Token name',
                                type: 'text',
                                multiline: false
                            })}
                        </li>
                        <li>
                            {this.renderTextfield({
                                state: 'token_ticker',
                                label: 'Token ticker',
                                type: 'text',
                                multiline: false
                            })}
                        </li>
                        <li>
                            {this.renderTextfield({
                                state: 'token_address',
                                label: 'Token address',
                                type: 'text',
                                multiline: false
                            })}
                        </li>
                        <li>
                            {this.renderTextfield({
                                state: 'token_description',
                                label: 'Token description',
                                type: 'text',
                                multiline: true
                            })}
                        </li>
                        <li>
                            {this.renderSelectMultiple({
                                state: 'data_categories',
                                id: 'token_category',
                                active: 'data_categories_active',
                                label: 'Project categories',
                            })}
                        </li>
                        <li>
                            {this.renderSelectMultiple({
                                state: 'data_industries',
                                id: 'token_industry',
                                active: 'data_industries_active',
                                label: 'Project industries',
                            })}
                        </li>
                        <li>
                            {this.renderSelectSingle({
                                state: 'data_countries',
                                id: 'token_country',
                                active: 'data_countries_active',
                                label: 'Project country',
                            })}
                        </li>
                        <li>
                            {this.renderSelectSingle({
                                state: 'data_platforms',
                                id: 'token_platform',
                                active: 'data_platforms_active',
                                label: 'Token platform',
                            })}
                        </li>
                        <li>
                            {this.renderSelectSingle({
                                state: 'data_statuses',
                                id: 'token_status',
                                active: 'data_statuses_active',
                                label: 'Project status',
                            })}
                        </li>
                        <li>
                            {favicon_random_name && <FaviconUploader faviconName={favicon_random_name} />}
                        </li>
                        <li>
                            <LinksPreviewer onSaveLinks={this.onSaveLinks} />
                        </li>
                        <li>
                            <FormButton
                                disabled={form_disabled}
                                type="submit"
                                label="Save"
                                fullWidth
                                />
                        </li>
                    </ul>
                </form>
            </BlockForm>
		);
	}
}


PageUserStocksAdd.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};


export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			// onSelectCoins: (payload) => {
			// 	dispatch({type: 'SELECT_COINS', payload})
			// },
		})
	))
	(withStyles(styles, { withTheme: true })(PageUserStocksAdd))
);
