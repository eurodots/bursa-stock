import React, {Component} from 'react';
import Icon from 'material-ui/Icon';
// import Typography from 'material-ui/Typography';

import BlockPreview from '../BlockPreview'
import SvgIcon from '../SvgIcon';

import theme from './theme.scss'


export default class PreviewBlocks extends Component {

    state = {}


    renderBlockPreview(init) {

        return (
            <ul className={init.align == 'right' ? theme.previewWrapperRight : theme.previewWrapperLeft}>
                <li data-li="preview">
                    <BlockPreview
                        dataImage={init.image} />
                </li>
                <li data-li="content">
                    <ul data-el="title">
                        {init.icon &&
                            <li data-li="icon">
                                <Icon>{init.icon}</Icon>
                            </li>
                        }
                        {init.icon_svg &&
                            <li data-li="icon-svg">
                                <SvgIcon name={init.icon_svg} gradient={true} />
                            </li>
                        }
                        <li>
                            <h3 data-tag="h3" data-color="primary">
                                {init.title}
                            </h3>
                        </li>
                    </ul>
                    <p>
                        {init.description}
                    </p>
                    {init.links &&
                        <ul data-el="links">
                            {init.links.map((link, ind) => {
                                return (
                                    <li key={ind}>
                                        <a href={link.url} target="_blank" data-link>{link.label}</a>
                                    </li>
                                )
                            })}
                        </ul>
                    }
                </li>
            </ul>
        )
    }

	render() {

		return (
			<div className={theme.wrapper}>
                {this.renderBlockPreview({
                    title: "Single Mode",
                    description: "If you already have your own token, then a single mode will allow your customers to buy and sell your coins directly on your site.",
                    links: [
                        {
                            label: "Hot to create token?",
                            url: "http://...",
                        },
                        {
                            label: "Demo",
                            url: "http://...",
                        },
                    ],
                    icon: "accessibility",
                    icon_svg: false,
                    align: "left",
                    image: "dashboard1.png",
                })}
                {this.renderBlockPreview({
                    title: "Multi-mode",
                    description: "In multi-mode, a list of all tokens in the Ether system appears on your exchange. You receive a percentage from each transaction of your user.",
                    links: [
                        {
                            label: "How does it work?",
                            url: "http://...",
                        },
                        {
                            label: "Demo",
                            url: "http://...",
                        },
                    ],
                    icon: "timeline",
                    icon_svg: false,
                    align: "right",
                    image: "dashboard2.png",
                })}
                {this.renderBlockPreview({
                    title: "Smart Contract",
                    description: "Our smart contract is so clever that you do not need to think about security, mutual settlements with traders, taxes and other trifles. Our smart contract available in open source.",
                    links: [
                        {
                            label: "About Smart Contracts",
                            url: "http://...",
                        },
                        {
                            label: "Github",
                            url: "http://...",
                        },
                    ],
                    icon: false,
                    icon_svg: "brain",
                    align: "left",
                    image: "dashboard3.png",
                })}
            </div>
		)
	}
}
