import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom'
import {Link} from 'react-router-dom';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
// import Icon from 'material-ui/Icon';


import SvgIcon from '../SvgIcon';
import BlockBenefits from '../BlockBenefits';
import PreviewBlocks from './PreviewBlocks';
import BlockPreview from '../BlockPreview';
import BlockPricing from '../PagePricing/BlockPricing';
import BlockSubscription from '../BlockSubscription';

import theme from './theme.scss'

import {apiDomain, confSite} from '../../config/init'
import {apiFaviconsList} from '../../utils/functions'

const styles = {
};

class PageHome extends Component {

    state = {
        favicons_meta: false,
        favicons_list: [],
    }

    componentDidMount() {
        apiFaviconsList().then((response) => {
            let d = response.data
            this.setState({
                favicons_meta: d.meta,
                favicons_list: d.results
            }, () => {})
            // console.log(this.state.favicons_list)
        })
    }

    renderFaviconsList() {
        const {favicons_list, favicons_meta} = this.state;

        if(favicons_list.length == 0) {
            return <div />
        }
        return (
            <div className={theme.faviconsListWrapper} data-box="new">
                <Typography variant="headline" gutterBottom align="center">
                    <strong>Working with us:</strong> {favicons_meta.total} ICO-projects
                </Typography>

                <ul>
                    {favicons_list.map((item, index) => {
                        return (
                            <li key={index}>
                                <img src={item.favicon} />
                                <label>{item.name}</label>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }

    openLink(label) {

        window.open(confSite.links[label])

    }

    renderContentLeft() {
        return (
            <div className={theme.contentLeft}>
                <div>
                    <div data-el="mobileIcon">
                        <div>
                            <SvgIcon name="ethereum" gradient={false} />
                        </div>
                    </div>

                    <h1>Script for personal crypto stock!</h1>
                    <h2>{confSite.slogan}</h2>

                    <ul className={theme.thesesList}>
                        <li>The smallest commission in the world!</li>
                        <li>Safely! Created by the Ethereum-network.</li>
                    </ul>
                    <ul className={theme.actionsList}>
                        <li data-li="download">
                            <Link to="/p/signup?redirect=%2Fp%2Fuser%2Fstocks">
                                <Button variant="raised" size="large" color="secondary">
                                    Download free
                                </Button>
                            </Link>
                        </li>
                        <li data-li="other">
                            <Link to="/p/demo">
                                <Button size="large" color="primary" data-button="outline">
                                    Demo
                                </Button>
                            </Link>
                            <Link to="/p/pricing">
                                <Button size="large" color="primary" data-button="outline">
                                    Pricing
                                </Button>
                            </Link>
                        </li>
                    </ul>

                    <ul className={theme.iconsList}>
                        <li onClick={() => this.openLink('facebook')}>
                            <SvgIcon name="facebook" gradient={false} />
                        </li>
                        <li onClick={() => this.openLink('bitcointalk')}>
                            <SvgIcon name="bitcoin" gradient={false} />
                        </li>
                    </ul>
                </div>
            </div>
        )
    }

    renderContentRight() {
        return (
            <div className={theme.contentRight}>
                <div data-el="preview">
                    <BlockPreview dataImage="dashboard3.png" />
                </div>
            </div>
        )
    }



	render() {
		return (
            <div>
    			<div className={theme.background} style={{backgroundImage: `url(${apiDomain}/media/material-bgs/jpg/13.jpg)`}}>
                    <ul className={theme.contentWrapper} data-content-inner>
                        <li>
                            {this.renderContentLeft()}
                        </li>
                        <li>
                            {this.renderContentRight()}
                        </li>
                    </ul>
    			</div>

                <div data-content-inner>
                    <BlockBenefits />
                    <PreviewBlocks />
                </div>
                <div data-content-inner>
                    <ul className={theme.schemeBlock} data-box="new">
                        <li data-li="scheme" style={{backgroundImage: `url(${apiDomain}/media/landing/scheme_howitworks.png)`}}></li>
                        <li data-li="content">
                            <h3 data-tag="h3">
                                How does it work?
                            </h3>
                            <p>
                                Regardless of anything, you always receive a commission from all trades of your traders.
                                The order is placed through our smart contract, which guarantees payments to all participants of the transaction.
                            </p>
                        </li>
                    </ul>
                    {this.renderFaviconsList()}
                </div>

                <div className={theme.blockPricing} style={{backgroundImage: `url(${apiDomain}/media/material-bgs/jpg/13.jpg)`}}>
                    <div data-content-inner>
                        <BlockPricing />
                    </div>
                </div>

                <BlockSubscription />
            </div>
		)
	}
}

PageHome.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(PageHome));
