import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';
import Button from 'material-ui/Button';
import Input, { InputLabel } from 'material-ui/Input';
import TextField from 'material-ui/TextField';
import { FormControl, FormHelperText } from 'material-ui/Form';

import {apiLinkPreview} from '../../utils/functions'
import theme from './theme.scss'



const styles = mui => ({
    formControl: {
        width: '100%',
        marginBottom: 10,
    },
    textFieldRoot: {
      padding: 0,
      marginTop: 5,
      // 'label + &': {
      //   marginTop: mui.spacing.unit * 3,
      // },
    },
    textFieldInput: {
      borderRadius: 2,
      backgroundColor: mui.palette.common.white,
      border: '1px solid rgba(63, 81, 181, .2)',
      fontSize: 18,
      padding: '15px 20px',
      width: 'calc(100% - 24px)',
      transition: mui.transitions.create(['border-color', 'box-shadow']),
      '&:focus': {
        borderColor: 'rgba(63, 81, 181, .5)',
        boxShadow: '0 0 0 0.2rem rgba(63, 81, 181,.25)',
      },
    },
    textFieldFormLabel: {
      fontSize: 18,
    },
    formHelper: {
        marginBottom: 10,
    },
});

class LinksPreviewer extends Component {

    state = {
        link_preview_arr: [],
        field_link: '',
    }

    linkPreview = (request_link) => {

        let data = {link: request_link}


        document.activeElement.blur();
        apiLinkPreview(data).then((response) => {

            let d = response.data

            if(d) {

                let newState = this.state.link_preview_arr;
                newState.push(d)
                this.setState({link_preview_arr: newState}, () => {});

                this.updateSavedLinks()
            }

        })
    }

    updateSavedLinks() {
        let newState = this.state.link_preview_arr;
        let links_arr = []
        newState.map((item, index) => {
            links_arr.push(item.request)
        })
        this.props.onSaveLinks(links_arr)
    }

    removePreview(index) {
        let newState = this.state.link_preview_arr;
        newState.splice(index, 1);
        this.setState({link_preview_arr: newState}, () => {});
        this.updateSavedLinks()
    }

    renderPreviews() {
        const {link_preview_arr} = this.state

        return (
            <div className={theme.wrapper}>
                {link_preview_arr.map((item, index) => {
                    return (
                        <ul key={index}>
                            <li data-el="preview" onClick={() => window.open(item.request)}>
                                {item.image && <img src={item.image} />}
                            </li>
                            <li data-el="content">
                                <ul>
                                    <li>
                                        <a href={item.request} target="_blank" data-link>
                                            <Typography variant="button" gutterBottom data-short>
                                                {item.title}
                                            </Typography>
                                        </a>
                                        <p data-el="description" data-short>
                                            {item.description}
                                        </p>
                                    </li>
                                    <li data-short>
                                        <a href={item.request} target="_blank" data-link data-el="link">
                                            {item.request}
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li data-el="close">
                                <Icon data-link onClick={() => this.removePreview(index)}>close</Icon>
                            </li>
                        </ul>
                    )
                })}
            </div>
        )
    }


    inputSubmit = (event) => {
        event.preventDefault()
        event.stopPropagation()

        const { field_link } = this.state
        this.linkPreview(field_link)
        this.setState({field_link: ''}, () => {})
    }

    handleChangeInput = (event, name) => {
        this.setState({
            [name]: event.target.value,
        });
    };


    renderInput() {
        const { classes } = this.props;
        const { field_link, field_disabled } = this.state

        return (
            <form onSubmit={(event) => this.inputSubmit(event)}>

                <FormControl className={classes.formControl}>
                    <label data-label>Any project links</label>
                    <div className={theme.isMobileInput}>
                        <TextField
                            placeholder="Past and press Enter"
                            onChange={(event) => this.handleChangeInput(event, 'field_link')}
                            value={field_link}
                            disabled={field_disabled}
                            InputProps={{
                              disableUnderline: true,
                              classes: {
                                root: classes.textFieldRoot,
                                input: classes.textFieldInput,
                              },
                            }}
                            InputLabelProps={{
                              shrink: true,
                              className: classes.textFieldFormLabel,
                            }}
                          />
                          <Button type="submit" color="primary">
                              <Icon>check</Icon>
                          </Button>
                    </div>
                </FormControl>

            </form>
        )
    }

    render() {
        return (
            <div>
                {this.renderInput()}
                {this.renderPreviews()}
            </div>
        )
    }
}


LinksPreviewer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LinksPreviewer);
