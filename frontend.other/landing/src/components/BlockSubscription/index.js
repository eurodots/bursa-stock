import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

import FormField from '../BlockForm/FormField'
import FormButton from '../BlockForm/FormButton'

import {cookie} from '../../utils/functions'

import theme from './theme.scss'


const styles = mui => ({

});

class BlockSubscription extends Component {

    state = {
        field_name: '',
        field_email: '',
        form_disabled: false,

        errors: {
            field_name: false,
            field_email: false,
        }
    }

    // componentWillMount() {
    //     let email_check = cookie('get','email')
    //     if(email_check) {
    //         // ...hide form
    //     }
    // }

    handleChangeInput = (event, name) => {
        this.setState({
            [name]: event.target.value,
        });
    };

    submitForm = (event) => {
        event.preventDefault()

        const {field_name, field_email} = this.state

        cookie('set',['email',field_email])
        cookie('set',['username',field_name])

        this.props.history.push(`/p/signup`)
    }

	render() {

        const { classes } = this.props;

		return (
			<div className={theme.wrapper}>
                <div data-content-inner>

                    <Typography variant="display1" color="primary">
                        Start now for free!
                    </Typography>
                    <p>Signup now. Its free and takes less than 3 minutes</p>

                    <form onSubmit={this.submitForm}>
                        <FormField
                            state_value={this.state.field_name}
                            state_name="field_name"
                            label="Your name"
                            type="text"
                            required={false}
                            disabled={false}
                            errors={{}}
                            onChange={this.handleChangeInput}
                             />

                        <FormField
                            state_value={this.state.field_email}
                            state_name="field_email"
                            label="Email Address"
                            type="text"
                            required={false}
                            disabled={false}
                            errors={{}}
                            onChange={this.handleChangeInput}
                             />

                         <FormButton
                             disabled={false}
                             type="submit"
                             label="Get started for free"
                             variant="colored"
                             fullWidth
                             />
                    </form>

                </div>
            </div>
		)
	}
}


BlockSubscription.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
};

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({})
	))
	(withStyles(styles)(BlockSubscription))
);
