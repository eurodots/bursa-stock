import React, {Component} from 'react';
import PropTypes from 'prop-types';

import BlockHeaderBg from '../BlockHeaderBg';
import FormField from '../BlockForm/FormField'
import FormButton from '../BlockForm/FormButton'
import BlockDonation from '../BlockDonation'

import {apiContactForm} from '../../utils/functions'

import theme from './theme.scss'



class PageSupport extends Component {

	state = {
		email: 'markolofsen@gmail.com',
		message: 'Test message for you!',
		errors: {
			email: '',
			message: '',
		},
		form_disabled: false,
		form_type: 'form',
	}


	formSubmit = (event) => {
		event.preventDefault()

		const {email, message, errors} = this.state

		this.setState({form_disabled: true})
		let request = {email, message}
		apiContactForm(request).then((response) => {
			console.log(response)

			let d = response.data
			if(d.error) {
				let newState = Object.assign({}, errors);
                _.keys(newState).map((item, index) => {
                    newState[item] = false
                })
                _.keys(d.errors).map((item, index) => {
                    newState[item] = d.errors[item][0];
                })
                this.setState({errors: newState}, () => {});
			} else {
				this.setState({form_type: 'complete'}, () => {})
			}

			this.setState({form_disabled: false})
		})
	}

	handleChangeInput = (event, name) => {
        this.setState({
            [name]: event.target.value,
        });
    };

	renderSupportForm() {
		// const {classes} = this.props
        const {form_disabled, errors} = this.state

		return (
			<div>
				<form onSubmit={(event) => this.formSubmit(event)}>

					<ul className={theme.supportWrapperForm}>
						<li>
							<FormField
								state_value={this.state.email}
								state_name="email"
								label="Email Address"
								type="email"
								required={true}
								disabled={form_disabled}
								errors={errors}
								onChange={this.handleChangeInput}
								 />
						</li>
						<li>
							<FormField
								state_value={this.state.message}
								state_name="message"
								label="Your message"
								type="text"
								multiline={true}
								required={true}
								disabled={form_disabled}
								errors={errors}
								onChange={this.handleChangeInput}
								 />
						</li>
						<li>
                            <FormButton
                                disabled={form_disabled}
                                type="submit"
                                label="Send"
                                fullWidth
                                />
                        </li>
					</ul>

				</form>
			</div>
		)
	}

	renderSupportFormComplete() {
		return (
			<div className={theme.supportWrapperFormCompleted} data-box="new">
				<div>
					<h3 data-tag="h3">Message was sent</h3>
					<BlockDonation />
				</div>
			</div>
		)
	}

	render() {

		const {form_type} = this.state

		return (
			<div className={theme.supportWrapper}>
				<BlockHeaderBg
					bgid={34}
					title='Support'
					description={`
						If you have a story to share or a question that has not been answered on our website,
						please get in touch with us via contact details listed below or fill in the form on the right.
						`} />

				<div data-content-inner>
					{form_type == 'form' &&
						<ul data-box className={theme.container}>
							<li data-li="content">
								123
							</li>
							<li data-li="form">
								 {this.renderSupportForm()}
							</li>
						</ul>
					}

					{form_type == 'complete' && this.renderSupportFormComplete()}
				</div>
            </div>
		)
	}
}

export default PageSupport
