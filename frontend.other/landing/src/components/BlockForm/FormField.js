import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import Input, { InputLabel } from 'material-ui/Input';
import TextField from 'material-ui/TextField';
import { FormControl, FormHelperText } from 'material-ui/Form';

const styles = mui => ({
  formControl: {
      width: '100%',
      marginBottom: 10,
  },
  textFieldRoot: {
    padding: 0,
    'label + &': {
      marginTop: mui.spacing.unit * 3,
    },
  },
  textFieldInput: {
    borderRadius: 2,
    backgroundColor: mui.palette.common.white,
    border: '1px solid rgba(63, 81, 181, .2)',
    fontSize: 18,
    padding: '15px 20px',
    // width: 'calc(100% - 24px)',
    width: '100%',
    transition: mui.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderColor: 'rgba(63, 81, 181, .5)',
      boxShadow: '0 0 0 0.2rem rgba(63, 81, 181,.25)',
    },
  },
  textAFieldTextareaRoot: {
  },
  textAFieldTextarea: {
      boxSizing: 'border-box',
  },
  textFieldFormLabel: {
    fontSize: 18,
  },
  formHelper: {
      marginBottom: 10,
  },
});

class FormField extends Component {

    state = {}


	render() {

        const {classes, state_name, state_value, label, type, multiline=false, required, disabled, errors} = this.props
        const error_id = `field-${state_name}-error`
        const field_id = `field-${state_name}`



		return (
			<div>
                <FormControl className={classes.formControl} error={errors[state_name] ? true : false} aria-describedby={error_id}>
                    <TextField
                        placeholder={label}
                        defaultValue={state_value}
                        value={state_value}
                        id={field_id}
                        type={type}
                        required={required}
                        disabled={disabled}
                        multiline={multiline}
                        rowsMax="40"
                        rows="8"
                        InputProps={{
                          disableUnderline: true,
                          classes: {
                            root: classes.textFieldRoot,
                            input: classes.textFieldInput,
                            multiline: classes.textAFieldTextareaRoot,
                            inputMultiline: classes.textAFieldTextarea,
                          },
                        }}
                        InputLabelProps={{
                          shrink: true,
                          className: classes.textFieldFormLabel,
                        }}
                        {...this.other}
                        onChange={(event) => this.props.onChange(event, state_name)}
                      />
                    {errors[state_name] && <FormHelperText className={classes.formHelper} id={error_id}>{errors[state_name]}</FormHelperText>}
                </FormControl>
            </div>
		)
	}
}

FormField.propTypes = {
  state_name: PropTypes.string.isRequired,
  state_value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  required: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  // onChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(FormField)
