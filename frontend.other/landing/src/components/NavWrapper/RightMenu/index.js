import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import ListSubheader from 'material-ui/List/ListSubheader';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Collapse from 'material-ui/transitions/Collapse';

import Divider from 'material-ui/Divider';
import Icon from 'material-ui/Icon';

const _ = require('lodash');

// import style from './theme';
const styles = theme => ({
	root: {
		width: '100%',
		maxWidth: 360,
		background: theme.palette.background.paper
	},
	nested: {
		paddingLeft: theme.spacing.unit * 4
	},
});

class RightMenu extends React.Component {

	state = {
		menu_user: true,
	}


	handleClick = (value) => {
		this.setState({ [value]: !this.state.menu_user });
	};

	render() {
		const { classes } = this.props;

		return (
			<div>

				<ListItem button onClick={() => this.handleClick('menu_user')}>
					<ListItemIcon>
						<Icon>account_box</Icon>
					</ListItemIcon>
					<ListItemText inset primary="Menu"/>
					{this.state.menu_user ? <Icon>expand_less</Icon> : <Icon>expand_more</Icon>}
				</ListItem>
				<Collapse in={this.state.menu_user} timeout="auto">
					...
				</Collapse>
				<Divider/>


			</div>
		);
	}
}

RightMenu.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RightMenu);
