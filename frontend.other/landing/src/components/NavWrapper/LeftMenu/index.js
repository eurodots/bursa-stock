import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import ListSubheader from 'material-ui/List/ListSubheader';
import List, {ListItem, ListItemIcon, ListItemText} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Icon from 'material-ui/Icon';


const _ = require('lodash');

import theme from './theme.scss'

// import style from './theme';
const styles = theme => ({
	root: {
		width: '100%',
		maxWidth: 360,
		background: theme.palette.background.paper
	},
	nested: {
		paddingLeft: theme.spacing.unit * 4
	}
});

class LeftMenu extends React.Component {

	state = {};

	renderCoinsMenu() {

		return (
			<List
				component="nav"
				subheader={<ListSubheader component="div">Public menu</ListSubheader>}
				>

				<Link to="/">
					<ListItem button>
						<ListItemIcon>
							<Icon>home</Icon>
						</ListItemIcon>
						<ListItemText primary="Home"/>
					</ListItem>
				</Link>
				<Link to="/p/pricing">
					<ListItem button>
						<ListItemIcon>
							<Icon>grade</Icon>
						</ListItemIcon>
						<ListItemText primary="Pricing"/>
					</ListItem>
				</Link>
				<Link to="/p/tokens">
					<ListItem button>
						<ListItemIcon>
							<Icon>gavel</Icon>
						</ListItemIcon>
						<ListItemText primary="Tokens"/>
					</ListItem>
				</Link>
				<Link to="/p/demo">
					<ListItem button>
						<ListItemIcon>
							<Icon>https</Icon>
						</ListItemIcon>
						<ListItemText primary="Demo"/>
					</ListItem>
				</Link>
				<Divider/>
				<Link to="/p/docs">
					<ListItem button>
						<ListItemIcon>
							<Icon>library_books</Icon>
						</ListItemIcon>
						<ListItemText primary="Docs"/>
					</ListItem>
				</Link>
				<Link to="/p/support">
					<ListItem button>
						<ListItemIcon>
							<Icon>help</Icon>
						</ListItemIcon>
						<ListItemText primary="Support"/>
					</ListItem>
				</Link>
			</List>
		)
	}

	renderUserMenu() {

		const {user} = this.props

		if(user.data) {
			return (
				<List
					component="nav"
					subheader={<ListSubheader component="div">Account menu</ListSubheader>}
					>
					<Link to="/p/user/stocks">
						<ListItem button>
							<ListItemIcon>
								<Icon>insert_chart</Icon>
							</ListItemIcon>
							<ListItemText primary="My stocks"/>
						</ListItem>
					</Link>
					<Link to="/">
						<ListItem button>
							<ListItemIcon>
								<Icon>people</Icon>
							</ListItemIcon>
							<ListItemText primary="Affiliates"/>
						</ListItem>
					</Link>
					<Link to="/">
						<ListItem button>
							<ListItemIcon>
								<Icon>account_box</Icon>
							</ListItemIcon>
							<ListItemText primary="Profile"/>
						</ListItem>
					</Link>

					<Divider />
				</List>
			)
		}
	}

	render() {

		return (
			<div className={theme.leftMenu}>
				{this.renderUserMenu()}
				{this.renderCoinsMenu()}
			</div>
		);
	}
}

LeftMenu.propTypes = {
	classes: PropTypes.object.isRequired
};

// export default withStyles(styles)(LeftMenu);


export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			// onSelectCoins: (payload) => {
			// 	dispatch({type: 'SELECT_COINS', payload})
			// },
		})
	))
	(withStyles(styles)(LeftMenu))
);
