import React from 'react';
import {Link} from 'react-router-dom';

import Button from 'material-ui/Button';
import Dialog, {DialogActions, DialogContent, DialogContentText, DialogTitle} from 'material-ui/Dialog';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';

import {CopyToClipboard} from 'react-copy-to-clipboard';

import theme from './theme.scss'

class AlertDialog extends React.Component {
	state = {
		open: false,
        widget_content: '123',
        widget_copied: false,

	};

	handleClickOpen = () => {
		this.setState({open: true});
	};

	handleClose = () => {
		this.setState({open: false});
	};

    renderCopyButton() {
        const {widget_content, widget_copied} = this.state

        return (
            <CopyToClipboard text={widget_content}
              onCopy={() => this.setState({widget_copied: true})}>

              {widget_copied
              ?
                  <Button variant="raised" size="small" aria-label="Copied">
                    <Icon>check</Icon>
                    <span data-el="btn-label">Copied</span>
                  </Button>
              :
                  <Button variant="raised" color="primary" size="small" aria-label="Copy">
                    <Icon>content_copy</Icon>
                    <span data-el="btn-label">Copy</span>
                  </Button>
              }
            </CopyToClipboard>
        )
    }

	render() {
        const {domain} = this.props
        const {widget_content} = this.state

		return (
			<div>
                <Button
                    onClick={this.handleClickOpen}
                    color="primary"
                    size="small"
                    variant="raised"
                    >
                    Download script
                </Button>

				<Dialog open={this.state.open} onClose={this.handleClose}>
					<DialogTitle>{"Script downloading..."}</DialogTitle>
					<DialogContent>
						<DialogContentText>
                            <ul className={theme.dialogContent}>
                                <li data-li="code">
                                    <Typography variant="title" gutterBottom>
                                        CDN
                                    </Typography>
                                    Place this code at your website {domain}
                                    <code>{widget_content}</code>
                                    {this.renderCopyButton()}
                                </li>
                                <li>
                                    <Typography variant="title" gutterBottom>
                                        Download
                                    </Typography>
                                    Also you can to download this script.
                                    <p>
                                        <Link to="/p/docs/#_install" data-link>Instructions.</Link>
                                    </p>
                                </li>
                            </ul>
						</DialogContentText>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.handleClose} color="primary">
							Close
						</Button>
					</DialogActions>
				</Dialog>
			</div>
		);
	}
}

export default AlertDialog;
