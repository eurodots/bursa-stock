import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';

import BlockHeaderBg from '../BlockHeaderBg';
import FormButton from '../BlockForm/FormButton'
import FormConfirmer from '../BlockForm/FormConfirmer'
import BlockDonation from '../BlockDonation'

import Preloader from '../Preloader'
import DrawerChart from './DrawerChart'
import DialogDownload from './DialogDownload'

import NumberFormat from 'react-number-format';
import TimeAgo from 'react-timeago';
import {apiUserStocksList, apiUserStocksDelete} from '../../utils/functions'

import theme from './theme.scss'


class NumberFormatting extends Component {
	render() {
		const {value, decimals, prefix, postfix, wrapper} = this.props
		let prefix_ = prefix ? `${prefix} ` : ''
		let postfix_ = postfix ? ` ${postfix}` : ''
		let value_ = value ? value : 0

		if (value_ < 0) {
			value_ = Math.abs(value_)
			prefix_ = `${prefix_} —`
		}
		let wrapper_ = wrapper ? wrapper : ['','']

		return (
			<span>
				{wrapper_[0]}
				<NumberFormat value={value_} displayType={'text'} decimalScale={decimals} thousandSeparator={true} prefix={prefix_} />
				{postfix_}
				{wrapper_[1]}
			</span>
		)
	}
}
NumberFormatting.propTypes = {
	decimals: PropTypes.number.isRequired,
};


class PageUserStocks extends Component {

	state = {
		data: false,
	}

	componentDidMount() {
		this.loadStocks()
	}

	loadStocks = () => {
		this.setState({data: false}, () => {})
		apiUserStocksList().then((response) => {
			this.setState({data: response.data.results}, () => {})
		})
	}

	renderStocksNotFound() {
		return (
			<div className={theme.stocksEmptyWrapper}>
				<Typography variant="display1" gutterBottom align="center">
					You haven't stocks yet
				</Typography>
				<Link to="/p/user/stocks/add">
					<FormButton
						disabled={false}
						type="button"
						label="Add stock"
						variant="colored"
						/>
				</Link>
			</div>
		)
	}

	deleteCurrentStock(id) {

		apiUserStocksDelete(id).then((response) => {
			if(response.data.error) {
				alert(response.data.message)
			} else {
				this.loadStocks()
			}
		})

	}

	renderStocksList() {
		const {data} = this.state

		return (
			<div className={theme.stocksListWrapper}>
				<div className={theme.stocksListHeader}>

					<Button onClick={this.loadStocks}>
						Refresh
					</Button>

					<Link to="/p/user/stocks/add" data-link-button>
						<Button variant="raised" color="primary">
							Add stock
						</Button>
					</Link>
				</div>

				<ul className={theme.stocksListContainer}>
					{data.map((item, index) => {
						let domain_http = `http://${item.token_domain}`
						let token_wallet_url = `https://ethplorer.io/address/${item.token_wallet}`
						let token_address_url = `https://ethplorer.io/address/${item.token_address}`
						return (
							<li key={index}>
								<ul data-ul="content-header">
									<li data-li="favicon">
										{item.favicon && <img src={item.favicon} />}
									</li>
									<li>
										<Typography variant="title" gutterBottom>
											{item.token_name} {`(${item.token_ticker})`}
										</Typography>
										<h4>
											<strong>Token</strong> {item.ethplorer.meta ?
												<a href={token_address_url} target="_blank" data-link>{item.token_address}</a>
												: item.token_address
											}
										</h4>
									</li>
									<li data-li="remove">
										<FormConfirmer
											onClick={() => this.deleteCurrentStock(item.id)}
											title='Deleting the stock'
											description={
												'Are you sure you want to delete this stock?<br />'
												+'Recovery will be impossible'}
											buttonConfirm="Delete"
											buttonCancel="Cancel"
											>
											<Icon data-link>close</Icon>
										</FormConfirmer>
									</li>
								</ul>
								<ul data-ul="content-inner">
									<li>
										<ul data-list="meta">
											<li>
												<label>Website</label>
												<a href={domain_http} target="_blank" data-link="underlined">
													{item.token_domain}
												</a>
											</li>
											<li>
												<label>Platform</label>
												{item.token_platform}
											</li>
											<li>
												<label>Status</label>
												{item.token_status}
											</li>
											<li>
												<label>Added</label>
												<TimeAgo date={item.created_at} />
											</li>
											<li>
												<label>Tariff</label>
												Free {`(version ${item.script_version})`}
											</li>
											<li>
												<label>Earned</label>
												<NumberFormatting
													value={0}
													decimals={2}
													prefix="$"
													/>
											</li>
											<li>
												<label>Wallet</label>
												<a href={token_wallet_url} target="_blank" data-link="underlined">Show credentials</a>
											</li>
										</ul>

									</li>
									<li>
										{item.ethplorer.meta &&
											<ul data-list="meta">
												<li>
													<label>Transactions</label>
													<NumberFormatting
														value={item.ethplorer.meta.transactions}
														decimals={0}
														/>
												</li>
												<li>
													<label>Total Supply</label>
													<div>
														<NumberFormatting
															value={item.ethplorer.meta.totalSupply}
															decimals={2}
															prefix={item.ethplorer.meta.symbol}
															/>
														&nbsp;
														<NumberFormatting
															value={item.ethplorer.meta.totalSupplyUsd}
															decimals={2}
															prefix="$"
															wrapper={['(',')']}
															/>
													</div>
												</li>
												<li>
													<label>Holders</label>
													<NumberFormatting
														value={item.ethplorer.meta.holders}
														decimals={0}
														prefix=""
														/>
												</li>
												<li>
													<label>Price</label>
													<div>
														<NumberFormatting
															value={item.ethplorer.meta.price}
															decimals={2}
															prefix="$"
															/>
														&nbsp;
														<NumberFormatting
															value={item.ethplorer.meta.price_diff}
															decimals={2}
															prefix=""
															postfix="%"
															wrapper={['(',')']}
															/>
													</div>
												</li>
											</ul>
										}
									</li>
								</ul>
								<ul data-ul="content-inner">
									<li>
										<DialogDownload
											domain={item.token_domain}
											/>
									</li>
									<li>
										{item.ethplorer.meta && <DrawerChart tokenAddress={item.token_address} />}
									</li>
								</ul>
							</li>
						)
					})}
				</ul>

			</div>
		)
	}


	render() {

		const {data} = this.state

		return (
			<div className={theme.userStocksWrapper}>
				<BlockHeaderBg
					bgid={10}
					title='My stocks'
					description={`Manage your stocks`} />

				<div data-content-inner>
					<div data-box>
						{!data ? <Preloader /> :
							data.length == 0 ? this.renderStocksNotFound() : this.renderStocksList()
						}
					</div>

					<BlockDonation style={{marginTop: 30}} />
				</div>

			</div>
		)


	}
}

export default PageUserStocks
