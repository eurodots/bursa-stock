import React, {Component} from 'react';
import PropTypes from 'prop-types';
// import Icon from 'material-ui/Icon';
// import Typography from 'material-ui/Typography';
import BlockPricing from './BlockPricing';
import BlockHeaderBg from '../BlockHeaderBg';

import theme from './theme.scss'


class PagePricing extends Component {


	render() {

		return (
			<div>
				<BlockHeaderBg
					bgid={18}
					title='Pricing'
					description={`Prices are given per month.`} />

				<div className={theme.pricingWrapper}>
					<BlockPricing data-content-inner />
				</div>
            </div>
		)
	}
}

export default PagePricing
