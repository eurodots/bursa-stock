import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import {Link} from 'react-router-dom';

import Typography from 'material-ui/Typography';
// import Button from 'material-ui/Button';
import FormButton from '../../BlockForm/FormButton'

import theme from './theme.scss'


class BlockPricing extends Component {

    state = {
        data: [
            {
                priority: false,
                url: '/p/user/stocks/add/free',
                plan: "Start",
                price: "FREE",
                list: [
                    {
                        label: "Always free",
                    },
                    {
                        label: "-",
                    },
                    {
                        label: "-",
                    },
                    {
                        label: "-",
                    },
                    {
                        label: "-",
                    },
                ]
            },
            {
                priority: true,
                url: '/p/user/stocks/add/pro',
                plan: "Pro",
                price: "$129",
                list: [
                    {
                        label: "MobileApp",
                    },
                    {
                        label: "Customization",
                    },
                    {
                        label: "Adding to list",
                    },
                    {
                        label: "-",
                    },
                    {
                        label: "-",
                    },
                ]
            },
            {
                priority: false,
                url: '/p/user/stocks/add/enterprise',
                plan: "Enterprise",
                price: "Individual",
                list: [
                    {
                        label: "MobileApp",
                    },
                    {
                        label: "Customization",
                    },
                    {
                        label: "Own Smart Contract",
                    },
                    {
                        label: "Marketing Solutions",
                    },
                    {
                        label: "White-label",
                    },
                ]
            },
        ]
    }


    renderPrices() {
        // const {classes} = this.props;
        const {data} = this.state;

        return (
            <div className={theme.pricingList} {...this.props}>
                {data.map((item, index) => {
                    return (
                        <ul key={index}>
                            <li data-li="plan">
                                {item.plan}
                            </li>
                            <li data-li="price">
                                {item.price}
                            </li>
                            <li data-li="list">
                                <ul>
                                    {item.list.map((list, i) => {
                                        return (
                                            <li key={i}>
                                                {list.label}
                                            </li>
                                        )
                                    })}
                                </ul>
                            </li>
                            <li data-li="button">
                                <Link to={item.url}>
                                    <FormButton
                                        disabled={false}
                                        type="button"
                                        label="Choose this plan"
                                        variant={item.priority ? 'colored' : 'colored-outline'}
                                        />
                                </Link>
                            </li>
                        </ul>
                    )
                })}
            </div>
        )
    }

	render() {


		return (
			<div className={theme.wrapper}>
                {this.renderPrices()}
            </div>
		)
	}
}


BlockPricing.propTypes = {
  children: PropTypes.node,
  // classes: PropTypes.object.isRequired,
};

export default BlockPricing;
