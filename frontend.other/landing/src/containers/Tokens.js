import React, {Component} from 'react';
import PageTokens from '../components/PageTokens'

export default class Tokens extends Component {
	render() {
		return (
			<div data-content>
				<PageTokens />
			</div>
		)
	}
}
