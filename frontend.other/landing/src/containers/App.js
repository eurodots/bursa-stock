import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cssModules from 'react-css-modules';
import styles from '../style/index.scss';

import NavWrapper from '../components/NavWrapper'
import Preloader from '../components/Preloader'

import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {isBrowser, isMobile} from 'react-device-detect';
const _ = require('lodash');

import {apiTokenAuth, pageTitle, cookie} from '../utils/functions'

@cssModules(styles)
class App extends Component {


  constructor(props) {
    super(props)

    this.state = {
    }
  }

  componentDidMount() {

      this.checkAuth()
      this.titleChecker()

      if(isBrowser) {
          this.props.onToggleLeftMenu(true)
      }

  }


  checkAuth = () => {

      let token = cookie('get', 'token')
      if(token) {
          apiTokenAuth().then((response) => {

              console.log(response)

              if(response.status == 200) {
                  let u = response.data.user
                  this.props.onLoginByToken({
                      token: token,
                      name: u.email,
                      email: u.email,
                      isAdmin: false
                  })
              } else {
                  cookie('delete', 'token')
              }

          }, () => {})
      }
  }


  componentWillReceiveProps(nextProps) {

    // SCROLL TO TOP IF PAGE CHANGED
    let nextpage = nextProps.routing.location.pathname
    if (this.props.routing.location.pathname != nextpage) {
    	window.scrollTo(0, 0);
    	this.titleChecker(nextpage)
        if(isMobile) {
            this.props.onToggleLeftMenu(false)
        }
    }
  }

  titleChecker(nextpage=false) {
      if(!nextpage) {
          nextpage = this.props.routing.location.pathname
          // pageTitle()
      }

      let titles_arr = [
          {
              title: 'Home',
              path: '/',
              exact: true,
          },
          {
              title: 'Pricing',
              path: '/p/pricing',
              exact: false,
          },
          {
              title: 'Tokens',
              path: '/p/tokens',
              exact: false,
          },
          {
              title: 'Docs',
              path: '/p/docs',
              exact: false,
          },
          {
              title: 'Demo',
              path: '/p/demo',
              exact: false,
          },
          {
              title: 'Support',
              path: '/p/support',
              exact: false,
          },
          {
              title: 'Auth',
              path: '/p/login',
              exact: false,
          },
          {
              title: 'SingUp',
              path: '/p/signup',
              exact: false,
          },
          {
              title: 'My Stocks',
              path: '/p/user/stocks',
              exact: true,
          },
          {
              title: 'New stock',
              path: '/p/user/stocks/add',
              exact: true,
          },
      ]

      for (let i=0; i < titles_arr.length; i++) {
          if(!titles_arr[i].exact && nextpage.indexOf(titles_arr[i].path) !=-1) {
              document.title = titles_arr[i].title
              break
          } else if(titles_arr[i].exact && nextpage == titles_arr[i].path) {
              document.title = titles_arr[i].title
          }
      }

  }

  renderWrapper() {
      const { children, styles } = this.props;

      return (
        <div className={styles.container}>
            <NavWrapper {...this.props}>

                {children}

            </NavWrapper>
        </div>
      );
  }


  render() {

      return (
          <div>
              {this.renderWrapper()}

          </div>
      )
  }
}



App.propTypes = {
    children: PropTypes.any.isRequired,
    styles: PropTypes.object
};

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onToggleLeftMenu: (payload) => {
				dispatch({type: 'LEFT_MENU_TOGGLE', payload})
			},
			onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
            onLoginByToken: (payload) => {
				dispatch({type: 'USER_LOGGED_IN', payload})
			},
		})
	))
	(App)
);
