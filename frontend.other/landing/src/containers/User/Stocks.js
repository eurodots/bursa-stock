import React, {Component} from 'react'
import { connect } from 'react-redux'
import PageUserStocks from '../../components/PageUserStocks'

// import theme from './theme.scss'

class Protected extends Component {

    render() {
        const {authData} = this.props

        return (
            <div>
                <PageUserStocks />
            </div>
        )
    }
}


export default connect(state => ({ authData: state.user.data }))(Protected)
