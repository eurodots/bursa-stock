import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import {Link} from 'react-router-dom';

import Preloader from '../components/Preloader'
import PairsTable from '../components/Dashboard/PairsTable'

const styles = theme => ({
});

class Coins extends Component {


    render() {

        // let showLayer = this.props.config.coins_arr.length > 0 ? true : false
        //
        //
        // if(!showLayer) {
        //     return <Preloader />
        // }
        return (
          <div>
              <PairsTable {...this.props} />
          </div>
        )
    }
}

Coins.propTypes = {
    styles: PropTypes.object
};

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onSelectCoins: (payload) => {
				dispatch({type: 'SELECT_COINS', payload})
			},
		})
	))
	(Coins)
);
