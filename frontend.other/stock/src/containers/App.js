import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cssModules from 'react-css-modules';
import styles from '../style/index.scss';

import NavWrapper from '../components/NavWrapper'
import Preloader from '../components/Preloader'
import WindowListener from '../components/WindowListener'



// import { withStyles } from 'material-ui/styles';

import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import {getCoinsArr} from '../utils/functions'

const _ = require('lodash');
// import { connect } from 'react-redux';
// import * as ConfigActions from '../actions/config';
// import wrapActionCreators from '../utils/wrapActionCreators';

// @connect(state => ({
//     config: state.config
// }), wrapActionCreators(ConfigActions))

@cssModules(styles)
class App extends Component {


  constructor(props) {
    super(props)

    this.state = {
      data_loaded: false,
    }
  }

  componentDidMount() {
      this.initStockConfig()
      this.preloadData()
  }

  initStockConfig() {
    function getElementAttrs(el) {
      return [].slice.call(el.attributes).map((attr) => {
          return {name: attr.name, value: attr.value}
      });
    }

    let el = document.getElementById('stock-widget')
    let attrs = getElementAttrs(el)
    let stock_name = _.find(attrs, { 'name': 'data-stock-name' })
    let stock_token = _.find(attrs, { 'name': 'data-stock-token' })
    let stock_private = _.find(attrs, { 'name': 'data-stock-private' })

    this.props.onStockInit({
        stock_name: stock_name ? stock_name.value : '',
        stock_token: stock_token ? stock_token.value : '',
        stock_private: stock_private ? stock_private.value : '',
    })
  }


  preloadData() {
      getCoinsArr().then((response) => {
          this.props.onPreloadCoins(response.data)
      })
  }


  renderWrapper() {
      const { children, styles } = this.props;

      // let config = this.props.config

      // {React.cloneElement(this.props.children, this.props)}
      return (
        <div className={styles.container}>
            <NavWrapper {...this.props}>

                {children}

                <WindowListener onWindowResize={this.props.onWindowResize} windowSize={this.props.config.windowSize} />

            </NavWrapper>
        </div>
      );
  }


  render() {

      // let showLayer = false
      // if(this.props.config) {
      //     showLayer = true
      // }
      // {showLayer ? this.renderWrapper() : <Preloader />}

      return (
          <div>

              {this.renderWrapper()}

          </div>
      )
  }
}


// static propTypes = {
//   children: PropTypes.any.isRequired,
//   styles: PropTypes.object
// };
App.propTypes = {
    children: PropTypes.any.isRequired,
    styles: PropTypes.object
};

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onToggleLeftMenu: (payload) => {
				dispatch({type: 'LEFT_MENU_TOGGLE', payload})
			},
			onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
            onPreloadCoins: (payload) => {
                dispatch({type: 'PRELOAD_COINS', payload})
            },
            onSelectCoins: (payload) => {
				dispatch({type: 'SELECT_COINS', payload})
			},
            onWindowResize: (payload) => {
				dispatch({type: 'WINDOW_RESIZE', payload})
			},
            onStockInit: (payload) => {
				dispatch({type: 'STOCK_INIT', payload})
			},
		})
	))
	(App)
);
