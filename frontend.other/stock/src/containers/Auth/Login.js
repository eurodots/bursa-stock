import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { login } from '../../actions/user'

export class LoginContainer extends Component {

  static propTypes = {
    login: PropTypes.func.isRequired
  };

  onClick = (e) => {
    e.preventDefault()
    this.props.login({
      name: this.refs.name.value,
      isAdmin: this.refs.admin.checked
    })
  };

  render() {
    return (
      <div>
        <div><input type="text" ref="name" placeholder="Enter your username" /></div>
        <label><input type="checkbox" ref="admin" />Are you an Administrator?</label>
        <div><button onClick={this.onClick}>Login</button></div>
      </div>
    )
  }

}
export default connect(null, { login })(LoginContainer)
