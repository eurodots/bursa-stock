import React from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';
import NumberFormat from 'react-number-format';
import Input from 'material-ui/Input';
import Slider from 'react-toolbox/lib/slider';


import Preloader from '../../../Preloader'
import {web3balanceForToken, web3balanceOf, web3approve} from '../../../../utils/getWeb3.js'


function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      ref={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      prefix={props.prefix}
    />
  );
}

NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};



const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input: {
    margin: theme.spacing.unit,
  },
  buttonApproved: {
      width: '100%',
      display: 'block',
  },
  numberFormat: {
      margin: '0 4px',
  },
});

class BalanceApprove extends React.Component {
  state = {
    open: false,
    balance_approved: 0,
    balance_disabled: true,
    balance_token: 0,
    slider_value: 0,
    slider_minimum: 0,
    balance_difference: 0,
  };

  timeout = false
  clearTimer = () => {
      var that = this
      clearTimeout(that.timeout)
  }


  componentWillUnmount () {
      this.props.onRef(undefined)
      this.clearTimer()
  }

  componentDidMount() {
      this.props.onRef(this)
      this.getBalanceApproved()
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleChangeSlider = (value) => {
    this.setState({slider_value: value});
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  approveExtra(value) {

      this.setState({
          balance_difference: value,
          slider_value: value,
          slider_minimum: value,
          open: true,
      })
  }

  saveForm = () => {
      let init = {
          token: this.props.tokenAddress,
          value: this.state.slider_value
      }

      web3approve(init).then((res) => {
          console.log(res)
      })

      this.handleClose()
      this.props.onToggleRightMenu('hideAccount')
  }



  async getBalanceApproved() {
      let user_address = this.props.userAddress
      let token_address = this.props.tokenAddress

      const { slider_value } = this.state

      if(token_address && token_address) {
          web3balanceForToken(token_address, user_address).then((res) => {
              this.setState({
                  balance_approved: res,
                  balance_disabled: false,
                  slider_value: slider_value == 0 ? res : slider_value
              })
          })

          web3balanceOf(token_address, user_address).then((res) => {
              console.log('callTokenBalance()')
              console.log(res)
              this.setState({balance_token: res})
          })
      }


      this.timeout = setTimeout(() => {
          this.getBalanceApproved()
      }, 3000)


  }

  renderForm() {
      const { classes } = this.props;
      const { slider_value, slider_minimum, balance_approved, balance_disabled, balance_token } = this.state;

      return (
        <div>

            <ul className={classes.container}>
                <li>
                    <Input
                      value={slider_value}
                      onChange={this.handleChange('slider_value')}
                      inputComponent={NumberFormatCustom}
                      className={classes.input}
                      disabled={balance_disabled}
                      inputProps={{
                        'aria-label': 'Description',
                        'prefix': ''
                      }}
                    />
                </li>
                <li>
                    Approved:
                    <NumberFormat className={classes.numberFormat} value={balance_approved} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='' />
                    from
                    <NumberFormat className={classes.numberFormat} value={balance_token} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='' />
                </li>
            </ul>

            <div>
                <Slider
                    min={0}
                    max={balance_token}
                    value={slider_value}
                    onChange={(value) => this.handleChangeSlider(value)} />
            </div>

      </div>
      )
  }

  render() {
    const { fullScreen, classes } = this.props;
    const { balance_approved, balance_disabled, balance_difference } = this.state;

    if(balance_disabled) {
        return <Preloader size={20} />
    }
    return (
      <div>
        <Button onClick={this.handleClickOpen} className={classes.buttonApproved} color="primary" variant="raised">
            <span style={{marginRight: 4}}>Approved:</span>
            <NumberFormat value={balance_approved} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='' />
        </Button>
        <Dialog
          fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Approving the amount for the token"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Specify the amount allowed for payment when executing the orders of this token.
            </DialogContentText>
            <div>
                {balance_difference}
            </div>
            {this.renderForm()}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Close
            </Button>
            <Button onClick={this.saveForm} color="primary" variant="raised" autoFocus>
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

BalanceApprove.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
};

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
            onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
		})
	))
	(withMobileDialog()(withStyles(styles)(BalanceApprove)))
);
