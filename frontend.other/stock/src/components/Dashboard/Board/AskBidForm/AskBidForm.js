import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

import NumberFormat from 'react-number-format';
import Input from 'material-ui/Input';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import green from 'material-ui/colors/green';


import dateFormat from 'dateformat';
import {
	web3ordersBook,
	depositWeb3,
	web3orderSell,
	web3orderBuy,
	web3balanceOf,
	web3balanceForToken,
	web3findEmptySpot
} from '../../../../utils/getWeb3'

import theme from './theme.scss'

const _ = require('lodash');


function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  let prefix = `[${props.prefix}]   `
  return (
    <NumberFormat
      {...other}
      ref={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      placeholder={props.prefix}
      prefix={prefix} />
  );
}
NumberFormatCustom.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};



const mui = createMuiTheme({
  palette: {
    primary: {
		main: green[500],
		contrastText: 'white',
	}, // Purple and green play nicely together.
    // primary: { main: '#11cb5f' }, // This is just green.A700 as hex.
  },
});

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  input: {
    // margin: theme.spacing.unit,
  },
  actionButton: {
      display: 'block',
      width: '100%',
  },
});

class AskBidForm extends React.Component {

    state = {
        data_orders: false,
        amount: 0,
        price: 0,
        recommend_price_sell: 0,
        recommend_price_buy: 0,
        balance_token: 0,
        balance_approved: 0,
        balance_approved_difference: 0,
        total_amount_sell: 0,
        submit_disabled: false,
        // price_min: 0,
        // price_max: 10,
    }


    timeout = false
    clearTimer = () => {
        var that = this
        clearTimeout(that.timeout)
    }

    componentWillUnmount () {
        this.clearTimer()
    }

    componentWillMount() {
        // this.getRecommendPrice()
        // this.loadWeb3()

        // this.props.onHideAllMenus()
    }





    async loadWeb3() {

        const { data_orders } = this.state;

        console.log('loadWeb3()')

        let token_address = this.props.data.token_address
        let user_address = this.props.data.user_address

        if(token_address && token_address) {

            web3ordersBook(token_address).then((res) => {
                console.log('web3ordersBook()')
                this.setState({data_orders: res})

                res.map((item, index) => {
                    let price = item.price
                    price = Number(price).toPrecision(8)
                    if(index == 0 && item.type == 'sell') this.setState({recommend_price_sell: price})
                    if(index == data_orders.length-1 && item.type == 'buy') this.setState({recommend_price_buy: price})
                })

            })

            web3balanceOf(token_address, user_address).then((res) => {
                this.setState({balance_token: res})
            })

            web3balanceForToken(token_address, user_address).then((res) => {
                this.setState({balance_approved: res})
            })

            this.applyAprovedBalance()

        }


        // this.checkAprovedBalance()

        let that = this
        this.timeout = setTimeout(function() {
            that.loadWeb3()
        }, 5000)
    }


    applyAprovedBalance() {
        const {data_orders, balance_approved, amount} = this.state
        const user_address  = this.props.data.user_address

        if(user_address && data_orders.length > 0) {
            let total_amount_sell = 0

            data_orders.map((item, index) => {
                if(user_address == item.user_address) {
                    if(item.type == 'sell') {
                        total_amount_sell = total_amount_sell + parseFloat(item.amount)
                    }
                }
            })

            this.setState({total_amount_sell: total_amount_sell})

            let value = 0
            let total_amount = total_amount_sell + Number(amount)
                if( total_amount > balance_approved ) {
                    value = balance_approved - total_amount
                } else {
                    value = total_amount_sell - balance_approved
                }
            this.setState({
                balance_approved_difference: value,
            })

        }
    }


    // initDepositWeb3(value) {
	//
	// 	let that = this
    //     that.props.onToggleRightMenu('hideAccount')
	//
	// 	let result = depositWeb3(value).then((res) => {
	// 		if(res) {
	// 			// console.log('TUT')
	// 			console.log(res)
	//
	// 			let data = {
	// 				user_address: that.props.config.web3Metamask.user_address,
    //                 transactions: {
    //                     coin: that.props.data.coin_from,
    //                     value: value,
    //                     hash: res,
    //                     date: dateFormat(new Date(), "isoUtcDateTime"),
    //                     created_at: Date.now(),
    //                 }
	// 			}
	//
    //             that.props.onSaveWeb3Transactions(data)
	//
    //             // console.log(that.props.data.coin_from)
    //             // console.log(data)
	//
	// 		}
	// 	})
	//
	// }

    // initDepositWeb3(0.1)

    async initOrderPlace(type) {

        const {data_orders, amount, price, balance_approved_difference} = this.state
        // let that = this

        // if(balance_approved_difference <= 0 && type == 'sell') {
        //     let value = Math.abs(balance_approved_difference) + Number(amount)
        //     this.props.refBalanceApprove.approveExtra(value)
        //     return false
        // }


		let token_address = this.props.data.token_address



        // console.log(data_orders)
        // this.setState({submit_disabled: true}, () => {})

        // GET EMPTY ID IN ORDERS BOOK
        // let web3data = await this.getOrdersData()

        // if(data_orders.length > 0) {
            this.setState({submit_disabled: false}, () => {})


            // let spot_ids = []
            // data_orders.map((item, index) => { if(item.type == type) spot_ids.push(item.spot_id) })
            // spot_ids = _.orderBy(spot_ids);
            // let empty_spot = _.last(spot_ids)+1




            // for(let i=10; i < spot_ids.length; i++) {
            //     let index = i+1
            //     if(spot_ids[i] != index) {
            //         empty_spot = index
            //         break
            //     }
            // }
            // console.log('EMPTY spot_id: '+empty_spot)
            // alert('Empty order # '+empty_spot)
            this.props.onToggleRightMenu('hideAccount')

            if(type == 'sell') {

				await web3findEmptySpot({type: 'sell', token: token_address}).then( async (empty_spot) => {

					alert('SPOT_ID #'+empty_spot)

					await web3orderSell({
	                    amount: amount,
	                    price: price,
	                    token: token_address,
	                    spot_id: empty_spot
	                }).then((res) => {
	                    alert(res)
	        			// if(res) {
	        			// 	// console.log('TUT')
	        			// 	// console.log(res)
	                    //
	                    //
	        			// 	// let data = {
	        			// 	// 	user_address: that.props.config.web3Metamask.user_address,
	                    //     //     transactions: {
	                    //     //         coin: that.props.data.coin_from,
	                    //     //         value: value,
	                    //     //         hash: res,
	                    //     //         date: dateFormat(new Date(), "isoUtcDateTime"),
	                    //     //         created_at: Date.now(),
	                    //     //     }
	        			// 	// }
	                    //     //
	                    //     // that.props.onSaveWeb3Transactions(data)
	                    //
	                    //     // console.log(that.props.data.coin_from)
	                    //     // console.log(data)
	                    //
	        			// }
	        		// })
	                })
		        })


            } else if(type == 'buy') {
				// alert('buy!')
				await web3findEmptySpot({type: 'sell', token: token_address}).then( async (empty_spot) => {

					alert('SPOT_ID #'+empty_spot)

	                await web3orderBuy({
	                    amount: amount,
	                    price: price,
	                    token: token_address,
	                    spot_id: empty_spot
	                }).then((res) => {
	                    alert(res)
	                })
				})
            }

        // }

    }


    handleChange = name => event => {
        this.setState({[name]: event.target.value}, () => {
            this.applyAprovedBalance()
        });
    };




    insertRecommendPrice = (name, value) => {
        this.setState({[name]: value})
    }

    renderFormPriceRangeInput(init) {
        const { classes, data } = this.props;
        // const {number_from, number_to} = this.state;

        return (
            <div>
                <ul className={theme.priceRangeLabel}>
                    <li>
                        <label>{init.label}</label>
                    </li>
                    <li data-el="recommend">
                        Recommend <span onClick={() => this.insertRecommendPrice(init.state_name, init.recommend_price)}>
                            <NumberFormat value={init.recommend_price} displayType={'text'} decimalScale={8} thousandSeparator={true} prefix='' />
                        </span>
                    </li>
                </ul>
                <Input
                  value={this.state[init.state_name]}
                  onChange={this.handleChange(init.state_name)}
                  inputComponent={NumberFormatCustom}
                  className={classes.input}
                  fullWidth
                  inputProps={{
                    'prefix': init.prefix,
                  }} />
            </div>
        )
    }

    insertBalanceToAmount(value) {
        this.setState({amount: value}, function() {})
    }

    renderForm() {
        const { classes, data } = this.props;
        const {
        	amount,
        	price,
        	balance_token,
        	balance_approved,
        	balance_approved_difference,
        	submit_disabled,
            total_amount_sell
        } = this.state;
		// const {data} = this.state


        // let total = amount * (( price_min/2 ) + ( price_max/2))
        let total = amount * price

        return (
			<MuiThemeProvider theme={mui}>
				<div className={theme.AskBidForm}>

	                <div>
	                    <label>
	                        Balance: <NumberFormat value={balance_token} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='' />
	                    </label>
	                </div>
	                <div>
	                    <label>
	                        Total amount: {total_amount_sell}
	                    </label>
	                </div>


	                <ul className={theme.priceRangeLabel}>
	                    <li>
	                        <label>Amount to {data.type}</label>
	                    </li>
	                    <li data-el="recommend">
	                        {balance_approved_difference <= 0
	                        ?
	                            <div>
	                                Need to approve: {Math.abs(balance_approved_difference)}
	                            </div>
	                        :
	                            <div>
	                                Available:
	                                <NumberFormat
	                                    value={balance_approved_difference}
	                                    displayType={'text'}
	                                    decimalScale={0}
	                                    thousandSeparator={true}
	                                    prefix=''
	                                    onClick={() => this.insertBalanceToAmount(balance_approved_difference)} />
	                            </div>
	                        }

	                    </li>
	                </ul>
	                <Input
	                  value={amount}
	                  onChange={this.handleChange('amount')}
	                  inputComponent={NumberFormatCustom}
	                  className={classes.input}
	                  fullWidth
	                  inputProps={{
	                    'prefix': data.coin_from,
	                  }} />

	                {data.type == 'sell' && this.renderFormPriceRangeInput({
	                    label: 'Price sell',
	                    state_name: 'price',
	                    recommend_price: this.state.recommend_price_sell,
	                    prefix: data.coin_to
	                })}

	                {data.type == 'buy' && this.renderFormPriceRangeInput({
	                    label: 'Price buy',
	                    state_name: 'price',
	                    recommend_price: this.state.recommend_price_buy,
	                    prefix: data.coin_to
	                })}



	                <Typography variant="subheading" className={theme.total}>
	                    <span data-el="totalLabel">Total:</span>
	                    <span>
	                        <span data-el="totalCoin">
	                            [{data.coin_to}]
	                        </span>
	                        <span data-el="totalNumber">
	                            <NumberFormat data-el="numberFormat" value={total} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='≈ ' />
	                        </span>
	                    </span>
	                </Typography>

	                <div data-el="hint">
	                    Your order is in cross with the best sell order in the order book (price = 0.028253397565235212).
	                </div>

	                {data.type == 'sell' &&
	                <Button variant="raised" disabled={submit_disabled} size="large" color="secondary" className={classes.actionButton} onClick={() => this.initOrderPlace('sell')} >
	                  Sell «{data.coin_from}»
	                </Button>}

	                {data.type == 'buy' &&
	                <Button variant="raised" disabled={submit_disabled} size="large" color="primary" className={classes.actionButton} onClick={() => this.initOrderPlace('buy')} >
	                  Buy «{data.coin_from}»
	                </Button>}

				</div>
			</MuiThemeProvider>
		)
    }



	render() {

        return (
            <div>{this.renderForm()}</div>
        )

	}
}

AskBidForm.propTypes = {
	classes: PropTypes.object.isRequired
};

// export default withStyles(styles)(AskBidForm);

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
            onSaveWeb3Transactions: (payload) => {
				dispatch({type: 'SAVE_WEB3_TRANSACTIONS', payload})
			},
		})
	))
	(withStyles(styles)(AskBidForm))
);
