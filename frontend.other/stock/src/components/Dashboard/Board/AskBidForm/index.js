import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
// import SwipeableViews from 'react-swipeable-views';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Typography from 'material-ui/Typography'

import AskBidForm from './AskBidForm';

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: '20px 0 0' }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    // width: 500,
  },
  appBar: {
      boxShadow: 'none',
      marginTop: '-10px',
  },
  indicator: {
      height: 5,
  },
  tab: {
      minWidth: 'inherit',
  }
});

class AskBidTabs extends React.Component {
  state = {
    value: 'sell',
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  // handleChangeIndex = index => {
  //   this.setState({ value: index });
  // };

  render() {
    const { classes, theme, coinFrom } = this.props;

    const { value } = this.state

    return (
      <div className={classes.root}>
        <AppBar position="static" color="inherit" className={classes.appBar}>
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorClassName={classes.indicator}
            indicatorColor="primary"
            textColor="primary"
            fullWidth
          >
            <Tab value="sell" label="Sell" className={classes.tab} />
            <Tab value="buy" label="Buy" className={classes.tab} />
          </Tabs>
        </AppBar>

        <TabContainer dir={theme.direction}>
            <AskBidForm
                refBalanceApprove={this.props.refBalanceApprove}
                data={{
                    token_address: this.props.tokenAddress,
                    user_address: this.props.userAddress,
                    type: value,
                    coin_from: coinFrom,
                    coin_to: 'ETH'
                }} />
        </TabContainer>

      </div>
    );
  }
}

AskBidTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(AskBidTabs);
