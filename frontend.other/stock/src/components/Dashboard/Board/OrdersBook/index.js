import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
// import SwipeableViews from 'react-swipeable-views';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Typography from 'material-ui/Typography'

import Orders from './Orders';
import OrdersOwner from './OrdersOwner';
import {web3ordersBook} from '../../../../utils/getWeb3.js'
import Preloader from '../../../Preloader'

const _ = require('lodash');


function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: '20px 0 0' }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    // width: 500,
  },
  appBar: {
      boxShadow: 'none',
      marginTop: '-10px',
  },
  indicator: {
      height: 5,
  },
  tab: {
      minWidth: 'inherit',
  },
});

class AskBidTabs extends React.Component {
  state = {
    tab_active: 'all',
    data_orders: false,
    data_orders_owner: false,
    data_preloader: true,
  };

  handleChangeTab = (event, value) => {
    this.setState({ tab_active: value });
  };

  timeout = false
  clearTimer = () => {
      var that = this
      clearTimeout(that.timeout)
  }

  componentWillUnmount () {
      // this.props.onRef(undefined)
      this.clearTimer()
  }

  componentDidMount() {
      // this.props.onRef(this)
      this.getOrdersData()
  }

  async getOrdersData() {

      console.log('getOrdersData()')

      const {tokenAddress, userAddress} = this.props

      this.setState({
          // data_orders: [],
          data_preloader: true,
      }, () => {})

      let getOrders = await web3ordersBook(tokenAddress).then((res) => {
          return res
      })
      // if(typeof(getOrders) !== 'array') {
      //     this.setState({data_orders: []}, () => {})
      // }
      if(!getOrders) {
          // this.setState({data_orders: []}, () => {})
      } else {
          let data_orders_owner = []
          getOrders.map((item, index) => {
              if(item.user_address == userAddress) {
                 data_orders_owner.push(item)
              }
          })
          this.setState({
              data_orders: getOrders,
              data_orders_owner: data_orders_owner,
              data_preloader: false,
          }, () => {})
      }

      this.timeout = setTimeout(() => {
          this.getOrdersData()
      }, 3000)

  }

  render() {
    const { classes, theme, coinFrom } = this.props;
    const { tab_active, data_orders, data_orders_owner, data_preloader } = this.state

    let title_my_orders = `My (${_.values(data_orders_owner).length})`

    if(data_orders === false) {
        return (<div>No orders</div>)
    }

    let title_orders = 'Orders Book'
    if(data_preloader) {
        title_orders = <Preloader size={20} />
    }


    return (
      <div className={classes.root}>

        <div>
            <AppBar position="static" color="inherit" className={classes.appBar}>
              <Tabs
                value={this.state.tab_active}
                onChange={this.handleChangeTab}
                indicatorClassName={classes.indicator}
                indicatorColor="primary"
                textColor="primary"
                fullWidth
                >
                <Tab value="all" label={title_orders} className={classes.tab} style={{flex: '1 1 55%'}} />
                <Tab value="my" label={title_my_orders} className={classes.tab} />
              </Tabs>
            </AppBar>

            <TabContainer dir={theme.direction}>
                {tab_active == 'all' && <Orders
                    dataOrders={data_orders}
                    tokenAddress={this.props.tokenAddress}
                    userAddress={this.props.userAddress} />}
                {tab_active == 'my' && <OrdersOwner
                    dataOrders={data_orders_owner}
                    tokenAddress={this.props.tokenAddress}
                    userAddress={this.props.userAddress} />}
            </TabContainer>
        </div>

      </div>
    );
  }
}

AskBidTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(AskBidTabs);
