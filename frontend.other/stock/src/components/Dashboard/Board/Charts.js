import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Typography from 'material-ui/Typography'

import HeikinAshi from '../../charts/HeikinAshi'
import Crocodile from '../../charts/Crocodile'


function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: '20px 0 0' }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    // width: 500,
  },
  appBar: {
      boxShadow: 'none',
      marginTop: '-10px',
  },
  indicator: {
      height: 5,
  },
  tab: {
      minWidth: 'inherit',
  }
});

class Charts extends React.Component {

    state = {
      value: 'a',
    };

    handleChange = (event, value) => {
      this.setState({ value });
    };

	render() {

        const { classes, theme, tokenAddress } = this.props;

        const { value } = this.state

		return (
            <div className={classes.root}>
              <AppBar position="static" color="inherit" className={classes.appBar}>
                <Tabs
                  value={this.state.value}
                  onChange={this.handleChange}
                  indicatorClassName={classes.indicator}
                  indicatorColor="primary"
                  textColor="primary"
                >
                  <Tab value="a" label="Chart" className={classes.tab} />
                  <Tab value="b" label="Volume" className={classes.tab} />
                </Tabs>
              </AppBar>

              <TabContainer dir={theme.direction}>
                  {value == 'a' && <HeikinAshi tokenAddress={tokenAddress} />}
                  {value == 'b' && <Crocodile tokenAddress={tokenAddress} />}
              </TabContainer>

            </div>
		)
	}
}


Charts.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Charts);
