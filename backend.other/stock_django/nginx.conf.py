# mysite_nginx.conf

upstream django {
    # server unix:///home/admin/web/bursadex.com/public_html/mysite.sock; # взаимодействие с uwsgi через Unix-сокет (мы воспользуемся этим вариантом позже)
    server 127.0.0.1:8000; # взаимодействие с uwsgi через веб-порт
}

server {
    listen      80;
    server_name     bursadex.com www.bursadex.com;
    error_log  /var/log/apache2/domains/bursadex.com.error.log error;
    charset     utf-8;

    location / {
        uwsgi_pass  django;
        include     /home/admin/web/bursadex.com/public_html/uwsgi_params; # файл uwsgi_params, который мы только что взяли с github
        proxy_pass      http://127.0.0.1:8000/;
        location ~* ^.+\.(jpg|jpeg|gif|png|ico|svg|css|zip|tgz|gz|rar|bz2|exe|pdf|doc|xls|ppt|txt|odt|ods|odp|odf|tar|bmp|rtf|js|mp3|avi|mpeg|flv|html|htm)$ {
            root           /home/admin/web/bursadex.com/public_html;
            access_log     /var/log/apache2/domains/bursadex.com.log combined;
            access_log     /var/log/apache2/domains/bursadex.com.bytes bytes;
            expires        max;
            try_files      $uri @fallback;
#            add_header Access-Control-Allow-Origin "*";
        }
    }

    # максимальный размер загружаемых на сервер данных
    client_max_body_size 75M;

    # обслуживание медиа файлов и статики
    location /media  {
        alias /home/admin/web/bursadex.com/public_html/media;  # расположение медиафайлов (при необходимости измените)
    }

    location /static {
        alias /home/admin/web/bursadex.com/public_html/static;  # расположение статики (при необходимости измените)

    }

    location /error/ {
        alias   /home/admin/web/bursadex.com/document_errors/;
    }

    location @fallback {
        proxy_pass      http://127.0.0.1:8000;
    }

    location ~ /\.ht    {return 404;}
    location ~ /\.svn/  {return 404;}
    location ~ /\.git/  {return 404;}
    location ~ /\.hg/   {return 404;}
    location ~ /\.bzr/  {return 404;}

    disable_symlinks if_not_owner from=/home/admin/web/bursadex.com/public_html;

    include /home/admin/web/bursadex.com/private/*.conf;
    include /home/admin/conf/web/nginx.bursadex.com.conf*;
}
