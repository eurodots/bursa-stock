from django.db import models
from django.core.validators import MinLengthValidator
from ckeditor.fields import RichTextField

# —————————————————————————
from pages.dicts.init import *
# —————————————————————————


class Faq(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, blank=True, editable=False)
    category = models.CharField(null=False, max_length=100, choices=CHOICES_CATEGORY)
    title = models.CharField(null=False, max_length=100, validators=[MinLengthValidator(4)])
    description = RichTextField(config_name='default', max_length=20000)

    # def __str__(self):
    #     return self.title