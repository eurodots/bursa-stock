from django.contrib import admin
from django import forms

from .models import *


class FaqAdmin( admin.ModelAdmin ):
    list_display = ('id', 'title', 'category', 'created_at')
    list_display_links = ('id', 'title')
    search_fields = ('title', 'description')
    list_per_page = 25

    class Meta:
        model = Faq
        exclude = [""]


    class Media:
        # js = ('js/admin/my_own_admin.js',)
        css = {
            'all': ('css/admin.css',)
        }


admin.site.register(Faq, FaqAdmin)

