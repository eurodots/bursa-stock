from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import FaqSerializer

from .models import Faq

# —————————————————————————
from pages.dicts.init import *
# —————————————————————————

# clear history
# from django.contrib.admin.models import LogEntry
# LogEntry.objects.all().delete()


@api_view(['GET'])
def apiViewSet(request):

    # serializer = FaqSerializer(data=request.data)

    data = Faq.objects.all()
    serializer = FaqSerializer(data, many=True)

    pages_arr = {}
    for cat in CHOICES_CATEGORY_DATA:
        cat_id = str(cat['name'])
        pages_arr[cat_id] = {
            'category_id': cat['name'],
            'category_name': cat['name'],
            'category_description': cat['description'],
            'pages': [],
        }

    for p in serializer.data:
        cat_id = p.get('category')
        pages_arr[cat_id]['pages'].append(p)


    return Response({
        'results': pages_arr
    })