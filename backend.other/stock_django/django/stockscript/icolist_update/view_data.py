import sqlite3
import os
from DEFAULT.settings import BASE_DIR, DATABASES
from stockscript.icolist_update._functions import *
from stockscript.utils.favicon_finder import findFavicon

itemModel = [
    # TABLE: ICOLIST
    'id_list', 'token_name', 'token_platform', 'token_rank', 'token_address', 'token_updated_at', 'token_ticker',

     # TABLE: MARKET
     'id_market', 'changes_1h', 'changes_24h', 'changes_7d',
     'volume24_usd', 'volume24_btc', 'suply_number', 'price_usd', 'price_btc', 'capital_usd', 'capital_btc',
     'platform', 'updated_at', 'coinmarketcap', 'coinmarketcap_id', 'name'
]



def coinArr(coin):
    conn = sqlite3.connect(DATABASES['default']['NAME'])
    cur = conn.cursor()

    token_ticker = coin.upper()

    # CONCATINATE TOKEN DATA
    cur.execute("SELECT * FROM stockscript_icolist list INNER JOIN stockscript_icolist_market market ON list.token_ticker = market.name WHERE list.token_platform = 'Ethereum' AND list.token_ticker = ?", (token_ticker,))
    coin_arr = cur.fetchone()
    coin_arr = sqlite_serializer([coin_arr], itemModel)[0] if coin_arr else False

    if coin_arr:
        # ADD MAX RANK
        cur.execute("SELECT token_rank FROM stockscript_icolist order by token_rank desc")
        maxrank_arr = cur.fetchone()
        maxrank_arr = maxrank_arr[0] if maxrank_arr else False
        coin_arr['token_rank_max'] = maxrank_arr

        # ADD TOKEN LINKS
        cur.execute("SELECT label, value FROM stockscript_icolist_links WHERE coin = ?", (coin,))
        links_arr = cur.fetchall()
        links_arr = sqlite_serializer(links_arr, ['label', 'link'])
        coin_arr['links'] = links_arr

        # ADD FAVICON IF EXIST
        coin_arr['favicon'] = findFavicon(coin_arr['token_ticker'])

        return {
            "results": coin_arr
        }
    else:

        # CONCATINATE TOKEN DATA
        cur.execute("SELECT token_platform, token_name, token_links, token_ticker, token_address FROM stockscript_subsribers WHERE token_ticker = ?", (token_ticker,))
        coin_arr = cur.fetchone()

        subsriber_model = ['token_platform', 'token_name', 'token_links', 'token_ticker', 'token_address']
        coin_arr = sqlite_serializer([coin_arr], subsriber_model)[0] if coin_arr else False

        if coin_arr:

            # ADD FAVICON IF EXIST
            coin_arr['favicon'] = findFavicon(coin_arr['token_ticker'])

            return {
                "results": coin_arr
            }
        else:
            return {
                "error": True,
                "message": "Not found"
            }



def icoListArr():

    conn = sqlite3.connect(DATABASES['default']['NAME'])
    cur = conn.cursor()

    cur.execute("SELECT * FROM stockscript_icolist list INNER JOIN stockscript_icolist_market market ON list.token_ticker = market.name WHERE list.token_platform = 'Ethereum'")
    ico_list = cur.fetchall()

    ico_list = sqlite_serializer(ico_list, itemModel)



    # ADD FAVICON IF EXIST
    for index, ico in enumerate(ico_list):
        ico_list[index]['favicon'] = findFavicon(ico['token_ticker'])
        ico_list[index]['active'] = False
        ico_list[index]['id'] = ico['token_ticker'] # neccesary for reactjs table iterations


    # COLLECT COINS ARRAY
    coins_arr = {}
    data_arr = {}
    for ico in ico_list:
        name = str(ico['token_ticker'])
        coins_arr[name] = name
        data_arr[name] = ico


    return {
        "coins": coins_arr,
        "data": data_arr
    }


def coinsFaviconsArr():
    conn = sqlite3.connect(DATABASES['default']['NAME'])
    cur = conn.cursor()

    cur.execute(
        "SELECT token_name, token_ticker FROM stockscript_icolist")
    favicons_arr = cur.fetchall()

    favicons_list = []
    for index, f in enumerate(favicons_arr):
        if index < 35:
            icon = findFavicon(f[1])
            if icon:
                favicons_list.append({
                    'name': f[0],
                    'favicon': icon
                })

    # print(favicons_list)

    return {
        "meta": {
            "total": len(favicons_arr)
        },
        "results": favicons_list
    }


# icoListArr()
# coinArr('OMG')
