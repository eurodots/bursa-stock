import pytz
import time
import datetime
import dateparser
import json

from stockscript.utils.config import sysConf


def sqlite_insert(conn, table, row):
    cols = ', '.join('"{}"'.format(col) for col in row.keys())
    vals = ', '.join(':{}'.format(col) for col in row.keys())
    sql = 'INSERT INTO "{0}" ({1}) VALUES ({2})'.format(table, cols, vals)
    lastrowid = conn.cursor().execute(sql, row).lastrowid
    conn.commit()

    print('SQLITE: INSERTED')
    return lastrowid

def sqlite_update(conn, table, row, id):
    cols = ', '.join('{0} = "{1}"'.format(key, row[key]) for key in row.keys())
    sql = 'UPDATE "{0}" SET {1} WHERE {2} = "{3}"'.format(table, cols, id['key'], id['value'])
    conn.cursor().execute(sql, row)
    conn.commit()
    print('SQLITE: UPDATED')


def sqlite_serializer(rows, model):
    arr = []
    for row in rows:
        a = {}
        for index, val in enumerate(row):
            a[model[index]] = val
        arr.append(a)
    return arr



def split(arr, size):
    arrs = []
    while len(arr) > size:
        pice = arr[:size]
        arrs.append(pice)
        arr = arr[size:]
    arrs.append(arr)
    return arrs


def timePeriod(value):
    time_period = datetime.datetime.now() - datetime.timedelta(hours=value)
    time_period = int(time_period.timestamp())
    return time_period


def timestamp(format=False):
    local_tz = pytz.timezone(sysConf['timeZone'])
    datetime_without_tz = datetime.datetime.now()
    datetime_with_tz = local_tz.localize(datetime_without_tz, is_dst=None) # No daylight saving time
    datetime_in_utc = datetime_with_tz.astimezone(pytz.utc)
    if format:
        timeNow = datetime_in_utc.strftime(format)
        timeNow = dateparser.parse(timeNow)
        timeNow = time.mktime(timeNow.timetuple())
        # '%Y-%m-%d %H:%M:%S %Z'
    else:
        timeNow = time.mktime(datetime_in_utc.timetuple())

    return int(timeNow)




def d(value=False):
    print('\n')
    print('------',value,'------')
    print('\n')



def jprint(arr):
    print(json.dumps(arr, indent=4, sort_keys=False))