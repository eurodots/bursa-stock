"""django2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework import routers

from . import views

# router = routers.DefaultRouter()
# router.register(r'platform', views.platformViewSet, base_name='api-platform')

urlpatterns = [
    url(r'^user/stocks/add/', views.userStocksAddViewSet, name='api-stocks-add'),
    url(r'^user/stocks/delete/', views.userStocksDeleteViewSet, name='api-stocks-delete'),
    url(r'^user/stocks/list/', views.userStocksListViewSet, name='api-stocks-list'),

    url(r'^icosignup_favicon_upload/', views.icosignupFaviconUpload, name='api-signup-favicon-upload'),
    url(r'^coins_favicons/', views.coinsFaviconsViewSet, name='api-icolist'),
    url(r'^coins/', views.coinsListViewSet, name='api-icolist'),
    url(r'^coin/(?P<coin>\w{0,10})/$', views.coinViewSet, name='api-coin'),

    url(r'^token/(?P<address>\w{0,100})/$', views.ethTokenViewSet, name='api-token-data'),
    url(r'^token_history/(?P<address>\w{0,100})/$', views.ethTokenHistoryViewSet, name='api-token-history'),

    url(r'^linkpreview/', views.linkPreviewSet, name='api-linkpreview'),
    # url(r'^router/', include(router.urls)),
    url(r'^form/icosignup/', views.form_view, name="form-icosignup"),
]
