from django.db import models
from django_countries.fields import CountryField
from django.core.validators import MinLengthValidator
from six import text_type
from stockscript.storage import OverwriteStorage

# —————————————————————————
import re
from stockscript.dicts.init import *
from ckeditor.fields import RichTextField
# —————————————————————————


class FaviconUploadModel(models.Model):
    file = models.ImageField(upload_to='favicons.tmp/', default='favicons.tmp/None/no-img.jpg', storage=OverwriteStorage())


class Subsribers(models.Model):
    user_id = models.IntegerField(null=False)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, editable=False)
    token_platform = models.CharField(null=False, max_length=1, default=0, editable=False)
    token_category = models.CharField(null=False, max_length=30)
    token_industry = models.CharField(null=False, max_length=30)
    token_domain = models.CharField(null=False, max_length=30)
    token_wallet = models.CharField(null=False, max_length=50)
    token_country = CountryField(null=False, verbose_name="Project country")
    token_tariff = models.CharField(null=False, max_length=20, choices=CHOICES_TARIFF)
    token_platform = models.CharField(null=False, max_length=3, choices=CHOICES_PLATFORM)
    token_status = models.CharField(null=False, max_length=3, choices=CHOICES_STATUS)
    token_name = models.CharField(null=False, max_length=20, validators=[MinLengthValidator(3)])
    token_ticker = models.CharField(null=False, unique=False, max_length=10, validators=[MinLengthValidator(3)], verbose_name="Token's ticker code")
    token_description = RichTextField(config_name='default', max_length=5000)
    token_links = models.CharField(null=False, max_length=500)
    token_address = models.CharField(null=False, unique=False, max_length=100)
    script_version = models.CharField(null=False, max_length=3, default="1.0")
    active = models.IntegerField(null=False, default=1)

    def __get_label(self, field):
        return text_type(self._meta.get_field(field).verbose_name)


    # token_ticker.upper()
    def save(self, *args, **kwargs):
        for field_name in ['token_ticker']:
            val = getattr(self, field_name, False)
            if val:
                setattr(self, field_name, re.sub(r'[\W_]+', '', val).upper())
        super(Subsribers, self).save(*args, **kwargs)



class IcoList(models.Model):
    token_ticker = models.CharField(null=False, unique=True, max_length=10)
    token_name = models.CharField(null=False, max_length=20)
    token_platform = models.CharField(null=False, max_length=3)
    token_rank = models.IntegerField(null=False)
    token_address = models.CharField(null=False, max_length=100)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True, editable=False)


class IcoList_links(models.Model):
    coin = models.CharField(null=False, unique=False, max_length=10)
    label = models.CharField(null=False, unique=False, max_length=30)
    value = models.CharField(null=False, unique=False, max_length=200)


class IcoList_market(models.Model):
    changes_1h = models.CharField(null=False, max_length=20)
    changes_24h = models.CharField(null=False, max_length=20)
    changes_7d = models.CharField(null=False, max_length=20)

    volume24_usd = models.CharField(null=False, max_length=20)
    volume24_btc = models.CharField(null=False, max_length=20)

    suply_number = models.CharField(null=False, max_length=20)

    price_usd = models.CharField(null=False, max_length=20)
    price_btc = models.CharField(null=False, max_length=20)

    capital_usd = models.CharField(null=False, max_length=20)
    capital_btc = models.CharField(null=False, max_length=20)

    platform = models.CharField(null=False, max_length=20)
    name = models.CharField(null=False, unique=True, max_length=20)
    updated_at = models.CharField(null=False, max_length=20)
    coinmarketcap = models.CharField(null=False, max_length=20)
    coinmarketcap_id = models.CharField(null=False, max_length=20)
