from django.contrib import admin
from django import forms

from .models import *

class SubsribersAdminForm( forms.ModelForm ):
    token_links = forms.CharField(widget=forms.Textarea(attrs={'rows': 5, 'cols': 100}))


    class Meta:
        model = Subsribers
        exclude = [""]

class SubsribersAdmin( admin.ModelAdmin ):
    form = SubsribersAdminForm


admin.site.register(Subsribers, SubsribersAdmin)

