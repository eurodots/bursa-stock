

CHOICES_TARIFF_DATA = (
    'Free',
    'Pro',
    'Enterprise'
)


CHOICES_CATEGORY_DATA = (
    'Blockchain Infrastructure',
    'Crypto - Currency',
    'Distributed',
    'Application',
    'Identity',
    'Platform',
    'Smart - contract',
    'Platform',
    'Tokenized',
    'Application',
    'Non - software(just foundraising)',
    'Other'
)


CHOICES_INDUSTRY_DATA = (
    'Coal',
    'Oil & Gas',
    'Oil & Gas Related Equipment and Services',
    'Renewable Energy',
    'Uranium',
    'Chemicals',
    'Metals & Mining',
    'Construction Materials',
    'Paper & Forest Products',
    'Containers & Packaging',
    'Aerospace & Defense',
    'Machinery, Equipment & Components',
    'Construction & Engineering',
    'Diversified Trading & Distributing',
    'Professional & Commercial Services',
    'Industrial Conglomerates',
    'Freight & Logistics Services',
    'Passenger Transportation Services',
    'Transport Infrastructure',
    'Automobiles & Auto Parts',
    'Textiles & Apparel',
    'Homebuilding & Construction Supplies',
    'Household Goods',
    'Leisure Products',
    'Hotels & Entertainment Services',
    'Media & Publishing',
    'Diversified Retail',
    'Other Specialty Retailers',
    'Beverages',
    'Food & Tobacco',
    'Personal & Household Products & Services',
    'Food & Drug Retailing',
    'Banking Services',
    'Investment Banking & Investment Services',
    'Insurance',
    'Real Estate Operations',
    'Residential & Commercial REIT',
    'Collective Investments',
    'Holding Companies',
    'Healthcare Equipment & Supplies',
    'Healthcare Providers & Services',
    'Pharmaceuticals',
    'Biotechnology & Medical Research',
    'Semiconductors & Semiconductor Equipment',
    'Communications & Networking',
    'Electronic Equipment & Parts',
    'Office Equipment',
    'Computers, Phones & Household Electronics',
    'Software & IT Services',
    'Telecommunications Services',
    'Electrical Utilities & IPPs',
    'Natural Gas Utilities',
    'Water Utilities'
)

CHOICES_PLATFORM_DATA = (
    'Bitshares',
    'Counterparty',
    'Ethereum',
    'Ethereum ERC20',
    'NEO',
    'NXT',
    'Omni',
    'Waves',
    'Non - blockchain platform',
    'Other'
)



CHOICES_STATUS_DATA = (
    'Live',
    'Development'
)

CHOICES_TARIFF = tuple((c.lower(), c) for c in CHOICES_TARIFF_DATA)
CHOICES_INDUSTRY = tuple((str(index), c) for index, c in enumerate(CHOICES_INDUSTRY_DATA))
CHOICES_CATEGORY = tuple((str(index), c) for index, c in enumerate(CHOICES_CATEGORY_DATA))
CHOICES_PLATFORM = tuple((str(index), c) for index, c in enumerate(CHOICES_PLATFORM_DATA))
CHOICES_STATUS = tuple((str(index), c) for index, c in enumerate(CHOICES_STATUS_DATA))

# print(CHOICES_CATEGORY)
# print(CHOICES_INDUSTRY)
# print(CHOICES_PLATFORM)
# print(CHOICES_STATUS)
