from django import forms
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _


from .models import *


from stockscript.dicts.init import *


class IcosignupForm(forms.ModelForm):
    token_description = forms.CharField(widget=forms.Textarea)
    token_links = forms.CharField(widget=forms.Textarea)
    token_industry = forms.MultipleChoiceField(widget=forms.SelectMultiple, choices=CHOICES_INDUSTRY)

    class Meta:
        model = Subsribers
        exclude = [""]



class PlatformData(forms.ModelForm):
    token_description = forms.CharField(widget=forms.Textarea)
    token_links = forms.CharField(widget=forms.Textarea)
    token_industry = forms.MultipleChoiceField(widget=forms.SelectMultiple, choices=CHOICES_INDUSTRY)

    class Meta:
        model = Subsribers
        exclude = [""]



# MAX IMAGE FILESIZE
# https://chriskief.com/2013/10/19/limiting-upload-file-size-with-django-forms/
class RestrictedImageField(forms.ImageField):
    def __init__(self, *args, **kwargs):
        self.max_upload_size = kwargs.pop('max_upload_size', None)
        # if not self.max_upload_size:
        #     self.max_upload_size = settings.MAX_UPLOAD_SIZE
        super(RestrictedImageField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(RestrictedImageField, self).clean(*args, **kwargs)
        try:
            if data.size > self.max_upload_size:
                raise forms.ValidationError(_('File size must be under %s. Current file size is %s.') % (filesizeformat(self.max_upload_size), filesizeformat(data.size)))
        except AttributeError:
            pass

        return data


class ImageUploadForm(forms.Form):
    """Image upload form."""

    # file = forms.ImageField()
    file = RestrictedImageField(max_upload_size=2*1024*1024)
