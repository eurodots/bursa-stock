from rest_framework import serializers
from .models import Subsribers, IcoList
# from recaptcha.fields import ReCaptchaField



class FormSerializer(serializers.ModelSerializer):
    # recaptcha_response = ReCaptchaField(write_only=True)

    class Meta:
        model = Subsribers
        fields = ("user_id", "token_tariff", "token_wallet", "token_domain", "token_category", "token_industry", "token_country", "token_platform",
                  "token_status", "token_name", "token_description", "token_links", "token_ticker", "token_platform", "created_at",
                  "token_address", "script_version", "active")


class UserStocksSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subsribers
        fields = ("id", "token_tariff", "token_wallet", "token_domain", "token_category", "token_industry", "token_country", "token_platform",
                  "token_status", "token_name", "token_description", "token_links", "token_ticker", "token_platform", "created_at",
                  "token_address", "script_version", "active")


class EthTokenSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subsribers
        fields = ("token_name", "token_ticker", "token_links", "token_platform", "token_address")


class IcoListSerializer(serializers.ModelSerializer):

    class Meta:
        model = IcoList
        fields = ('token_ticker', 'token_name', 'token_platform', 'token_address', 'updated_at', 'token_rank')

