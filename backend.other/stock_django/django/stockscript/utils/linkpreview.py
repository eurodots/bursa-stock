from link_preview import link_preview

import re

def convertLinks(text):

    def check(t):
        return re.findall(r'https?://[^\s<>"]+|www\.[^\s<>"]+', str(t))

    if len(check(text)) == 0:
        if '.' in text:
            return 'http://'+text
        else:
            return False

    return text

def linkPreviewMaker(url):
    url = convertLinks(url)
    url = url.lower()
    data = link_preview.generate_dict(url)
    data['request'] = url

    print(data)

    return data


# linkPreviewMaker('https://t.me/bitcoin')
