import json
import requests

# ————————————————————————————————————————————
from stockscript.utils.config import *
# ————————————————————————————————————————————


def verifyToken(token=""):
    # token = "0xd26114cd6ee289accf82350c8d8487fedb8a0c07"

    url = 'https://api.ethplorer.io/getAddressInfo/'+token+'?apiKey=freekey'
    r = requests.get(url, headers=sysConf['header'])
    data = json.loads(r.content.decode('utf8'))


    if 'error' in data:
        msg = data['error']['message']
        print(msg)
        return msg
    else:
        return True


def ethplorerData(token=""):
    url = 'https://api.ethplorer.io/getAddressInfo/'+token+'?apiKey=freekey'
    r = requests.get(url, headers=sysConf['header'])
    data = json.loads(r.content.decode('utf8'))

    if 'error' in data:
        return False


    def tokenInfo(value):
        if data['tokenInfo']:
            return value
        return False

    decimals = tokenInfo(data['tokenInfo']['decimals'])
    price = tokenInfo(data['tokenInfo']['price']['rate'])
    totalSupply = data['tokenInfo']['totalSupply']
    totalSupplyLength = len(totalSupply) - decimals
    totalSupply = totalSupply[0:totalSupplyLength]
    totalSupply = float(totalSupply)
    totalSupplyUsd = totalSupply * float(price)

    meta = {
        'transactions': data['countTxs'] if data['countTxs'] else False,
        'price': price,
        'price_diff': tokenInfo(data['tokenInfo']['price']['diff']),
        'owner': tokenInfo(data['tokenInfo']['owner']),
        'symbol': tokenInfo(data['tokenInfo']['symbol']),
        'decimals': decimals,
        'holders': tokenInfo(data['tokenInfo']['holdersCount']),
        'totalSupply': totalSupply,
        'totalSupplyUsd': totalSupplyUsd
    }
    data['meta'] = meta

    # print(data)
    return data


# ethplorerData('0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0')

def ethplorerHistory(token=""):
    url = 'https://api.ethplorer.io/getPriceHistoryGrouped/' + token + '?apiKey=freekey'
    r = requests.get(url, headers=sysConf['header'])
    data = json.loads(r.content.decode('utf8'))

    if 'error' in data:
        return False

    return data


def verifyWalletEth(value):
    if '0x' == value[0:2].lower() and len(value) == 42:
        print(True)
        return value
    else:
        print(False)
        return False
    # return value

# verifyWalletEth('0x0DE0BcB0703FF8f1AEb8c892EdbE692683Bd8F30')
# ethplorerHistory('0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0')