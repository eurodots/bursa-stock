from DEFAULT.settings import MEDIA_ROOT
import os, subprocess
import PIL
from PIL import Image
import time


def findFavicon(ticker=False):

    media_marketcap = '/media/favicons.marketcap/32x32'
    media_uploaded = '/media/favicons.uploaded/32x32'

    folder_marketcap = MEDIA_ROOT + '/favicons.marketcap/32x32'
    folder_uploaded = MEDIA_ROOT + '/favicons.uploaded/32x32'

    favicon_found = False

    if ticker:

        for f in os.listdir(folder_marketcap):
            f_ext = f.split('.')[-1:][0]
            f_name = f[:-(len(f_ext) + 1)]

            if str(f_name).upper() == str(ticker).upper():
                favicon_found = media_marketcap + '/' + f
                break

            if not favicon_found:
                for f in os.listdir(folder_uploaded):
                    f_ext = f.split('.')[-1:][0]
                    f_name = f[:-(len(f_ext) + 1)]
                    if str(f_name).upper() == str(ticker).upper():
                        favicon_found = media_uploaded + '/' + f
                        break

    return 'http://127.0.0.1:8000'+favicon_found






def resizeImage(path, mywidth=10):
    img = Image.open(path)
    wpercent = (mywidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((mywidth,hsize), PIL.Image.ANTIALIAS)
    img.save(path)



def faviconRenameResize(name=False, ticker=False):
    tmp = MEDIA_ROOT+'/favicons.tmp'
    dst = MEDIA_ROOT+'/favicons.uploaded'

    # create folders
    if not os.path.isdir(tmp):
        os.makedirs(tmp)
    if not os.path.isdir(dst):
        os.makedirs(dst)

    if name and ticker:

        # find file in folder
        for f in os.listdir(tmp):
            f_ext = f.split('.')[-1:][0]
            f_name = f[:-(len(f_ext)+1)]

            if str(f_name).upper() == str(name).upper():

                # create folders
                dst_path_32 = dst + '/32x32'
                dst_path_128 = dst + '/128x128'
                if not os.path.isdir(dst_path_32):
                    os.makedirs(dst_path_32)
                if not os.path.isdir(dst_path_128):
                    os.makedirs(dst_path_128)

                f_name = ticker + '.' + f_ext
                tmp_path = tmp + '/' + f
                dst_path_32 = dst_path_32 + '/' + str(f_name).upper()
                dst_path_128 = dst_path_128 + '/' + str(f_name).upper()


                # copy & resize
                subprocess.Popen("cp" + " " + tmp_path + " " + dst_path_32, shell=True)
                time.sleep(0.5)
                subprocess.Popen("cp" + " " + tmp_path + " " + dst_path_128, shell=True)

                resizeImage(dst_path_32, 32)
                resizeImage(dst_path_128, 128)


                break




# faviconRenameResize('cool', 'VAT')
