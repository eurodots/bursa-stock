from django.shortcuts import render
# from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token

from django_countries import countries
from stockscript.utils.token_verify import verifyToken, verifyWalletEth, ethplorerData, ethplorerHistory


from .forms import IcosignupForm, ImageUploadForm
from .models import Subsribers, IcoList, FaviconUploadModel

from .serializers import FormSerializer, EthTokenSerializer, IcoListSerializer, UserStocksSerializer
from stockscript.dicts.init import *
from stockscript.icolist_update.view_data import icoListArr, coinArr, coinsFaviconsArr
from stockscript.utils.load_webpack import getWebpackBundle

from stockscript.utils.linkpreview import linkPreviewMaker
from stockscript.utils.favicon_finder import findFavicon, faviconRenameResize



def urlToDomain(url):
    spltAr = url.split("://")
    i = (0, 1)[len(spltAr) > 1]
    dm = spltAr[i].split("?")[0].split('/')[0].split(':')[0].lower()
    return dm



def form_view(request):
    test = "Hi Mark"
    form = IcosignupForm(request.POST or None)

    if request.method == 'POST' and form.is_valid():

        token_ticker = request.POST.getlist('token_ticker')[0].upper()

        new_form = form.save()
        return render(request, 'default/success.html', {
            'token_ticker': token_ticker
        }, locals())

    # return render(request, 'reactjs/inner.html', locals())
    return render(request, 'default/form.html', locals())


@api_view(['POST'])
def icosignupFaviconUpload(request):

    def name_modifier(name_original, name_new):
        ext = name_original.split('.')[-1:][0]
        return str(name_new)+'.'+str(ext)


    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['file']
            file_name = name_modifier(file.name, request.POST['name'])
            file.name = file_name

            m = FaviconUploadModel(file=file)
            m.save()

            print(file_name)
            file_src = 'http://127.0.0.1:8000/media/favicons.tmp/'+file_name
            return Response({
                'error': False,
                'message': 'Image uploaded!',
                'file_src': file_src,
                'file_name': file_name
            })
        else:
            msg = {
                "error": True,
                "errors": form.errors,
            }
            return Response(msg)

    return Response({
        'error': True,
        'message': 'Only POST request allowed'
    })


def tokenChecker(token):
    try:
        user_token = Token.objects.get(key=token)
        return user_token.user_id
    except:
        return Response({
            "error": True,
            "message": "Token broken"
        })


@api_view(['GET','POST'])
def userStocksAddViewSet(request):


    if request.method == 'POST':

        data = request.data
        data['user_id'] = tokenChecker(request.data['user_token'])
        data['token_domain'] = urlToDomain(request.data['token_domain'])
        data['token_wallet'] = verifyWalletEth(data['token_wallet'])
        # data['script_version'] = "1.0"

        serializer = FormSerializer(data=data)
        # print('saved!------')
        if serializer.is_valid():

            # return Response({request.data['logotype']})
            token_ticker = request.data['token_ticker']
            token_address = request.data['token_address']
            token_name = request.data['token_name']
            favicon_name = request.data['favicon_name']



            # recaptcha_response = request.data['recaptcha_response']


            # CHECK TOKEN AVAILIBILITY
            token_status = verifyToken(token_address)
            stockWidget = False
            if token_status:
                stockWidget = getWebpackBundle({'stock_name': token_name ,'stock_token': token_address, 'stock_private': token_status})

            serializer.save()
            faviconRenameResize(favicon_name, token_ticker)

            return Response({
                "error": False,
                "message": "Data stored!",
                "widget": stockWidget
            })
        else:
            msg = {
                "error": True,
                "errors": serializer.errors,
            }
            return Response(msg)

    elif request.method == 'GET':

        def choicesTransformer(arr):
            arr_new = []
            for c in arr:
                arr_new.append({
                    'id': c[0],
                    'value': c[1],
                })
            return arr_new

        data = {
            "tariffs": choicesTransformer(CHOICES_TARIFF),
            "categories": choicesTransformer(CHOICES_CATEGORY),
            "industries": choicesTransformer(CHOICES_INDUSTRY),
            "platforms": choicesTransformer(CHOICES_PLATFORM),
            "statuses": choicesTransformer(CHOICES_STATUS),
            "countries": choicesTransformer(countries),
        }
        return Response(data)


@api_view(['POST'])
def userStocksDeleteViewSet(request):
    user_id = tokenChecker(request.data['user_token'])
    stock_id = request.data['stock_id']

    data = Subsribers.objects.filter(user_id=user_id, id=stock_id, active=1)
    data = data.get() if data else False

    if data:
        data.active = 0
        data.save()

        return Response({
            "error": False,
            "message": "Stock was delete!",
        })

    return Response({
        "error": True,
        "message": "Stock not found",
    })


@api_view(['POST'])
def userStocksListViewSet(request):

    user_id = tokenChecker(request.data['user_token'])
    data = Subsribers.objects.filter(user_id=user_id, active=1).order_by('created_at').reverse()
    results = []
    if data:
        serializer = UserStocksSerializer(data, many=True)
        serializerData = serializer.data

        for r in serializerData:
            item = r
            item['favicon'] = findFavicon(r['token_ticker'])
            item['token_platform'] = CHOICES_PLATFORM_DATA[int(item['token_platform'])]
            item['token_status'] = CHOICES_STATUS_DATA[int(item['token_status'])]

            # ethplorer data
            item['ethplorer'] = ethplorerData(item['token_address'])

            # item['token_country'] = [cou[1] for cou in countries if cou[0] == item['token_country']][0]

            # categories
            # categories_arr = []
            # for cat in json.loads(item['token_category']):
            #     category = CHOICES_CATEGORY_DATA[int(cat)]
            #     categories_arr.append(category)
            # item['token_category'] = categories_arr

            # industries
            # industry_arr = []
            # for industry in json.loads(item['token_industry']):
            #     industry = CHOICES_INDUSTRY_DATA[int(industry)]
            #     industry_arr.append(industry)
            # item['token_industry'] = industry_arr

            results.append(item)

    return Response({
        "error": False,
        "results": results,
    })


@api_view(['GET'])
def ethTokenViewSet(request, address):
    # data = Subsribers.objects.all()

    system_data = False
    favicon = False

    ethplorer_data = ethplorerData(address)

    data = Subsribers.objects.filter(token_address=address)
    if data:
        serializer = EthTokenSerializer(data, many=True)
        system_data = serializer.data[0]
        token_ticker = serializer.data[0].get('token_ticker')
        favicon = findFavicon(token_ticker)
    else:
        data = IcoList.objects.filter(token_address=address)
        if data:
            serializer = IcoListSerializer(data, many=True)
            system_data = serializer.data[0]

            if 'tokenInfo' in ethplorer_data:
                favicon = findFavicon(ethplorer_data['tokenInfo']['symbol'])


    return Response({
        "system": system_data,
        "favicon": favicon,
        "ethplorer": ethplorer_data,
    })

@api_view(['GET'])
def ethTokenHistoryViewSet(request, address):

    ethplorer_history = ethplorerHistory(address)

    return Response({
        "error": False,
        "results": ethplorer_history,
    })

@api_view(['GET'])
def coinsFaviconsViewSet(request):
    return Response(coinsFaviconsArr())


@api_view(['GET'])
def coinsListViewSet(request):
    return Response(icoListArr())

@api_view(['GET'])
def coinViewSet(request, coin):
    return Response(coinArr(coin))


@api_view(['POST'])
def linkPreviewSet(request):

    # charset = 'utf-8'

    link = request.data['link']
    link_data = linkPreviewMaker(link)

    return Response(link_data)


# class platformViewSet(viewsets.ReadOnlyModelViewSet):
#     queryset = Subsribers.objects.all()
#     serializer_class = FormSerializer