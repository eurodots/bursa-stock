# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve

admin.autodiscover()

urlpatterns = [

    # PAGES
    url(r'^api/', include('stockscript.urls')),
    url(r'^api/pages/', include('pages.urls')),

    # AUTH
    url(r'^api/accounts/', include('accounts.api.urls')),
    url(r'^api/teams/', include('teams.api.urls')),

    # SUPPORT
    url(r'^api/support/', include('support.urls')),


    # CMS FOR ALL OTHER'S
    path('admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),


    # OTHER URLS FOR WEBPACK
    url(r'^', include('webpack.urls')),


]




# This is only needed when using runserver.
# if settings.DEBUG:
urlpatterns = [
    url(r'^media/(?P<path>.*)$', serve,
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'^static/(?P<path>.*)$', serve,
        {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
    ] + staticfiles_urlpatterns() + urlpatterns
