import os
from django.conf import settings


ALLOWED_HOSTS = ['127.0.0.1']
# ALLOWED_HOSTS = ['bursadex.com']

# CKEDITOR_BASEPATH = '/static/'
CKEDITOR_UPLOAD_PATH = os.path.join(settings.BASE_DIR, 'static')
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            ['RemoveFormat', 'Source']
        ]
    }
}


# CORS
CORS_ORIGIN_ALLOW_ALL = True
# CORS_ORIGIN_WHITELIST = (
#     'localhost',
# )
CORS_ALLOW_CREDENTIALS = True
CORS_REPLACE_HTTPS_REFERER = True
USE_ETAGS = True
# CORS_ALLOW_CREDENTIALS = True
#
# CORS_ALLOW_HEADERS = (
#     'accept',
#     'accept-encoding',
#     'Authorization',
#     'content-type',
#     'dnt',
#     'origin',
#     'user-agent',
#     'x-csrftoken',
#     'x-requested-with',
# )
# CORS_ORIGIN_WHITELIST = (
#     # 'read.only.com',
#     'localhost:3000',
# )
#
# CSRF_TRUSTED_ORIGINS = (
#     # 'change.allowed.com',
#     'localhost:3000',
# )


# This is for local development
WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'static/bundles/stock/',
        'STATS_FILE': os.path.join(settings.BASE_DIR, 'static/bundles/stock/webpack-stats.json')
    },
    'LANDING': {
        'BUNDLE_DIR_NAME': 'static/bundles/landing/',
        'STATS_FILE': os.path.join(settings.BASE_DIR, 'static/bundles/landing/webpack-stats.json')
    }
}



# REST API AUTH
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'markolofsen@gmail.com'
EMAIL_HOST_PASSWORD = 'ygqW3h6Z?#xKVG'
DEFAULT_FROM_EMAIL = "markolofsen@gmail.com"
EMAIL_PORT = 587

VERIFICATION_KEY_EXPIRY_DAYS = 2

SITE_NAME = "Default name"
