import threading
from multiprocessing import Process
import time
import datetime
# ————————————————————————————————————————————
from models import PairsModel, StocksModel, Global as GlobalSettings
# ————————————————————————————————————————————
from _utils.functions import *
from _utils.config import *
# ————————————————————————————————————————————
from Stocks.Qryptos.index import getPairsQryptosIds
from Coinmarketcap.index import parsCoinmarketcap
from saveOrders import filesToDb
from ForceMode import ForceMode
# ————————————————————————————————————————————
# CONFIG
GLOBAL_PAIRS = []
GLOBAL_STOCK_AMOUNT = 0 # 0 — Parse all within blacklist
# ————————————————————————————————————————————

def threadsLimit(stock_name):

    if stock_name in ['Binance', 'Liqui', 'Mercatox']:
        return 20
    else:
        return 15


def parsOrdersByPairs():

    getSimilarPairs()
    modelStocks = StocksModel.select().where(StocksModel.active==1)


    def threadStocks(stock):

        stock_ident = 'STOCK-'+str(stock.id)
        if stock_ident in GLOBAL_PAIRS:
            _parser = __import__('Stocks.' + stock.name + '.index', globals(), locals(), [''], 0)
            pairs_split = split(GLOBAL_PAIRS[stock_ident], threadsLimit(stock.name))
            print(GLOBAL_PAIRS)

            for pair in pairs_split:
                # threading.Thread(target=_parser.getOrders, args=(stock.id, pair,)).start()
                Process(target=_parser.getOrders, args=(stock.id, pair,)).start()
                # _parser.getOrders(stock.id, pair)



    for stock in modelStocks:
        # threading.Thread(target=threadStocks, args=(stock,)).start()
        Process(target=threadStocks, args=(stock,)).start()



def parsPairs():

    def loadNewPairs(stock_pairs, stock_name):
        for pair in stock_pairs:
            if len(pair[0]) > 0 and len(pair[1]) > 0:
                pair_label = '_'.join(pair).upper().strip()
                insertNewPairs(pair, pair_label, stock_name)


    modelStocks = StocksModel.select()
    for stock in modelStocks:

        _parser = __import__('Stocks.'+stock.name+'.index', globals(), locals(), [''], 0).getPairs()
        loadNewPairs(_parser, stock.name)


def insertNewPairs(pair, pair_label, stock_name):
    modelStocks = StocksModel.select().where(StocksModel.name==stock_name).get()
    stock_id = modelStocks.id
    print('stock_id',stock_id)
    modelOrders = PairsModel.select().where(PairsModel.pair==pair_label, PairsModel.stock==stock_id)
    if modelOrders:
        # print( modelOrders.get().coin_from )
        d('Exist')
    else:
        model = PairsModel()
        model.stock = StocksModel.select().where(StocksModel.id==stock_id).get()
        model.stock_name = stock_name
        model.coin_from = pair[0].strip()
        model.coin_to = pair[1].strip()
        model.pair = pair_label
        model.arr_asks = ""
        model.arr_bids = ""
        model.bestprice_ask = 0
        model.bestprice_bid = 0
        model.updated_at = datetime.datetime.now()
        model.save()

        d('Created')

    print(pair_label)





def getSimilarPairs():

    global GLOBAL_PAIRS

    modelOrders = PairsModel.select()
    modelStocks = StocksModel.select().where(StocksModel.active==1)


    blacklist_arr = []
    prohibit_coins = []


    for s in modelStocks:
        blacklist = s.blacklist.split('\r\n') + s.holdlist.split('\r\n')
        for coin in blacklist:
            coin = coin.split('#', 1)[0].strip()
            if len(coin) > 0:
                prohibit_coins.append(coin)


    # Append global blacklist
    globalCoins = GlobalSettings.select().where(GlobalSettings.name == 'blacklist').get()
    globalCoins_arr = globalCoins.value.split(',')
    globalCoins_arr = [c.split('#',1)[0].strip() for c in globalCoins_arr]

    blacklist_arr = set(prohibit_coins + globalCoins_arr)

    pairs_tmp = {}
    for row in modelOrders:

        if not row.pair in pairs_tmp:
            pairs_tmp[row.pair] = []

        if not row.coin_from in blacklist_arr and not row.coin_to in blacklist_arr:
            if not row.stock.id in blacklist_arr:
                pairs_tmp[row.pair].append({
                    'pair': row.pair,
                    'stock_id': row.stock.id
                })


    # GET PAIRS WHICH EXIST AT MORE THEN [GLOBAL_STOCK_AMOUNT] STOCKS
    pairs_arr = {}
    for row in pairs_tmp:
        if len(pairs_tmp[row]) >= GLOBAL_STOCK_AMOUNT:
            for p in pairs_tmp[row]:
                stock_id = 'STOCK-'+str(p['stock_id'])
                if not stock_id in pairs_arr:
                    pairs_arr[stock_id] = []
                pairs_arr[stock_id].append(p['pair'].split('_'))

        GLOBAL_PAIRS = pairs_arr


    #HACK FOR QRYPTOS
    if 'STOCK-17' in GLOBAL_PAIRS:
        hack_qryptos_arr = []
        for pair in GLOBAL_PAIRS['STOCK-17']:
            for hack in getPairsQryptosIds():
                if pair[0] == hack[0] and pair[1] == hack[1]:
                    hack_qryptos_arr.append(hack)
                    break

        GLOBAL_PAIRS['STOCK-17'] = hack_qryptos_arr



def stopAllScripts():
    time.sleep(60*10)
    exit()


if __name__ == "__main__":


    # parsPairs()
    threading.Thread(target=parsCoinmarketcap, args=()).start()
    threading.Thread(target=parsOrdersByPairs, args=()).start()
    threading.Thread(target=filesToDb, args=()).start()
    threading.Thread(target=ForceMode, args=()).start()
    # threading.Thread(target=stopAllScripts, args=()).start()

    # parsOrdersByPairs()

    pass
