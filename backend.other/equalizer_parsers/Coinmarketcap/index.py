import requests
import json
import time
import threading
import datetime
import copy
# ————————————————————————————————————————————
from models import CoinmarketcapModel
# ————————————————————————————————————————————
from _utils.functions import *
from _utils.config import *
# ————————————————————————————————————————————



def parsCoinmarketcap():

    while True:
    # if True:

        url = 'https://api.coinmarketcap.com/v1/ticker/?limit=0&convert=ETH'

        r = requests.get(url)
        data = json.loads(r.content.decode('utf8'))
        data_arr = {}
        for d in data:
            data_arr[d['id']] = d


        # Additional currencies
        urls = {
            'price_cny': 'https://api.coinmarketcap.com/v1/ticker/?limit=0&convert=CNY',
            'price_rub': 'https://api.coinmarketcap.com/v1/ticker/?limit=0&convert=RUB',
        }

        for url in urls:
            r = requests.get(urls[url])
            data = json.loads(r.content.decode('utf8'))

            for d in data:
                data_arr[d['id']][url] = d[url]


        XRB = copy.deepcopy(data_arr['nano'])
        XRB['symbol'] = 'XRB'
        XRB['name'] = 'Nano / XRB'
        data_arr['xrb'] = XRB

        saveData(data_arr)


        # Threading...
        # data_ = [data_arr[d] for d in ]
        # arr_split = split(data_, 20)
        # for arr in arr_split:
        #     threading.Thread(target=saveData, args=(arr,)).start()

        d('COINMARKETCAP IS DONE!')
        time.sleep(60 * 10)


def manipulator(model, item):

    def checkIfNone(value):
        if not value is None:
            return float(value)
        else:
            return 0


    # Continue....
    model.ticker = item['symbol']
    model.name = item['name']
    model.rank = item['rank']
    model.percent_change_1h = checkIfNone(item['percent_change_1h'])
    model.percent_change_24h = checkIfNone(item['percent_change_24h'])
    model.percent_change_7d = checkIfNone(item['percent_change_7d'])
    model.price_usd = checkIfNone(item['price_usd'])
    model.price_btc = checkIfNone(item['price_btc'])
    model.price_eth = checkIfNone(item['price_eth'])
    model.price_cny = checkIfNone(item['price_cny'])
    model.price_rub = checkIfNone(item['price_rub'])
    model.updated_at = datetime.datetime.now()
    model.save()




def saveData(items):

    for item in items:

        try:
            ticker = items[item]['symbol']
            model = CoinmarketcapModel.select().where(CoinmarketcapModel.ticker == ticker)
            if model:
                d('Coinmarket: update ' + ticker)
                manipulator(model.get(), items[item])

            else:
                d('Coinmarket: add ' + ticker)
                manipulator(CoinmarketcapModel(), items[item])

        except Exception as e:

            print('>>>ERROR:', e)
            exit(items[item])



if __name__ == "__main__":
    parsCoinmarketcap()
    pass
