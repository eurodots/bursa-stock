import os
import time
import json
from os import listdir
import datetime

# ————————————————————————————————————————————
from models import PairsModel
# from _utils.functions import *
# ————————————————————————————————————————————
DATA_DIR = os.path.dirname(os.path.realpath(__file__))


# def saveOrdersToJson(filename, data_arr=False):
#
#
#     # folder_stock = '_data/orders/'+data_arr['stock_name']
#     # file_pair = data_arr['pair_label']+'.json'
#     #
#     # if not os.path.exists(folder_stock):
#     #     os.makedirs(folder_stock)
#     #
#     # path = os.path.join(folder_stock, file_pair)
#     # with open(path, 'w') as outfile:
#     #     json.dump(data_arr, outfile)
#     #
#     # print(path)
#
#     print(filename)
#     exit()


def saveOrdersToDb(arr):

    arr_asks = [[float(order[0]), float(order[1])] for order in arr['orders']['asks']]
    arr_asks = sorted(arr_asks, key=lambda k: k[0], reverse=False)

    arr_bids = [[float(order[0]), float(order[1])] for order in arr['orders']['bids']]
    arr_bids = sorted(arr_bids, key=lambda k: k[0], reverse=True)

    stock_id = arr['stock_id']
    pair_label = '_'.join(arr['pair'])
    filename = str(stock_id) + '.' + pair_label + '.json'

    filepath = os.path.join(DATA_DIR, '_data/', filename)
    print(filepath)

    data_arr = {
        'asks': arr_asks,
        'bids': arr_bids,
    }

    with open(filepath, 'w') as outfile:
        json.dump(data_arr, outfile)

    print('SAVED: ',filepath)



def filesToDb():

    while True:
        time.sleep(3)
        folder = os.path.join(DATA_DIR, '_data/')
        files_arr = listdir(folder)
        if len(files_arr) > 0:
            for f in files_arr:
                try:

                    file = f.split('.')
                    filepath = os.path.join(folder, f)
                    data = json.load(open(filepath))
                    os.unlink(filepath)

                    if data:

                        modelOrders = PairsModel.get(PairsModel.pair==file[1], PairsModel.stock==file[0])
                        modelOrders.arr_asks = data['asks']
                        modelOrders.arr_bids = data['bids']
                        modelOrders.bestprice_ask = data['asks'][0][0] if data['asks'][0] else 0
                        modelOrders.bestprice_bid = data['bids'][0][0] if data['bids'][0] else 0

                        modelOrders.arr_asks_count = len(data['asks'])
                        modelOrders.arr_bids_count = len(data['bids'])
                        modelOrders.updated_at = datetime.datetime.now()

                        modelOrders.save()

                        print('ADDED TO DB: ', f)
                except:
                    print('ORDERS TO DB ERROR!!!')


if __name__ == "__main__":
    filesToDb()
    pass
