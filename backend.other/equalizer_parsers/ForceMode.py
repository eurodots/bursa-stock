import threading
from multiprocessing import Process
import datetime
import pytz
# ————————————————————————————————————————————
from models import PairsForcemodeModel
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————
GLOBAL_FETCH_MINUTES = 5
# ————————————————————————————————————————————



def ForceMode():

    while True:

        date = datetime.datetime.now(pytz.utc) - datetime.timedelta(minutes=GLOBAL_FETCH_MINUTES)
        modelForcePairs = PairsForcemodeModel.select().where(PairsForcemodeModel.created_at > date)

        pairs_arr = {}
        for pair in modelForcePairs:

            stock_name = pair.pair.stock.name
            stock_id = pair.pair.stock.id

            if not stock_name in pairs_arr:
                pairs_arr[stock_name] = []
                pairs_arr[stock_name] = {
                    'stock_id': stock_id,
                    'pairs': [],
                }

            pairs_arr[stock_name]['pairs'].append([pair.pair.coin_from, pair.pair.coin_to])


        for stock_name in pairs_arr.keys():

            _parser = __import__('Stocks.' + stock_name + '.index', globals(), locals(), [''], 0)
            pairs_split = split(pairs_arr[stock_name]['pairs'], 3)

            stock_id = pairs_arr[stock_name]['stock_id']

            for pair in pairs_split:
                Process(target=_parser.getOrders, args=(stock_id, pair, False, )).start()



if __name__ == "__main__":
    ForceMode()

    pass
