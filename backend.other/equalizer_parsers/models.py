from peewee import *
from functools import partial
from _utils.config import *

# Connect to a MySQL database on network.
mysql_db = MySQLDatabase(DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST, port=DB_PORT, charset='utf8mb4')





class BaseModel(Model):
    class Meta:
        database = mysql_db


MoneyField = partial(DecimalField, decimal_places=18)


class Global(BaseModel):
    id = AutoField()
    name = CharField()
    value = TextField()

    class Meta:
        table_name = 'settings_global'


class CoinmarketcapModel(BaseModel):
    id = AutoField()
    ticker = CharField()
    name = CharField()
    rank = IntegerField()
    percent_change_1h = MoneyField()
    percent_change_24h = MoneyField()
    percent_change_7d = MoneyField()
    price_usd = MoneyField()
    price_btc = MoneyField()
    price_eth = MoneyField()
    price_cny = MoneyField()
    price_rub = MoneyField()

    updated_at = DateTimeField()

    class Meta:
        table_name = 'market_coinmarketcap'



class StocksModel(BaseModel):
    id = AutoField()
    active = IntegerField()
    name = CharField()
    website = CharField()
    blacklist = CharField()
    holdlist = CharField()
    fastlink = CharField()


    class Meta:
        table_name = 'market_stocks'



class PairsModel(BaseModel):
    id = AutoField()
    stock = ForeignKeyField(StocksModel, on_delete='CASCADE')
    stock_name = CharField()
    coin_from = CharField()
    coin_to = CharField()
    pair = CharField()
    arr_asks = CharField()
    arr_bids = CharField()
    arr_asks_count = CharField()
    arr_bids_count = CharField()
    bestprice_ask = MoneyField()
    bestprice_bid = MoneyField()
    updated_at = DateTimeField()

    class Meta:
        table_name = 'market_pairs'


class PairsForcemodeModel(BaseModel):
    pair = ForeignKeyField(PairsModel, on_delete='CASCADE')
    created_at = DateTimeField()

    class Meta:
        table_name = 'market_pairs_forcemode'