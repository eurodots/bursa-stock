import time

import requests
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————


def getPairs():
    try:
        url = 'https://poloniex.com/public?command=returnTicker'
        r = requests.get(url)
        data = json.loads(r.content.decode('utf8'))
        data = data
        allPairs = []

        for i, pair in data.items():
            # print(i)
            label = i.upper()
            label = label.split('_')

            allPairs.append([label[1], label[0]])

        return allPairs
    except Exception as e:
        print('>>>ERROR:', e)
        return []


def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    # 6 calls per second to the public API, or repeatedly and needlessly fetching excessive amounts of data, can result in your IP being banned.

    try:
        url = 'https://poloniex.com/public?command=returnOrderBook&currencyPair=all&depth=20'
        print(url)

        while repeat:

            time.sleep(5)
            
            r = requests.get(url)
            data = json.loads(r.content.decode('utf8'))
            data = data

            for pair in pairs:
                tradeInfo = data['_'.join(pair)]

                print(pair)

                itemAsks = tradeInfo['asks']
                itemBids = tradeInfo['bids']


                orders_arr = {
                    'asks': [],
                    'bids': []
                }

                for order in itemAsks:
                    orders_arr['asks'].append([order[0], order[1]])

                for order in itemBids:
                    orders_arr['bids'].append([order[0], order[1]])

                response_arr = {
                    'stock_id': stock_id,
                    'pair': pair,
                    'orders': orders_arr,
                }

                saveOrdersToDb(response_arr)



    except Exception as e:
        # print('exept err')
        print('>>>ERROR:', e)
        # print('exept err')








if __name__ == "__main__":
    pass
