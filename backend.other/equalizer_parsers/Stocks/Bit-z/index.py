import requests
import time
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————

def getPairs():
    try:
        url = 'https://www.bit-z.com/api_v1/tickerall'
        r = requests.get(url)
        data = json.loads(r.content.decode('utf8'))
        data = data['data']
        allPairs = []

        for item, pair in data.items():
            label = item.upper()
            label = label.split('_')

            allPairs.append([label[0], label[1]])

        return allPairs
    except Exception as e:
        print('>>>ERROR:', e)
        return []


def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(5)
        for pair in pairs:

            pair_labels = '_'.join(pair).lower()


            url = 'https://www.bit-z.com/api_v1/depth?coin=' + pair_labels
            print(url)

            try:
                r = requests.get(url)
                data = json.loads(r.content.decode('utf8'))
                data = data['data']
                # print(data)

                orders_arr = {
                    'asks': [],
                    'bids': []
                }

                for order in data['asks']:
                    orders_arr['asks'].append([order[0], order[1]])


                for order in data['bids']:
                    orders_arr['bids'].append([order[0], order[1]])

                response_arr = {
                    'stock_id': stock_id,
                    'pair': pair,
                    'orders': orders_arr,
                }

                saveOrdersToDb(response_arr)

                # print(response_arr)
            except Exception as e:
                print('>>>ERROR:', e)



if __name__ == "__main__":
    pass
