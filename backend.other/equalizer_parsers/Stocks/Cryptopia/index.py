import time

import requests
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————


def getPairs():
    url = 'https://www.cryptopia.co.nz/api/GetTradePairs'
    r = requests.get(url)
    data = json.loads(r.content.decode('utf8'))

    allPairs = []

    for pair in data['Data']:

        label = pair['Label']
        label = label.split('/')

        allPairs.append([label[0], label[1]])

    return allPairs



def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(5)

        for pair in pairs:

            url = 'https://www.cryptopia.co.nz/api/GetMarketOrderGroups/' + (pair[0]+'_'+pair[1])
            print(url)

            r = requests.get(url)
            data = json.loads(r.content.decode('utf8'))

            if len(data['Data']) > 0:
                data = data['Data'][0]

                orders_arr = {
                    'asks': [],
                    'bids': []
                }

                for order in data['Sell']:
                    orders_arr['asks'].append([order['Price'], order['Volume']])

                for order in data['Buy']:
                    orders_arr['bids'].append([order['Price'], order['Volume']])


                response_arr = {
                    'stock_id': stock_id,
                    'pair': pair,
                    'orders': orders_arr,
                }

                saveOrdersToDb(response_arr)


if __name__ == "__main__":
    pass
