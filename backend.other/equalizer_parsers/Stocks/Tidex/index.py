import requests
import time
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————


def getPairs():
    url = 'https://api.tidex.com/api/3/info'
    r = requests.get(url)
    data = json.loads(r.content.decode('utf8'))

    allPairs = []

    for pair in data['pairs']:
        # print(pair)
        label = pair
        label = label.split('_')

        allPairs.append([label[0], label[1]])

    return allPairs



def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(5)

        for pair in pairs:

            pair_label = '_'.join(pair).lower()
            url = 'https://api.tidex.com/api/3/depth/' + pair_label
            print(url)

            r = requests.get(url)
            data = json.loads(r.content.decode('utf8'))
            data = data[pair_label]
            # print(data)


            orders_arr = {
                'asks': [],
                'bids': []
            }

            for order in data['asks']:
                orders_arr['asks'].append([order[0], order[1]])


            for order in data['bids']:
                orders_arr['bids'].append([order[0], order[1]])


            response_arr = {
                'stock_id': stock_id,
                'pair': pair,
                'orders': orders_arr,
            }

            saveOrdersToDb(response_arr)

            # print(response_arr)


if __name__ == "__main__":
    pass
