import time
import json
import requests
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————


def getPairsQryptosIds():
    try:
        url = 'https://api.quoine.com/products'
        r = requests.get(url)
        data = json.loads(r.content.decode('utf8'))

        allPairs = []
        dictionary = []
        for pair in data:
            allPairs.append([pair['base_currency'], pair['quoted_currency'], pair['id']])

        return allPairs
    except Exception as e:
        print('>>>ERROR:', e)
        return []


def getPairs():
    try:
        url = 'https://api.quoine.com/products'
        r = requests.get(url)
        data = json.loads(r.content.decode('utf8'))

        allPairs = []
        for pair in data:
            allPairs.append([pair['base_currency'], pair['quoted_currency']])

        return allPairs
    except Exception as e:
        print('>>>ERROR:', e)
        return []



def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    # https://api.quoine.com/products/50/price_levels

    while repeat:
        time.sleep(5)

        for pair in pairs:

            try:
                url = 'https://api.quoine.com/products/' + pair[2] + '/price_levels'
                print(url)

                r = requests.get(url)
                data = json.loads(r.content.decode('utf8'))

                # print(data)

                orders_arr = {
                    'asks': [],
                    'bids': []
                }

                for order in data['sell_price_levels']:
                    orders_arr['asks'].append([order[0], order[1]])

                for order in data['buy_price_levels']:
                    orders_arr['bids'].append([order[0], order[1]])

                response_arr = {
                    'stock_id': stock_id,
                    'pair': [pair[0],pair[1]],
                    'orders': orders_arr,
                }

                saveOrdersToDb(response_arr)

                # print(response_arr)

            except Exception as e:
                print('>>>ERROR:', e)



if __name__ == "__main__":

    print( getPairsQryptosIds() )

    pass
