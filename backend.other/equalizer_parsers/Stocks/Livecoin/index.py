import requests
import time
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————



def getPairs():
    url = 'https://api.livecoin.net/exchange/ticker'
    r = requests.get(url)
    data = json.loads(r.content.decode('utf8'))
    print(url)

    allPairs = []

    for pair in data:
        label = pair['symbol']
        label = label.split('/')

        allPairs.append([label[0], label[1]])

    return allPairs



def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(5)
        for pair in pairs:

            url = 'https://api.livecoin.net/exchange/order_book?currencyPair=' + '/'.join(pair)
            print(url)

            r = requests.get(url)
            data = json.loads(r.content.decode('utf8'))
            data = data
            # data = data['data']


            orders_arr = {
                'asks': [],
                'bids': []
            }

            for order in data['asks']:
                orders_arr['asks'].append([order[0], order[1]])


            for order in data['bids']:
                orders_arr['bids'].append([order[0], order[1]])


            response_arr = {
                'stock_id': stock_id,
                'pair': pair,
                'orders': orders_arr,
            }

            saveOrdersToDb(response_arr)
            # time.sleep(1)


if __name__ == "__main__":
    pass
