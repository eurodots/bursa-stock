import requests
import time
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————


def getPairs():
    try:
        url = 'https://api.bitfinex.com/v1/symbols'
        r = requests.get(url)
        data = json.loads(r.content.decode('utf8'))

        allPairs = []

        for pair in data:
            allPairs.append([pair[:3].upper(), pair[3:].upper()])

        return allPairs
    except Exception as e:
        print('>>>ERROR:', e)
        return []



def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(10)
        for pair in pairs:

        # https://api.bitfinex.com/v1/book/btcusd
        # Ratelimit: 60 req/min
            try:
                pair_label = ''.join(pair).lower()
                url = 'https://api.bitfinex.com/v1/book/' + pair_label
                print(url)

                r = requests.get(url)
                data = json.loads(r.content.decode('utf8'))
                data = data
                # print(data['asks'])

                orders_arr = {
                    'asks': [],
                    'bids': []
                }
                for order in data['asks']:
                    orders_arr['asks'].append([order['price'], order['amount']])

                for order in data['bids']:
                    orders_arr['bids'].append([order['price'], order['amount']])

                response_arr = {
                    'stock_id': stock_id,
                    'pair': pair,
                    'orders': orders_arr,
                }

                saveOrdersToDb(response_arr)


            except Exception as e:
                # print('exept err')
                print('>>>ERROR:', e)
                # print('exept err')



if __name__ == "__main__":
    pass
