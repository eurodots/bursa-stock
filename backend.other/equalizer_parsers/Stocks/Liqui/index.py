import requests
import time

# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————


def getPairs():
    url = 'https://api.liqui.io/api/3/info'
    r = requests.get(url)
    data = json.loads(r.content.decode('utf8'))

    allPairs = []

    remove_arr = []
    for pair, info in data['pairs'].items():

        if info['hidden'] == 0: #Remove pairs marked in json like hidden=1
            label = pair.upper()
            label = label.split('_')
            allPairs.append([label[0], label[1]])
        else:
            remove_arr.append(pair.upper())


    print(remove_arr)

    return allPairs


def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(3)
        for pair in pairs:

            try:
                # url = 'https://api.liqui.io/api/3/depth/'
                sufix = '_'.join(pair)

                url = 'https://api.liqui.io/api/3/depth/' + sufix.lower()
                print(url)

                r = requests.get(url)
                data = json.loads(r.content.decode('utf8'))

                if not 'error' in data:

                    for key in data:
                        if data[key] and 'asks' in data[key] and 'bids' in data[key]:
                            orders_sell = data[key]['asks']
                            orders_buy = data[key]['bids']


                            orders_arr = {
                                'asks': [],
                                'bids': []
                            }

                            for order in orders_sell:
                                orders_arr['asks'].append([format(order[0], '.8f'), order[1]])

                            for order in orders_buy:
                                orders_arr['bids'].append([format(order[0], '.8f'), order[1]])


                            response_arr = {
                                'stock_id': stock_id,
                                'pair': pair,
                                'orders': orders_arr,
                            }

                            saveOrdersToDb(response_arr)
                            # print(response_arr)


            except Exception as e:
                print('>>>ERROR:', e)



if __name__ == "__main__":
    # getOrders(6, [['ENG','USDT']])
    getPairs()
    pass
