import ssl
import websocket
import time
import json
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————



def getPairs():
    ws = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
    ws.connect('wss://stream.binance.com:9443/ws/!ticker@arr')

    msg = json.loads(ws.recv())

    tickers = []
    for pair in msg:
        coin = pair['s']
        if coin[-4:] == 'USDT':
            tickers.append([coin[0:-4], coin[-4:]])
        else:
            tickers.append([coin[0:-3], coin[-3:]])


    return tickers


def getOrders(stock_id, pairs):
    from saveOrders import saveOrdersToDb

    for pair in pairs:

        ws_pair = pair[0].lower()+pair[1].lower()
        ws = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE, "check_hostname": False})
        socket_url = 'wss://stream.binance.com:9443/ws/'+ws_pair+'@depth20'
        ws.connect(socket_url)

        # while True:
        if True:
            time.sleep(2)
            print(socket_url)
            response_arr = []

            response = json.loads(ws.recv())

            orders_arr = {
                'asks': [],
                'bids': []
            }
            for resp in response:

                if resp in ['asks','bids']:
                    orders = response[resp]

                    for order in orders:
                        order_tmp = [order[0], order[1]]
                        orders_arr[resp].append(order_tmp)

            response_arr = {
                'stock_id': stock_id,
                'pair': pair,
                'orders': orders_arr,
            }

            saveOrdersToDb(response_arr)
            # print(response_arr)


# connectSocket(1, [['SYS','BTC']])


if __name__ == "__main__":
    pass
