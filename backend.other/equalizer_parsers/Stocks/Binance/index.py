import requests
import time
import json
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————



def getPairs():
    try:
        url = 'https://api.binance.com/api/v1/exchangeInfo'
        r = requests.get(url)
        data = json.loads(r.content.decode('utf8'))

        allPairs = []

        for pair in data['symbols']:
            allPairs.append([pair['baseAsset'], pair['quoteAsset']])

        return allPairs
    except Exception as e:
        print('>>>ERROR:', e)
        return []


def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(5)
        for pair in pairs:

            try:
                pair_label = ''.join(pair)
                url = 'https://api.binance.com/api/v1/depth?symbol='+pair_label
                print(url)

                r = requests.get(url)
                data = json.loads(r.content.decode('utf8'))
                data = data

                orders_arr = {
                    'asks': [],
                    'bids': []
                }
                for order in data['asks']:
                    orders_arr['asks'].append([order[0], order[1]])

                for order in data['bids']:
                    orders_arr['bids'].append([order[0], order[1]])

                response_arr = {
                    'stock_id': stock_id,
                    'pair': pair,
                    'orders': orders_arr,
                }

                saveOrdersToDb(response_arr)
                # print(response_arr)

            except:
                print('Binance error')


if __name__ == "__main__":
    # print( getOrders(2, [['ETH','BTC']]) )
    pass
