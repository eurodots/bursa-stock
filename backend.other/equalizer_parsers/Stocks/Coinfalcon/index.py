import time

import cfscrape
import requests
from bs4 import BeautifulSoup

# ————————————————————————————————————————————
from _utils.functions import *
from _utils.config import *
# ————————————————————————————————————————————



def getPairs():
    try:
        url = 'https://coinmarketcap.com/exchanges/coinfalcon/'
        print(url)
        # time.sleep(.5)
        scraper = cfscrape.create_scraper()
        site = scraper.get(url, headers=sysConf['header']).content
        soup = BeautifulSoup(site, "lxml")

        tables = soup.find('tbody').find_all('a')
        allPairs = []
        tables = tables[::1]
        for table in tables:
            storeValueRows = table.string
            if '/' in storeValueRows:
                coin = []
                coin = storeValueRows.split('/')
                allPairs.append([coin[0], coin[1]])
                # print(storeValueRows

        return allPairs
    except Exception as e:
        print('>>>ERROR:', e)
        return []



def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    # We throttle public endpoints by IP: 3 requests per second.

    while repeat:
        time.sleep(5)
        for pair in pairs:

            pair_label = '-'.join(pair)


            url = 'https://coinfalcon.com/api/v1/markets/' + pair_label + '/orders?level=2'
            print(url)
            try:
                time.sleep(1)
                r = requests.get(url, headers=sysConf['header'])
                data = json.loads(r.content.decode('utf8'))
                data = data['data']
                # print(data)


                orders_arr = {
                    'asks': [],
                    'bids': []
                }

                for order in data['asks']:
                    orders_arr['asks'].append([order['price'], order['size']])


                for order in data['bids']:
                    orders_arr['bids'].append([order['price'], order['size']])

                response_arr = {
                    'stock_id': stock_id,
                    'pair': pair,
                    'orders': orders_arr,
                }

                saveOrdersToDb(response_arr)


                # print(response_arr)
            except Exception as e:
                print('>>>ERROR:', e)



if __name__ == "__main__":
    pass
