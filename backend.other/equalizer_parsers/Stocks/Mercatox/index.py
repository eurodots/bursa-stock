import cfscrape
import requests
from bs4 import BeautifulSoup
from lxml import html
import time

# ————————————————————————————————————————————
from _utils.functions import *
from _utils.config import *
# ————————————————————————————————————————————

def getPairs():

    url = 'https://mercatox.com/public/json24'
    r = requests.get(url, headers=sysConf['header'])
    data = json.loads(r.content.decode('utf8'))

    allPairs = []
    for item in data['pairs']:
        coin = []
        coin = item.split('_')

        allPairs.append([coin[0], coin[1]])

    return allPairs




def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(1)
        for pair in pairs:

            url = 'https://mercatox.com/exchange/'+'/'.join(pair)
            print(url)

            try:

                scraper = cfscrape.create_scraper()
                site = scraper.get(url, headers=sysConf['header']).content
                soup = BeautifulSoup(site, "lxml")

                orders_arr = {
                    'asks': [],
                    'bids': []
                }

                orders_sell = soup.find('div', {'class': 'left_order_book_page'}).find_all('div', {'class': 'row_fix'})
                for item in orders_sell:
                    orders_arr['asks'].append([item.get('price'), item.get('amount')])

                orders_buy = soup.find('div', {'class': 'right_order_book_page'}).find_all('div', {'class': 'row_fix'})
                for item in orders_buy:
                    orders_arr['bids'].append([item.get('price'), item.get('amount')])

                response_arr = {
                    'stock_id': stock_id,
                    'pair': pair,
                    'orders': orders_arr,
                }

                saveOrdersToDb(response_arr)

            except Exception as e:
                print('>>>ERROR:', e)


if __name__ == "__main__":
    pass
