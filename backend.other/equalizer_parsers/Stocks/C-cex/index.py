import requests
import time
# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————



def getPairs():
    url = 'https://c-cex.com/t/api_pub.html?a=getmarkets'
    r = requests.get(url)
    data = json.loads(r.content.decode('utf8'))

    allPairs = []

    for pair in data['result']:
        label = pair['MarketName']
        label = label.split('-')

        allPairs.append([label[0], label[1]])
    return allPairs



def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(5)
        for pair in pairs:
            pair_label = '-'.join(pair)


            url_buy = 'https://c-cex.com/t/api_pub.html?a=getorderbook&market=' + pair_label + '&type=buy'
            r_b = requests.get(url_buy)
            data_b = json.loads(r_b.content.decode('utf8'))
            data_b = data_b['result']['buy'] if len(data_b) > 0 else []

            url_sell = 'https://c-cex.com/t/api_pub.html?a=getorderbook&market=' + pair_label + '&type=sell'
            r_s = requests.get(url_sell)
            data_s = json.loads(r_s.content.decode('utf8'))
            data_s = data_s['result']['sell'] if len(data_s['result']['sell']) > 0 else []


            print(url_buy)
            print(url_sell)



            # print(data_b)



            orders_arr = {
                'asks': [],
                'bids': []
            }

            for order_s in data_s:
                orders_arr['asks'].append([order_s['Rate'], order_s['Quantity']])


            for order_b in data_b:
                orders_arr['bids'].append([order_b['Rate'], order_b['Quantity']])

            response_arr = {
                'stock_id': stock_id,
                'pair': pair,
                'orders': orders_arr,
            }

            saveOrdersToDb(response_arr)

    # print(response_arr)



if __name__ == "__main__":
    pass
