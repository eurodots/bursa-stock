import requests
import time

# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————

def getPairs():
    try:
        url = 'https://api.kraken.com/0/public/AssetPairs'
        r = requests.get(url)
        data = json.loads(r.content.decode('utf8'))
        data = data['result']
        allPairs = []

        for i, pair in data.items():
            item = pair['altname']
            if item[-2] != '.':
                countA = len(item)
                if countA == 6:
                    second = item[3:]
                    first = item[:3]
                if countA == 7:
                    second = item[4:]
                    first = item[:4]

            allPairs.append([first, second])
        return allPairs
    except Exception as e:
        print('>>>ERROR:', e)
        return []





def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(5)
        for pair in pairs:
            try:
                pair_label = ''.join(pair).lower()

                url = 'https://api.kraken.com/0/public/Depth?pair=' + pair_label + '&count=100'
                print(url)

                r = requests.get(url)
                data = json.loads(r.content.decode('utf8'))
                data = data['result']

                orders_arr = {
                    'asks': [],
                    'bids': []
                }

                for i, trade in data.items():
                    itemAsks = trade['asks']
                    itemBids = trade['bids']

                    for order in itemAsks:
                        orders_arr['asks'].append([order[0], order[1]])

                    for order in itemBids:
                        orders_arr['bids'].append([order[0], order[1]])

                    response_arr = {
                        'stock_id': stock_id,
                        'pair': pair,
                        'orders': orders_arr,
                    }

                    saveOrdersToDb(response_arr)


            except Exception as e:
                # print('exept err')
                print('>>>ERROR:', e)
                # print('exept err')


if __name__ == "__main__":
    pass
