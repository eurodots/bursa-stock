import requests
import time

# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————


def getPairs():
    url = 'https://btc-alpha.com/api/v1/pairs/'
    r = requests.get(url)
    data = json.loads(r.content.decode('utf8'))

    allPairs = []

    for pair in data:
        label = pair['name']
        label = label.split('_')

        allPairs.append([label[0], label[1]])

    return allPairs



def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(5)
        for pair in pairs:

            url = 'https://btc-alpha.com/api/v1/orderbook/' + '_'.join(pair)
            print(url)

            r = requests.get(url)
            data = json.loads(r.content.decode('utf8'))
            data = data
            # print(data)

            orders_arr = {
                'asks': [],
                'bids': []
            }

            for order in data['sell']:
                orders_arr['asks'].append([order['price'], order['amount']])


            for order in data['buy']:
                orders_arr['bids'].append([order['price'], order['amount']])

            response_arr = {
                'stock_id': stock_id,
                'pair': pair,
                'orders': orders_arr,
            }

            saveOrdersToDb(response_arr)



if __name__ == "__main__":
    pass
