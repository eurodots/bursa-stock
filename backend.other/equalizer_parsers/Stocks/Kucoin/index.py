import requests
import time

# ————————————————————————————————————————————
from _utils.functions import *
# ————————————————————————————————————————————
from kucoin.client import Client
client = Client('5a9e7a90a57c5708e0cb4b73', '62900b36-9ce3-4ba9-9cc9-78c94040a863')
# ————————————————————————————————————————————


def getPairs():
    url = 'https://api.kucoin.com/v1/market/open/symbols'
    r = requests.get(url)
    data = json.loads(r.content.decode('utf8'))

    allPairs = []

    for pair in data['data']:

        label = pair['symbol']
        label = label.split('-')

        allPairs.append([label[0], label[1]])

    return allPairs



def getOrders(stock_id, pairs, repeat=True):
    from saveOrders import saveOrdersToDb

    while repeat:
        time.sleep(5)

        for pair in pairs:

            data = client.get_order_book('-'.join(pair), limit=50)

            orders_arr = {
                'asks': [],
                'bids': []
            }

            for order in data['SELL']:
                orders_arr['asks'].append([order[0], order[1]])

            for order in data['BUY']:
                orders_arr['bids'].append([order[0], order[1]])


            response_arr = {
                'stock_id': stock_id,
                'pair': pair,
                'orders': orders_arr,
            }

            saveOrdersToDb(response_arr)

            # print(response_arr)


if __name__ == "__main__":
    pass
