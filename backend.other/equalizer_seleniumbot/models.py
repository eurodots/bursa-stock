from peewee import *
from functools import partial

from utils.settings import *
# import threading
import time

# Connect to a MySQL database on network.
mysql_db = MySQLDatabase(DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST, port=DB_PORT, charset='utf8mb4')


# def refreshDB():
#     global mysql_db
#     mysql_db = MySQLDatabase(DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST, port=DB_PORT, charset='utf8mb4')
#     mysql_db.connect()
#
#     time.sleep(3)
#     refreshDB()
#
#
# threading.Thread(target=refreshDB, args=()).start()


class BaseModel(Model):
    class Meta:
        database = mysql_db


MoneyField = partial(DecimalField, decimal_places=18)


class MarketStocks(BaseModel):
    id = AutoField()
    active = IntegerField()
    name = CharField()
    website = CharField()
    blacklist = CharField()
    holdlist = CharField()
    fastlink = CharField()


    class Meta:
        table_name = 'market_stocks'



class MarketPairs(BaseModel):
    id = AutoField()
    stock = ForeignKeyField(MarketStocks, on_delete='CASCADE')
    stock_name = CharField()
    coin_from = CharField()
    coin_to = CharField()
    pair = CharField()
    arr_asks = CharField()
    arr_bids = CharField()
    arr_asks_count = CharField()
    arr_bids_count = CharField()
    updated_at = DateTimeField()

    class Meta:
        table_name = 'market_pairs'


class TasksAccounts(BaseModel):
    id = AutoField()
    email = CharField()
    stock = ForeignKeyField(MarketStocks, on_delete='CASCADE')
    one_time_key = CharField()
    password = CharField()

    class Meta:
        table_name = 'tasks_accounts'



class TasksOrders(BaseModel):
    id = AutoField()
    type = CharField()
    pair = ForeignKeyField(MarketPairs, on_delete='CASCADE')
    amount = MoneyField()
    total_in_out = MoneyField()
    price = MoneyField()
    price_border = MoneyField()
    price_border_updated = DateTimeField()
    price_source = MoneyField()
    price_history = MoneyField()
    stop_loss_percent = IntegerField()
    ignored_amount_usd = IntegerField()
    account = ForeignKeyField(TasksAccounts, on_delete='CASCADE')
    status = CharField()
    autopilot = BooleanField()
    api_status = CharField()
    api_updated = DateTimeField()
    created_at = DateTimeField()
    updated_at = DateTimeField()
    mercatox_id = IntegerField()

    class Meta:
        table_name = 'tasks_orders'
        # validate_backrefs = False
