import datetime

# ————————————————————————————————————————————
import os
# import django
#
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
# django.setup()
#
# from django.utils import timezone

# ————————————————————————————————————————————
from bs4 import BeautifulSoup
from lxml import html
# ————————————————————————————————————————————
# from tasks.models import Orders, Accounts

from models import TasksOrders as Orders, TasksAccounts as Accounts
from utils.fastink import fastlinkMaker
from utils.settings import *

# ————————————————————————————————————————————
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# ————————————————————————————————————————————




# def getMercatoxCreds():
#     modelAccount = Accounts.select().where(Accounts.id == MERCATOX_ACCOUNT_ID, Accounts.stock == MERCATOX_STOCK_ID).get()
#     return modelAccount



def getTaskOdrder():

    response = False

    modelTasksOders = Orders.select().where(Orders.api_status == 'new', Orders.status == 'new', Orders.account == MERCATOX_ACCOUNT_ID)

    for task in modelTasksOders:

        if task.account.stock.id == MERCATOX_STOCK_ID:

            data = {
                "mercatox_id": task.mercatox_id,
                "status": task.status,
                "type": task.type,
                "pair": [task.pair.coin_from, task.pair.coin_to],
                "amount": task.amount,
                "price": task.price,
                # "price_bought": task.price_bought,
                "fastlink": fastlinkMaker(task.pair.coin_from, task.pair.coin_to, task.account.stock.fastlink),
            }

            response = data
            break


    # print(response)

    return response



def getBrowserOrders(html=False):

    # if not html:
    #     path = os.path.join(BASE_DIR, 'selenium', 'html.txt')
    #     html = open(path, 'r').read()

    orders_html_arr = []

    soup = BeautifulSoup(html, "lxml")
    div_orders = soup.find('div', {'class': 'load-wallet-wrapper'}).find_all('div', {'class': 'ord-row'})
    for order in div_orders:
        cols = order.find_all('div', {'class': 'text-center'})

        data = {
            "mercatox_id": order.get('data-order-id'),
            "date": cols[0].text,
            "type": cols[1].text.split('/')[0].strip(),
            "pair": cols[2].text.split(' / '),
            "amount": float(cols[3].text),
            "price": float(cols[4].text),
            "total": float(cols[5].text),
        }

        orders_html_arr.append(data)




    # Fetch orders for deleting...
    modelTasksOders = Orders.select().where(Orders.status == 'changeprice', Orders.mercatox_id.is_null(False), Orders.account == MERCATOX_ACCOUNT_ID)

    orders_arr = False
    if modelTasksOders:
        orders_arr = [c.mercatox_id for c in modelTasksOders if c.mercatox_id]


    # Deactivate all mercatox orders & Reactivate only from html
    updateOrdersStatus(orders_html_arr)

    return orders_arr


def changeStatusForDeletedOrders():
    modelOrders = Orders.select().where(Orders.account == MERCATOX_ACCOUNT_ID, Orders.status == 'changeprice')
    for model in modelOrders:
        model.status = 'new'
        model.api_status = 'new'
        model.api_updated = datetime.datetime.now()
        model.save()

def updateOrdersStatus(orders_arr):

    # Annulate all orders before appling new actual orders from Mercatox
    modelOrders = Orders.select().where(Orders.account == MERCATOX_ACCOUNT_ID, Orders.api_status == 'progress')
    for order in modelOrders:
        order.api_status = 'completed'
        order.api_updated = datetime.datetime.now()
        order.save()

    for order in orders_arr:
        print(order)
        applyOrderStatusProgress(order)


def applyOrderStatusProgress(order):

    modelTasksOrder = Orders.select().where(Orders.account == MERCATOX_ACCOUNT_ID, Orders.price == order['price'], Orders.amount == order['amount'], Orders.type == order['type'].lower())
    # ^^^^^ ADD COIN_FROM & COIN_TO

    if modelTasksOrder:
        model = modelTasksOrder.get()
        # model.status = 'progress'
        model.api_status = 'progress'
        model.api_updated = datetime.datetime.now()
        model.mercatox_id = order['mercatox_id']
        model.save()

        print('Updated status for task #', model.id)

    # print(data)

if __name__ == "__main__":
    # getMercatoxCreds()
    # print( datetime.datetime.now() )
    # print( getMercatoxCreds() )
    # print( getTaskOdrder() )
    # getMercatoxCreds()
    # updateOrdersStatus()
    # getOrdersToDelete()

    pass
