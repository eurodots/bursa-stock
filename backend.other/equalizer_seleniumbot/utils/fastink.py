
def fastlinkMaker(coin_from, coin_to, link):
    if coin_from and coin_to and len(link) > 0:
        link_arr = link.split('|')

        link_new = [link_arr[0]]
        for l in link_arr:
            v = l.split('=')
            if v[0] == 'coin_from':
                if v[1] == 'upper':
                    link_new.append(coin_from.upper())
                else:
                    link_new.append(coin_from.lower())
            elif v[0] == 'coin_to':
                if v[1] == 'upper':
                    link_new.append(coin_to.upper())
                else:
                    link_new.append(coin_to.lower())
            elif v[0] == 'divider':
                link_new.append(v[1])

        link_new = ''.join(link_new)
        return link_new

    else:
        return link