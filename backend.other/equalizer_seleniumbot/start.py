from listener import getTaskOdrder, getBrowserOrders, applyOrderStatusProgress, changeStatusForDeletedOrders

# ————————————————————————————————————————————
import os
import time
import pyotp
# ————————————————————————————————————————————
from selenium import webdriver
import selenium.webdriver.support.ui as ui
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.common.by import By

# ————————————————————————————————————————————
# chrome://version/
seleniumConf = {
    'profile': "user-data-dir=/Users/%%%username%%%/Library/Application Support/Google/Chrome/Profile 1",
    'prefs': {"profile.managed_default_content_settings.images": 0}, #0 - Normal // 2 - disable
}
# ————————————————————————————————————————————
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DRIVER_FILE = os.path.join(BASE_DIR, 'driver', 'chromedriver')
# print(DRIVER_FILE)
# exit()
# ————————————————————————————————————————————
options =  webdriver.ChromeOptions()
# options.add_argument(seleniumConf['profile'])  # Path to your chrome profile
# options.add_experimental_option("prefs", seleniumConf['prefs']) # disable images
browser = webdriver.Chrome(executable_path=DRIVER_FILE, chrome_options=options)
# browser = False
# ————————————————————————————————————————————

from utils.settings import *

# INIT CREDS
# auth_data = getMercatoxCreds()
# MERCATOX_LOGIN = auth_data.email
# MARCATOX_PASSWORD = auth_data.password
# ONETIME_PASS = auth_data.one_time_key

# INIT PYTOP
# totp = pyotp.TOTP(ONETIME_PASS)



def startSelenium():


    browser.get('https://mercatox.com/auth')

    # Cloudeflare waiting...
    ui.WebDriverWait(browser, 1000).until(lambda browser: browser.find_element_by_id('client-email'))


    login = browser.find_element_by_xpath("//input[@id='client-email']")
    login.send_keys(MERCATOX_LOGIN)

    password = browser.find_element_by_xpath("//input[@id='client-password']")
    password.send_keys(MARCATOX_PASSWORD)

    # if consoleConfrimation('Continue? Yes/No'):

        # Check google auth
        # ui.WebDriverWait(browser, 20).until(lambda browser: browser.find_element_by_id('ga-pin'))
        # auth = browser.find_element_by_xpath("//input[@id='ga-pin']")
        # auth.send_keys(totp.now())


        # time.sleep(5)

    ui.WebDriverWait(browser, 1000).until(lambda browser: browser.find_element_by_id('client-notifications'))
    listener()

        # listener()



browser_refresh_iter = 0
def refresher():

    global browser_refresh_iter

    print('browser_refresh_iter', browser_refresh_iter)
    if browser_refresh_iter == 0:
        browser_refresh_iter = 10
        return True
    else:
        browser_refresh_iter -= 1
        return False



def listener():

    print('LISTENER()')

    while True:
    # if True:

        time.sleep(1)

        data = getTaskOdrder()

        if data:
            browser.get(data['fastlink'])

            direction = False
            if data['type'] == 'sell':
                direction = 'sell'
            elif data['type'] == 'buy':
                direction = 'buy'

            time.sleep(3)

            # price
            try:
                price = browser.find_element_by_xpath(
                "//div[@id='buy-sell-component']//div[@direction='"+direction+"']//input[@id='"+direction+"-price']")
                price.send_keys(str(data['price']))
            except:
                print('Price error')

            # amount
            try:
                amount = browser.find_element_by_xpath(
                    "//div[@id='buy-sell-component']//div[@direction='" + direction + "']//input[@id='" + direction + "-amount']")
                amount.send_keys(str(data['amount']))
            except:
                print('Amount error')

            time.sleep(3)

            # if consoleConfrimation('Confirm this action? Yes/No'):
            if True:
                print('CLICK BUTTON!')
                print('CHANGE TASK STATUS')

                element = browser.find_element_by_id(direction+'-button')
                browser.execute_script("arguments[0].click();", element)

                applyOrderStatusProgress(data)

            else:
                print('CHANGE STATUS TO AWAIT')


            # RESTART LISTENER
            time.sleep(3)
            listener()


        else:

            # path = os.path.join(BASE_DIR, 'selenium', 'html.txt')
            # fh = open(path, "w")
            # fh.write(html)
            # fh.close()
            # exit()

            if refresher():
                print('Refresh browser')
                browser.get('https://mercatox.com/wallet/orders')
                html = browser.page_source


                orders_to_delete = getBrowserOrders(html)
                print('----orders_to_delete------')
                print(orders_to_delete)

                if orders_to_delete:
                    for order_id in orders_to_delete:
                        try:
                            try:
                                checkDeleted = ui.WebDriverWait(browser, 100).until(lambda browser: browser.find_element_by_xpath("//div[@data-order-id-remove="+str(order_id)+"]"))
                                while checkDeleted:
                                    element = browser.find_element_by_xpath("//div[@data-order-id-remove="+str(order_id)+"]")
                                    browser.execute_script("arguments[0].click();", element)
                                    print('DELETED MERCATOX_ID #', order_id)
                                    time.sleep(1)
                            except:
                                print('Continue deleteing....')

                        except:
                            print('MERCATOX_ID NOT FOUND ON PAGE #',order_id)


                        time.sleep(1)


                    changeStatusForDeletedOrders()

                # browser.refresh()







def consoleConfrimation(label):
    getCommand = input(label+'\n')
    if getCommand.lower() == 'yes':
        return True
    else:
        return False





if __name__ == "__main__":

    startSelenium()

    # print( getMercatoxCreds() )
    # getBrowserOrders()
    #
    # print(getCommand)
    # print( getTaskOdrder() )

    pass
