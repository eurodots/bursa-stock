import json
from DEFAULT.settings import BASE_DIR, WEBPACK_LOADER
# from icosignup.icolist_update._functions import *

file = WEBPACK_LOADER['DEFAULT']['STATS_FILE']
# print(file)

def getWebpackBundle(init):
    webpack_bundle = False
    with open(file) as handle:
        dictdump = json.loads(handle.read())
        for f in dictdump['chunks']['main']:
            name = f['name'].split('.')
            if name[0] == 'bundle':
                webpack_bundle = f['publicPath']
                break


    el_js = '<script type="text/javascript" src="' + webpack_bundle + '"></script>'
    el_div = '<div id="stock-widget" ' \
            'data-stock-name="'+str(init['stock_name'])+'" ' \
            'data-stock-token="'+str(init['stock_token'])+'" ' \
            'data-stock-private="'+str(init['stock_private'])+'"></div>'

    innerScript = el_div+"\n"+el_js
    print(innerScript)
    return innerScript
    # return ['ok']

# getWebpackBundle({'stock_name': 'New name' ,'stock_token': '12334556', 'stock_private': 'true'})
