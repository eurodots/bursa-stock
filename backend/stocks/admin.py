from django.contrib import admin

from stocks.models import *



@admin.register(Customers)
class CustomersAdmin(admin.ModelAdmin):
    list_display = ('id', 'token_name')
    list_display_links = ('id', 'token_name')


    # token_links = forms.CharField(widget=forms.Textarea(attrs={'rows': 5, 'cols': 100}))


@admin.register(Orderbooks)
class OrderbooksAdmin(admin.ModelAdmin):
    list_display = ('id', 'bestprice_ask', 'bestprice_bid')
    # list_display_links = ('id')
    raw_id_fields = ("customer",)
