# ————————————————————————————————————————————
from DEFAULT.utils.django_setup import *
from DEFAULT.utils.functions import *
# ————————————————————————————————————————————
from stocks.models import Traders as StocksTraders, Traders_orders as StocksTradersOrders, Customers as StocksCustomers
# ————————————————————————————————————————————
from django.contrib.auth import get_user_model
User = get_user_model()
# ————————————————————————————————————————————


def matemaskAuth(user_id, wallet):

    if wallet:
        modelStocksTraders = StocksTraders.objects.filter(token_wallet=wallet)

        if not modelStocksTraders:
            model = StocksTraders()
            model.token_wallet = wallet
        else:
            model = StocksTraders.objects.get(token_wallet=wallet)

        if user_id:
            model.user = User.objects.get(pk=user_id)
        model.save()

        # print('token_wallet')
        # print(model.token_wallet)


    return True


def metamaskOrdersSave(wallet, data):

    print(data)

    model = StocksTradersOrders()

    model.wallet = StocksTraders.objects.get(token_wallet=wallet)
    model.type = data['type']
    model.status = 'active'
    model.amount = data['amount']
    model.price = data['price']
    model.spot_id = data['spot_id']
    # model.hash = data['hash']
    model.stock = StocksCustomers.objects.get(token_address=data['stock_token'])
    model.save()

    return True



def metamaskOrdersCancel(wallet, data):

    modelOrders = StocksTradersOrders.objects.filter(stock__token_address=data['stock_token'], wallet__token_wallet=wallet, spot_id=data['spot_id'])
    if modelOrders:
        model = modelOrders[0]
        model.status = 'canceled'
        model.save()
        return True

    return False


def metamaskAllOrdersOfUser(wallet):
    model = StocksTradersOrders.objects.filter(wallet__token_wallet=wallet)

    orders_arr = {}
    for m in model:
        stock_token = m.stock.token_address
        if not stock_token in orders_arr:
            orders_arr[stock_token] = {
                'stock_name': m.stock.token_name,
                'stock_token': stock_token,
                'orders': [],
            }

        orders_arr[stock_token]['orders'].append({
            'type': m.type,
            'amount': float(m.amount),
            'price': float(m.price),
            'spot_id': m.spot_id,
            'created_at': dateConverter(m.created_at),
            'updated_at': dateConverter(m.updated_at),
            'status': m.status,
            'stock_token': stock_token,
        })

    orders_arr = [c for c in orders_arr.values()]
    # jprint(orders_arr)
    return {
        'error': False,
        'results': orders_arr,
    }


if __name__ == "__main__":
    pass

    metamaskAllOrdersOfUser(wallet = '0xaf8aae0b3aa6e871e7d00c77266b4e5cbe8eb63f')


    # d = {
    #     'type': 'sell',
    #     'amount': 0.40000000,
    #     'stock_token': '0xafe5a978c593fe440d0c5e4854c5bd8511e770a4',
    #     'price': 0.40000000,
    #     'spot_id': 0
    # }
    # metamaskOrdersCancel('0xaf8aae0b3aa6e871e7d00c77266b4e5cbe8eb63f', d)