import json
import threading
# ————————————————————————————————————————————————
from django.utils import timezone
from DEFAULT.utils.django_setup import *
from DEFAULT.utils.functions import *
# ————————————————————————————————————————————————
from stocks.web3.web3_init import * # Init web3 contract and environments
# ————————————————————————————————————————————————
# from market.models import Coins as MarketCoins
from stocks.models import Orderbooks
# ————————————————————————————————————————————————
from stocks.web3.orderbook import makeOrderbook
# ————————————————————————————————————————————————


def getOrderbookByUser(user_address):

    # earlier = timezone.now() - timezone.timedelta(minutes=GLOBAL_ORDERBOOK_MINUTES)
    # if modelByDate.updated_at < earlier:  # not fresh

    modelOrderbooks = Orderbooks.objects.all()
    for book in modelOrderbooks:

        # Update all orderbooks before getting actual orders of current user
        # makeOrderbook(book.customer.token_address)

        item = {
            'token_name': book.customer.token_name,
            'token_ticker': book.customer.token_ticker,
            'token_address': book.customer.token_address,
            'arr_asks': [c for c in book.arr_asks_ if c['user_address'] == user_address],
            'arr_bids': [c for c in book.arr_bids_ if c['user_address'] == user_address],
            'updated_at': dateConverter(book.updated_at),
        }


        jprint(item)



def getAprrovedBalance(token_address=False, user_address=False):

    # Опрашиваем баланс только для тех токенов, которые получены из ордербуков
    # Обновляе раз в минуту и храним вытяжку в базе (актуальные ордера и актулаьные апрувы. Актуальные ордера заносить в базу в момент обновления получения ордербука из другого файла.

    user_address = '0xAF8aaE0B3Aa6e871e7D00c77266b4E5CBE8eB63f'
    # user_address = w3.toChecksumAddress(user_address)

    token_address = '0xafe5a978c593fe440d0c5e4854c5bd8511e770a4'
    token_address = w3.toChecksumAddress(token_address)

    approvedForToken = contract.call().balanceApprovedForToken(token=token_address, user=user_address)
    approvedForToken = fromWei(approvedForToken)

    print(approvedForToken)

if __name__ == "__main__":

    getAprrovedBalance()
    # getOrderbookByUser('0xAF8aaE0B3Aa6e871e7D00c77266b4E5CBE8eB63f')