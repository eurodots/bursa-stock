from DEFAULT.utils.functions import *
import json
# ————————————————————————————————————————————————
import eth_abi
from web3 import Web3, HTTPProvider, RPCProvider
from web3.contract import ConciseContract

# ————————————————————————————————————————————————


# http://web3py.readthedocs.io/en/stable/
# parity --chain ropsten
# provider = HTTPProvider('http://localhost:8545')


# provider = HTTPProvider('https://ropsten.infura.io')
provider = RPCProvider(host='ropsten.infura.io', port= 443, ssl=True)
w3 = Web3(provider)
assert w3.isConnected()



contract_json = json.load(open('contract.json'))
contract_address = contract_json['address']
contract_abi = contract_json['abi']


contract = w3.eth.contract(abi=contract_abi, address=contract_address) #, ContractFactoryClass=ConciseContract

# jprint(  dir()  )

def toWei(value):
    # return eth_abi.encode_abi('uint256', value)
    return w3.toWei(str(value), 'ether')

def fromWei(value):
    value = w3.fromWei(value, 'ether')
    return float(value)


amount = toWei(5)
token = '0xafe5a978c593fe440d0c5e4854c5bd8511e770a4'
price = toWei(1)
bid_order_spot = 3
# print(bid_order_spot)

# s = contract.call().willbuy(amount=amount, token=token, price_each=price_each, bid_order_spot=bid_order_spot)

for s in range(0,50):
    s = contract.call().willsellInfo(token=token, ask_order=s)
    user_id = s[0]
    price = fromWei(s[1])
    amount = fromWei(s[2] )

    print({
        'user_id': user_id,
        'price': price,
        'amount': amount,
    })
