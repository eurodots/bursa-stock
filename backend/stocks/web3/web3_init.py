import json
import os
# ————————————————————————————————————————————————
from django.conf import settings
# ————————————————————————————————————————————————
# import eth_abi
from web3 import Web3, HTTPProvider #, RPCProvider
from web3.contract import ConciseContract
# ————————————————————————————————————————————————
if settings.PRODUCTION:
    # provider = HTTPProvider('http://localhost:8545')
    provider = HTTPProvider('https://ropsten.infura.io')
else:
    provider = HTTPProvider('https://ropsten.infura.io')
    # provider = RPCProvider(host='ropsten.infura.io', port=443, ssl=True)

w3 = Web3(provider)
assert w3.isConnected()

contract_path = os.path.join(os.path.dirname(__file__), 'contract.json')
contract_json = json.load(open(contract_path))
contract_address = contract_json['address']
contract_abi = contract_json['abi']

contract = w3.eth.contract(abi=contract_abi, address=contract_address) #, ContractFactoryClass=ConciseContract
# ————————————————————————————————————————————————




def toWei(value):
    # return eth_abi.encode_abi('uint256', value)
    return w3.toWei(str(value), 'ether')


def fromWei(value):
    value = w3.fromWei(value, 'ether')
    return float(value)




if __name__ == "__main__":
    pass
