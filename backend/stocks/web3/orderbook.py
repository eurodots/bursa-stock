import json
import threading
# from dateutil import parser

# ————————————————————————————————————————————————
from django.utils import timezone
from DEFAULT.utils.django_setup import *
from DEFAULT.utils.functions import *
# ————————————————————————————————————————————————
from stocks.web3.web3_init import * # Init web3 contract and environments
# ————————————————————————————————————————————————
from market.models import Coins as MarketCoins
from stocks.models import Orderbooks
# ————————————————————————————————————————————————
GLOBAL_MAX_ORDERS_FETCHING = 30
GLOBAL_ORDERBOOK_MINUTES = (1/60) * 10
# ————————————————————————————————————————————————



def makeOrderbook(customer_token):

    modelOrderbooks = Orderbooks.objects.filter(customer__token_address=customer_token)
    # if not modelOrderbooks:
    #     modelMarketCoins = MarketCoins.objects.filter(token=customer_token)
    #     if modelMarketCoins:
    #         print(modelMarketCoins.get().pk)
    #         exit()

    if modelOrderbooks:
        modelByDate = Orderbooks.objects.get(customer__token_address=customer_token)

        def getOrders(model):
            arr_asks = model.arr_asks_
            arr_bids = model.arr_bids_


            # Calculation middle price
            price_middle = 0
            if arr_asks and arr_bids:
                price_middle = (arr_asks[0]['price'] + arr_bids[0]['price']) / 2
            elif not arr_asks and arr_bids:
                price_middle = arr_bids[0]['price']
            elif not arr_bids and arr_asks:
                price_middle = arr_asks[0]['price']


            # Total amount
            total_amount_asks = [c['amount'] for c in arr_asks]
            total_amount_asks = sum(total_amount_asks)

            total_amount_bids = [c['amount'] for c in arr_bids]
            total_amount_bids = sum(total_amount_bids)

            return {
                'updated_at': dateConverter(model.updated_at),
                'price_middle': price_middle,
                'bestprice_ask': float(model.bestprice_ask),
                'bestprice_bid': float(model.bestprice_bid),
                'total_amount_asks': total_amount_asks,
                'total_amount_bids': total_amount_bids,
                'arr_asks': arr_asks,
                'arr_bids': arr_bids,
            }


        earlier = timezone.now() - timezone.timedelta(minutes=GLOBAL_ORDERBOOK_MINUTES)

        print('modelByDate.updated_at', modelByDate.updated_at)
        print('earlier',earlier)

        if modelByDate.updated_at < earlier: # not fresh
            updateOrderbook(customer_token)
            # print('UPDATED NOW')
        else: # fresh
            # modelByDate.save()
            # print('FRESH')
            pass


        # print(getOrders(modelByDate))

        return {
            'error': False,
            'results': getOrders(modelByDate),
        }


    return {
        'error': True,
        'message': 'Token not found'
    }




def updateOrderbook(customer_token):

    model = Orderbooks.objects.get(customer__token_address=customer_token)

    # orderbook_arr = False
    # threading.Thread(target=fetchOrders, args=(token_address,orderbook_arr,)).start()

    orderbook_arr = fetchOrders(customer_token)
    arr_asks = [c for c in orderbook_arr if c['type'] == 'sell']
    arr_bids = [c for c in orderbook_arr if c['type'] == 'buy']

    arr_asks = sorted(arr_asks, key=lambda k: k['price'], reverse=False)
    arr_bids = sorted(arr_bids, key=lambda k: k['price'], reverse=True)

    # jprint(arr_asks)
    # jprint(arr_bids)

    model.arr_asks = json.dumps(arr_asks)
    model.arr_bids = json.dumps(arr_bids)
    model.arr_asks_count = len(arr_asks)
    model.arr_bids_count = len(arr_bids)

    model.bestprice_ask = arr_asks[0]['price'] if arr_asks else 0
    model.bestprice_bid = arr_bids[0]['price'] if arr_bids else 0

    model.save()

    return True





def fetchOrders(token_address):

    def singleOrder(type, index, arr, orders_iter):

        token = w3.toChecksumAddress(token_address)

        try:
            if type == 'sell':
                s = contract.call().willsellInfo(token=token, ask_order=index)
            elif type == 'buy':
                s = contract.call().willbuyInfo(token=token, bid_order=index)

            user_id = s[0].lower()
            price = fromWei(s[1])
            amount = fromWei(s[2])

            print(s)

            if price > 0 and amount > 0:
                arr.append({
                    'type': type,
                    'user_address': user_id,
                    'price': price,
                    'amount': amount,
                    'total': price * amount,
                    'spot_id': index,
                })

            orders_iter.append(1)
        except Exception as e:
            print('>>', e)

    orders_arr = []
    orders_iter = []
    orders_range = GLOBAL_MAX_ORDERS_FETCHING
    for index in range(0, orders_range):
        threading.Thread(target=singleOrder, args=('sell', index,orders_arr,orders_iter,)).start()
        threading.Thread(target=singleOrder, args=('buy', index, orders_arr, orders_iter,)).start()
        # singleOrder(index,orders_arr,orders_iter)

    while len(orders_iter) < orders_range*2:
        pass


    return orders_arr






if __name__ == "__main__":
    makeOrderbook('0xafe5a978c593fe440d0c5e4854c5bd8511e770a4')
    # makeOrderbook('0x46b9ad944d1059450da1163511069c718f699d31')
