"""django2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework import routers

from stocks.views import *

# router = routers.DefaultRouter()
# router.register(r'platform', views.platformViewSet, base_name='api-platform')

urlpatterns = [
    url(r'^user/stocks/edit/(?P<stock_id>[-\w.]+)/$', userStocksGet, name='api-stock-update'),
    url(r'^user/stocks/delete/', userStocksDeleteViewSet, name='api-stocks-delete'),
    url(r'^user/stocks/list/', userStocksListViewSet, name='api-stocks-list'),
    url(r'^user/stocks/favicon/(?P<stock_id>[-\w.]+)/$', faviconUpload, name='api-favicon-upload'),
    url(r'^linkpreview/', linkPreviewSet, name='api-linkpreview'),

    url(r'^web3/orderbook/(?P<token>\w{0,60})/$', web3OrderbookView, name='api-web3-orderbook'),
    url(r'^web3/metamask/(?P<wallet>\w{0,60})/auth/$', web3MetamaskAuthView, name='api-web3-metamask-auth'),
    url(r'^web3/metamask/(?P<wallet>\w{0,60})/orders/save/$', web3MetamaskOrdersSaveView, name='api-web3-metamask-orders-save'),
    url(r'^web3/metamask/(?P<wallet>\w{0,60})/orders/cancel/$', web3MetamaskOrdersCancelView, name='api-web3-metamask-orders-cancel'),
    url(r'^web3/metamask/(?P<wallet>\w{0,60})/orders/list/$', web3MetamaskAllOrdersOfUserView, name='api-web3-metamask-orders-list'),


    url(r'^form/icosignup/', form_view, name="form-icosignup"),
]
