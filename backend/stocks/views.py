import json
# ————————————————————————————————————————————
from django.shortcuts import render
# from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
# ————————————————————————————————————————————
from DEFAULT.utils.functions import *
from django_countries import countries
from stocks.utils.token_verify import verifyToken, verifyWalletEth, ethplorerData
from stocks.web3.orderbook import makeOrderbook
from stocks.web3.metamask import matemaskAuth, metamaskOrdersSave, metamaskOrdersCancel, metamaskAllOrdersOfUser
# ————————————————————————————————————————————
from stocks.forms import IcosignupForm
from stocks.models import Customers, Orderbooks
from market.models import Coins as MarketCoins
# ————————————————————————————————————————————
from stocks.serializers import FormSerializer, UserStocksSerializer
from stocks.dicts.init import *
# from stocks.utils.load_webpack import getWebpackBundle
# ————————————————————————————————————————————
from stocks.utils.linkpreview import linkPreviewMaker
from django.conf import settings
# ————————————————————————————————————————————

def urlToDomain(url):
    spltAr = url.split("://")
    i = (0, 1)[len(spltAr) > 1]
    dm = spltAr[i].split("?")[0].split('/')[0].split(':')[0].lower()
    return dm



def tokenChecker(request):
    try:
        if not request or not 'HTTP_AUTHORIZATION' in request.META:
            return False

        token = request.META['HTTP_AUTHORIZATION'].split('Token ')[1]
        user_token = Token.objects.get(key=token)
        return user_token.user_id

    except:
        return False



def form_view(request):
    test = "Hi Mark"
    form = IcosignupForm(request.POST or None)

    if request.method == 'POST' and form.is_valid():

        token_ticker = request.POST.getlist('token_ticker')[0].upper()

        new_form = form.save()
        return render(request, 'default/success.html', {
            'token_ticker': token_ticker
        }, locals())

    # return render(request, 'reactjs/inner.html', locals())
    return render(request, 'default/form.html', locals())




@api_view(['POST'])
def faviconUpload(request, stock_id):

    user_id = tokenChecker(request)
    modelCustomers = Customers.objects.filter(user=user_id, id=stock_id)
    if modelCustomers:

        m = Customers.objects.get(user=user_id, id=stock_id)
        m.favicon = request.FILES['file']
        m.save()

        return Response({
            'error': False,
            'favicon': m.favicon_src('40x40'),
        })


    return Response({
        'error': True,
        'message': 'Something wrong...'
    })






@api_view(['GET','POST'])
def userStocksGet(request, stock_id):

    if request.method == 'POST':

        user_id = tokenChecker(request)
        # user_id = 3
        stock_id = request.data['stock_id']

        data = request.data
        # data['user'] = tokenChecker(request)
        data['user'] = user_id
        data['token_domain'] = urlToDomain(request.data['token_domain'])
        data['token_wallet'] = verifyWalletEth(data['token_wallet'])
        data['script_version'] = "1.0"

        data['token_category'] = json.dumps(data['token_category'])
        data['token_industry'] = json.dumps(data['token_industry'])


        # CHECK IF TOKEN EXIST IN MARKET COINS
        if MarketCoins.objects.filter(token=data['token_address']):
            return Response({
                "error": True,
                "errors": {
                    'token_address': ['This token already exist!'],
                }
            })


        # UPDATE IF STOCK_ID EXIST
        if Customers.objects.filter(pk=stock_id, user=user_id):
            serializer = FormSerializer(Customers.objects.get(pk=stock_id), data=data, partial=True)
        else:
            # ADDING NEW IF STOCK_ID NOT FOUND OR "0"
            serializer = FormSerializer(data=data)


        if serializer.is_valid():
            serializer.save()

            # Create ordersbook
            modelOrderbooks = Orderbooks()
            modelOrderbooks.customer = Customers.objects.get(pk=serializer.data['id'])
            modelOrderbooks.save()


            return Response({
                "error": False,
                "message": "Data stored!",
            })

            # CHECK TOKEN AVAILIBILITY
            # token_ticker = request.data['token_ticker']
            # token_address = request.data['token_address']
            # token_name = request.data['token_name']
            # token_status = verifyToken(token_address)
            # stockWidget = False
            # if token_status:
            #     stockWidget = getWebpackBundle({'stock_name': token_name ,'stock_token': token_address, 'stock_private': token_status})


        else:
            msg = {
                "error": True,
                "errors": serializer.errors,
            }
            return Response(msg)

    elif request.method == 'GET':

        def choicesTransformer(arr):
            arr_new = []
            for c in arr:
                arr_new.append({
                    'id': c[0],
                    'value': c[1],
                })
            return arr_new

        data = {
            "stock_id": 0,
            'token_tariff': '',
            'token_category': [],
            'token_industry': [],
            'token_country': '',
            'token_platform': '',
            'token_status': '',

            'token_wallet': '',
            'token_domain': '',
            'token_name': '',
            'token_ticker': '',
            'token_address': '',
            'token_description': '',
            'token_links': [],


            "data_tariffs": choicesTransformer(CHOICES_TARIFF),
            "data_categories": choicesTransformer(CHOICES_CATEGORY),
            "data_industries": choicesTransformer(CHOICES_INDUSTRY),
            "data_platforms": choicesTransformer(CHOICES_PLATFORM),
            "data_statuses": choicesTransformer(CHOICES_STATUS),
            "data_countries": choicesTransformer(countries),
        }



        # ADDING CUSTOMER STOCK DATA
        if Customers.objects.filter(pk=stock_id):
            s = Customers.objects.get(pk=stock_id)

            data['stock_id'] = s.pk
            data['token_tariff'] = s.token_tariff if s.token_tariff else ''
            data['token_category'] = json.loads(s.token_category) if s.token_category else []
            data['token_industry'] = json.loads(s.token_industry) if s.token_industry else []
            data['token_country'] = str(s.token_country) if s.token_country else ''
            data['token_platform'] = s.token_platform if s.token_platform else ''
            data['token_status'] = s.token_status if s.token_status else ''
            data['token_wallet'] = s.token_wallet if s.token_wallet else ''
            data['token_domain'] = s.token_domain if s.token_domain else ''
            data['token_name'] = s.token_name if s.token_name else ''
            data['token_ticker'] = s.token_ticker if s.token_ticker else ''
            data['token_address'] = s.token_address if s.token_address else ''
            data['token_description'] = s.token_description if s.token_description else ''
            data['token_links'] = s.token_links.split('||') if s.token_links else []


        return Response(data)


@api_view(['POST'])
def userStocksDeleteViewSet(request):
    user_id = tokenChecker(request)
    stock_id = request.data['stock_id']

    Customers.objects.filter(user=user_id, id=stock_id).delete()

    return Response({
        "error": False,
        "message": "Stock was delete!",
    })


@api_view(['GET','POST'])
def userStocksListViewSet(request):

    user_id = tokenChecker(request)
    # print( 'user_id', user_id )
    data = Customers.objects.filter(user=user_id).order_by('created_at').reverse()


    if data:
        serializer = UserStocksSerializer(data, many=True)
        serializerData = serializer.data


        results = []
        for item in serializerData:
            item['ethplorer'] = ethplorerData(item['token_address']) # ethplorer data
            results.append(item)


        return Response({
            "error": False,
            "results": results,
        })

    return Response({
        "error": True,
        "message": 'Not found',
    })


@api_view(['POST'])
def linkPreviewSet(request):

    link = request.data['link']
    link_data = linkPreviewMaker(link)

    return Response(link_data)




# METAMASK

@api_view(['GET'])
def web3OrderbookView(request, token):
    return Response(makeOrderbook(token))

@api_view(['GET'])
def web3MetamaskAuthView(request, wallet):

    user_id = tokenChecker(request)

    return Response(matemaskAuth(user_id, wallet))

@api_view(['POST'])
def web3MetamaskOrdersSaveView(request, wallet):

    return Response(metamaskOrdersSave(wallet, request.data))


@api_view(['POST'])
def web3MetamaskOrdersCancelView(request, wallet):

    return Response(metamaskOrdersCancel(wallet, request.data))


@api_view(['GET'])
def web3MetamaskAllOrdersOfUserView(request, wallet):

    return Response(metamaskAllOrdersOfUser(wallet))
