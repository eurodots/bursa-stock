from rest_framework import serializers
from .models import Customers
# from recaptcha.fields import ReCaptchaField
# ————————————————————————————————————————————


class FormSerializer(serializers.ModelSerializer):

    class Meta:
        model = Customers
        fields = ("id","user", "token_tariff", "token_wallet", "token_domain", "token_category", "token_industry", "token_country", "token_platform",
                  "token_status", "token_name", "token_description", "token_links", "token_ticker", "token_platform", "created_at",
                  "token_address", "script_version", "active", "favicon_src")


# class FormUpdateSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Customers
#         fields = ("active")

    # def create(self, validated_data):
    #     return FormSerializer(**validated_data)
    #
    # def update(self, instance, validated_data):
    #     # instance.token_address = validated_data.get('email', instance.email)
    #     instance.token_address = validated_data.get('token_address', instance.token_address)
    #     # instance.created = validated_data.get('created', instance.created)
    #     # instance.save()
    #     return instance

class UserStocksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customers
        fields = ("id", "user", "token_tariff", "token_wallet", "token_domain", "token_category", "token_industry", "token_country", "token_platform",
                  "token_status", "token_name", "token_description", "token_links", "token_ticker", "token_platform", "created_at",
                  "token_address", "script_version", "active", "favicon_src")


# class EthTokenSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = Subsribers
#         fields = ("token_name", "token_ticker", "token_links", "token_platform", "token_address")


# class IcoListSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = IcoList
#         fields = ('token_ticker', 'token_name', 'token_platform', 'token_address', 'updated_at', 'token_rank')

