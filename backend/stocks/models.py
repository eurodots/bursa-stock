import json
# ————————————————————————————————————————————
from django.db import models
from django_countries.fields import CountryField
from django.core.validators import MinLengthValidator
from six import text_type
from django.core.validators import FileExtensionValidator
# ————————————————————————————————————————————
from sorl.thumbnail import ImageField, get_thumbnail
# ————————————————————————————————————————————
from django.contrib.auth import get_user_model
User = get_user_model()
# ————————————————————————————————————————————
from django.utils import timezone
from django.conf import settings
from DEFAULT.utils.functions import *
# ————————————————————————————————————————————
import re
from stocks.dicts.init import *
from ckeditor.fields import RichTextField
# ————————————————————————————————————————————



class Customers(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, editable=False)
    token_platform = models.CharField(null=False, max_length=1, default=0, editable=False)
    token_category = models.CharField(null=False, max_length=30)
    token_industry = models.CharField(null=False, max_length=30)
    token_domain = models.CharField(null=False, max_length=30)
    token_wallet = models.CharField(null=False, max_length=50)
    token_country = CountryField(null=False, verbose_name="Project country")
    token_tariff = models.CharField(null=False, max_length=20, choices=CHOICES_TARIFF)
    token_platform = models.CharField(null=False, max_length=50, choices=CHOICES_PLATFORM)
    token_status = models.CharField(null=False, max_length=20, choices=CHOICES_STATUS)
    token_name = models.CharField(null=False, max_length=20, validators=[MinLengthValidator(3)])
    token_ticker = models.CharField(null=False, unique=False, max_length=10, validators=[MinLengthValidator(3)], verbose_name="Token's ticker code")
    token_description = RichTextField(config_name='default', max_length=5000)
    token_links = models.TextField(blank=True)
    token_address = models.CharField(null=False, unique=True, max_length=100)
    script_version = models.CharField(null=False, max_length=3, default="1.0")
    active = models.BooleanField(null=False, default=1)
    favicon = models.ImageField(blank=True, upload_to='favicons_uploaded')

    @property
    def created_at_(self):
        return dateConverter(self.created_at)

    def __get_label(self, field):
        return text_type(self._meta.get_field(field).verbose_name)


    # @property
    def favicon_src(self, size='40x40'):
        if self.favicon:
            favicon = get_thumbnail(self.favicon, size, crop='center', format='PNG', quality=99).url
            return str(favicon)
        else:
            return False

        # if not self.id:
        #     # Have to save the image (and imagefield) first
        #     super(Foo, self).save(*args, **kwargs)
        #     # obj is being created for the first time - resize
        #     resized = get_thumbnail(self.image, "100x100"...)
        #     # Manually reassign the resized image to the image field
        #     self.image.save(resized.name, resized.read(), True)


    def save(self, *args, **kwargs):

        # self.user = User.object.get(pk=3)

        for field_name in ['token_ticker']:
            val = getattr(self, field_name, False)
            if val:
                setattr(self, field_name, re.sub(r'[\W_]+', '', val).upper())
        super(Customers, self).save(*args, **kwargs)



class Orderbooks(models.Model):
    customer = models.ForeignKey(Customers, on_delete=models.CASCADE)
    arr_asks = models.TextField(blank=True)
    arr_bids = models.TextField(blank=True)
    arr_asks_count = models.IntegerField(blank=True, default=0)
    arr_bids_count = models.IntegerField(blank=True, default=0)
    bestprice_ask = models.DecimalField(blank=True, default=0, max_digits=18, decimal_places=8)
    bestprice_bid = models.DecimalField(blank=True, default=0, max_digits=18, decimal_places=8)
    updated_at = models.DateTimeField(auto_now=True, blank=True, editable=False)

    @property
    def arr_asks_(self):
        return json.loads(self.arr_asks) if self.arr_asks else False

    @property
    def arr_bids_(self):
        return json.loads(self.arr_bids) if self.arr_bids else False

    def save(self, *args, **kwargs):
        self.updated_at = timezone.now()
        return super(Orderbooks, self).save(*args, **kwargs)




class Traders(models.Model):
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    token_wallet = models.CharField(null=False, unique=True, max_length=100)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        self.updated_at = timezone.now()
        return super(Traders, self).save(*args, **kwargs)

class Traders_orders(models.Model):
    stock = models.ForeignKey(Customers, null=True, on_delete=models.CASCADE)
    wallet = models.ForeignKey(Traders, null=True, on_delete=models.CASCADE)
    type = models.CharField(blank=False, max_length=20)
    status = models.CharField(blank=False, max_length=20)
    amount = models.DecimalField(blank=True, default=0, max_digits=18, decimal_places=8)
    price = models.DecimalField(blank=True, default=0, max_digits=18, decimal_places=8)
    spot_id = models.IntegerField(null=False, default=0)
    # hash = models.CharField(null=False, unique=True, max_length=100)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        self.updated_at = timezone.now()
        return super(Traders_orders, self).save(*args, **kwargs)

