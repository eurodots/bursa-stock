
# Django
uwsgi --http :8000 --module DEFAULT.wsgi &
uwsgi --http-keepalive --http :8000 --module DEFAULT.wsgi &

nohup sh run.sh &


———————————————



Подключить MySQL:
mysqld --verbose --help | grep ^datadir
# export PATH=$PATH:/usr/local/mysql/bin
# vestacp
  export PATH=$PATH:/vesta/var/lib/mysql/
  sudo apt-get install libmysqlclient-dev
sudo pip3 install mysqlclient


Настроить конфиг Mysql
[mysql]
sql_mode = "STRICT_TRANS_TABLES"


———————————————

TMUX
https://habrahabr.ru/post/327630/

tmux ls // список сессий
tmux new -s <s> // создать сессию
tmux a -t <s> // подключиться к сессии
CTRL+B -> c // новое окон
CTRL+B -> w // список окон
CTRL+B -> 0 // переключиться на первое окно
CTRL+B -> стрелка/n/p // переключиться на соседнее окно
CTRL+B+D // выйти из сессии, не закрываю сессию
exit // выйти из сессии
