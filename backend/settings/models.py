from django.db import models


class Global(models.Model):
    name = models.CharField(null=False, max_length=50, unique=True)
    value = models.TextField()