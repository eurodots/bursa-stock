from django.contrib import admin
from .models import Global


@admin.register(Global)
class GlobalAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'value')
    list_display_links = ('id','name',)
    search_fields = ('name', 'value')
    list_per_page = 25