from django.db import models
from django.utils import timezone
# ————————————————————————————————————————————
from DEFAULT.utils.model_mixins import modelMixin_DateAgo
from market.models import Pairs as MarketPairs, Stocks as MarketStocks



from market.models import Pairs as MarketPairs, Stocks as MarketStocks



class Accounts(models.Model):
    email = models.EmailField(null=False, max_length=50)
    password = models.CharField(null=False, default="", max_length=100)
    one_time_key = models.CharField(null=False, default="", max_length=100)
    stock= models.ForeignKey(MarketStocks, on_delete=models.CASCADE)



CHOICES_TYPES = (
    ('sell', 'Sell'),
    ('buy', 'Buy'),
)
CHOICES_STATUSES = (
    ('draft', 'Draft'),
    ('new', 'New'),
    ('progress', 'In progress'),
    ('changeprice', 'Changing price...'),
    ('completed', 'Completed'),
    ('deleted', 'Deleted')
)
CHOICES_STATUSES_API = (
    ('new', 'New'),
    ('progress', 'In progress'),
    ('completed', 'Completed'),
    ('canceled', 'Cancleled')
)


class Orders(models.Model):
    type = models.CharField(null=False, max_length=20, choices=CHOICES_TYPES)
    pair = models.ForeignKey(MarketPairs, on_delete=models.CASCADE)
    amount = models.DecimalField(null=True, max_digits=18, decimal_places=8, editable=False)
    total_in_out = models.DecimalField(null=False, max_digits=18, decimal_places=8)
    price = models.DecimalField(null=False, max_digits=18, decimal_places=8)
    price_border = models.DecimalField(null=True, max_digits=18, decimal_places=8)
    price_border_updated = models.DateTimeField(null=True, editable=True)
    price_source = models.DecimalField(null=True, default=0.00000, max_digits=18, decimal_places=8)
    price_history = models.DecimalField(null=True, default=0.00000, max_digits=18, decimal_places=8)
    stop_loss_percent = models.IntegerField(null=True, default=0)
    ignored_amount_usd = models.IntegerField(null=True, default=0)

    account = models.ForeignKey(Accounts, on_delete=models.CASCADE)
    status = models.CharField(null=False, max_length=20, default='new', choices=CHOICES_STATUSES)

    autopilot = models.BooleanField('autopilot', default=0, help_text='Autopilot')

    api_status = models.CharField(null=False, max_length=20, default='new', choices=CHOICES_STATUSES_API)
    api_updated = models.DateTimeField(null=True, editable=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, null=True, editable=False)


    # MERCATOX
    mercatox_id = models.IntegerField(null=True, default=0)

    @property
    def stop_loss_percent_(self):
        return float(self.stop_loss_percent)

    @property
    def ignored_amount_usd_(self):
        return float(self.ignored_amount_usd)

    @property
    def price_(self):
        return float(self.price)

    @property
    def amount_(self):
        return float(self.amount)

    @property
    def price_border_updated_(self):
        return modelMixin_DateAgo(self.price_border_updated)

    @property
    def updated_ago(self):
        return modelMixin_DateAgo(self.updated_at)


    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()


        # Hack for Mercatox & Selenium autopilot
        if self.account.stock.name == 'Mercatox' and self.mercatox_id == 0:
            self.autopilot = 0
        if self.status == 'new' and self.api_status == 'new':
            self.mercatox_id = 0


        if self.type == 'buy':
            if self.price > 0:
                self.amount = float(self.total_in_out) / float(self.price)
        elif self.type == 'sell':
            self.amount = self.total_in_out

        self.price = "{0:.8f}".format(self.price)
        self.total_in_out = "{0:.8f}".format(self.total_in_out)
        self.price_source = "{0:.8f}".format(self.price_source)
        self.price_history = "{0:.8f}".format(self.price_history)
        self.amount = "{0:.8f}".format(self.amount)

        return super(Orders, self).save(*args, **kwargs)
