from django.contrib import admin
from django.urls import reverse
# from django.forms import ModelForm, ChoiceField, forms, models
from django.utils.safestring import mark_safe

from .models import Orders as TasksOrders, Accounts as TasksAccounts


def toFixed(value, decimals=8):
    s = "{0:."+str(decimals)+"f}"
    return s.format(value)


@admin.register(TasksOrders)
class TaskAdmin(admin.ModelAdmin):

    list_display = ('id', 'autopilot', 'type', 'pair_name', 'account_email', "price_coin", "amount_coin", "status", "api_status", "updated_ago")
    list_display_links = ('id', 'type',)

    raw_id_fields = ("pair","account",)


    fields = ('created_at', 'account','pair','type','autopilot','price_source','price_history','pair_name','price','amount_','total_','total_in_out','price_border_','price_border_updated_','mercatox_id','stop_loss_percent','ignored_amount_usd','status','api_status', 'api_updated', 'updated_at', 'updated_ago')
    readonly_fields = ("pair_name", "account_email", "api_updated", "created_at", "updated_at", "updated_ago", 'mercatox_id','price_border_','price_border_updated_','amount_','total_',)


    # def pair_name(self, obj):
    #     return '{}'.format(obj.pair_id.pair)

    def amount_(self, obj):
        if obj.amount:
            return '{} {}'.format(toFixed(obj.amount), obj.pair.coin_from)
        else:
            return "—"

    def total_(self, obj):
        if obj.price and obj.total:
            return '{} {}'.format(toFixed(obj.price * obj.amount), obj.pair.coin_to)
        else:
            return "—"


    # def info(self, obj):
    #     # return 'no'
    #     return mark_safe('({} {}_{}) &nbsp;&nbsp;&nbsp; AMOUNT <b>{}</b> {} ___ PRICE <b>{}</b> {} = <b>{}</b> {}'.format(
    #         obj.type.upper(), obj.pair.coin_from, obj.pair.coin_to,
    #         obj.amount, obj.pair.coin_from,
    #         obj.price, obj.pair.coin_to,
    #         round((float(obj.price)*float(obj.amount)), 8), obj.pair.coin_to
    #     ))

    def price_border_(self, obj):
        if not obj.price_border:
            return '—'
        if obj.type == 'sell':
            return 'Minimal price: {} {}'.format(toFixed(obj.price_border), obj.pair.coin_to)
        elif obj.type == 'buy':
            return 'Maximal price: {} {}'.format(toFixed(obj.price_border), obj.pair.coin_to)


    def price_coin(self, obj):
        return '{1}: {0}'.format(obj.price, obj.pair.coin_to)

    def amount_coin(self, obj):
        return '{1}: {0}'.format(obj.amount, obj.pair.coin_from)

    def pair_name(self, obj):
        return mark_safe('<a href="{}">{} at {}</a>'.format(
            reverse("admin:market_pairs_change", args=(obj.pair.pk,)),
            obj.pair.pair, obj.pair.stock_name
        ))

    def account_email(self, obj):
        return mark_safe('<a href="{}">{} at {}</a>'.format(
            reverse("admin:tasks_accounts_change", args=(obj.account.pk,)),
            obj.account.email, obj.account.stock.name
        ))

@admin.register(TasksAccounts)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'email')
    list_display_links = ('id', 'email',)

    raw_id_fields = ("stock",)

    # def pair_id(self, obj):
    #     return obj.pair_id.pair

    # def get_queryset(self, request):
    #     return super(StocksAdmin, self).get_queryset(request).select_related(
    #         'pair_id')
