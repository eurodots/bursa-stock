# from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view


from tasks.fishrod.start import fishrodAnalytics
from tasks.fishrod.apply_price import fishRoadApplyPrice


@api_view(['GET','POST'])
def fishrodApplyNewPriceViewSet(request):

    if request.method == 'POST':

        order_id = request.data['order_id'] if request.data['order_id'] else False
        new_price = request.data['new_price'] if request.data['new_price'] else False

        if not order_id or not new_price:
            return Response({
                "error": True,
                "message": "Data is broken!",
            })


        applyResult = fishRoadApplyPrice(order_id, new_price)

        return Response(applyResult)

    # if request.method == 'GET':
    #     return Response({
    #         'ok': 'ok',
    #     })


@api_view(['GET'])
def fishrodViewSet(request):

    return Response({
        "error": False,
        "results": fishrodAnalytics(),
    })
