from tasks.models import Orders

# ————————————————————————————————————————————
# MERCATOX_STOCK_ID = 8
# MERCATOX_ACCOUNT_ID = 1
# ————————————————————————————————————————————

def toFixed(value, decimals=8):
    s = "{0:."+str(decimals)+"f}"
    return s.format(value)

def fishRoadApplyPrice(order_id, new_price):

    modelOrder = Orders.objects.get(pk=order_id)
    if modelOrder:

        if modelOrder.status == 'changeprice':
            return {
                "error": True,
                "message": "This order already in status 'changeprice', wait...",
            }


        # Check new_price & price_border
        price_border = float(modelOrder.price_border)
        new_price = float(new_price)
        if modelOrder.type == 'sell' and new_price < price_border:
            return {
                "error": True,
                "message": "New price lower than price border "+toFixed(price_border),
            }
        elif modelOrder.type == 'buy' and new_price > price_border:
            return {
                "error": True,
                "message": "New price bigger than price border " + toFixed(price_border),
            }


        modelOrder.price = new_price
        modelOrder.status = 'changeprice'
        modelOrder.save()

        return {
            "error": False,
            "message": "Price changed",
        }

    return {
        "error": True,
        "message": "Order not found ot wrong order status"
    }
