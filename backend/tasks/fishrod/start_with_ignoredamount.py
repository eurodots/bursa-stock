import datetime
# ————————————————————————————————————————————
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
django.setup()
# ————————————————————————————————————————————
from tasks.models import Orders as TasksOrders
from market.models import Pairs as MarketPairs
from DEFAULT.utils.functions import *
from tasks.fishrod.apply_price import fishRoadApplyPrice
from market.classes.Coinmarketcap import CMCap
# ————————————————————————————————————————————


STOP_LOSS_APPROX = 1 #погрешность в проценте для stop_loss_percent
PRICE_SHIFT = 0.00000001  #минимальная единица для перебивания цены ордера конкурента
MINIMAL_PERCENT_FOR_PRICE_CHANGING = 10


def getDifference(value1, value2):
    num1 = float(value1)
    num2 = float(value2)

    if num1 < num2:
        return (num1 / num2 * 100)
    else:
        return (num2 / num1 * 100)



def getMarketsPrice(type='sell', pair=['ETH','BTC']):


    price_book = []
    bestprice = 0

    modelOrders = MarketPairs.objects.filter(stock__name__in=['Binance','Liqui'], pair='_'.join(pair))
    if modelOrders:
        for m in modelOrders:

            if bestprice == 0:
                bestprice = m.bestprice_ask_ if type == 'sell' else m.bestprice_bid_

            price_book.append({
                'stock_name': m.stock.name,
                'bestprice': m.bestprice_ask_ if type == 'sell' else m.bestprice_bid_,
                'updated_at': m.updated_,
                'fastlink': m.fastlink_,
            })

    response_arr = {
        'coinmarketcap': CMCap().get(pair[1]).all(),
        'pricebook': price_book,
        'bestprice': bestprice,
    }


    # jprint(response_arr)
    return response_arr




def fishrodAnalytics():


    response_arr = []

    # modelOrders = TasksOrders.objects.filter(status__in=['new','progress','changeprice'], api_status__in=['new','progress'])
    modelOrders = TasksOrders.objects.all()


    for taskOrder in modelOrders:

        # d('ORDER #'+str(taskOrder.pk))


        order_id = taskOrder.id
        type = taskOrder.type
        coin_from = taskOrder.pair.coin_from
        coin_to = taskOrder.pair.coin_to


        price_usd_from = CMCap.get(coin_from).USD
        price_usd_to = CMCap.get(coin_to).USD


        price = taskOrder.price_
        amount = taskOrder.amount_
        total = price*amount

        stop_loss_percent = taskOrder.stop_loss_percent_
        ignored_amount_usd = taskOrder.ignored_amount_usd_

        arr_asks = taskOrder.pair.arr_asks_
        arr_bids = taskOrder.pair.arr_bids_


        #SELL: XRB_ETH
        # type = 'sell'
        # price =  0.05560000  #ETH
        # amount = 0.41998000  #XRB
        # total = price * amount #ETH
        # stop_loss_percent = 2.45

        #BUY: XRB_ETH
        # type = 'buy'
        # price = 0.01220000  # ETH
        # amount = 26.9590102  # XRB
        # total = price * amount
        # stop_loss_percent = 1.45

        # arr_asks = [[0.05437,     0.82646882], [0.05437399, 0.931521], [0.05480000, 3.30230486], [0.05560000, 0.41998000], [0.05579989, 0.004375], [0.05609989, 0.10488273], [0.0561, 0.000138], [0.05628999, 0.0154357], [0.05714999, 0.07620842], [0.05774, 1.47796246], [0.05775, 0.26766412], [0.058, 2.18489805], [0.05808, 16.99], [0.0580847, 2.0], [0.05808476, 0.32686514], [0.05808799, 3.07188362], [0.0588, 0.05285537], [0.0594899, 4.1210906], [0.0595, 0.01248001], [0.062, 0.01], [0.062169, 0.0313081], [0.06289999, 0.4], [0.064, 0.01], [0.06431, 0.18], [0.066, 1.129243], [0.067999, 0.5], [0.0679993, 0.42374641], [0.069999, 0.41227001], [0.0714, 0.0196], [0.075, 4.01721554], [0.0769, 0.10546666], [0.0779, 0.05], [0.0796, 0.96580941], [0.0817999, 0.0137795], [0.082, 0.002], [0.0826, 0.00588], [0.0829105, 0.008], [0.083, 0.001], [0.0830981, 0.01254], [0.0831, 7.09665157], [0.084, 0.001], [0.085, 0.0030323], [0.086, 0.001], [0.087, 0.13155831], [0.08779459, 0.0553329], [0.088, 0.001], [0.089, 0.001], [0.09, 0.001], [0.09093, 0.05340715], [0.091, 2.10903652], [0.09100005, 0.00571921], [0.0942, 0.022867], [0.095, 3.0], [0.0975, 2.00485657], [0.1, 0.02942099], [0.11, 0.09809918], [0.11389984, 1.26294654], [0.119, 0.04735536], [0.1202, 0.03572662], [0.12311, 0.001745], [0.1259, 0.12661262], [0.14499989, 0.03556985], [0.15, 0.10655458], [0.16, 0.01], [0.181, 0.2660358], [0.19, 0.00061834], [0.1932, 1.5524], [0.2, 106.0], [0.24, 1.003664], [0.31, 0.034452], [0.52, 2.542], [0.68, 1.51207037], [0.99999999, 0.01091128], [20.0, 0.022886]]
        #             # price ETH   amount XRB
        # arr_bids = [[0.01329663,  9.964648], [0.0125979, 75.17645877], [0.01245301, 5.46356503], [0.01220000, 26.9590102], [0.01200001, 0.25], [0.012, 24.7013], [0.0116, 5.3], [0.01, 2189.2907871], [0.005, 0.55], [0.00010001, 1.0], [0.0001, 1.0], [8e-05, 2.0], [6.5e-05, 3.0], [5e-05, 6.0], [2.601e-05, 100.0], [2.6e-05, 17.0], [2.4e-05, 24.0], [2.3e-05, 11.0], [1.101e-05, 5000.0], [1.1e-05, 12.0], [1.07e-05, 57.0], [6.7e-06, 76.0], [6.1e-06, 356.0], [5.4e-06, 455.0]]



        def ordersBefore(type, orders_arr):
            orders_before_total = []

            if type == 'sell':
                orders_before_total = [a for a in orders_arr if a[0] < price]
            elif type == 'buy':
                orders_before_total = [a for a in orders_arr if a[0] > price]

            return orders_before_total



        def direction(type, orders_arr):

            MESSAGES = []
            NEW_PRICE_APPLY = False
            PRICE_BORDER = False

            orders_before_total = ordersBefore(type, orders_arr)
            orders_before_total_sum = [a[0] * a[1] for a in orders_before_total]
            orders_before_total_sum = sum(orders_before_total_sum)
            orders_before_total_usd = round((orders_before_total_sum * price_usd_to), 2)


            # Проверяем какой у нас price border
            price_market = getMarketsPrice(type, [coin_from, coin_to])
            border_bestprice = price_market['bestprice']


            if type == 'sell':
                PRICE_BORDER = border_bestprice + (border_bestprice / 100 * (stop_loss_percent + STOP_LOSS_APPROX))
            elif type == 'buy':
                PRICE_BORDER = border_bestprice - (border_bestprice / 100 * (stop_loss_percent + STOP_LOSS_APPROX))


            # 1. узнаем какой объем до нас в USD
            # 2. проверяем если объем больше чем amount_ignored

            if len(orders_before_total) > 0 and orders_before_total_usd > ignored_amount_usd:

                # находим ордер перед нами с наибольшим объемом и проверяем цену цену наибольшего ордера по отношению к нашей
                # find_bigger_order_by_amount = sorted(orders_before_total, key=lambda k: k[1], reverse=True)[0]


                # если sell то в одну сторону
                if type == 'sell':

                    # фиксируем наш игнорируемый объем
                    ignored_amount_usd_ = ignored_amount_usd

                    for order in orders_arr:

                        order_price = order[0]
                        order_volume = order[1]
                        order_total = order_price * order_volume
                        order_total_usd = order_total * price_market['coinmarketcap']['prices']['USD']



                        # проверяем остаток минимального объема ордеров
                        ignored_amount_usd_ -= order_total_usd

                        if ignored_amount_usd_ <= 0:
                            if order_price > PRICE_BORDER:
                                tmp_price = order_price - PRICE_SHIFT

                                # защита от дурака
                                if tmp_price > PRICE_BORDER:
                                    NEW_PRICE_APPLY = tmp_price
                                    MESSAGES = [
                                        'встаем ниже ордера',
                                        'order price: ' + toFixed(order_price),
                                        'наш price: ' + toFixed(tmp_price),
                                        'minimum price : ' + toFixed(PRICE_BORDER),
                                    ]
                                else:
                                    NEW_PRICE_APPLY = order_price
                                    MESSAGES = [
                                        'встаем по цене ордера'
                                        'order price: ' + toFixed(order_price),
                                        'наш price: ' + toFixed(order_price),
                                        'minimum price : ' + toFixed(PRICE_BORDER),
                                    ]

                                break
                            else:
                                NEW_PRICE_APPLY = PRICE_BORDER
                                MESSAGES = [
                                    'уперлись в stop_loss: ' + toFixed(stop_loss_percent, 2) + '% (+' + toFixed(
                                        STOP_LOSS_APPROX, 2) + '%)',
                                    'встаем по минималке: ' + toFixed(PRICE_BORDER),
                                ]
                                break


                # если buy то в другую сторону
                elif type == 'buy':

                    # фиксируем наш игнорируемый объем
                    ignored_amount_usd_ = ignored_amount_usd

                    for order in orders_arr:

                        order_price = order[0]
                        order_volume = order[1]
                        order_total = order_price * order_volume
                        order_total_usd = order_total * price_market['coinmarketcap']['prices']['USD']



                        # проверяем остаток минимального объема ордеров
                        ignored_amount_usd_ -= order_total_usd

                        if ignored_amount_usd_ <= 0:
                            if order_price < PRICE_BORDER:
                                tmp_price = order_price + PRICE_SHIFT

                                # защита от дурака
                                if tmp_price < PRICE_BORDER:
                                    NEW_PRICE_APPLY = tmp_price
                                    MESSAGES = [
                                        'встаем выше ордера',
                                        'order price: '+toFixed(order_price),
                                        'наш price: '+toFixed(tmp_price),
                                        'maximum price : ' + toFixed(PRICE_BORDER),
                                    ]
                                else:
                                    NEW_PRICE_APPLY = order_price
                                    MESSAGES = [
                                        'встаем по цене ордера'
                                        'order price: ' + toFixed(order_price),
                                        'наш price: ' + toFixed(order_price),
                                        'maximum price : ' + toFixed(PRICE_BORDER),
                                    ]
                                break
                            else:
                                NEW_PRICE_APPLY = PRICE_BORDER
                                MESSAGES = [
                                    'уперлись в stop_loss: '+toFixed(stop_loss_percent, 2)+'% (+'+toFixed(STOP_LOSS_APPROX, 2)+'%)',
                                    'встаем по максималке: '+toFixed(PRICE_BORDER),
                                ]
                                break




            # a < b = ((b - a) / a) * 100
            # a > b = ((a - b) / b) * 100


            def priceDifference(type, price_calc):
                price_calc = float(price_calc)

                if not price_market['bestprice'] or not price_calc:
                    return False


                if type == 'sell':
                    percent = ((price_calc - price_market['bestprice']) / price_market['bestprice']) * 100

                elif type == 'buy':
                    percent = ((price_market['bestprice'] - price_calc) / price_calc) * 100

                percent_to_usd = ((total / 100) * percent) * price_usd_to
                # print('TYPE',type)
                # print('total',total)
                # print('price',price)
                # print('percent',percent)
                # print('price_usd_to', price_usd_to)
                # print('percent_to_usd', percent_to_usd)
                # print('---------')

                return {
                    'percent': round(percent, 2),
                    'usd': round(percent_to_usd, 2),
                }



            # Generate ordersbook for this task
            ordersbook = []
            arr_orders_by_type = arr_asks if type == 'sell' else arr_bids
            for o in arr_orders_by_type:
                ordersbook.append({
                    'price': toFixed(o[0]),
                    'amount': toFixed(o[1]),
                    'total': toFixed(o[0] * o[1]),
                })




            # Save PRICE_BORDER into model
            # && Update volume depends of the ordee type
            if PRICE_BORDER:
                taskOrder.price_border = PRICE_BORDER
                taskOrder.price_border_updated = datetime.datetime.now()
                taskOrder.save()



            ERRORS_ARR = []

            # Stop new_price changing if difference beetwee new price in old less then ...%
            if getDifference(price, NEW_PRICE_APPLY) < MINIMAL_PERCENT_FOR_PRICE_CHANGING or price == NEW_PRICE_APPLY or \
                (getDifference(price, NEW_PRICE_APPLY) - stop_loss_percent) < 1: #if less then 1 %

                NEW_PRICE_APPLY = False
                MESSAGES = []

            else:

                # Check if autopilot
                if PRICE_BORDER and NEW_PRICE_APPLY:
                    if taskOrder.autopilot:
                        autopilot_message = fishRoadApplyPrice(order_id, NEW_PRICE_APPLY)
                        autopilot_message = 'Autopilote message: ' + autopilot_message['message']
                        MESSAGES += [autopilot_message]
                        print(autopilot_message)



                # ERRORS
                if type == 'sell' and price < PRICE_BORDER:
                    ERRORS_ARR.append('PRICE TO SMALL!!!')
                elif type == 'buy' and price > PRICE_BORDER:
                    ERRORS_ARR.append('PRICE TO BIG!!!')

                if NEW_PRICE_APPLY and taskOrder.api_status == 'progress':
                    ERRORS_ARR.append('New price is ' + toFixed(NEW_PRICE_APPLY) + ' '+coin_to)



            # print(MESSAGES)
            return {
                'order_id': order_id,
                'type': type,
                'autopilot': taskOrder.autopilot,
                'pair': [coin_from, coin_to],
                'fastlink': taskOrder.pair.fastlink_,
                'amount': float(taskOrder.amount),
                'total': total,
                'total_usd': round((total * price_usd_to), 2),
                'price': float(price),
                'price_difference': priceDifference(type, price),

                'price_source': float(taskOrder.price_source),
                'price_source_difference': priceDifference(type, taskOrder.price_source),

                'price_history': float(taskOrder.price_history),
                'stop_loss_percent': taskOrder.stop_loss_percent,
                'ignored_amount_usd': ignored_amount_usd,
                'account_email': taskOrder.account.email,

                'stock_id': taskOrder.account.stock.id,
                'stock_name': taskOrder.account.stock.name,
                'status': taskOrder.status,
                'api_status': taskOrder.api_status,
                'api_updated': dateConverter(taskOrder.api_updated),
                'updated_at': dateConverter(taskOrder.updated_at),
                'created_at': dateConverter(taskOrder.created_at),

                'new_price': NEW_PRICE_APPLY,
                'new_price_difference': priceDifference(type, NEW_PRICE_APPLY),
                'price_border': toFixed(PRICE_BORDER),
                'total_before_us': orders_before_total_usd,
                'messages': MESSAGES,
                'orderbook_updated': dateConverter(taskOrder.pair.updated_at),

                'admin_url': '/admin/tasks/orders/'+str(order_id)+'/change/',

                'market': price_market,
                'ordersbook': ordersbook,
                'errors': ERRORS_ARR,

            }



        if type == 'sell':
            task_response = direction('sell', arr_asks)

        elif type == 'buy':
            task_response = direction('buy', arr_bids)




        # print(task_response)
        response_arr.append(task_response)

    jprint(response_arr)
    return response_arr

if __name__ == "__main__":
    fishrodAnalytics()
    # getMarketsPrice()
    # t = checkPriceBinance('asks',['OMG','ETH'])
    # jprint(t)

    pass
