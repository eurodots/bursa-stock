# import datetime
from django.utils import timezone


# ————————————————————————————————————————————
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
django.setup()
# ————————————————————————————————————————————
from tasks.models import Orders as TasksOrders
from market.models import Pairs as MarketPairs
from DEFAULT.utils.functions import *
from tasks.fishrod.apply_price import fishRoadApplyPrice
from market.classes.Coinmarketcap import CMCap
# ————————————————————————————————————————————


STOP_LOSS_APPROX = 1 #погрешность в проценте для stop_loss_percent
PRICE_SHIFT = 0.00000001  #минимальная единица для перебивания цены ордера конкурента
MINIMAL_PERCENT_FOR_PRICE_CHANGING = 10
PRICEBOOK_DATE_LIMIT = 10 # in minutes

def getDifference(value1, value2):
    num1 = float(value1)
    num2 = float(value2)

    if num1 < num2:
        return (num1 / num2 * 100)
    else:
        return (num2 / num1 * 100)



def getMarketsPrice(type='sell', pair=['ETH','BTC']):


    price_book = []
    bestprice = 0
    MESSAGES = []

    date_checker = timezone.now() - timezone.timedelta(minutes=PRICEBOOK_DATE_LIMIT)


    modelOrders = MarketPairs.objects.filter(stock__name__in=['Binance','Liqui'], pair='_'.join(pair))
    if modelOrders:
        for m in modelOrders:

            if bestprice == 0:
                bestprice = m.bestprice_ask_ if type == 'sell' else m.bestprice_bid_


            # Price updated date checker
            if m.updated_at < date_checker:
                MESSAGES.append('Date of '+m.stock.name+' are old!')


            pricebook_price = m.bestprice_ask_ if type == 'sell' else m.bestprice_bid_
            price_book.append({
                'stock_name': m.stock.name,
                'bestprice': toFixed(pricebook_price),
                'updated_at': m.updated_,
                'fastlink': m.fastlink_,
            })

    response_arr = {
        'coinmarketcap': CMCap().get(pair[1]).all(),
        'pricebook': price_book,
        'bestprice': bestprice,
        'messages': MESSAGES,
    }


    # jprint(response_arr)
    return response_arr




def fishrodAnalytics():


    response_arr = []

    # modelOrders = TasksOrders.objects.filter(status__in=['new','progress','changeprice'], api_status__in=['new','progress'])
    modelOrders = TasksOrders.objects.all()


    for taskOrder in modelOrders:

        # d('ORDER #'+str(taskOrder.pk))


        order_id = taskOrder.id
        type = taskOrder.type
        coin_from = taskOrder.pair.coin_from
        coin_to = taskOrder.pair.coin_to


        price_usd_from = CMCap.get(coin_from).USD
        price_usd_to = CMCap.get(coin_to).USD


        price = float(toFixed(taskOrder.price_, 8))
        amount = taskOrder.amount_
        total = price*amount

        stop_loss_percent = taskOrder.stop_loss_percent_
        ignored_amount_usd = taskOrder.ignored_amount_usd_

        arr_asks = taskOrder.pair.arr_asks_
        arr_bids = taskOrder.pair.arr_bids_



        def direction(type, orders_arr):

            MESSAGES = []
            NEW_PRICE_APPLY = False
            PRICE_BORDER = False


            # Поставить проверку, что б ордеры были свежие, иначе автопилот не делать


            # BORDERS
            market = getMarketsPrice(type, [coin_from, coin_to])
            market_price = market['bestprice']

            if type == 'sell':
                BORDER_TOP = market_price + (market_price / 100 * (stop_loss_percent + 5))
                BORDER_MIDDLE = market_price + (market_price / 100 * (stop_loss_percent + 2.5))
                BORDER_BOTTOM = market_price + (market_price / 100 * (stop_loss_percent))
            elif type == 'buy':
                BORDER_TOP = market_price - (market_price / 100 * (stop_loss_percent + 5))
                BORDER_MIDDLE = market_price - (market_price / 100 * (stop_loss_percent + 2.5))
                BORDER_BOTTOM = market_price - (market_price / 100 * (stop_loss_percent))


            PRICE_BORDER = BORDER_BOTTOM
            first_order_price = orders_arr[0][0]
            price_with_shift = price


            if type == 'sell':
                price_with_shift = first_order_price - PRICE_SHIFT
            elif type == 'buy':
                price_with_shift = first_order_price + PRICE_SHIFT

            if price_with_shift <= BORDER_BOTTOM:
                if price != first_order_price:
                    NEW_PRICE_APPLY = price_with_shift
                    MESSAGES = ['Перебиваем ордер с ценой ' + toFixed(first_order_price) + ' на цену ' + toFixed(NEW_PRICE_APPLY)]
                # Аннулируем NEW_PRICE_APPLY если текущая наша цена равна NEW_PRICE_APPLY
                if price == NEW_PRICE_APPLY:
                    NEW_PRICE_APPLY = False
                    MESSAGES = []
            elif price < BORDER_TOP or price > BORDER_BOTTOM:
                NEW_PRICE_APPLY = BORDER_MIDDLE
                MESSAGES = [
                    toFixed(BORDER_MIDDLE)  +' Ставим BORDER_MIDDLE',
                    '----',
                    toFixed(BORDER_TOP)     +' BORDER_TOP',
                    toFixed(BORDER_MIDDLE)  +' BORDER_MIDDLE',
                    toFixed(BORDER_BOTTOM)  +' BORDER_BOTTOM',
                ]

            NEW_PRICE_APPLY = float(toFixed(NEW_PRICE_APPLY, 8)) # На всякий случай до 8 знаков


            def priceDifference(type, price_calc):
                price_calc = float(price_calc)

                if not market or not price_calc:
                    return False

                if type == 'sell':
                    percent = ((price_calc - market_price) / market_price) * 100

                elif type == 'buy':
                    percent = ((market_price - price_calc) / price_calc) * 100
                percent_to_usd = ((total / 100) * market_price) * price_usd_to


                return {
                    'percent': round(percent, 2),
                    'usd': round(percent_to_usd, 2),
                }



            # Generate ordersbook for this task
            ordersbook = []
            arr_orders_by_type = arr_asks if type == 'sell' else arr_bids
            for o in arr_orders_by_type:

                differnce_percent = toFixed((( (float(market_price) - float(toFixed(o[0]))) / float(toFixed(o[0]))  ) * 100 ), 1) # Beetween binance and order price

                ordersbook.append({
                    'price': toFixed(o[0]),
                    'amount': toFixed(o[1]),
                    'total': toFixed(o[0] * o[1]),
                    'difference': differnce_percent,
                })




            # Save PRICE_BORDER into model
            # && Update volume depends of the ordee type
            if PRICE_BORDER:
                taskOrder.price_border = PRICE_BORDER
                taskOrder.price_border_updated = timezone.now()
                taskOrder.save()



            ERRORS_ARR = []

            # Check if autopilot
            if PRICE_BORDER and NEW_PRICE_APPLY:
                if taskOrder.autopilot:
                    autopilot_message = fishRoadApplyPrice(order_id, NEW_PRICE_APPLY)
                    autopilot_message = 'Autopilote message: ' + autopilot_message['message']
                    MESSAGES += [autopilot_message]
                    print(autopilot_message)



            # ERRORS
            if type == 'sell' and price < PRICE_BORDER:
                ERRORS_ARR.append('PRICE TO SMALL!!!')
            elif type == 'buy' and price > PRICE_BORDER:
                ERRORS_ARR.append('PRICE TO BIG!!!')

            if NEW_PRICE_APPLY and taskOrder.api_status == 'progress':
                ERRORS_ARR.append('New price is ' + toFixed(NEW_PRICE_APPLY) + ' '+coin_to)


            # From pricebook (about older date)
            for m in market['messages']:
                MESSAGES.append(m)


            # print(MESSAGES)
            return {
                'order_id': order_id,
                'type': type,
                'autopilot': taskOrder.autopilot,
                'pair': [coin_from, coin_to],
                'fastlink': taskOrder.pair.fastlink_,
                'amount': float(taskOrder.amount),
                'total': total,
                'total_usd': round((total * price_usd_to), 2),
                'price': float(price),
                'price_difference': priceDifference(type, price),

                'price_source': float(taskOrder.price_source),
                'price_source_difference': priceDifference(type, taskOrder.price_source),

                'price_history': float(taskOrder.price_history),
                'stop_loss_percent': taskOrder.stop_loss_percent,
                'ignored_amount_usd': ignored_amount_usd,
                'account_email': taskOrder.account.email,

                'stock_id': taskOrder.account.stock.id,
                'stock_name': taskOrder.account.stock.name,
                'status': taskOrder.status,
                'api_status': taskOrder.api_status,
                'api_updated': dateConverter(taskOrder.api_updated),
                'updated_at': dateConverter(taskOrder.updated_at),
                'created_at': dateConverter(taskOrder.created_at),

                'new_price': NEW_PRICE_APPLY,
                'new_price_difference': priceDifference(type, NEW_PRICE_APPLY),
                'price_border': toFixed(PRICE_BORDER),
                # 'total_before_us': orders_before_total_usd,
                'messages': MESSAGES,
                'orderbook_updated': dateConverter(taskOrder.pair.updated_at),

                'admin_url': '/admin/tasks/orders/'+str(order_id)+'/change/',

                'market': market,
                'ordersbook': ordersbook,
                'errors': ERRORS_ARR,

            }



        if type == 'sell':
            task_response = direction('sell', arr_asks)

        elif type == 'buy':
            task_response = direction('buy', arr_bids)




        # print(task_response)
        response_arr.append(task_response)

    # jprint(response_arr) #jprint does not work
    # print(response_arr)
    return response_arr

if __name__ == "__main__":
    fishrodAnalytics()
    # getMarketsPrice()
    # t = checkPriceBinance('asks',['OMG','ETH'])
    # jprint(t)

    pass
