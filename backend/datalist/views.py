#from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
# ————————————————————————————————————————————
from .utils.coinslist import CoinsList
from .utils.coinstlist_current import CurrentCoinData
from .utils.ethplorer import ethplorerHistory
from .utils.price_page import priceData
# ————————————————————————————————————————————


@api_view(['GET'])
def coinsViewSet(request):

    return Response({
        'results': CoinsList(),
    })




@api_view(['GET'])
def currentCoinViewSet(request, coin):

    return Response( CurrentCoinData(coin) )


@api_view(['GET'])
def ethplorerViewSet(request, token):

    return Response({
        'results': ethplorerHistory(token)
    })


# PRICE LIST PAGE
@api_view(['GET'])
def pricelistViewSet(request):

    return Response({
        'error': False,
        'results': priceData(),
    })

