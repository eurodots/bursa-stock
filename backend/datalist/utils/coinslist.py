
# ————————————————————————————————————————————
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
django.setup()
# ————————————————————————————————————————————
from market.models import Coins as MarketCoins
# ————————————————————————————————————————————
from DEFAULT.utils.functions import *
from DEFAULT.utils.blacklist import globalBlacklist
from DEFAULT.utils.price_converter import BtcPriceExtender
# ————————————————————————————————————————————

def CoinsList():

    coins_arr = []

    modelMarketCoins = MarketCoins.objects.all().exclude(ticker__in=globalBlacklist())
    for coin in modelMarketCoins:

        check_name = False if coin.ticker == coin.name else coin.name

        # Final json
        coin_item = {
            'id': coin.ticker,
            'ticker': coin.ticker,
            'name': check_name,
            'bestprice_ask': BtcPriceExtender(coin.bestprice_ask_btc_),
            'bestprice_bid': BtcPriceExtender(coin.bestprice_bid_btc_),
            'token': coin.token_eth_,
            'favicon': coin.favicon_src(),
            'updated_at': coin.updated_,
        }

        coins_arr.append(coin_item)

    # jprint(coins_arr)
    return coins_arr


if __name__ == "__main__":
    CoinsList()

    pass