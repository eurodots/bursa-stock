

def priceData():
    data = [
        {
            'priority': False,
            'url': '/p/user/stocks/add/free',
            'plan': "Start",
            'price': "FREE",
            'list': [
                {
                    'label': "Always free",
                },
                {
                    'label': "-",
                },
                {
                    'label': "-",
                },
                {
                    'label': "-",
                },
                {
                    'label': "-",
                },
            ]
        },
        {
            'priority': True,
            'url': '/p/user/stocks/add/pro',
            'plan': "Pro",
            'price': "$129",
            'list': [
                {
                    'label': "MobileApp",
                },
                {
                    'label': "Customization",
                },
                {
                    'label': "Adding to list",
                },
                {
                    'label': "-",
                },
                {
                    'label': "-",
                },
            ]
        },
        {
            'priority': False,
            'url': '/p/user/stocks/add/enterprise',
            'plan': "Enterprise",
            'price': "Individual",
            'list': [
                {
                    'label': "MobileApp",
                },
                {
                    'label': "Customization",
                },
                {
                    'label': "Own Smart Contract",
                },
                {
                    'label': "Marketing Solutions",
                },
                {
                    'label': "White-label",
                },
            ]
        },
    ]

    return data