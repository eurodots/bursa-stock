from urllib.parse import urlsplit
# ————————————————————————————————————————————
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
django.setup()
# ————————————————————————————————————————————
from market.models import Coins as MarketCoins, Coins_Links as MarketCoinsLinks, Pairs as MarketPairs
from stocks.models import Customers as StocksCustomers
# ————————————————————————————————————————————
from DEFAULT.utils.functions import *
# from DEFAULT.utils.price_converter import BtcPriceExtender
# ————————————————————————————————————————————
from market.utils.ethplorer import ethplorerTxsChecker
# ————————————————————————————————————————————



def fetchCustomerToken(token):
    modelStocksCustomers = StocksCustomers.objects.filter(token_address=token)
    if modelStocksCustomers:
        m = StocksCustomers.objects.get(token_address=token)

        # Fetch links
        links_arr = []
        for link in m.token_links.split('||'):
            link_label = "{0.netloc}/".format(urlsplit(link))
            link_label = link_label.replace('/','').replace('www.','')
            links_arr.append({
                'label': link_label,
                'link': link,
            })


        agregated_data = {
            'ticker': m.token_ticker,
            'name': m.token_name,
            'favicon': m.favicon_src(),
            'platform': m.token_platform,
            'links': links_arr,
            'token': m.token_address,
            'pairs': {},
            'updated_at': m.created_at_,
            'coinmarketcap': False,
            # 'ethplorer': ethplorerTxsChecker(m.token_address),
            'ethplorer': True,
        }
        return {
            'error': False,
            'results': agregated_data,
        }

    return {
        'error': True,
        'message': 'Coin not found!',
    }

def CurrentCoinData(tokenOrCoin=False):

    if tokenOrCoin[0:2] == '0x':
        modelMarketCoins = MarketCoins.objects.filter(token=tokenOrCoin)
    else:
        coin = tokenOrCoin.upper()
        modelMarketCoins = MarketCoins.objects.filter(ticker=coin)

    if not modelMarketCoins:

        return fetchCustomerToken(token=tokenOrCoin)


    if modelMarketCoins:
        m = modelMarketCoins.get()


        # Fetch links
        links_arr = []
        modelLinks = MarketCoinsLinks.objects.filter(coin=m.pk)
        for link in modelLinks:
            links_arr.append({
                'label': link.label,
                'link': link.value,
            })


        # Fetch pairs & stocks
        pairs_arr = {}
        modelMarketPairs = MarketPairs.objects.filter(coin_from=m.ticker, stock__active=1)
        for pair in modelMarketPairs:

            if not pair.coin_to in pairs_arr:
                pairs_arr[pair.coin_to] = []

            pairs_arr[pair.coin_to].append({
                'stock_id': pair.stock.pk,
                'stock_name': pair.stock_name,
                'fastlink': pair.fastlink_,
                'pair': pair.pair,
                # 'arr_asks': pair.arr_asks_,
                # 'arr_bids': pair.arr_bids_,
                'bestprice_ask': pair.bestprice_ask_,
                'bestprice_bid': pair.bestprice_bid_,
                'updated_at': pair.updated_,
            })


        # Fetch coinmarketcap
        coinmarketcap_arr = False
        if m.coinmarketcap:
            coinmarketcap_arr = {
                'rank': m.coinmarketcap.rank,
                'percent_change_1h': m.coinmarketcap.percent_change_1h_,
                'percent_change_24h': m.coinmarketcap.percent_change_24h_,
                'percent_change_7d': m.coinmarketcap.percent_change_7d_,
            }


        # Final json
        check_name = False if m.ticker == m.name else m.name
        agregated_data = {
            'ticker': m.ticker,
            'name': check_name,
            'favicon': m.favicon_src(),
            'platform': m.platform_,
            'links': links_arr,
            'token': m.token_eth_,
            'pairs': pairs_arr,
            'updated_at': m.updated_,
            'coinmarketcap': coinmarketcap_arr,
            # 'ethplorer': ethplorerTxsChecker(m.token_eth_),
            'ethplorer': True,
        }

        return {
            'error': False,
            'results': agregated_data,
        }



if __name__ == "__main__":
    jprint( CurrentCoinData('eth') )

    pass