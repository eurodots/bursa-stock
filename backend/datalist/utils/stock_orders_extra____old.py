from random import randint
# ———————————————DJANGO ENVIRON———————————————
from DEFAULT.utils.django_setup import *
from DEFAULT.utils.functions import *
# ————————————————————————————————————————————
from market.models import Coins as MarketCoins
from API.utils.Calc import calculatorMath
# ————————————————————————————————————————————



def rendomCalculator(ticker, coin_id):


    # PREPARE ITERATORS FOR ORDERS
    coin_id_ = (10 + int(str(coin_id)[0:1])) / 100
    iter_asks = int(coin_id_ * 40)
    iter_bids = int(coin_id_ * 60)


    calc = calculatorMath('ETH', ticker, 1)
    calc_price = calc['results']['answer']['price']
    calc_price = float(calc_price)



    book = {
        'sell': {
            # 'type': 'sell',
            'range': [94, 130],
            'price_extra': 0.1,
            'volume': 3,
            'user_address': 4511,  # any random
        },
        'buy': {
            # 'type': 'buy',
            'range': [94, 130],
            'price_extra': 0.1,
            'volume': 3,
            'user_address': 462,  # any random
        },
    }

    orders_arr = []

    def iterations(type, iter):

        # arr = []
        b = book[type]

        for i in range(0,iter):
            price = b['price_extra'] / 100
            price_range = price * randint(b['range'][0], b['range'][1])
            price_range = calc_price + (price_range * calc_price)

            volume = b['volume'] / 100
            volume_range = volume * randint(b['range'][0], b['range'][1])

            total = price_range * volume_range
            order = {
                'type': type,
                'user_address': ''.join([str(coin_id), str(i), str(b['user_address'])]),
                'price': price_range,
                'amount': volume_range,
                'total': total,
                'spot_id': 0,
                'wei_amount': False,
                'wei_price': False,
            }
            orders_arr.append(order)

        # return arr

    # for i in iter_asks:
    iterations('sell', iter_asks)
    iterations('buy', iter_bids)

    # jprint(orders_arr)



    return orders_arr


def stockOrdersExtra(token):
    modelMarketCoins = MarketCoins.objects.filter(token=token)
    if modelMarketCoins:
        m = MarketCoins.objects.get(token=token)

        orders_arr = rendomCalculator(m.ticker, m.pk)
        # jprint(orders_arr)
        return {
            'error': False,
            'results': orders_arr,
        }

    return {
        'error': True,
        'message': 'Token not found',
    }



if __name__ == "__main__":
    # stockOrdersExtra('0x4cf488387f035ff08c371515562cba712f9015d4')

    pass