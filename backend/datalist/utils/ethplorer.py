import json
import requests
import datetime
# ————————————————————————————————————————————




def ethplorerHistory(token=False):

    if token:
        url = 'https://api.ethplorer.io/getPriceHistoryGrouped/' + token + '?apiKey=freekey'
        r = requests.get(url)
        data = json.loads(r.content.decode('utf8'))

        if 'error' in data:
            return False


        if len(data['history']['prices']) == 0:
            data['history']['prices'] = defaultHistoryPrices()

        return data

    else:
        return False




# DEMO PRICES
def defaultHistoryPrices():


    prices_arr = []
    total_days = 5
    for i in range(0, total_days):

        date = datetime.datetime.today() - datetime.timedelta(days=total_days-i)
        date = date.strftime('%Y-%m-%d')

        item = {
            "ts": 0,
            "date": date,  # 2018-03-03
            "hour": 0,
            "open": 0,
            "close": 0,
            "high": 0,
            "low": 0,
            "volume": 0,
            "volumeConverted": 0,
            "cap": 0,
            "average": 0,
        }

        prices_arr.append(item)


    return prices_arr




if __name__ == "__main__":
    # test()

    pass