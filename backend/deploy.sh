GIT="/home/admin/web/stock2/backend/equalizer_django"
WEB="/home/admin/web/kupi.net"


# Start parity
pkill -f parity
parity --chain ropsten &

# Kill uwsgi tasks
pkill -f uwsgi

# Files operations...
cd $GIT && git pull

cd $WEB
rm -rf $WEB/public_html
cp -r $GIT $WEB/public_html

PROJECT=$WEB/public_html/DEFAULT
rm $PROJECT/settings_local.py
mv $PROJECT/settings_public.py $PROJECT/settings_local.py

MEDIA=$WEB/public_html/media
unlink $MEDIA/reactjs
mv $MEDIA/reactjs__ $MEDIA/reactjs


cd $WEB/public_html
python3 manage.py collectstatic
python3 manage.py makemigrations
python3 manage.py migrate


/bin/sleep 2
uwsgi --http-keepalive --http :8000 --module DEFAULT.wsgi &
# uwsgi --http :8000 --module DEFAULT.wsgi
