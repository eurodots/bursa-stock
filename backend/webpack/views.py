from django.shortcuts import render
from lockdown.decorators import lockdown

# Create your views here.


# @lockdown()
def webpackViewSet(request):
    # test = "Hi Mark"
    # form = IcosignupForm(request.POST or None)
    #
    # if request.method == 'POST' and form.is_valid():
    #
    #     token_ticker = request.POST.getlist('token_ticker')[0].upper()
    #
    #     new_form = form.save()
    #     return render(request, 'default/success.html', {
    #         'token_ticker': token_ticker
    #     }, locals())

    # return render(request, 'reactjs/inner.html', locals())
    return render(request, 'webpack/bundle.html', locals())
