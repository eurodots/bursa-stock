import json

def toFixed(value, decimals=8):
    if value:
        s = "{0:."+str(decimals)+"f}"
        return s.format(value)
    else:
        return 0


def dateConverter(value):
    # return json.dumps(value, indent=4, sort_keys=True, default=str)
    if not value is None:
        return value.strftime('%Y-%m-%d %H:%M:%S %Z%z')

    return False

def split(arr, size):
    arrs = []
    while len(arr) > size:
        pice = arr[:size]
        arrs.append(pice)
        arr = arr[size:]
    arrs.append(arr)
    return arrs



def d(value=False):
    print('\n\n-------',value,'-------')


def jprint(arr):
    print(json.dumps(arr, indent=4, sort_keys=False))



if __name__ == "__main__":
    pass
