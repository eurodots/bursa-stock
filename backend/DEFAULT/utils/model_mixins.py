from datetime import datetime



def modelMixin_DateAgo(field):
    time = datetime.now()

    if field == 0:
        return 'The date not specified'

    if field.day == time.day:
        return str(time.hour - field.hour) + " hours ago"
    else:
        if field.month == time.month:
            return str(time.day - field.day) + " days ago"
        else:
            if field.year == time.year:
                return str(time.month - field.month) + " months ago"
    return field