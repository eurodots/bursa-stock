
from market.models import Stocks as MarketStocks
from settings.models import Global as GlobalSettings


def globalBlacklist(): # Get blacklist arr

    # global GLOBAL_BLACKLIST

    prohibit_coins = []
    modelStocks = MarketStocks.objects.filter(active=1)
    for s in modelStocks:
        blacklist = s.blacklist.split('\r\n') + s.holdlist.split('\r\n')
        for coin in blacklist:
            coin = coin.split('#', 1)[0].strip()
            if len(coin) > 0:
                prohibit_coins.append(coin)


    # Append global blacklist
    globalCoins = GlobalSettings.objects.filter(name='blacklist').get()
    globalCoins_arr = globalCoins.value.split(',')
    globalCoins_arr = [c.split('#',1)[0].strip() for c in globalCoins_arr]

    return set(prohibit_coins + globalCoins_arr)