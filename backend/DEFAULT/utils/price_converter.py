# ————————————————————————————————————————————
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
django.setup()
# ————————————————————————————————————————————
from market.models import Coinmarketcap
from DEFAULT.utils.functions import *
# ————————————————————————————————————————————

def BtcPriceExtender(btc=False):

    model = Coinmarketcap.objects.get(ticker='BTC')

    response = {
        'ETH': toFixed((model.price_eth_ * btc), 5),
        'USD': toFixed((model.price_usd_ * btc), 2),
        'CNY': toFixed((model.price_cny_ * btc), 2),
        'RUB': toFixed((model.price_rub_ * btc), 2),
        'BTC': toFixed((btc), 8),
    }

    # print(response)
    return response
