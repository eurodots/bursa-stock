import os
from django.conf import settings

# ——————————————————————————————————————————————

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
PRODUCTION = True

# ——————————————————————————————————————————————

ALLOWED_HOSTS = ['*']
# SITE_HOST = 'http://127.0.0.1'

# ——————————————————————————————————————————————

# CORS
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CORS_REPLACE_HTTPS_REFERER = True
USE_ETAGS = True


# ——————————————————————————————————————————————

# This is for local development
WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'media/reactjs/',
        'STATS_FILE': os.path.join(settings.BASE_DIR, 'media/reactjs/webpack-stats.json')
    },
}


# ——————————————————————————————————————————————

# PROTECTED PAGES
LOCKDOWN_PASSWORDS = ('btceth')


# ——————————————————————————————————————————————

# REST API AUTH
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'robot@kupi.net'
EMAIL_HOST_PASSWORD = '0jfj2jt2ifr2nt'
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
EMAIL_PORT = 587
VERIFICATION_KEY_EXPIRY_DAYS = 2

SITE_NAME = "KUPI.NET"



# ——————————————————————————————————————————————

DATABASES = {
    # 'default': {
    #     'ENGINE': 'django.db.backends.mysql',
    #     'NAME': 'equalizer2',
    #     'USER': 'root',
    #     'PASSWORD': 'root',
    #     'HOST': '127.0.0.1',   # Or an IP Address that your DB is hosted on
    #     'PORT': '8889',
    #     'OPTIONS': {
    #         # 'sql_mode': 'traditional',
    #         'init_command': "SET sql_mode='STRICT_TRANS_TABLES', storage_engine=INNODB, character_set_connection=utf8, collation_connection=utf8_unicode_ci",
    #         'autocommit': True,
    #
    #     },
    # },
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'admin_stock',
        'USER': 'admin_stock',
        'PASSWORD': '35353535',
        'HOST': '127.0.0.1',   # Or an IP Address that your DB is hosted on
        # 'HOST': '148.251.67.84',  # Or an IP Address that your DB is hosted on
        'PORT': '3306',
        'OPTIONS': {
            # 'sql_mode': 'traditional',
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES', storage_engine=INNODB, character_set_connection=utf8, collation_connection=utf8_unicode_ci",
            'autocommit': True,

        },
    },
}


# ——————————————————————————————————————————————

# CKEDITOR_BASEPATH = '/static/'
CKEDITOR_UPLOAD_PATH = os.path.join(settings.BASE_DIR, 'static')
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            ['RemoveFormat', 'Source']
        ]
    }
}

# ——————————————————————————————————————————————

GOOGLE_ANALYTICS = 'UA-89615648-1'
