
"""DEFAULT URL Configuration


The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

# Additional
from django.conf.urls import url, include
# —————
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve
# —————


urlpatterns = [
    path('admin/', admin.site.urls),

    url(r'^api/tasks/', include('tasks.urls')),
    url(r'^api/equalizer/', include('equalizer.urls')),
    url(r'^api/datalist/', include('datalist.urls')),

    # API V.1
    url(r'^api/v1/', include('API.urls')),


    # PAGES
    url(r'^api/', include('stocks.urls')),
    url(r'^api/pages/', include('pages.urls')),

    # AUTH
    url(r'^api/accounts/', include('accounts.api.urls')),
    url(r'^api/teams/', include('teams.api.urls')),

    # SUPPORT
    url(r'^api/support/', include('support.urls')),

    # ADMIN
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    # OTHER URLS FOR WEBPACK
    url(r'^', include('webpack.urls')),
]


# This is only needed when using runserver.
# if settings.DEBUG:
urlpatterns = [
    url(r'^media/(?P<path>.*)$', serve,
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'^static/(?P<path>.*)$', serve,
        {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
    ] + staticfiles_urlpatterns() + urlpatterns
