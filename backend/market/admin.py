from django.contrib import admin
from django.urls import reverse
# from django.forms import ModelForm, ChoiceField, forms, models
from django.utils.safestring import mark_safe
# ————————————————————————————————————————————
from .models import Stocks as MarketStocks, Pairs as MarkerPairs, Coins as MarketCoins, Coins_Links as MarketCoinsLinks, Coinmarketcap
# ————————————————————————————————————————————


def toFixed(value):
    return '{:0.8f}'.format(value)




@admin.register(Coinmarketcap)
class CoinmarketcapAdmin(admin.ModelAdmin):
    list_display = ('id', 'ticker', 'name', 'rank', 'percent_1h', 'percent_24h', 'percent_7d', 'price_usd', 'price_btc','updated_ago')
    list_display_links = ('id', 'ticker', 'name')
    search_fields = ('ticker', 'name',)
    list_per_page = 25

    def percent_1h(self, obj):
        return '{} %'.format(obj.percent_change_1h)

    def percent_24h(self, obj):
        return '{} %'.format(obj.percent_change_24h)

    def percent_7d(self, obj):
        return '{} %'.format(obj.percent_change_7d)


@admin.register(MarketStocks)
class StocksAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'holdlist_', 'blacklist_', 'fastlink_exist', 'active')
    list_display_links = ('id', 'name')

    def holdlist_(self, obj):
        list = obj.holdlist
        list = list.split('\r\n')
        list = [l.split('#', 1)[0].strip() for l in list]
        list = ', '.join(list)
        return list

    def blacklist_(self, obj):
        list = obj.blacklist
        list = list.split('\r\n')
        list = [l.split('#', 1)[0].strip() for l in list]
        list = ', '.join(list)
        return list

    def fastlink_exist(self, obj):
        if obj.fastlink:
            return 'Yes'
        else:
            return '—'



@admin.register(MarkerPairs)
class PairsAdmin(admin.ModelAdmin):
    list_display = ('id', 'coin_from', 'coin_to', 'pair', 'arr_asks_count', 'arr_bids_count', 'stock_name', 'updated_at')
    list_display_links = ('id', 'pair', 'stock_name')
    search_fields = ('pair','stock_name', )
    list_per_page = 25

    raw_id_fields = ("stock",)
    exclude = ('stock_name', 'stock', 'arr_asks_count', 'arr_bids_count', 'coin_from', 'coin_to')
    readonly_fields = ('pair','arr_asks', 'arr_bids', 'stock_name', 'stock_pair_link')
    # autocomplete_fields = ("stock_id",)




    def stock_name(self, obj):
        return mark_safe('<a href="{}">{} (#{})</a>'.format(
            reverse("admin:market_stocks_change", args=(obj.stock.pk, )),
            obj.stock.name, obj.stock.pk
        ))

    def stock_pair_link(self, obj):
        if obj.fastlink_:
            return mark_safe('<a href="{}" target="_blank">{}</a>'.format(
                obj.fastlink_, obj.pair
            ))
        else:
            return obj.pair + ' (No fastlink)'



@admin.register(MarketCoins)
class CoinsAdmin(admin.ModelAdmin):
    list_display = ('id', 'favicon_img_', 'ticker', 'name', 'token_', 'platform', 'ask_btc_', 'bid_btc_', 'coinmarketcap_', 'links_', 'updated_ago')
    list_display_links = ('id', 'ticker')
    search_fields = ('pair','ticker','token', )
    list_per_page = 25


    def ask_btc_(self, obj):
        return '{}'.format( toFixed(obj.bestprice_ask_btc) )

    def bid_btc_(self, obj):
        return '{}'.format(toFixed(obj.bestprice_bid_btc))


    def links_(self, obj):
        return mark_safe('<a href="/admin/market/coins_links/?q={}" target="_blank">Links</a>'.format(obj.ticker))

    def token_(self, obj):
        if len(obj.token) > 0:
            return 'Yes'
        else:
            return '—'

    def favicon_img_(self, obj):
        if obj.favicon_:
            return mark_safe('<img src="{}" />'.format(
                obj.favicon_
            ))
        else:
            return '—'


    def coinmarketcap_(self, obj):
        if obj.coinmarketcap:
            return mark_safe('<a href="{}">Yes</a>'.format(
                reverse("admin:market_coinmarketcap_change", args=(obj.coinmarketcap.pk,))
            ))
        else:
            return '—'


@admin.register(MarketCoinsLinks)
class CoinsLinksAdmin(admin.ModelAdmin):
    list_display = ('id', 'ticker', 'label', 'value')
    list_display_links = ('id', 'label')
    search_fields = ('ticker', )
    list_per_page = 25
