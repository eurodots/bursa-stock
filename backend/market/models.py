import json
# ————————————————————————————————————————————
from django.db import models
from django.utils import timezone
from DEFAULT.utils.model_mixins import modelMixin_DateAgo
from DEFAULT.utils.functions import *
from DEFAULT.utils.fastlink import fastlinkMaker
# ————————————————————————————————————————————
from sorl.thumbnail import ImageField, get_thumbnail
# ————————————————————————————————————————————


class Coinmarketcap(models.Model):
    ticker = models.CharField(null=False, max_length=10, unique=True)
    name = models.CharField(null=False, max_length=50)
    rank = models.IntegerField(null=True, default=0)
    percent_change_1h = models.DecimalField(null=True, default=0, max_digits=10, decimal_places=2)
    percent_change_24h = models.DecimalField(null=True, default=0, max_digits=10, decimal_places=2)
    percent_change_7d = models.DecimalField(null=True, default=0, max_digits=10, decimal_places=2)
    price_usd = models.DecimalField(null=True, default=0, max_digits=18, decimal_places=8)
    price_btc = models.DecimalField(null=True, default=0, max_digits=18, decimal_places=8)
    price_eth = models.DecimalField(null=True, default=0, max_digits=18, decimal_places=8)
    price_cny = models.DecimalField(null=True, default=0, max_digits=18, decimal_places=8)
    price_rub = models.DecimalField(null=True, default=0, max_digits=18, decimal_places=8)
    updated_at = models.DateTimeField(auto_now=True, blank=True, editable=False)

    @property
    def price_usd_(self):
        return float(self.price_usd)

    @property
    def price_btc_(self):
        return float(self.price_btc)

    @property
    def price_eth_(self):
        return float(self.price_eth)

    @property
    def price_cny_(self):
        return float(self.price_cny)

    @property
    def price_rub_(self):
        return float(self.price_rub)

    @property
    def fastlink_(self):
        return 'https://coinmarketcap.com/currencies/' + self.name + '/'

    @property
    def percent_change_1h_(self):
        return float(self.percent_change_1h)

    @property
    def percent_change_24h_(self):
        return float(self.percent_change_24h)

    @property
    def percent_change_7d_(self):
        return float(self.percent_change_7d)

    @property
    def updated_ago(self):
        return modelMixin_DateAgo(self.updated_at)

    @property
    def updated_(self):
        return dateConverter(self.updated_at)

    def save(self, *args, **kwargs):
        self.updated_at = timezone.now()
        return super(Coins, self).save(*args, **kwargs)



class Stocks(models.Model):
    active = models.BooleanField('active', default=0, help_text='Active')
    name = models.CharField(null=False, max_length=50, unique=True)
    website = models.CharField(null=False, max_length=50)
    blacklist = models.TextField(null=True, blank=True, default='')
    holdlist = models.TextField(null=True, blank=True, default='')
    fastlink = models.CharField(null=True, default="", max_length=100)

    @property
    def website_(self):
        return 'http://{}'.format(self.website)


class Pairs(models.Model):
    stock = models.ForeignKey(Stocks, on_delete=models.CASCADE)
    stock_name = models.CharField(null=False, max_length=30)
    coin_from = models.CharField(null=False, max_length=10)
    coin_to = models.CharField(null=False, max_length=10)
    pair = models.CharField(null=False, max_length=20)
    arr_asks = models.TextField()
    arr_bids = models.TextField()
    arr_asks_count = models.IntegerField(null=True, default=0)
    arr_bids_count = models.IntegerField(null=True, default=0)
    bestprice_ask = models.DecimalField(null=True, default=0, max_digits=18, decimal_places=8)
    bestprice_bid = models.DecimalField(null=True, default=0, max_digits=18, decimal_places=8)

    updated_at = models.DateTimeField(auto_now=True, blank=True, editable=False)

    @property
    def updated_(self):
        return dateConverter(self.updated_at)

    @property
    def bestprice_ask_(self):
        if self.bestprice_ask > 0:
            return float(self.bestprice_ask)
        else:
            return False

    @property
    def bestprice_bid_(self):
        if self.bestprice_bid > 0:
            return float(self.bestprice_bid)
        else:
            return False

    @property
    def arr_asks_(self):
        if len(self.arr_asks) > 10:
            return json.loads(self.arr_asks)
        else:
            return []

    @property
    def arr_bids_(self):
        if len(self.arr_bids) > 10:
            return json.loads(self.arr_bids)
        else:
            return []

    @property
    def fastlink_(self):
        return fastlinkMaker(self.coin_from, self.coin_to, self.stock.fastlink)

    def save(self, *args, **kwargs):
        self.updated_at = timezone.now()
        return super(Coins, self).save(*args, **kwargs)



class Pairs_forcemode(models.Model):
    pair = models.OneToOneField(Pairs, unique=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, editable=False)

    def save(self, *args, **kwargs):
        self.created_at = timezone.now()
        return super(Pairs_forcemode, self).save(*args, **kwargs)


class Coins(models.Model):
    ticker = models.CharField(null=False, max_length=10, unique=True)
    name = models.CharField(null=True, max_length=50)
    bestprice_ask_btc = models.DecimalField(null=True, default=0, max_digits=18, decimal_places=8)
    bestprice_bid_btc = models.DecimalField(null=True, default=0, max_digits=18, decimal_places=8)
    # blacklist = models.BooleanField('blacklist', default=0, help_text='Blacklist')
    favicon = models.ImageField(upload_to='favicons_market', blank=True)
    coinmarketcap = models.ForeignKey(Coinmarketcap, null=True, on_delete=models.CASCADE)
    token = models.CharField(null=True, default="", max_length=100)
    platform = models.CharField(null=True, default="", max_length=50)
    updated_at = models.DateTimeField(auto_now=True, blank=True, editable=False)


    def favicon_src(self, size='32x32'):
        if self.favicon:
            return get_thumbnail(self.favicon, size, crop='center', format='PNG', quality=99).url
        else:
            return False

    @property
    def bestprice_ask_btc_(self):
        return float(self.bestprice_ask_btc)

    @property
    def bestprice_bid_btc_(self):
        return float(self.bestprice_bid_btc)

    @property
    def updated_ago(self):
        return modelMixin_DateAgo(self.updated_at)

    @property
    def updated_(self):
        return dateConverter(self.updated_at)

    @property
    def token_eth_(self):
        if '0x' == self.token[0:2].lower() and len(self.token) > 30:
            return self.token.lower()
        else:
            return False


    @property
    def platform_(self):
        if len(self.platform) > 0:
            return self.platform
        else:
            return False


    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        # if not self.id:
        self.updated_at = timezone.now()
        return super(Coins, self).save(*args, **kwargs)



class Coins_Links(models.Model):
    coin = models.ForeignKey(Coins, null=False, on_delete=models.CASCADE)
    ticker = models.CharField(null=False, max_length=10)
    label = models.CharField(null=False, max_length=30)
    value = models.CharField(null=False, max_length=200)