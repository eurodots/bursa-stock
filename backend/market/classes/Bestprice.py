import datetime
# ————————————————————————————————————————————
from django.db.models import Q
# ————————————————————————————————————————————
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
django.setup()
# ————————————————————————————————————————————
from market.models import Pairs as MarketPairs
from DEFAULT.utils.functions import *
# ————————————————————————————————————————————



def priceView(init):
    return {
        'price_btc': init['price_btc'],
        'stock_name': init['stock_name'],
        'stock_id': init['stock_id'],
        'updated_at': init['updated_at'],
        'fastlink': init['fastlink'],
    }


class KUPINET:
    def __init__(self):
        pass

    # class Stocks:
    #     def __init__(self, stock_name=False):
    #         self.suffix = False
    #         if stock_name:
    #             self.suffix = stock_name.lower()
    #         # ErrorMsg('Method not specified')
    #         # else:
    #             # self.suffix = False
    #             # self.info = False
    #
    #     def getInfo(self):
    #         if self.suffix:
    #             return CallApi('stocks/' + self.suffix ).content
    #         else:
    #             return CallApi('stocks').content
    #
    #     def getOrders(self, pair='_ALL'):
    #         if self.suffix:
    #             return CallApi('stocks/' + self.suffix + '/orders?pair=' + pair).content
    #         else:
    #             ErrorMsg('Stock name not specified')
    #
    #
    #     def getHistory(self, pair='', date_from='', date_to=''):
    #         if self.suffix:
    #             return CallApi('stocks/' + self.suffix + '/history?pair=' + pair + '&date_from=' + date_from + '&date_to=' + date_to).content
    #         else:
    #             ErrorMsg('Stock name not specified')
    #
    #     def getPairs(self):
    #         if self.suffix:
    #             return CallApi('stocks/' + self.suffix + '/pairs').content
    #         else:
    #             ErrorMsg('Stock name not specified')
    #
    #     def getChart(self, pair=''):
    #         if self.suffix:
    #             return CallApi('stocks/' + self.suffix + '/chart?pair=' + pair).content
    #         else:
    #             ErrorMsg('Stock name not specified')



    class getBtcPrice:
        def __init__(self, ticker=False):

            self.bestAsk = False
            self.bestBid = False
            self.bestAll = False
            self.price = False

            best_ask = False
            best_bid = False
            middle_price = False

            ticker_ = ticker.upper()

            if ticker_ == 'BTC':
                best_ask = {
                    'price_btc': 1,
                    'stock_name': False,
                    'stock_id': False,
                    'updated_at': dateConverter(datetime.datetime.now()),
                    'fastlink': False,
                }
                best_bid = {
                    'price_btc': 1,
                    'stock_name': False,
                    'stock_id': False,
                    'updated_at': dateConverter(datetime.datetime.now()),
                    'fastlink': False,
                }
                middle_price = 1


            else:

                modelMarketPairs = MarketPairs.objects.filter(coin_from=ticker_, coin_to='BTC', stock__active=1).exclude(Q(bestprice_ask=0) | Q(bestprice_bid=0))
                if modelMarketPairs:

                    modelMarketPairsBestAsk = modelMarketPairs.order_by('bestprice_ask')
                    modelMarketPairsBestBid = modelMarketPairs.order_by('-bestprice_bid')

                    m_ask =  modelMarketPairsBestAsk[0]
                    m_bid = modelMarketPairsBestBid[0]

                    best_ask = {
                        'price_btc': m_ask.bestprice_ask_,
                        'stock_name': m_ask.stock.name,
                        'stock_id': m_ask.stock.pk,
                        'updated_at': m_bid.updated_,
                        'fastlink': m_ask.fastlink_,
                    }
                    best_bid = {
                        'price_btc': m_bid.bestprice_bid_,
                        'stock_name': m_bid.stock.name,
                        'stock_id': m_bid.stock.pk,
                        'updated_at': m_bid.updated_,
                        'fastlink': m_bid.fastlink_,
                    }
                    middle_price = ((m_ask.bestprice_ask_ + m_bid.bestprice_ask_) / 2)



            self.bestAsk = best_ask
            self.bestBid = best_bid
            self.bestAll = {'best_ask': best_ask, 'best_bid': best_bid}
            self.price   =  middle_price

        # if not ticker:
        #     return "Coin not specified"
        #
        # return ticker



def getAllStocksTest():
    from market.models import Stocks as MarketStocks
    market = MarketStocks.objects.all()

    arr = []
    for m in market:
        arr.append(m.name)

    print(', '.join(arr))


if __name__ == "__main__":

    # print( KUPINET.getBtcPrice('ETH').bestAsk )
    # print( KUPINET.getBtcPrice('ETH').bestBid )
    # print( KUPINET.getBtcPrice('ETH').bestAll )
    # print( KUPINET.getBtcPrice('ETH').price )

    getAllStocksTest()


# CoinPrice.Orders.Coin('ETH').Stocks('Binance')

# получить лучшую цену из пар
# запрос: coin -> все биржи(), конкретная биржа(), аск или бид?
# ответ:
#               что за биржа, дата обновления цены
#               usd, cny, eth, rub, eth


    pass
