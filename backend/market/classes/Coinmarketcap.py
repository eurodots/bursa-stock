# ————————————————————————————————————————————
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
django.setup()
# ————————————————————————————————————————————
from market.models import Coins as MarketCoins
# ————————————————————————————————————————————

class CMCap:
    def __init__(self):
        pass


    class get:
        def __init__(self, ticker=False):
            self.ticker = ticker

            self.BTC = False
            self.ETH = False
            self.USD = False
            self.CNY = False
            self.RUB = False

            # Errors
            modelMarketCoins = MarketCoins.objects.filter(ticker=self.ticker, coinmarketcap__isnull=False)
            if not modelMarketCoins:
                self.m = False
                exit('Ticker '+ticker+' not found')
            else:
                self.m = modelMarketCoins.get().coinmarketcap
                self.BTC = self.m.price_btc_
                self.ETH = self.m.price_eth_
                self.USD = self.m.price_usd_
                self.CNY = self.m.price_cny_
                self.RUB = self.m.price_rub_



        def all(self):

            if not self.m:
                return "Not found"

            response = {
                'updated_at': self.m.updated_,
                'fastlink': self.m.fastlink_,
                'prices': {
                    'BTC': self.m.price_btc_,
                    'ETH': self.m.price_eth_,
                    'USD': round(self.m.price_usd_, 2),
                    'CNY': round(self.m.price_cny_, 2),
                    'RUB': round(self.m.price_rub_, 2),
                }

            }

            return response


if __name__ == "__main__":
    # print( CMCap().get('LTC').USD )
    print(CMCap().get('LTC').all() )


