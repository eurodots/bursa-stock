# ————————————————————————————————————————————
import requests
import json
# ————————————————————————————————————————————



def ethplorerTxsChecker(token=""):
    url = 'https://api.ethplorer.io/getAddressInfo/' + token + '?apiKey=freekey'
    r = requests.get(url)
    data = json.loads(r.content.decode('utf8'))

    if 'countTxs' in data and data['countTxs'] > 0:
        return True
    else:
        return False



# ethplorerTokenChecker('0xafe5a978c593fe440d0c5e4854c5bd8511e770a4')
# print( ethplorerTxsChecker('0x4CF488387F035FF08c371515562CBa712f9015d4') )