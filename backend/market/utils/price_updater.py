from django.db.models import Q
# ————————————————————————————————————————————
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
django.setup()
# ————————————————————————————————————————————
from DEFAULT.utils.functions import *
# from equalizer.models import Orders as OrdersByPairs
# ————————————————————————————————————————————
from DEFAULT.utils.blacklist import globalBlacklist
from market.models import Coins as MarketCoins, Coinmarketcap, Pairs as MarketPairs
# ————————————————————————————————————————————
GLOBAL_BLACKLIST = globalBlacklist()
GLOBAL_PRICE_BTC = {}
# ————————————————————————————————————————————


def priceFormat(value):
    return '{:0.8f}'.format(value)


def updateDbMarketCoins():

    # Add or update new coins from Stocks Pairs
    modelOrdersByPairs = MarketPairs.objects.filter(Q(stock__active=1)).exclude(Q(coin_from__in=GLOBAL_BLACKLIST) | Q(coin_to__in=GLOBAL_BLACKLIST))  #.order_by('-id')

    # Collection uniquie coins
    coins_arr = {}
    for pair in modelOrdersByPairs:
        coin = pair.coin_from
        if not coin in coins_arr:
            coins_arr[coin] = coin


    # Add or update new coins
    for coin in coins_arr:
        modelCoins = MarketCoins.objects.filter(ticker=coin)
        if modelCoins:
            pass
        else:
            model = MarketCoins()
            model.ticker = coin
            model.name = coin
            model.bestprice_ask_btc = 0
            model.bestprice_bid_btc = 0
            model.favicon = 0
            model.save()

    print('Coins updated')
    # jprint(coins_arr)


def connectCoinsCoinmarketcap():

    modelCoinmarketcap = Coinmarketcap.objects.all()
    for coin in modelCoinmarketcap:
        modelMarketCoins = MarketCoins.objects.filter(ticker=coin.ticker)
        if modelMarketCoins:
            modelMarketCoins = modelMarketCoins.get()
            modelMarketCoins.coinmarketcap = Coinmarketcap.objects.get(pk=coin.pk)
            modelMarketCoins.save()

    print('Coinmarketcap connected!')


def updateCoinsPriceBtcFromPairs(): # Update btc prices from actual orders

    iterator = 0

    # Apply prices
    modelMarketCoins = MarketCoins.objects.all()
    for coin in modelMarketCoins:

        modelMarketPairs = MarketPairs.objects.filter(coin_from=coin.ticker, coin_to='BTC').exclude(Q(bestprice_ask=0) | Q(bestprice_bid=0))
        if modelMarketPairs:
            modelMarketPairsBestAsk = modelMarketPairs.order_by('bestprice_ask')
            modelMarketPairsBestBid = modelMarketPairs.order_by('-bestprice_bid')

            bestprice_ask = modelMarketPairsBestAsk[0].bestprice_ask
            bestprice_bid = modelMarketPairsBestBid[0].bestprice_bid
            modelCoin = MarketCoins.objects.get(pk=coin.pk)
            modelCoin.bestprice_ask_btc = bestprice_ask
            modelCoin.bestprice_bid_btc = bestprice_bid
            modelCoin.save()

            iterator+=1

            # d(coin.ticker)


    print('New btc prices from orders updated!', iterator)


def updateCoinsPriceBtcFromCoinmarketCap(): # Update pairs if btc_price empties

    iterator = 0

    modelMarketCoins = MarketCoins.objects.filter(coinmarketcap__isnull=False)
    for coin in modelMarketCoins:
        modelCoinmarketcap = Coinmarketcap.objects.filter(ticker=coin.ticker)
        if modelCoinmarketcap:
            cap = modelCoinmarketcap.get()

            modelCoin = MarketCoins.objects.get(pk=coin.pk)
            modelCoin.name = cap.name
            modelCoin.bestprice_ask_btc = cap.price_btc
            modelCoin.bestprice_bid_btc = cap.price_btc
            modelCoin.save()

            iterator+=1
        else:
            modelCoin = MarketCoins.objects.get(pk=coin.pk)
            modelCoin.bestprice_ask_btc = 0
            modelCoin.bestprice_bid_btc = 0
            modelCoin.save()


    print('Prices from Coinmarketcap updated', iterator)


def coinslist():

    # Get price for BTC
    marketData = Coinmarketcap.objects.get(ticker='BTC')
    GLOBAL_PRICE_BTC = {
        'BTC': float(marketData.price_btc),
        'USD': float(marketData.price_usd),
        'ETH': float(marketData.price_eth),
        'CNY': float(marketData.price_cny),
        'RUB': float(marketData.price_rub),
    }


    # Get all coins within blacklist
    coinsData = MarketPairs.objects.filter(Q(stock__active=1)).exclude(Q(coin_from__in=GLOBAL_BLACKLIST) | Q(coin_to__in=GLOBAL_BLACKLIST)).order_by('-id')[:200:1]
    coins_arr = {}
    for coin in coinsData:

        coin_from = coin.coin_from
        coin_to = coin.coin_to

        if not coin_from in coins_arr:
            coins_arr[coin_from] = {}
        if not coin_to in coins_arr[coin_from]:
            coins_arr[coin_from][coin_to] = []

        coins_arr[coin_from][coin_to].append({
            # 'ticker': ticker,
            'pair': coin.pair,
            'stock_name': coin.stock.name,
            'stock_id': coin.stock.pk,
            'bestprice_ask': float(coin.bestprice_ask),
            'bestprice_bid': float(coin.bestprice_bid),
            'arr_asks_count': coin.arr_asks_count,
            'arr_bids_count': coin.arr_bids_count,
            'favicon': True,
        })


    # Calculate prices
    calc_arr = []
    for index, coin in enumerate(coins_arr):

        if 'BTC' in coins_arr[coin] and not coin in GLOBAL_PRICE_BTC:
            bestprice_btc = [[a['bestprice_ask'], a['bestprice_bid']] for a in coins_arr[coin]['BTC'] if a['bestprice_ask'] > 0 and a['bestprice_bid'] > 0]

            if bestprice_btc:
                bestprice_btc_ask = float(bestprice_btc[0][0])
                bestprice_btc_bid = float(bestprice_btc[0][1])

                prices_arr = {}
                for g in GLOBAL_PRICE_BTC:
                    bestprice_btc_ask_to = bestprice_btc_ask * GLOBAL_PRICE_BTC[g]
                    bestprice_btc_bid_to = bestprice_btc_bid * GLOBAL_PRICE_BTC[g]
                    if not 'ask' in prices_arr:
                        prices_arr['ask'] = {}
                    if not 'bid' in prices_arr:
                        prices_arr['bid'] = {}

                    prices_arr['ask'][g] = priceFormat(bestprice_btc_ask_to)
                    prices_arr['bid'][g] = priceFormat(bestprice_btc_bid_to)


                if not coin in calc_arr:
                    calc_arr.append({
                        'ticker': coin,
                        'stocks': coins_arr[coin],
                        'prices': prices_arr
                    })



        # bestprice_btc_ask = float(bestprice_btc_ask)
        # print(bestprice_btc_ask)

    # jprint(calc_arr)
    return calc_arr



if __name__ == "__main__":
    # updateDbMarketCoins()
    # connectCoinsCoinmarketcap()


    updateCoinsPriceBtcFromCoinmarketCap() #First <- important!
    updateCoinsPriceBtcFromPairs() #Second <- important!

    pass