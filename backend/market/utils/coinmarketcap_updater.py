import threading
import requests
import datetime
from bs4 import BeautifulSoup
import json
import re

# ———————————————DJANGO ENVIRON———————————————
from DEFAULT.utils.django_setup import *
# ————————————————————————————————————————————
from DEFAULT.utils.functions import *
from DEFAULT.settings import MEDIA_ROOT
# ————————————————————————————————————————————
from market.models import Coins as MarketCoins, Coins_Links as MarketCoinsLinks
# ————————————————————————————————————————————
path_favicons = os.path.join(MEDIA_ROOT, 'favicons.marketcap')
# ————————————————————————————————————————————


def parsList(threads=20):


    page_url = 'https://coinmarketcap.com/tokens/views/all/'
    html = requests.get(page_url)
    html = html.content.decode('utf8')
    soup = BeautifulSoup(html, "lxml")

    table_arr = soup.find('table', {'id': 'assets-all'}).find('tbody').find_all('tr', text=False)


    table_arr = split(table_arr, threads)
    # for arr in table_arr:
    #     parsListCurrent(arr)
    #     exit()


    for index, arr in enumerate(table_arr):
        threading.Thread(target=parsListCurrent, args=(arr,)).start()



def parsListCurrent(table_arr):

    for tr in table_arr:

        try:
            td_first = tr.find('td', {'class': 'currency-name'})
            td_name = td_first.find('a', text=True).text
            td_href = td_first.find('a').get('href')

            td_platform = tr.find('td', {'class': 'platform-name'})
            td_platform = td_platform.find('a', text=True).text if td_platform else False

            td_capital = tr.find('td', {'class': 'market-cap'})
            td_capital_usd = td_capital.get('data-usd')
            td_capital_btc = td_capital.get('data-btc')

            td_price = tr.find('a', {'class': 'price'})
            td_price_usd = td_price.get('data-usd')
            td_price_btc = td_price.get('data-btc')

            td_suply = tr.find('td', {'class': 'circulating-supply'}).find('a', text=True)
            td_suply_number = td_suply.get('data-supply') if td_suply else False

            td_volume24 = tr.find('a', {'class': 'volume'})
            td_volume24_usd = td_volume24.get('data-usd')
            td_volume24_btc = td_volume24.get('data-btc')

            td_changes = tr.find_all('td', {'class': 'percent-change'})
            td_changes_1h = False
            td_changes_24h = False
            td_changes_7d = False
            for index, td in enumerate(td_changes):

                num = td.get('data-percentusd')
                if index == 0:
                    td_changes_1h = num
                elif index == 1:
                    td_changes_24h = num
                elif index == 2:
                    td_changes_7d = num

            coinmarketcap_link = 'https://coinmarketcap.com' + str(td_href)
            extra = page(coinmarketcap_link)

            coin_arr = {
                'changes_1h': str(td_changes_1h),
                'changes_24h': str(td_changes_24h),
                'changes_7d': str(td_changes_7d),
                'volume24_usd': str(td_volume24_usd),
                'volume24_btc': str(td_volume24_btc),
                'suply_number': str(td_suply_number),
                'price_usd': str(td_price_usd),
                'price_btc': str(td_price_btc),
                'capital_usd': str(td_capital_usd),
                'capital_btc': str(td_capital_btc),
                'platform': str(td_platform),
                'name': str(td_name),
                'extra': extra,
                'updated_at': datetime.datetime.now()
            }

            ticker = str(td_name)
            fullname = str(extra['fullname'])

            saveFavicon(ticker, extra['favicon'])


            modelMarketCoins = MarketCoins.objects.filter(ticker=ticker)
            if modelMarketCoins:
                modelCoin = MarketCoins.objects.get(ticker=ticker)
                modelCoin.token = extra['contract_address']
                modelCoin.platform = coin_arr['platform']
                modelCoin.save()



                # TABLE: COINS_LINKS
                for link in extra['links']:
                    modelLinks = MarketCoinsLinks()
                    modelLinks.coin = MarketCoins.objects.get(ticker=ticker)
                    modelLinks.ticker = ticker
                    modelLinks.label = link['label'].title()
                    modelLinks.value = link['value']
                    modelLinks.save()



        except Exception as e:
            d('ERROR')
            print('>>>:', e)



def page(page_url):

    html = requests.get(page_url)
    html = html.content.decode('utf8')
    soup = BeautifulSoup(html, "lxml")

    d(page_url)

    get_image = soup.find('img', {'class': 'currency-logo-32x32'})
    get_image_src = get_image.get('src')
    get_image_src = re.sub('32x32', '128x128', str(get_image_src))

    get_fullname = get_image.get('alt')

    get_block = soup.find('div', {'class': 'col-sm-4 col-sm-pull-8'})
    get_block_links = get_block.find_all('a')
    links_arr = []
    for link in get_block_links:
        links_arr.append(link.get('href'))


    rank = get_block.find('span', {'class': 'label label-success'})
    if rank:
        rank = rank.text
        rank = re.findall(r'\d+', rank)
        rank = int(rank[0])
    else:
        rank = False


    fields = linksModifier(links_arr)
    return_arr = {
        'coinmarketcap': page_url,
        'favicon': get_image_src,
        'links': fields['links'],
        'data': fields['data'],
        'contract_address': fields['contract_address'],
        'rank': rank,
        'fullname': str(get_fullname),
    }

    # print(return_arr)
    return return_arr


def linksModifier(arr):

    data_arr = {
        # 'contract_address': False,
        'ethplorer': False,
        'etherscan': False,
        'website': False,
        'bitcointalk': False,
        'telegram': False,
        'github': False,
        'medium': False,
    }
    contract_address = False

    links_arr = []
    for index, link in enumerate(arr):

        label = re.sub('www.', '', str(link))
        label = re.search(':\/\/(.+?)\.', label).group(1)

        if label == 't':
            label = 'Telegram'


        links_arr.append({
            'label': label,
            'value': link
        })


        if index == 0:
            data_arr['website'] = link
        elif label == 'ethplorer':
            data_arr['ethplorer'] = link
        elif label == 'etherscan':
            data_arr['etherscan'] = link
        elif label == 'bitcointalk':
            data_arr['bitcointalk'] = link
        elif label == 't':
            data_arr['telegram'] = link
        elif label == 'github':
            data_arr['github'] = link
        elif label == 'medium':
            data_arr['medium'] = link

        if label == 'ethplorer':
            contract_address = link.split('/address/')
            contract_address = contract_address[1] if contract_address[1] else False
        if not contract_address and label == 'etherscan':
            contract_address = link.split('/token/')

            d(contract_address)

            if contract_address[1]:
                contract_address = contract_address[1]
            else:
                contract_address = link.split('/address/')
                contract_address = contract_address[1] if contract_address[1] else False


    return_arr = {
        'links': links_arr,
        'data': data_arr,
        'contract_address': contract_address
    }
    # print(return_arr)
    # exit()
    return return_arr


def saveFavicon(name, url):

    url_128px = url
    url_32px = re.sub('128x128', '32x32', str(url))

    path_128px = path_favicons + '/128x128/' + name + '.png'
    path_32px  = path_favicons + '/32x32/' + name + '.png'

    print(url_128px, path_128px)
    print(url_32px, path_32px)
    # exit()


    if not os.path.isdir(path_favicons):
        os.makedirs(path_favicons)

    if not os.path.isfile(path_32px):

        with open(path_128px, 'wb') as f:
            resp = requests.get(url_128px, verify=False)
            f.write(resp.content)

        with open(path_32px, 'wb') as f:
            resp = requests.get(url_32px, verify=False)
            f.write(resp.content)

    return True


def updateCurrentCoin(currentCoin):

    page_url = 'https://coinmarketcap.com/tokens/views/all/'
    html = requests.get(page_url)
    html = html.content.decode('utf8')
    soup = BeautifulSoup(html, "lxml")

    table = soup.find('table', {'id': 'assets-all'}).find('tbody').find_all('tr', text=False)


    # data_arr = []

    for tr in table:
        td_first = tr.find('td', {'class': 'currency-name'})
        td_name = td_first.find('a', text=False).text
        td_href = td_first.find('a').get('href')

        td_coinmarketcap_id = td_first.find('div', {'class': 'currency-logo-sprite'})
        if td_coinmarketcap_id:
            td_coinmarketcap_id = td_coinmarketcap_id.get('class')[0]
            td_coinmarketcap_id = td_coinmarketcap_id.split('-')[-1:][0]
        else:
            td_coinmarketcap_id = False


        td_platform = tr.find('td', {'class': 'platform-name'})
        td_platform = td_platform.find('a', text=True).text if td_platform else False

        td_capital = tr.find('td', {'class': 'market-cap'})
        td_capital_usd = td_capital.get('data-usd')
        td_capital_btc = td_capital.get('data-btc')

        td_price = tr.find('a', {'class': 'price'})
        td_price_usd = td_price.get('data-usd')
        td_price_btc = td_price.get('data-btc')

        td_suply = tr.find('td', {'class': 'circulating-supply'}).find('a', text=True)
        td_suply_number = td_suply.get('data-supply') if td_suply else False


        td_volume24 = tr.find('a', {'class': 'volume'})
        td_volume24_usd = td_volume24.get('data-usd')
        td_volume24_btc = td_volume24.get('data-btc')


        td_changes = tr.find_all('td', {'class': 'percent-change'})
        td_changes_1h = False
        td_changes_24h = False
        td_changes_7d = False
        for index, td in enumerate(td_changes):

            num = td.get('data-percentusd')
            if index == 0:
                td_changes_1h = num
            elif index == 1:
                td_changes_24h = num
            elif index == 2:
                td_changes_7d = num

        if str(td_name).upper() == currentCoin.upper():
            coinmarketcap_link = 'https://coinmarketcap.com'+str(td_href)
            # extra = page(coinmarketcap_link)
            # print('extra')
            # print(extra)

            coin_arr = {
                'changes_1h': str(td_changes_1h),
                'changes_24h': str(td_changes_24h),
                'changes_7d': str(td_changes_7d),
                'volume24_usd': str(td_volume24_usd),
                'volume24_btc': str(td_volume24_btc),
                'suply_number': str(td_suply_number),
                'price_usd': str(td_price_usd),
                'price_btc': str(td_price_btc),
                'capital_usd': str(td_capital_usd),
                'capital_btc': str(td_capital_btc),
                'platform': str(td_platform),
                'name': str(td_name),
                'updated_at': datetime.datetime.now(),
                'coinmarketcap': coinmarketcap_link,
                'coinmarketcap_id': td_coinmarketcap_id
            }
            return coin_arr

    return



def main():
    parsList()
    # page('https://coinmarketcap.com/currencies/airswap/')
    # page('https://coinmarketcap.com/currencies/eos/')
    # page('https://coinmarketcap.com/es/currencies/comsa-eth/')


if __name__ == "__main__":
    main()
    pass