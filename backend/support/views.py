# from django.shortcuts import render
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.core.mail import EmailMultiAlternatives
from django.contrib.sites.shortcuts import get_current_site


from rest_framework import serializers

import base


class CommentSerializer(serializers.Serializer):

    email = serializers.EmailField()
    message = serializers.CharField(min_length=10, max_length=1000)



@api_view(['POST'])
def contactFormViewSet(request):

    site = get_current_site(request)
    sendform = CommentSerializer(data=request.data)

    if sendform.is_valid():
        form_email = sendform.data['email']
        form_message = sendform.data['message']

        # send_mail("New contact form submission",
        #           form_message,
        #           form_email,
        #           [settings.EMAIL_HOST_USER],
        #           fail_silently=False
        #           )

        context = {
            'sender_email': form_email,
            'message': form_message,
            'site': site,
            'site_name': getattr(settings, 'SITE_NAME', None),
        }
        subject = render_to_string(
            'support/support_contactform_subject.txt', context
        )

        subject = ''.join(subject.splitlines())

        message = render_to_string(
            'support/support_contactform_content.txt', context
        )

        msg = EmailMultiAlternatives(subject, settings.SITE_NAME, form_email, [settings.EMAIL_HOST_USER])
        msg.attach_alternative(message, "text/html")
        msg.send()

        return Response({
            "error": False,
            "message": "Message was sent!"
        })


    return Response({
        "error": True,
        "errors": sendform.errors
    })