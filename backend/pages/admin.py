from django.contrib import admin
# ————————————————————————————————————————————
from .models import Faq, ApiDocs
# ————————————————————————————————————————————



@admin.register(Faq)
class FaqAdmin( admin.ModelAdmin ):
    list_display = ('id', 'title', 'category', 'created_at')
    list_display_links = ('id', 'title')
    search_fields = ('title', 'description')
    list_per_page = 25


@admin.register(ApiDocs)
class ApiDocsAdmin( admin.ModelAdmin ):
    list_display = ('id', 'title', 'category', 'created_at')
    list_display_links = ('id', 'title')
    search_fields = ('title', 'description')
    list_per_page = 25
