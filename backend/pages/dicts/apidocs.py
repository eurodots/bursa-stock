CHOICES_APIDOCS_CATEGORY_DATA = [
    {
        "name": "Installation Instructions",
        "description": "Python",
    },
    {
        "name": "Stocks API",
        "description": "Orders, Info",
    },
    {
        "name": "Pairs API",
        "description": "Fetch pairs",
    },
    {
        "name": "Best Prices API",
        "description": "Fetch best prces",
    },
    {
        "name": "Calc API",
        "description": "Crypto-calculator",
    },

]

CHOICES_APIDOCS_CATEGORY = tuple((c['name'], c['name']) for c in CHOICES_APIDOCS_CATEGORY_DATA)