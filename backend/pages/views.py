from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
# —————————————————————————
from .dicts.faq import CHOICES_CATEGORY_DATA
from .dicts.apidocs import CHOICES_APIDOCS_CATEGORY_DATA
# —————————————————————————
from .models import Faq as PagesFaq, ApiDocs as PagesApiDocs
# —————————————————————————

# clear history
# from django.contrib.admin.models import LogEntry
# LogEntry.objects.all().delete()


@api_view(['GET'])
def faqViewSet(request):


    pages_arr = {}
    for cat in CHOICES_CATEGORY_DATA:
        cat_id = str(cat['name'])
        pages_arr[cat_id] = {
            'category_id': cat['name'],
            'category_name': cat['name'],
            'category_description': cat['description'],
            'pages': [],
        }

    modelPagesFaq = PagesFaq.objects.all()
    for page in modelPagesFaq:
        cat_id = page.category
        pages_arr[cat_id]['pages'].append({
            'title': page.title,
            'description': page.description,
            'category': cat_id,
            'created_at_': page.created_at_,
        })


    return Response({
        'results': pages_arr
    })



@api_view(['GET'])
def apiDocsViewSet(request):

    pages_arr = {}
    for cat in CHOICES_APIDOCS_CATEGORY_DATA:
        cat_id = str(cat['name'])
        pages_arr[cat_id] = {
            'category_id': cat['name'],
            'category_name': cat['name'],
            'category_description': cat['description'],
            'pages': [],
        }

    modelPagesApiDocs = PagesApiDocs.objects.all()
    for page in modelPagesApiDocs:
        cat_id = page.category
        pages_arr[cat_id]['pages'].append({
            'title': page.title,
            'description': page.description,
            'category': cat_id,
            'request': page.request if len(page.request) > 0 else False,
            'created_at_': page.created_at_,
        })

    return Response({
        'results': pages_arr
    })