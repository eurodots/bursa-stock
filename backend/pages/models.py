from django.db import models
from django.core.validators import MinLengthValidator
from ckeditor.fields import RichTextField

# —————————————————————————
from DEFAULT.utils.functions import *
from pages.dicts.faq import CHOICES_CATEGORY
from pages.dicts.apidocs import CHOICES_APIDOCS_CATEGORY
# —————————————————————————


class Faq(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, blank=True, editable=False)
    category = models.CharField(null=False, max_length=100, choices=CHOICES_CATEGORY)
    title = models.CharField(null=False, max_length=100, validators=[MinLengthValidator(4)])
    description = RichTextField(config_name='default', max_length=20000)

    @property
    def created_at_(self):
        return dateConverter(self.created_at)



class ApiDocs(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, blank=True, editable=False)
    category = models.CharField(null=False, max_length=100, choices=CHOICES_APIDOCS_CATEGORY)
    title = models.CharField(null=False, max_length=100, validators=[MinLengthValidator(4)])
    description = RichTextField(config_name='default', max_length=20000)
    request = models.CharField(blank=True, max_length=200)

    @property
    def created_at_(self):
        return dateConverter(self.created_at)