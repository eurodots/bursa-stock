# from django.shortcuts import render

from rest_framework.response import Response
from rest_framework.decorators import api_view


from equalizer.combinations.v2 import initEqualizer


@api_view(['GET'])
def listViewSet(request):

    return Response({
        "error": False,
        "results": initEqualizer(),
    })