from django.contrib import admin
# from django.urls import reverse
# # from django.forms import ModelForm, ChoiceField, forms, models
# from django.utils.safestring import mark_safe
#
# from .models import Stocks, Orders, Coinmarketcap
#
# from DEFAULT.utils.fastlink import fastlinkMaker
#
# @admin.register(Coinmarketcap)
# class CoinmarketcapAdmin(admin.ModelAdmin):
#     list_display = ('id', 'ticker', 'name', 'rank', 'percent_1h', 'percent_24h', 'percent_7d', 'price_usd', 'price_btc', 'updated_at','updated_ago')
#     list_display_links = ('id', 'ticker', 'name')
#     search_fields = ('ticker', 'name',)
#     list_per_page = 25
#
#
# @admin.register(Stocks)
# class StocksAdmin(admin.ModelAdmin):
#
#     list_display = ('id', 'name', 'holdlist_', 'blacklist_', 'fastlink_exist', 'active')
#     list_display_links = ('id', 'name')
#
#     def holdlist_(self, obj):
#         list = obj.holdlist
#         list = list.split('\r\n')
#         list = [l.split('#', 1)[0].strip() for l in list]
#         list = ', '.join(list)
#         return list
#
#     def blacklist_(self, obj):
#         list = obj.blacklist
#         list = list.split('\r\n')
#         list = [l.split('#', 1)[0].strip() for l in list]
#         list = ', '.join(list)
#         return list
#
#     def fastlink_exist(self, obj):
#         if len(obj.fastlink) > 0:
#             return 'Yes'
#         else:
#             return '—'
#
#
#
# @admin.register(Orders)
# class OrdersAdmin(admin.ModelAdmin):
#     list_display = ('id', 'coin_from', 'coin_to', 'pair', 'arr_asks_count', 'arr_bids_count', 'stock_name', 'updated_at')
#     list_display_links = ('id', 'pair', 'stock_name')
#     search_fields = ('pair','stock_name', )
#     list_per_page = 25
#
#     raw_id_fields = ("stock",)
#     exclude = ('stock_name', 'stock', 'arr_asks_count', 'arr_bids_count', 'coin_from', 'coin_to')
#     readonly_fields = ('pair','arr_asks', 'arr_bids', 'stock_name', 'stock_pair_link')
#     # autocomplete_fields = ("stock_id",)
#
#
#
#
#     def stock_name(self, obj):
#         return mark_safe('<a href="{}">{} (#{})</a>'.format(
#             reverse("admin:equalizer_stocks_change", args=(obj.stock.pk, )),
#             obj.stock.name, obj.stock.pk
#         ))
#
#     def stock_pair_link(self, obj):
#         return mark_safe('<a href="{}" target="_blank">{}</a>'.format(
#             fastlinkMaker(obj.coin_from, obj.coin_to, obj.stock.fastlink), obj.pair
#         ))
#
#     # stock_name.short_description = 'Stock'
#     # stock_name.empty_value_display = 'Unknown'
#
#
#     # def stock_id(self, obj):
#     #     return obj.stock_id.pk
#
#
#     # def _projects(self, obj):
#     #     return obj.projects.all().count()
#
#
#
#     # def name(self, profile):
#     #     return profile.user.first_name + " " + profile.user.last_name
#     #
#     # def is_active(self, profile):
#     #     return profile.user.is_active
#     #
#     # def team(self, profile):
#     #     return profile.user.team
#     #
