from market.models import Pairs_forcemode as MarketPairsForcemode, Pairs as MarketPairs
from django.utils import timezone
# ————————————————————————————————————————————
GLOBAL_FETCH_MINUTES = 5
# ————————————————————————————————————————————

def enableForceMode(pair_id):

    earlier = timezone.now() - timezone.timedelta(minutes=GLOBAL_FETCH_MINUTES)
    modelForce = MarketPairsForcemode.objects.filter(pair__pk=pair_id, created_at__range=(earlier, timezone.now()))
    if not modelForce:
        model = MarketPairsForcemode.objects.get(pair=pair_id) if MarketPairsForcemode.objects.filter(pair=pair_id) else MarketPairsForcemode()
        model.pair = MarketPairs.objects.get(pk=pair_id)
        model.save()

