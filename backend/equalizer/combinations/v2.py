import copy
# ————————————————————————————————————————————
from DEFAULT.utils.django_setup import *
# ————————————————————————————————————————————
from django.utils import timezone
# ————————————————————————————————————————————
from market.models import Coinmarketcap
from market.models import Pairs as OrdersModel
# ————————————————————————————————————————————
from DEFAULT.utils.functions import *
from DEFAULT.utils.blacklist import globalBlacklist
from equalizer.combinations.forcemode import enableForceMode
# ————————————————————————————————————————————

TRANSACTIONS_MINIMAL_PROFIT_USD = 10

# ————————————————————————————————————————————


GLOBAL_BLACKLIST = globalBlacklist()
# GLOBAL_BEST_PRICES_ARR = []
GLOBAL_PERCENT_DIFFERENCE = 3
# GLOBAL_BTC_IN_USD = False
GLOBAL_FETCH_HOURS = 0.05
# GLOBAL_FETCH_HOURS = 1000
GLOBAL_COINMARKETCAP = {}


def CoinmarketcapPrices():
    global GLOBAL_COINMARKETCAP
    modelCoinmarketcap = Coinmarketcap.objects.all()
    for coin in modelCoinmarketcap:
        GLOBAL_COINMARKETCAP[coin.ticker] = {
            'btc': coin.price_btc_,
            'usd': coin.price_usd_,
        }

    # print(GLOBAL_COINMARKETCAP['BTC'][''])


def fetchPairs():

    # Get pairs from active stocks & pairs not in blacklist
    earlier = timezone.now() - timezone.timedelta(hours=GLOBAL_FETCH_HOURS)
    modelPairs = OrdersModel.objects\
        .filter(stock__active=1, updated_at__range=(earlier, timezone.now()))\
        .exclude(coin_from__in=GLOBAL_BLACKLIST).exclude(coin_to__in=GLOBAL_BLACKLIST)\
        .exclude(arr_asks__exact='').exclude(arr_bids__exact='')



    # Group pairs by pair_label
    grouped_by_coin_arr = {}
    for pair in modelPairs:
        pair_key = pair.coin_from

        if not pair_key in grouped_by_coin_arr:
            grouped_by_coin_arr[pair_key] = []


        grouped_by_coin_arr[pair_key].append({
            'coin_from': pair.coin_from,
            'coin_to': pair.coin_to,
            'pair': pair.pair,
            'pair_id': pair.pk,
            'stock_id': pair.stock.pk,
            'stock_name': pair.stock.name,
            'bestprice_ask': pair.bestprice_ask_,
            'bestprice_bid': pair.bestprice_bid_,
            'updated_at': dateConverter(pair.updated_at),
            'fastlink': pair.fastlink_,
            'arr_asks': pair.arr_asks_,
            'arr_bids': pair.arr_bids_,
        })



    # Get pairs which exist at not less then 2 stocks
    matched_coins_arr = []
    for pair_key in grouped_by_coin_arr:
        if len(grouped_by_coin_arr[pair_key]) > 1:
            # if grouped_by_coin_arr[pair_key][0]['pair_id'] in [1748,3867]:
            matched_coins_arr += grouped_by_coin_arr[pair_key]


    return matched_coins_arr


def makeCombinations():

    pairs_arr = fetchPairs()

    combs_arr = []
    for pair_from in pairs_arr:

        for pair_to in pairs_arr:
            if pair_from['coin_from'] in GLOBAL_COINMARKETCAP and \
                pair_from['coin_to'] in GLOBAL_COINMARKETCAP and \
                pair_to['coin_from'] in GLOBAL_COINMARKETCAP and \
                pair_to['coin_to'] in GLOBAL_COINMARKETCAP:


                if pair_from['stock_id'] != pair_to['stock_id']:
                    if pair_from['coin_from'] == pair_to['coin_from']:

                        # print('!')

                        # price_btc_from = bestPriceFinder(coin=pair_from['coin_to'], price=pair_from['bestprice_ask'], stock_id=pair_from['stock_id'])
                        # price_btc_to = bestPriceFinder(coin=pair_to['coin_to'], price=pair_to['bestprice_bid'], stock_id=pair_to['stock_id'])
                        #
                        # if price_btc_from and price_btc_to:
                        #
                        #     # print(price_btc_from['price_btc'])
                        #
                        # if float(price_btc_from['price_btc']) < float(price_btc_to['price_btc']):
                        # pair_from['btc'] = GLOBAL_COINMARKETCAP[pair_from['coin_to']]
                        # pair_to['btc'] = GLOBAL_COINMARKETCAP[pair_from['coin_from']]

                        if pair_from['arr_asks'] and pair_to['arr_bids']:
                            # if pair_from['arr_asks'][0][0] < pair_to['arr_bids'][0][0]:
                            pair_from['arr_bids'] = []
                            pair_to['arr_asks'] = []
                            combs_arr.append([pair_from, pair_to])


    # print(combs_arr)
    # exit()
    return combs_arr


# def addingOrdersToCombs():
#
#     combs_arr = makeCombinations()
#     combs_orders_arr = []
#
#
#     # GET ORDERS FOR EACH PAIR IN COMBS_ARR[]
#     for comb in combs_arr:
#
#         combWithOrders = comb
#         combWithOrders[0]['orders'] = OrdersModel.objects.filter(pk=comb[0]['pair_id']).get().arr_bids_
#         combWithOrders[1]['orders'] = OrdersModel.objects.filter(pk=comb[1]['pair_id']).get().arr_asks_
#
#         combs_orders_arr.append(combWithOrders)
#
#         # print(comb)
#         # exit()
#
#
#     # print(combs_orders_arr)
#     return combs_orders_arr


def priceFormat(value):
    return '{:0.8f}'.format(value)


# def getBestPriceBTC(coin='ETH'):
#     pair_label = coin + '_BTC'
#     if coin == 'BTC':
#         return 1
#     elif pair_label in GLOBAL_BEST_PRICES_ARR:
#         return GLOBAL_BEST_PRICES_ARR[pair_label][0]['bids']
#     else:
#         return False


def cupAndBucket(comb_asks, comb_bids):

    # Deep copy orders for manipulation
    orders_asks = copy.deepcopy(comb_asks['arr_asks'])
    orders_bids = copy.deepcopy(comb_bids['arr_bids'])

    # Current price in BTC
    # market_btc_cup = float(comb_asks['btc']['price_btc'])
    # market_btc_bucket  = float(comb_bids['btc']['price_btc'])

    # market_btc_cup_ = priceFormat(market_btc_cup)
    # market_btc_bucket_ = priceFormat(market_btc_bucket)


    # Save transactions
    transactions_arr = []

    # Max Index for cutting other orders
    max_index_asks = {}
    max_index_bids = {}




    # def asks(b, bucket):




    # def bids():

    for b, bucket in enumerate(orders_bids):

        for a, cup in enumerate(orders_asks):

            if cup[1] > 0:

                check = orders_bids[b][1] - cup[1]

                transaction_volume = 0

                if check < 0:  # bid empty
                    transaction_volume = bucket[1]

                    orders_asks[a][1] = abs(check)
                    orders_bids[b][1] = 0
                    # break

                else:  # ask empty
                    transaction_volume = cup[1]

                    orders_asks[a][1] = 0
                    orders_bids[b][1] = check

                # Market prices calculations...
                def generateOrdersInfo(init):
                    total = init['order_price'] * init['transaction_volume']
                    price_btc = GLOBAL_COINMARKETCAP[init['coin']]['btc'] * init['order_price']
                    total_btc = GLOBAL_COINMARKETCAP[init['coin']]['btc'] * total
                    total_usd = GLOBAL_COINMARKETCAP[init['coin']]['usd'] * total_btc

                    # if init['coin_to'] == 'ETH':
                    #     print(init)
                    #     print('price_btc', price_btc)
                    #     exit()

                    return {
                        'price': priceFormat(init['order_price']),
                        'total': priceFormat(total),
                        'price_btc': priceFormat(price_btc),
                        'total_btc': priceFormat(total_btc),
                        'total_usd': round(total_usd, 2),
                        'index': init['index'],  # for reactjs
                        'hover': False,  # for reactjs
                    }

                pay_arr = generateOrdersInfo({
                    'coin': comb_asks['coin_to'],
                    'order_price': cup[0],
                    # 'order_volume': cup[1],
                    'transaction_volume': transaction_volume,
                    'index': a,
                })

                get_arr = generateOrdersInfo({
                    'coin': comb_bids['coin_to'],
                    'order_price': bucket[0],
                    # 'order_volume': bucket[1],
                    'transaction_volume': transaction_volume,
                    'index': b,
                })

                profit_btc = float(get_arr['total_btc']) - float(pay_arr['total_btc'])
                profit_usd = profit_btc * GLOBAL_COINMARKETCAP['BTC']['usd']

                buy_total_btc = GLOBAL_COINMARKETCAP[comb_asks['coin_from']]['btc'] * transaction_volume
                buy_total_usd = buy_total_btc * GLOBAL_COINMARKETCAP['BTC']['usd']

                if profit_usd > TRANSACTIONS_MINIMAL_PROFIT_USD:
                # if True:

                    # print('profit_btc', profit_btc)
                    # print('profit_usd', profit_usd)
                    # exit()

                    transaction_item = {
                        'pay': pay_arr,
                        'buy': {
                            'coin_from': comb_asks['coin_from'],
                            'volume': transaction_volume,
                            'total_usd': round(buy_total_usd, 2),
                            'total_btc': priceFormat(buy_total_btc),
                            'profit_btc': priceFormat(profit_btc),
                            'profit_usd': round(profit_usd, 2),
                        },
                        'get': get_arr,
                    }

                    # print( 'profit_btc', round(profit_btc, 2) )
                    # print( 'profit_usd', round(profit_usd, 2) )
                    # print('- - ')


                    transactions_arr.append(transaction_item)

                    if not a in max_index_asks:
                        max_index_asks[a] = {
                            'price': comb_asks['arr_asks'][a][0],
                            'volume': comb_asks['arr_asks'][a][1],
                            'index_id': a,
                        }

                    if not b in max_index_bids:
                        max_index_bids[b] = {
                            'price': comb_bids['arr_bids'][b][0],
                            'volume': comb_bids['arr_bids'][b][1],
                            'index_id': b,
                        }

                # check_asks = [c[1] for c in orders_asks]
                # check_asks = sum(check_asks)
                #
                #
                # if check_asks > 0:
                #     print('check_asks', check_asks)
                #     asks(b, bucket)
                if check < 0:
                    break

    # bids()


    # if len(transactions_arr) > 0:
    #     jprint(transactions_arr)
    #     exit()

    # jprint(transactions_arr)
    # print('------')
    return {
        'transactions': transactions_arr,
        'max_index_asks': max_index_asks,
        'max_index_bids': max_index_bids,
    }



def pairsResultBuilder():

    combs_arr = []

    for index, comb in enumerate(makeCombinations()):
        print('Comb #', index)

        # Fetching orders...
        # comb_asks = comb[0]
        # comb_asks['arr_asks'] = OrdersModel.objects.get(pk=comb_asks['pair_id']).arr_asks_
        # comb_bids = comb[1]
        # comb_bids['arr_bids'] = OrdersModel.objects.get(pk=comb_bids['pair_id']).arr_bids_

        # return comb_bids


        calc = cupAndBucket(comb_asks=comb[0], comb_bids=comb[1])

        if len(calc['transactions']) > 0:


            # ENABLE FORCE MODE
            enableForceMode(comb[0]['pair_id'])
            enableForceMode(comb[1]['pair_id'])

            # Remove orders without transactions
            arr_asks_ = [b for a,b in calc['max_index_asks'].items()]
            arr_bids_ = [b for a,b in calc['max_index_bids'].items()]

            # arr_asks = comb[0]['orders']
            # arr_bids = comb[1]['orders']
            #
            # arr_asks_ = [o for i,o in enumerate(comb[0]['orders']) if i in calc['max_index_asks']]
            # arr_bids_ = [o for i,o in enumerate(arr_bids) if i in calc['max_index_bids']]

            # arr_asks_ = copy.deepcopy(comb[0]['orders'][:calc['max_index_asks']+1])
            # arr_bids_ = copy.deepcopy(comb[1]['orders'][:calc['max_index_bids']+1])

            # Format orders prices
            def viewFormat(comb, arr):

                view_orders = []
                for a in arr:
                    price_btc = GLOBAL_COINMARKETCAP[comb['coin_to']]['btc']
                    total = a['price'] * a['volume']
                    total_btc = price_btc * total

                    # best_price_btc = bestPriceFinder(coin=comb['coin_to'], price=a['price'], stock_id=comb['stock_id'])

                    view_orders.append({
                        'price': priceFormat(a['price']),
                        'price_btc': priceFormat(price_btc * a['price']),
                        'volume': priceFormat(a['volume']),
                        'total': priceFormat(total),
                        'total_btc': priceFormat(total_btc),
                        'total_usd': round(GLOBAL_COINMARKETCAP['BTC']['usd'] * total_btc, 2),
                        'index_id': a['index_id'],
                        'hover': False,
                    })


                comb_view = comb
                comb_view['fastlink'] = comb['fastlink']
                comb_view['arr_asks'] = []
                comb_view['arr_bids'] = []
                comb_view['data'] = view_orders

                return comb_view


            view_asks = viewFormat(comb[0], arr_asks_)
            view_bids = viewFormat(comb[1], arr_bids_)

            # Sum total transactions volumes
            comb_total_volume = 0
            comb_total_usd = 0
            comb_total_profit_btc = 0
            comb_total_profit_usd = 0
            comb_total_pay = 0
            comb_total_pay_btc = 0
            comb_total_get = 0
            comb_total_get_btc = 0



            for c in calc['transactions']:
                comb_total_volume += float(c['buy']['volume'])
                comb_total_usd += float(c['buy']['total_usd'])
                comb_total_profit_btc += float(c['buy']['profit_btc'])
                comb_total_profit_usd += float(c['buy']['profit_usd'])

                comb_total_pay += float(c['pay']['total'])
                comb_total_pay_btc += float(c['pay']['total_btc'])

                comb_total_get += float(c['get']['total'])
                comb_total_get_btc += float(c['get']['total_btc'])


            combs_arr.append({
                'asks': view_asks,
                'bids': view_bids,
                'transactions': {
                    'coin_pay': comb[0]['coin_to'],
                    'coin_buy': comb[0]['coin_from'],
                    'coin_get': comb[1]['coin_to'],

                    'total_volume': comb_total_volume,
                    'total_usd': round(comb_total_usd, 0),
                    'total_profit_btc': priceFormat(comb_total_profit_btc),
                    'total_profit_usd': round(comb_total_profit_usd, 0),

                    'total_pay': priceFormat(comb_total_pay),
                    'total_pay_btc': priceFormat(comb_total_pay_btc),

                    'total_get': priceFormat(comb_total_get),
                    'total_get_btc': priceFormat(comb_total_get_btc),

                    'data': calc['transactions'],
                },
            })

            # return combs_arr #DELETE THIS ROW!



    # jprint(combs_arr)
    combs_arr_sorted = sorted(combs_arr, key=lambda k: k['transactions']['total_profit_usd'], reverse=True)

    # print(combs_arr_sorted)

    return combs_arr_sorted


def test():
    a = {'coin_from': 'LTC', 'coin_to': 'ETH', 'pair': 'LTC_ETH', 'pair_id': 2032, 'stock_id': 2, 'stock_name': 'Binance', 'asks_price_min': 0.30549, 'bids_price_max': 0.30496, 'updated_at': '2018-04-07 01:27:03 UTC+0000', 'btc': {'price_btc': '0.01706116', 'stock_id': 1},
         'orders': [[1, 10], [1, 5], [1, 20], [1, 4]]}
    b = {'coin_from': 'LTC', 'coin_to': 'BTC', 'pair': 'LTC_BTC', 'pair_id': 225, 'stock_id': 1, 'stock_name': 'Cryptopia', 'asks_price_min': 0.01711758, 'bids_price_max': 0.01708026, 'updated_at': '2018-04-07 01:27:28 UTC+0000', 'btc': {'price_btc': 1, 'stock_id': 1},
         'orders': [[2, 7], [2, 3], [2, 11], [2, 8]]}

    # 10 - 7  = 3     #7
    # 3 - 3 = 0       #3
    # 5 - 11 = -6     #5
    # 20 - 6 = 14     #6
    # 14 - 8 = 8      #8

    res = cupAndBucket(a, b)
    jprint(res)


def initEqualizer():
    # setGlobalBTCinUSD()
    # globalBestPrices()
    # fetchPairs()
    # makeCombinations()
    # addingOrdersToCombs()
    # cupAndBucket()

    CoinmarketcapPrices()
    return pairsResultBuilder()

    # test()




if __name__ == "__main__":

    # print( initEqualizer() )

    pass
