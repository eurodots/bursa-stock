import copy
import statistics

# ————————————————————————————————————————————
import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DEFAULT.settings")
django.setup()
# ————————————————————————————————————————————
from market.models import Coinmarketcap as CoinmarketcapModel
from market.models import Pairs as OrdersModel
from market.models import Stocks as StocksModel
# ————————————————————————————————————————————
from DEFAULT.utils.functions import *
# from equalizer._utils.config import *
from DEFAULT.utils.fastlink import fastlinkMaker
# ————————————————————————————————————————————

# CONFIG
config_difference_percent = 3  # Max fidderence in prices between asks & bids
config_max_orders = 3000  # Max orders for request
config_minimal_profit_usd = 10

coinmarketcap_arr = []
orders_arr = []
stocks_arr = []
prices_arr = {}


def toNumber(value):
    num = '%.8f' % value
    num = float(num)
    return num


def getStockData(stock_name, coin_from=False, coin_to=False):

    for s in stocks_arr:
        if s.name.lower() == stock_name.lower():

            return {
                'stock_name': s.name,
                'fastlink': fastlinkMaker(coin_from, coin_to, s.fastlink)
            }
            break


def getPrice(coin, type='usd'):
    for c in coinmarketcap_arr:
        if c.ticker == coin:
            if type == 'usd':
                return float(c.price_usd)
            elif type == 'btc':
                return float(c.price_btc)

    return False


# CREATE ARRAY WITH PRICES FOR EACH COIN TO BTC
def priceBook(stock_name, coin_from, coin_to, arr_sell, arr_buy):
    global prices_arr

    if coin_to == 'BTC':

        price_sell = False
        price_buy = False

        median_sell = False
        if len(arr_sell) > 0:
            median_sell = arr_sell[0]['price'][0]
        #     median_tmp = []
        #     for a in arr_sell[:5]:
        #         median_tmp.append(a['price'][0])
        #     median_sell = statistics.median(median_tmp)
        #
        median_buy = False
        if len(arr_buy) > 0:
            median_buy = arr_buy[0]['price'][0]
        #     median_tmp = []
        #     for a in arr_buy[:5]:
        #         median_tmp.append(a['price'][0])
        #     median_buy = statistics.median(median_tmp)

        if coin_from not in prices_arr:
            prices_arr[coin_from] = {}
        if stock_name not in prices_arr[coin_from]:
            prices_arr[coin_from][stock_name] = {}
        if 'BTC' not in prices_arr[coin_from][stock_name]:
            prices_arr[coin_from][stock_name]['BTC'] = {
                'coin_from': coin_from,
                'coin_to': 'BTC',
                'price_sell': median_sell,
                'price_buy': median_buy,
                'median': statistics.median([median_sell, median_buy]),
            }



def stocksSort(arr):
    global orders_arr

    def orderCalc(type, coin_from, coin_to, orders_arr):
        response_arr = []

        # for o in orders_arr[:10]:
        for index, o in enumerate(orders_arr):
            o_price = float(o[0])
            o_volume = float(o[1])

            # round
            o_price = toNumber(o_price)
            o_volume = toNumber(o_volume)
            o_total = toNumber(o_price * o_volume)

            response_arr.append({
                'price': [o_price, coin_to],
                'volume': [o_volume, coin_from],
                'total': [o_total, coin_to],
                'index': index,
                'hover': False,
            })


        # sorting price by type of ordersbook
        if type == 'asks':
            response_arr = sorted(response_arr, key=lambda k: k.get('price', 0), reverse=False)
        elif type == 'bids':
            response_arr = sorted(response_arr, key=lambda k: k.get('price', 0), reverse=True)

        return response_arr

    for a in arr:

        stock_id = a.stock_id.pk
        stock_name = a.stock_name
        coin_from = a.coin_from
        coin_to = a.coin_to
        arr_asks = json.loads(a.arr_asks) if len(a.arr_asks) > 0 else []
        arr_bids = json.loads(a.arr_bids) if len(a.arr_bids) > 0 else []


        # сомнительое решение
        if coin_from in ['BTC', 'ETH'] or coin_to in ['BTC', 'ETH']:
            # сомнительое решение

            tmp_sell = orderCalc('asks', coin_from, coin_to, arr_asks)
            tmp_buy = orderCalc('bids', coin_from, coin_to, arr_bids)

            # CREATE ARRAY WITH PRICES FOR EACH COIN TO BTC
            priceBook(stock_name, coin_from, coin_to, tmp_sell, tmp_buy)

            arr_sell = []
            arr_buy = []

            # ADD PAIR TO ARRAY WITH COMBINATIONS IF COIN_FROM (to BTC) IN PRICES ARRAY
            if coin_to == 'BTC' or (coin_to != 'BTC' and coin_to in prices_arr):
                if coin_from in prices_arr and stock_name in prices_arr[coin_from]:

                    def appendSellBuy(append_arr, type):

                        for t in append_arr:
                            item = t
                            total_btc = False

                            if coin_to != 'BTC':
                                price_btc = prices_arr[coin_from][stock_name]['BTC']['median']
                                total = t['volume'][0]
                                total_btc = total * price_btc

                            else:
                                total_btc = t['total'][0]
                                price_btc = t['price'][0]

                            item['total_usd'] = getPrice(coin_to, 'usd') * t['total'][0]
                            item['total_btc'] = toNumber(total_btc)
                            item['price_btc'] = toNumber(price_btc)

                            if type == 'sell':
                                arr_sell.append(item)
                            elif type == 'buy':
                                arr_buy.append(item)

                    appendSellBuy(tmp_sell, 'sell')
                    appendSellBuy(tmp_buy, 'buy')

                    if len(arr_sell) > 0 and len(arr_buy) > 0:

                        stock_orders = {
                            'id': a.pk,
                            'stock_id': stock_id,
                            'stock_name': stock_name,
                            'coin_from': coin_from,
                            'coin_to': coin_to,
                            'arr_sell': arr_sell,
                            'arr_buy': arr_buy,
                            'updated_at': a.updated_at,
                        }

                        orders_arr.append(stock_orders)



def compositor(comb):
    transactions_arr = []

    total_arr = {
        'total_asks': {
            'original': 0,
            'btc': 0,
        },
        'total_coin': {
            'original': 0,
            'usd': 0,
        },
        'total_bids': {
            'original': 0,
            'btc': 0,
        },
        'total_profit': {
            'btc': 0,
            'usd': 0,
            'median': [],
            'percent': 0,
        },
    }

    orders_asks = comb['sell']['data']
    orders_bids = comb['buy']['data']

    coinmarketcap_btc_usd = getPrice('BTC', 'usd')
    coinmarketcap_coin_usd = getPrice(comb['sell']['coin_from'], 'usd')

    def priceBtcMaker(comb_arr):

        coin_from = comb_arr['coin_from']
        coin_to = comb_arr['coin_to']
        stock_name = comb_arr['stock_name']

        price_btc = False
        if coin_to != 'BTC':
            price_btc = prices_arr[coin_from][stock_name]['BTC']['median']

        return price_btc

    ask_price_btc = priceBtcMaker(comb['sell'])
    bid_price_btc = priceBtcMaker(comb['buy'])

    def prepareItem(ask_volume_to_btc, bid_volume_to_btc, volume, price_from, price_to, index_ask, index_bid):

        calc_volume_asks = toNumber(price_from * volume)
        calc_volume_bids = toNumber(price_to * volume)

        if calc_volume_bids > calc_volume_asks:

            asks_volume_btc = ask_volume_to_btc * volume
            coin_volume_usd = volume * coinmarketcap_coin_usd
            bids_volume_btc = bid_volume_to_btc * volume
            profit_btc = bids_volume_btc - asks_volume_btc
            profit_usd = profit_btc * coinmarketcap_btc_usd
            profit_percent = 100 - ((coin_volume_usd - profit_usd) / coin_volume_usd) * 100 if coin_volume_usd else 0
            profit_percent = round(profit_percent, 2)

            # increase total profit
            total_arr['total_asks']['original'] += calc_volume_asks
            total_arr['total_asks']['btc'] += asks_volume_btc

            total_arr['total_coin']['original'] += volume
            total_arr['total_coin']['usd'] += coin_volume_usd

            total_arr['total_bids']['original'] += calc_volume_bids
            total_arr['total_bids']['btc'] += bids_volume_btc

            total_arr['total_profit']['btc'] += profit_btc
            total_arr['total_profit']['usd'] += profit_usd
            total_arr['total_profit']['median'].append(profit_percent)

            # if comb['buy']['coin_to'] == 'ETH':
            #     print(volume)
            #     print(calc_volume_bids)
            #     # jprint(comb['buy'])
            #     exit()


            # d(profit_percent)

            # transaction row
            item = {
                'volume_asks': {
                    'original': [calc_volume_asks, comb['sell']['coin_to']],
                    'btc': toNumber(asks_volume_btc),
                    'order_id': index_ask,
                },
                'volume_coin': {
                    'original': [volume, comb['sell']['coin_from']],
                    'usd': toNumber(coin_volume_usd),
                },
                'volume_bids': {
                    'original': [calc_volume_bids, comb['buy']['coin_to']],
                    'btc': toNumber(bids_volume_btc),
                    'order_id': index_bid,
                },
                'profit': {
                    'btc': toNumber(profit_btc),
                    'usd': toNumber(profit_usd),
                    'median': profit_percent
                }
            }

            return item

        else:
            return False



    for a, ask in enumerate(orders_asks):

        for b, bid in enumerate(orders_bids):

            cup = ask['volume'][0]
            bucket = bid['volume'][0]

            price_from = ask['price'][0]
            price_to = bid['price'][0]

            ask_volume_to_btc = ask_price_btc if ask_price_btc else ask['price'][0]
            bid_volume_to_btc = bid_price_btc if bid_price_btc else bid['price'][0]

            index_ask = ask['index']
            index_bid = bid['index']

            check = bucket - cup

            if cup == 0:
                break


            if check < 0:
                orders_asks[a]['volume'][0] = abs(check)
                orders_bids[b]['volume'][0] = 0
                if bucket > 0:
                    item = prepareItem(ask_volume_to_btc, bid_volume_to_btc, bucket, price_from, price_to, index_ask, index_bid)
                    if item:
                        transactions_arr.append(item)

            elif check > 0:
                orders_asks[a]['volume'][0] = 0
                orders_bids[b]['volume'][0] = check
                if cup > 0:
                    item = prepareItem(ask_volume_to_btc, bid_volume_to_btc, cup, price_from, price_to, index_ask, index_bid)
                    if item:
                        transactions_arr.append(item)
                break

            elif check == 0:
                orders_asks[a]['volume'][0] = 0
                orders_bids[b]['volume'][0] = 0
                if cup > 0:
                    item = prepareItem(ask_volume_to_btc, bid_volume_to_btc, cup, price_from, price_to, index_ask, index_bid)
                    if item:
                        transactions_arr.append(item)

        else:
            continue



    # d()
    # jprint(transactions_arr)
    # exit()


    # Make median from array with percents for profit
    if total_arr['total_profit']['median']:
        total_arr['total_profit']['percent'] = round(statistics.median(total_arr['total_profit']['median']), 2)

    print(total_arr['total_profit']['percent'])

    response = {
        'data': transactions_arr,
        'total': total_arr
    }

    # jprint(response)
    # exit()

    return response


def smartEqualizer():
    # while True:
    if True:

        d('EQUALIZER: START!')

        # RESTORE DEFAULTS ARRAY'S
        global coinmarketcap_arr
        global orders_arr
        global stocks_arr
        coinmarketcap_arr = []
        orders_arr = []
        stocks_arr = []

        # START
        try:

            # COINMARKETCAP
            coinmarketcap_arr = CoinmarketcapModel.objects.all()

            # STOCKS DATA
            stocks_arr = StocksModel.objects.filter(active=1)

            # ORDERS ARR
            stocksSort(OrdersModel.objects.filter(stock_id__active=1))


            # MAKE COMBINATIONS
            combinations_arr = []
            for stock_a in orders_arr:
                for stock_b in orders_arr:
                    if stock_a['stock_id'] != stock_b['stock_id'] and \
                                    stock_a['coin_from'] == stock_b['coin_from']:

                        def prepare(type, arr_data):
                            item = {
                                'id': arr_data['id'],
                                'stock_id': arr_data['stock_id'],
                                'stock_name': arr_data['stock_name'],
                                'coin_from': arr_data['coin_from'],
                                'coin_to': arr_data['coin_to'],
                                'updated_at': dateConverter(arr_data['updated_at']),
                                'data': arr_data[type],
                                'fastlink': getStockData(arr_data['stock_name'], arr_data['coin_from'], arr_data['coin_to'])['fastlink'],
                            }

                            return item

                        arr_sell = prepare('arr_sell', stock_a)
                        arr_buy = prepare('arr_buy', stock_b)
                        arr_sell_tmp = []

                        for sell in arr_sell['data']:
                            price_sell = sell['price_btc']
                            price_buy = arr_buy['data'][0]['price_btc']

                            difference = ((price_buy - price_sell) / price_buy) * 100 if price_buy else 0
                            # print(difference)

                            if difference > config_difference_percent:
                                arr_sell_tmp.append(sell)

                        arr_sell['data'] = arr_sell_tmp

                        if len(arr_sell['data']) > 0 and len(arr_buy['data']) > 0:
                            # print( arr_sell['coin_from'], arr_buy['coin_to'], ' - ', arr_sell['coin_from'], arr_buy['coin_to'] )

                            combinations_arr.append({
                                'sell': arr_sell,
                                'buy': arr_buy,
                                'transactions': [],
                                'active': False,
                                # 'buy_stat': {
                                #     'count_old': len(arr_buy_tmp),
                                #     'count_new': len(arr_buy),
                                # }
                            })


            # COMPOSITOR
            combinations_with_transactions = []
            for index, comb in enumerate(combinations_arr):

                itemCompositor = compositor(copy.deepcopy(comb))
                if len(itemCompositor['data']) > 0:
                    item = comb
                    item['transactions'] = {
                        'coin_from': comb['sell']['coin_to'],
                        'coin_direction': comb['sell']['coin_from'],
                        'coin_to': comb['buy']['coin_to'],
                        'data': itemCompositor['data'],
                        'total': itemCompositor['total'],
                    }
                    combinations_with_transactions.append(item)


            # REMOVE SMALL PROFITABLE COMBS
            profit_combs_arr = []
            for comb in combinations_with_transactions:
                if comb['transactions']['total']['total_profit']['usd'] > config_minimal_profit_usd:
                    profit_combs_arr.append(comb)

            # SORTING BY PERCENT OF MEDIAN
            sorted_combs = sorted(profit_combs_arr, key=lambda k: k['transactions']['total']['total_profit'].get('percent', 0), reverse=True)

            d('EQUALIZER: SAVED!')
            jprint(sorted_combs)
            # exit()
            return sorted_combs


            # path = '_data/equalizer.json'
            # with open(path, 'w') as outfile:
            #     json.dump(sorted_combs, outfile)


            # time.sleep(10)

        except Exception as e:
            print('>>>ERROR:', e)


def main():
    smartEqualizer()
    pass


if __name__ == "__main__":
    main()
    pass