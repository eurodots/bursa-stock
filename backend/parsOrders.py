import threading

# ————————————————————————————————————————————
from equalizer.parsers.functions import parsOrdersByPairs
from equalizer.coinmarketcap.index import parsCoinmarketcap
# ————————————————————————————————————————————


threading.Thread(target=parsOrdersByPairs, args=()).start()
threading.Thread(target=parsCoinmarketcap, args=()).start()
