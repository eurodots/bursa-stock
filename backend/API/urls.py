"""django2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework import routers

from . import views

# router = routers.DefaultRouter()
# router.register(r'platform', views.platformViewSet, base_name='api-platform')

urlpatterns = [
    url(
        r'^(?P<api_key>\w{0,50})/stocks-list/$', views.stocksListViewSet, name='api-v1-stocks-list'),
    url(r'^(?P<api_key>\w{0,50})/stocks/(?P<stock_name>\w{0,50})/orders/(?P<coin_from>\w{0,10})/(?P<coin_to>\w{0,10})/$', views.stocksOrdersViewSet, name='api-v1-stocks-orders'),
    url(r'^(?P<api_key>\w{0,50})/stocks/(?P<stock_name>\w{0,50})/all-pairs/$', views.stocksAllPairsViewSet, name='api-v1-stocks-all-pairs'),
    url(r'^(?P<api_key>\w{0,50})/pairs/best-prices/(?P<coin_from>\w{0,10})/(?P<coin_to>\w{0,10})/$', views.pairsBestPricesViewSet, name='api-v1-pairs-best-prices'),
    url(r'^(?P<api_key>\w{0,50})/calc/math/(?P<coin_from>\w{0,10})/(?P<coin_to>\w{0,10})/(?P<amount>[-\w.]+)/$', views.calcMathViewSet, name='api-v1-calculator-math'),
    url(r'^(?P<api_key>\w{0,50})/calc/data/$', views.calcDataViewSet, name='api-v1-calculator-data'),

    url(r'^(?P<api_key>\w{0,50})/best-prices/ask/(?P<coin>\w{0,10})/$', views.BestPricesAskViewSet, name='api-v1-best-prices-ask'),
    url(r'^(?P<api_key>\w{0,50})/best-prices/bid/(?P<coin>\w{0,10})/$', views.BestPricesBidViewSet, name='api-v1-best-prices-bid'),

]
