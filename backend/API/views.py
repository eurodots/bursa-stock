from rest_framework.response import Response
from rest_framework.decorators import api_view

from .utils.Stocks import stocksList, getOrders, getAllPairs, getPairsBestPrices
from .utils.Calc import calculatorMath, calculatorData
from .utils.BestPrices import BestPrices




# STOCKS

@api_view(['GET'])
def stocksListViewSet(request, api_key):

    return Response(stocksList())


@api_view(['GET'])
def stocksOrdersViewSet(request, api_key, stock_name, coin_from, coin_to):

    return Response(getOrders(stock_name, coin_from, coin_to))




# PAIRS

@api_view(['GET'])
def stocksAllPairsViewSet(request, api_key, stock_name):

    return Response(getAllPairs(stock_name))


@api_view(['GET'])
def pairsBestPricesViewSet(request, api_key, coin_from, coin_to):

    return Response(getPairsBestPrices(coin_from, coin_to))



# CALCULATOR

@api_view(['GET'])
def calcMathViewSet(request, api_key, coin_from, coin_to, amount):

    return Response(calculatorMath(coin_from, coin_to, amount))


@api_view(['GET'])
def calcDataViewSet(request, api_key):

    return Response(calculatorData())




# BEST PRICES (ASK/BID)
@api_view(['GET'])
def BestPricesAskViewSet(request, api_key, coin):

    return Response(BestPrices('ask', coin))


@api_view(['GET'])
def BestPricesBidViewSet(request, api_key, coin):

    return Response(BestPrices('bid', coin))