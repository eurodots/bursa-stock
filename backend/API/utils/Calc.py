
# ————————————————————————————————————————————
from market.models import Coinmarketcap, Coins as MarketCoins
from .BestPrices import BestPrices
from DEFAULT.utils.functions import toFixed
# ————————————————————————————————————————————




def CoinBtcPriceExtender(coin, price_btc):

    coin = coin.upper()
    price_btc = float(price_btc)
    model = Coinmarketcap.objects.get(ticker='BTC')

    response = {
        'updated_at': model.updated_,
        'prices': {
            'BTC': toFixed((price_btc), 8),
            'ETH': toFixed((model.price_eth_ * price_btc), 8),
            'USD': toFixed((model.price_usd_ * price_btc), 2),
            'CNY': toFixed((model.price_cny_ * price_btc), 2),
            'RUB': toFixed((model.price_rub_ * price_btc), 2),
        }
    }

    if coin in response:
        # response.pop(coin, None)
        response[coin] = toFixed(1)

    return response


def calculatorMath(coin_from, coin_to, amount):

    coin_from  = str(coin_from).upper()
    coin_to    = str(coin_to).upper()
    amount     = float(amount)

    price_btc_ask = BestPrices('ask', coin_from)
    price_btc_bid = BestPrices('bid', coin_to)


    # ERRORS VALIDATOR
    errors_arr = {}
    if price_btc_ask['error']:
        errors_arr['coin_from'] = coin_from + ' not found'
    if price_btc_bid['error']:
        errors_arr['coin_to'] = coin_to + ' not found'
    if amount <= 0:
        errors_arr['amount'] = 'Amount is empty'
    if len(errors_arr) > 0:
        return {
            "error": True,
            "errors": errors_arr,
        }


    price_btc_ask = price_btc_ask['results']
    price_btc_bid = price_btc_bid['results']


    # Calculation
    a = price_btc_ask['price_btc']
    b = price_btc_bid['price_btc']
    total = float(a) / float(b)
    total = total * amount

    # Get price in BTC
    price_in_btc = price_btc_bid

    return {
        "error": False,
        "results": {
            'meta': {
                'coin_from': price_btc_ask,
                'coin_to': price_btc_bid,
            },
            'answer': {
                'coin': coin_to,
                'price': toFixed(total, 8),
                'rates': CoinBtcPriceExtender(coin_to, b),
            },
        },
    }




def calculatorData():

    coinst_arr = []

    modelMarketCoins = MarketCoins.objects.all()
    for coin in modelMarketCoins:

        label = coin.ticker
        name = False

        if coin.ticker.lower() != coin.name.lower():
            name = coin.name

        coinst_arr.append({
            'label': label,
            'name': name,
        })


    return {
        'error': False,
        'results': coinst_arr
    }




if __name__ == "__main__":
    # calculatorData()


    pass
