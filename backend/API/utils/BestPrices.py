import datetime
# ————————————————————————————————————————————
from django.db.models import Q
# ————————————————————————————————————————————
from market.models import Pairs as MarketPairs
from DEFAULT.utils.functions import toFixed
# ————————————————————————————————————————————


def BestPrices(type, coin):

    coin = coin.strip().upper()

    if coin == 'BTC':

        return {
            'error': False,
            'results': {
                'coin': 'BTC',
                'price_type': type,
                'price_btc': 1,
                'stock_name': False,
                'stock_id': False,
                'updated_at': datetime.datetime.now(),
                'fastlink': False,
            }
        }



    modelMarketPairs = MarketPairs.objects.filter(coin_from=coin, coin_to='BTC', stock__active=1).exclude(Q(bestprice_ask=0) | Q(bestprice_bid=0))
    if modelMarketPairs:
        if type == 'ask':
            m = modelMarketPairs.order_by('bestprice_ask')
            m = m[0]
            bestprice = m.bestprice_ask_

        elif type == 'bid':
            m = modelMarketPairs.order_by('-bestprice_bid')
            m = m[0]
            bestprice = m.bestprice_bid_


        return {
            'error': False,
            'results': {
                'coin': m.coin_from,
                'price_type': type,
                'price_btc': toFixed(bestprice),
                'stock_name': m.stock.name,
                'stock_id': m.stock.pk,
                'updated_at': m.updated_,
                'fastlink': m.fastlink_,
            }
        }

    return {
        'error': True,
        'message': 'Best prices for '+coin+' not found'
    }