from django.db.models import Q
# ————————————————————————————————————————————
from market.models import Pairs as MarketPairs, Stocks as MarketStocks
from DEFAULT.utils.functions import toFixed
# ————————————————————————————————————————————


def stocksList():

    stocks_arr = []

    modelMarketStocks = MarketStocks.objects.all()
    for stock in modelMarketStocks:
        stocks_arr.append({
            'stock_name': stock.name,
            'stock_id': stock.pk,
            'website': stock.website_,
            'active': stock.active,
        })

    return stocks_arr


def getOrders(stock_name, coin_from, coin_to):
    stock_name_ = str(stock_name).strip().upper()
    coin_from_  = str(coin_from).strip().upper()
    coin_to_    = str(coin_to).strip().upper()


    modelMarketPairs = MarketPairs.objects.filter(stock__name=stock_name_, coin_from=coin_from_, coin_to=coin_to_)
    if not modelMarketPairs:
        modelMarketPairs = MarketPairs.objects.filter(stock__name=stock_name_, coin_from=coin_to_, coin_to=coin_from_)
        if not modelMarketPairs:
            return {
                'error': True,
                'message': 'Pair '+coin_from_+'_'+coin_to_+' at '+stock_name_.capitalize()+' does not found...'
            }


    orders_arr = []
    for pair in modelMarketPairs:
        orders_arr.append({
            'stiock_name': pair.stock.name,
            'bestprice_ask': pair.bestprice_ask_,
            'bestprice_bid': pair.bestprice_bid_,
            'asks': pair.arr_asks_,
            'bids': pair.arr_bids_,
            'updated_at': pair.updated_,
        })

    return orders_arr



def getAllPairs(stock_name):
    stock_name_ = str(stock_name).strip().upper()

    modelMarketPairs = MarketPairs.objects.filter(stock__name=stock_name_)
    if not modelMarketPairs:
        return {
            'error': True,
            'message': 'Stock '+stock_name_+' does not found'
        }


    pairs_arr = []
    for pair in modelMarketPairs:
        pairs_arr.append({
            'pair_id': pair.pk,
            'pair_label': pair.pair,
            'coin_from': pair.coin_from,
            'coin_to': pair.coin_to,
        })

    return {
        'error': False,
        'results': pairs_arr,
    }



def getPairsBestPrices(coin_from, coin_to):
    coin_from_ = str(coin_from).strip().upper()
    coin_to_ = str(coin_to).strip().upper()


    modelMarketPairs = MarketPairs.objects.filter(coin_from=coin_from_, coin_to=coin_to_).exclude(Q(bestprice_ask=0) | Q(bestprice_bid=0))
    if not modelMarketPairs:
        modelMarketPairs = MarketPairs.objects.filter(coin_from=coin_to_, coin_to=coin_from_).exclude(Q(bestprice_ask=0) | Q(bestprice_bid=0))

        if not modelMarketPairs:
            return {
                'error': True,
                'message': 'Pair '+coin_from_+'_'+coin_to_+' not found...'
            }

    modelAsk = modelMarketPairs.order_by('bestprice_ask')
    modelBid = modelMarketPairs.order_by('-bestprice_bid')

    def arrView(modelItem, type):
        item = modelItem[0]

        price = item.bestprice_ask_ if type == 'ask' else item.bestprice_bid_
        return {
            'pair': item.pair,
            'price': [toFixed(price, 8), item.coin_to],
            'stock_name': item.stock.name,
            'stock_id': item.stock.pk,
            'updated_at': item.updated_,
            'fastlink': item.fastlink_,
        }

    view_ask = arrView(modelAsk, 'ask')
    view_bid = arrView(modelBid, 'bid')

    return {
        'ask': view_ask,
        'bid': view_bid,
    }
