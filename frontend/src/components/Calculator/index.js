import React, {Component} from 'react';
import PropTypes from 'prop-types';


import TimeAgo from 'react-timeago';
import FieldAutocomplete from './FieldAutocomplete'
import Preloader from '../Preloader'
import FormField from '../BlockForm/FormField'
import {apiCalculatorData, apiCalculatorMath, numConvert} from '../../utils/functions'
import theme from './theme.scss'


class Rates extends Component {

    render() {
        const {data, ticker} = this.props

        return (
            <ul className={theme.ratesTable}>
                {_.keys(data.prices).map((item, index) => {
                    if(ticker != item) {
                        return (
                            <li key={index}>
                                = {data.prices[item]} {item}
                            </li>
                        )
                    }
                })}
                <li data-li="date">
                    <TimeAgo date={data.updated_at} />
                </li>
            </ul>
        )
    }
}
Rates.propTypes = {
    data: PropTypes.object.isRequired,
    ticker: PropTypes.string.isRequired,
};


class Calculator extends Component {

    state = {
        data_coins: false,

        rates: false,
        meta: false,
        answer: 0,

        amount: 1,
        coin_from: '',
        coin_to: '',

        errors: {
            coin_from: false,
            coin_to: false,
            amount: false,
        },
    }

    componentDidMount() {
        this.loadData()
        this.setState({
            coin_from: this.props.coinFrom,
            coin_to: this.props.coinTo,
        }, () => {})
    }

    loadData() {
        apiCalculatorData().then((response) => {
          if(response.status == 200) {
              this.setState({data_coins: response.data.results}, () => {})
              this.calcData()
          }
        }, () => {})
    }

    calcData = () => {

        const {coin_from, coin_to, amount} = this.state

        // if(coin_from.toString().length >= 2 && coin_to.toString().length >= 2 && amount > 0) {

            apiCalculatorMath(coin_from, coin_to, amount).then((response) => {
              if(response.status == 200) {
                  if(!response.data.error) {
                      this.setState({
                          rates: response.data.results.answer.rates,
                          meta: response.data.results.meta,
                          answer: response.data.results.answer.price,
                          errors: {
                              coin_from: false,
                              coin_to: false,
                              amount: false,
                          }
                      }, () => {})
                  } else {
                      this.setState({
                          errors: response.data.errors,
                      }, () => {})
                      console.log(response.data.errors)
                  }
              }
            }, () => {})

        // }
    }
    switchCoins = () => {
        const {coin_to, coin_from, answer, amount, data_coins} = this.state
        this.setState({
            coin_from: coin_to,
            coin_to: coin_from,
            amount: answer,
            answer: amount,
            data_coins: false,
        }, () => {
            this.setState({data_coins}, () => {})
            this.calcData()
        })
    }

    handleInputValueFrom = (coin_from) => {
        // console.log('---from---')
        // console.log(value)
        this.setState({coin_from}, () => {
            this.calcData()
        })
    }
    handleInputValueTo = (coin_to) => {
        // console.log('---to---')
        // console.log(value)
        this.setState({coin_to}, () => {
            this.calcData()
        })
    }

    handleChangeInput = (event, name) => {

        let value = event.target.value
        if(value == '' || value < 0) {
            value = 0
        }

        this.setState({
            [name]: value
        }, () => {
            this.calcData()
        });

    };

	render() {
        const {data_coins, rates, coin_from, coin_to, meta, amount, answer, errors} = this.state

        if(!data_coins) {
            return (
                <Preloader />
            )
        }

        let amount_label = `Amount`
        if(!errors.coin_from) {
            amount_label = `Amount in ${coin_from}`
        }

		return (
            <div>
    			<div className={theme.calculatorWrapper}>
                    <div className={theme.message}>
                        Search for exchange rates occurs <br />in real time by all crypto-exchanges.
                    </div>
                    <div className={theme.formWrapper}>
                        <ul>
                            <li data-li="input">
                                <FormField
                                    state_value={this.state.amount}
                                    state_name="amount"
                                    label={amount_label}
                                    type="number"
                                    required={false}
                                    disabled={false}
                                    error={errors.amount}
                                    onChange={this.handleChangeInput}
                                     />
                            </li>
                            <li data-li="number">
                                {!errors.coin_from && !errors.coin_to ?
                                    <span data-link onClick={this.switchCoins}>Switch to {coin_to}</span>
                                : ''}

                            </li>
                        </ul>
                        <ul>
                            <li data-li="input">
                                <FieldAutocomplete handleInputValue={this.handleInputValueFrom} suggestions={data_coins} defaultValue={coin_from} label={`Coin from`} error={errors.coin_from} />
                            </li>
                            <li data-li="number">
                                = <span dangerouslySetInnerHTML={{__html: numConvert(amount)}} /> {!errors.coin_from && coin_from}
                                {!errors.coin_from && meta ?
                                    <ul data-ul="meta">
                                        <li>
                                            {meta.coin_from.stock_name && `Founded at ${meta.coin_from.stock_name}`}
                                        </li>
                                        <li>
                                            {meta.coin_from.updated_at && <TimeAgo date={meta.coin_from.updated_at} />}
                                        </li>
                                    </ul>
                                : ''}
                            </li>
                        </ul>
                        <ul>
                            <li data-li="input">
                                <FieldAutocomplete handleInputValue={this.handleInputValueTo} suggestions={data_coins} defaultValue={coin_to} label={`Coin to`} error={errors.coin_to} />
                            </li>
                            <li data-li="number">
                                = <span dangerouslySetInnerHTML={{__html: numConvert(answer)}} /> {!errors.coin_to && coin_to}
                                {!errors.coin_to && meta ?
                                    <ul data-ul="meta">
                                        <li>
                                            {meta.coin_to.stock_name && `Founded at ${meta.coin_to.stock_name}`}
                                        </li>
                                        <li>
                                            {meta.coin_to.stock_name && meta.coin_to.updated_at ? <TimeAgo date={meta.coin_to.updated_at} /> : ''}
                                        </li>
                                    </ul>
                                : ''}

                                {rates && !errors.coin_to ? <Rates data={rates} ticker={coin_to} /> : ''}
                            </li>
                        </ul>

                    </div>

                </div>
            </div>
		)
	}
}

Calculator.propTypes = {
    coinFrom: PropTypes.string.isRequired,
    coinTo: PropTypes.string.isRequired,
};

export default Calculator
