import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import SvgIcon from '../SvgIcon';

import theme from './theme.scss'


export default class BlockBenefits extends Component {

    state = {
        data: [
            {
                title: "MetaMask<sup>®</sup> plugin",
                description: "The MetaMask plug-in is already integrated into the system, which is a high security standard.",
                subtext: "Chrome, Opera, Firefox, IE",
                icon: false,
                icon_svg: "metamask",
                link: '/p/metamask'
            },
            {
                title: "Ethereum network",
                description: "The system is developed on the basis of Ethereum network with high reliability of work.",
                subtext: "",
                icon: false,
                icon_svg: "ethereum",
            },
            {
                title: "Smart Contract",
                description: "Our smart contract is Open Source and provides all functioning of the trades.",
                subtext: "github.com",
                icon: false,
                icon_svg: "github",
            },
            {
                title: "Mobile App",
                description: "The ability to run your own application for trading (In developing...)",
                subtext: "iOS, Android",
                icon: "touch_app",
                icon_svg: false,
            },
            {
                title: "Anti Hackers protection",
                description: "We guarantee the best in the world protection against hacking.",
                subtext: "We also do not have access",
                icon: "security",
                icon_svg: false,
            },
            {
                title: "Easy installation",
                description: "Setting up and running the system takes 10 minutes. No special knowledge required.",
                subtext: "",
                icon: "timer_off",
                icon_svg: false,
            },
        ]
    }

    renderElement(element) {
        return (
            <ul className={theme.blockInner}>
                <li data-li="icon">
                    {element.icon && <Icon>{element.icon}</Icon>}
                    {element.icon_svg && <SvgIcon name={element.icon_svg} gradient={true} />}
                </li>
                <li data-li="content">
                    <Typography variant="title" gutterBottom>
                        {element.link
                            ?
                                <Link to={element.link} data-link="underlined">
                                    <span dangerouslySetInnerHTML={{__html: element.title}} />
                                </Link>
                            : <span dangerouslySetInnerHTML={{__html: element.title}} />
                        }
                    </Typography>
                    {element.description}
                    {element.subtext && <div data-el="subtext">{element.subtext}</div>}
                </li>
            </ul>
        )
    }

	render() {

        const {data} = this.state

		return (
			<ul className={theme.wrapper}>
                {data.map((item, index) => {
                    return (
                        <li key={index}>
                            {this.renderElement(item)}
                        </li>
                    )
                })}
            </ul>
		)
	}
}
