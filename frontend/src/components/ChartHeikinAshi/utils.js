import { timeParse } from "d3-time-format";
import axios from 'axios';
const _ = require('lodash');
import {apiEndpoint} from '../../config/init'


const parseDate = timeParse("%Y-%m-%d");


function jsonPrepare(arr) {

	let result = []
	_.values(arr).map((item, index) => {
		result.push({
			absoluteChange: '',
			close: item.close,
			date: parseDate(item.date),
			dividend: '',
			high: item.high,
			low: item.low,
			open: item.open,
			percentChange: '',
			split: '',
			volume: item.volume,
		})
	})

	return result


}
export async function getData(tokenAddress) {

	// let url = `https://api.ethplorer.io/getPriceHistoryGrouped/${tokenAddress}?apiKey=freekey`
	let url = `${apiEndpoint}/datalist/ethplorer/${tokenAddress}/`
	console.log(url)

	const data = await axios.get(url).then((response) => {

		let arr = false
		if(response.data.results.history !== 'undefined') {
			arr = jsonPrepare(response.data.results.history.prices)
		}

		return arr
	}).catch(function(error) {
		console.log(error);
	})

	return data

}
