import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';


import BlockForm from '../../BlockForm'
import Calculator from '../../Calculator'

import theme from './theme.scss'

const styles = {
};

class PageCalculator extends Component {

    state = {
    }


	render() {
		return (
            <div>
    			<BlockForm bgid={33} title="Cryptocurrency Converter (Calculator)">
                    <Calculator coinFrom={`ETH`} coinTo={'BTC'} />
                </BlockForm>
            </div>
		)
	}
}

PageCalculator.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PageCalculator);
