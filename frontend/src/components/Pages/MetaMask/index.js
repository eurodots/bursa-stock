import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import BlockHeaderBg from '../../BlockHeaderBg';
import SvgIcon from '../../SvgIcon';
import {confSite} from '../../../config/init'
import theme from './theme.scss'


class PageMetaMask extends Component {


	render() {

		return (
			<div>
				<BlockHeaderBg
					bgid={25}
					title='MetaMask plugin'
					description={`Brings Ethereum to your browser`} />

				<div data-content-inner data-box>

					<ul className={theme.metamaskWrapper} >
						<li data-li="youtube">
							<iframe src="https://www.youtube.com/embed/6Gf_kRE4MJU?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						</li>
						<li data-li="content">

							<ul data-el="title">
								<li>
									<SvgIcon name="metamask" gradient={false} />
								</li>
								<li>
									<Typography variant="display2">
										How it works
									</Typography>
								</li>
							</ul>

							<p>
								MetaMask is a bridge that allows you to visit the distributed web of tomorrow in your browser today. It allows you to run Ethereum dApps right in your browser without running a full Ethereum node.
							</p>
							<p>
								MetaMask includes a secure identity vault, providing a user interface to manage your identities on different sites and sign blockchain transactions.
							</p>
							<p>
								Our mission is to make Ethereum as easy to use for as many people as possible.
							</p>
							<div>
								<Button color="primary" onClick={() => window.open(confSite.metamask_plugin.chrome)} variant="raised">
									Get Chrome Extension
								</Button>
								<Button color="primary" onClick={() => window.open(confSite.metamask_plugin.github)}>
									Github
								</Button>
							</div>
						</li>
					</ul>

				</div>
            </div>
		)
	}
}

export default PageMetaMask
