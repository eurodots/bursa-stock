import React from 'react';
import Button from 'material-ui/Button';
import Snackbar from 'material-ui/Snackbar';

class PositionedSnackbar extends React.Component {
  state = {
    open: true,
  };

  handleClick = state => () => {
    this.setState({ open: true, ...state });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
      // this.handleClick({ vertical: 'bottom', horizontal: 'center' })
  }

  render() {
    const { vertical, horizontal, open } = this.state;
    const {message} = this.props
    return (
      <span>
        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
          open={open}
          onClose={this.handleClose}
          SnackbarContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{message}</span>}
        />
      </span>
    );
  }
}

export default PositionedSnackbar;
