// @flow weak

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import Button from 'material-ui/Button';

import {numConvert} from '../../../../utils/functions'

import theme from './theme.scss'

const styles = theme => ({
});

class ContractBar extends React.Component {


    render() {
        const {classes} = this.props;

        const {data, tokenAddress, ordersbook} = this.props
        // console.log(config)

        return (
            <ul className={theme.dataList}>
                <li>
                    <ul className={theme.dataListInfo}>
                        <li>
                            <label>{data.ticker} Contract:</label>
                            <span>
                                {tokenAddress}
                            </span>
                        </li>
                        {ordersbook &&
                            <li>
                                <label>Middle price</label>
                                <span>
                                    <span dangerouslySetInnerHTML={{__html: numConvert(ordersbook.price_middle, 8)}} /> ETH
                                </span>
                            </li>}
                    </ul>
                </li>
                <li>
                    {this.props.children}
                </li>
            </ul>
        )
    }
}

ContractBar.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ContractBar);
