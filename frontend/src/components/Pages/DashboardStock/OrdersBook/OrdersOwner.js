// @flow weak

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import Typography from 'material-ui/Typography';
import Tooltip from 'material-ui/Tooltip';
import Icon from 'material-ui/Icon';


import {numConvert} from '../../../../utils/functions'
import {web3orderCancel} from '../../../../utils_stock/getWeb3.js'

const _ = require('lodash');

import theme from './theme.scss'

const styles = theme => ({
    root: {
        // padding: '10px 20px 20px',
    }
});

class OrdersOwner extends React.Component {

    state = {
        // data_orders: [],
    }

    // componentDidMount() {
    //     this.setState({data_orders: this.props.dataOrders})
    // }

    cancelOrder(type, item) {

        let type_ = type == 'arr_asks' ? 'sell' : 'buy'

        let init = {
            type: type_,
            amount: item.amount,
            stock_token: this.props.tokenAddress,
            price: item.price,
            spot_id: item.spot_id
        }

        web3orderCancel(init)
        this.props.onToggleRightMenu('hideAccount')

        // console.log(init)

        // return false
    }

    renderOrdersList(type) {
        const {ordersbook, userAddress} = this.props

        let ordersbook_arr = _.filter(ordersbook[type], {user_address: userAddress.toLowerCase()})


        return (
            <ul data-el="ordersOwner">
                <li>
                    <ul data-el="rowHeader">
                        <li>#</li>
                        <li>Price</li>
                        <li>Amount</li>
                        <li>&nbsp;</li>
                    </ul>
                </li>
                {ordersbook_arr.map((item, index) => {
                    return (
                        <li key={index}>
                            <ul>
                                <li data-el="spot">{item.spot_id}</li>
                                <li data-el="price">
                                    <span dangerouslySetInnerHTML={{__html: numConvert(item.price, 8)}} />
                                </li>
                                <li data-el="amount">
                                    <span dangerouslySetInnerHTML={{__html: numConvert(item.amount, 8)}} />
                                </li>
                                <li>
                                    <Tooltip title="Cancel this order" placement="right">
                                        <Icon
                                            data-el="iconDelete"
                                            onClick={() => this.cancelOrder(type, item)}>close</Icon>
                                    </Tooltip>
                                </li>
                            </ul>
                        </li>
                    )
                })}
            </ul>
        )
    }

    render() {
        const {classes} = this.props;
        // const {data_orders} = this.state

        return (
            <div className={classes.root}>

                <Typography variant="button">
                    SELL
                </Typography>
                {this.renderOrdersList('arr_asks')}

                <Typography variant="button">
                    BUY
                </Typography>
                {this.renderOrdersList('arr_bids')}

            </div>
        )
    }
}

OrdersOwner.propTypes = {
	classes: PropTypes.object.isRequired
};

// export default withStyles(styles)(OrdersOwner);

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
		})
	))
	(withStyles(styles)(OrdersOwner))
);
