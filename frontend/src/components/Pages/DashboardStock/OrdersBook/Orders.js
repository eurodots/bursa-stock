// @flow weak

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Tooltip from 'material-ui/Tooltip';

import Blockies from 'react-blockies';
import {numConvert} from '../../../../utils/functions'

import theme from './theme.scss'
const _ = require('lodash');

const styles = theme => ({
    root: {
        // padding: '10px 20px 20px',
    }
});


class OrdersBook extends React.Component {

    state = {
        // data_orders: [],
    }

    // componentDidMount() {
    //     this.applyData(this.props.dataOrders)
    // }
    //
    // componentWillReceiveProps(nextProps) {
    //     console.log('nextProps')
    //     console.log(nextProps)
    //     this.applyData(nextProps.dataOrders)
    //
    // }
    // applyData(data) {
    //     this.setState({data_orders: data})
    // }


    renderOrdersList(type) {

        const {ordersbook} = this.props
        // let data_orders = this.props.dataOrders
        let data_orders_arr = ordersbook[type]

        let totalAmount = type == 'arr_asks' ? ordersbook.total_amount_asks : ordersbook.total_amount_bids;
        // let totalAmount = _.sumBy(data_orders_arr, 'amount')

        const {init} = this.props

        return (
            <div className={theme.ordersListWrapper}>


                <div data-el={type}>
                    <ul data-el="title">
                        <li>
                            <Typography variant="button">
                                {type == 'arr_asks' ? 'Sell' : 'Buy'} ({data_orders_arr.length})
                            </Typography>
                        </li>
                        <li>
                            Total amount: <span dangerouslySetInnerHTML={{__html: numConvert(totalAmount, 8)}} />
                        </li>
                    </ul>
                    <ul data-el="list">
                        <li>
                            <ul data-el="rowHeader">
                                <li>#</li>
                                <li></li>
                                <li>Price {init.coin_to}</li>
                                <li>Amount {init.coin_from}</li>
                                <li>Total {init.coin_to}</li>
                            </ul>
                        </li>
                        {data_orders_arr.map((item, index) => {

                            let volumeBar = (100 / totalAmount) * parseFloat(item.amount)
                                volumeBar = volumeBar.toFixed(0)
                                volumeBar = `${volumeBar}%`

                            // let price = Number(item.price).toFixed(6)
                            // let amount = Number(item.amount).toFixed(5)
                            // let total = Number(item.total).toFixed(3)

                            return (
                                <li key={index} onClick={() => this.props.autocompleteByOrder({price: item.price, amount: item.amount})}>
                                    <div data-el="volumeBar">
                                        <div style={{width: volumeBar}}></div>
                                    </div>
                                    <ul data-el={this.props.userAddress.toLowerCase() == item.user_address.toLowerCase() ? 'rowOrderOwner' : 'rowOrder'}>
                                        <li data-el="id">
                                            {item.spot_id}
                                        </li>
                                        <li data-el="user">
                                            <Tooltip title={item.user_address}>
                                                <div data-el="userIcon">
                                                    <Blockies
                                                        seed={item.user_address}
                                                        size={5}
                                                        scale={3}
                                                        color="#3f51b5"
                                                        bgColor="#fff"
                                                        spotColor="#FF4081"
                                                      />
                                                </div>
                                            </Tooltip>
                                        </li>
                                        <li data-el="price">
                                            <span dangerouslySetInnerHTML={{__html: numConvert(item.price, 8)}} />
                                        </li>
                                        <li data-el="amount">
                                            <span dangerouslySetInnerHTML={{__html: numConvert(item.amount, 5)}} />
                                        </li>
                                        <li data-el="total">
                                            <span dangerouslySetInnerHTML={{__html: numConvert(item.total, 5)}} />
                                        </li>
                                    </ul>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            </div>
        )
    }
    render() {
        const {classes} = this.props;
        // const {data_orders} = this.state

        return (
            <div className={classes.root}>
                {this.renderOrdersList('arr_asks')}
                {this.renderOrdersList('arr_bids')}
            </div>
        )
    }
}

OrdersBook.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(OrdersBook);
