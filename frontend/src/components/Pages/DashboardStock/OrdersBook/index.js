import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
// import SwipeableViews from 'react-swipeable-views';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Typography from 'material-ui/Typography'

import Orders from './Orders';
import OrdersOwner from './OrdersOwner';
import Preloader from '../../../Preloader'

const _ = require('lodash');


function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: '20px 0 0' }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    // width: 500,
  },
  appBar: {
      boxShadow: 'none',
      marginTop: '-10px',
  },
  indicator: {
      height: 5,
  },
  tab: {
      minWidth: 'inherit',
  },
});

class AskBidTabs extends React.Component {
  state = {
    tab_active: 'all',
    data_preloader: true,
  };

  handleChangeTab = (event, value) => {
    this.setState({ tab_active: value });
  };



  render() {
    const { classes, theme, init, ordersbook, userAddress, ordersbookPreloader } = this.props;
    const { tab_active, data_preloader } = this.state


    let title_orders = 'Orders Book'
    if(ordersbookPreloader) {
        title_orders = <Preloader size={20} />
    }

    let ordersbookOwnerSum = 0
    if(ordersbook && userAddress) {
        let asks = _.filter(ordersbook.arr_asks, {user_address: userAddress.toLowerCase()})
        let bids = _.filter(ordersbook.arr_bids, {user_address: userAddress.toLowerCase()})
        ordersbookOwnerSum = asks.length + bids.length
    }


    if(ordersbook === false) {
        return (<div>No orders</div>)
    }

    return (
      <div className={classes.root}>

        <div>
            <AppBar position="static" color="inherit" className={classes.appBar}>
              <Tabs
                value={this.state.tab_active}
                onChange={this.handleChangeTab}
                indicatorClassName={classes.indicator}
                indicatorColor="primary"
                textColor="primary"
                fullWidth
                >
                <Tab value="all" label={title_orders} className={classes.tab} style={{flex: '1 1 55%'}} />
                <Tab value="my" label={`My (${ordersbookOwnerSum})`} className={classes.tab} />
              </Tabs>
            </AppBar>

            <TabContainer dir={theme.direction}>
                {tab_active == 'all' && <Orders
                    autocompleteByOrder={this.props.autocompleteByOrder}
                    init={init}
                    ordersbook={ordersbook}
                    tokenAddress={this.props.tokenAddress}
                    userAddress={this.props.userAddress} />}
                {tab_active == 'my' && <OrdersOwner
                    ordersbook={ordersbook}
                    tokenAddress={this.props.tokenAddress}
                    userAddress={this.props.userAddress} />}
            </TabContainer>
        </div>

      </div>
    );
  }
}

AskBidTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(AskBidTabs);
