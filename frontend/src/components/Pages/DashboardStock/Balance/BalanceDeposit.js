import React from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';
// import NumberFormat from 'react-number-format';
import Input from 'material-ui/Input';
import Slider from 'react-toolbox/lib/slider';


import FormField from '../../../BlockForm/FormField'
import Preloader from '../../../Preloader'
import SnackMetaMask from '../SnackMetaMask'
import {depositWeb3} from '../../../../utils_stock/getWeb3.js'
import {numConvert} from '../../../../utils/functions'

import theme from './theme.scss'


const styles = theme => ({
  buttonApproved: {
      width: '100%',
      display: 'block',
  },
});

class BalanceApprove extends React.Component {
  state = {
    open: false,
    slider_value: '',
    slider_minimum: 0,
    snackbar: false,
  };

  timeout = false
  clearTimer = () => {
      var that = this
      clearTimeout(that.timeout)
  }


  componentWillUnmount () {
      this.props.onRef(undefined)
      this.clearTimer()
  }

  componentDidMount() {
      this.props.onRef(this)
      // this.setState({
      //
      // })
      // this.getBalanceApproved()
  }

  handleChangeApprove = (event) => {
    this.setState({slider_value: event.target.value}, () => {});
  };

  handleChangeSlider = (value) => {
    this.setState({slider_value: value});
  };

  handleClickOpen = () => {
    this.setState({
        open: true,
        snackbar: false,
    });
  };

  handleClose = () => {
    this.setState({
        open: false,
    });
  };

  saveForm = () => {
      const {slider_value} = this.state
      if(slider_value > 0) {
          depositWeb3(slider_value)
      }

      this.setState({snackbar: true})

      this.handleClose()
      this.props.onToggleRightMenu(true)
  }

  checkBalance = (amount) => {
      const {stock} = this.props
      let balance_deposited = stock.web3Metamask.contract_balance

      if(amount > balance_deposited) {
          // alert('Difference is '+(amount - balance_approved))
          this.setState({
              slider_value: (amount - balance_deposited)
          })
          this.handleClickOpen()
          return false
      } else {
          return true
      }
  }

  // async getBalanceApproved() {
  //     let user_address = this.props.userAddress
  //     let token_address = this.props.tokenAddress
  //
  //     const { slider_value } = this.state
  //
  //     if(token_address && token_address) {
  //         web3balanceForToken(token_address, user_address).then((res) => {
  //             this.setState({
  //                 balance_deposited: res,
  //                 slider_value: slider_value == 0 ? res : slider_value
  //             })
  //         })
  //
  //         web3balanceOf(token_address, user_address).then((res) => {
  //             console.log('callTokenBalance()')
  //             console.log(res)
  //             this.setState({balance_available: res})
  //         })
  //     }
  //
  //
  //     this.timeout = setTimeout(() => {
  //         this.getBalanceApproved()
  //     }, 3000)
  //
  //
  // }

  renderForm() {
      const { classes, ticker, stock } = this.props;
      const { slider_value, slider_minimum } = this.state;

      let balance_deposited = stock.web3Metamask.contract_balance
      let balance_available = stock.web3Metamask.user_balance

      return (
        <div>
            <ul className={theme.balanceData}>
                <li>
                    <label>Deposited:</label>
                    <span>
                        <span dangerouslySetInnerHTML={{__html: numConvert(balance_deposited)}} />
                        <span data-el="ticker">{ticker}</span>
                    </span>
                </li>
                <li>
                    <label>Available:</label>
                    <span>
                        <span dangerouslySetInnerHTML={{__html: numConvert(balance_available)}} />
                        <span data-el="ticker">{ticker}</span>
                    </span>
                </li>
                <li>
                    <label>Will deposited:</label>
                    <span>
                        <span dangerouslySetInnerHTML={{__html: numConvert(parseFloat(balance_deposited) + parseFloat(slider_value))}} />
                        <span data-el="ticker">{ticker}</span>
                    </span>
                </li>
            </ul>

            <FormField
                state_value={this.state.slider_value}
                state_name="slider_value"
                label="Increasing deposit"
                placeholder="Amount"
                type="money"
                required={false}
                disabled={false}
                error={false}
                onChange={(event) => this.handleChangeApprove(event)}
                autoFocus={true}
                 />

            <div>
                <Slider
                    min={0}
                    max={parseFloat(balance_available)}
                    value={parseFloat(slider_value)}
                    onChange={(value) => this.handleChangeSlider(value)} />
            </div>

      </div>
      )
  }

  render() {
    const { fullScreen, classes, ticker, stock } = this.props;
    const { slider_value, snackbar } = this.state;

    const contract_balance = stock.web3Metamask.contract_balance

    // let disabledButton = false
    // if(slider_value == 0) {
    //     disabledButton = true
    // }

    return (
      <div>

          <span onClick={this.handleClickOpen} data-link>
              <span dangerouslySetInnerHTML={{__html: numConvert(contract_balance)}} /> {ticker}
          </span>

          {snackbar && <SnackMetaMask
              message="Confirm this action in MetaMask"
           />}

        <Dialog
          fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
          maxWidth="xs"
        >
          <DialogTitle id="responsive-dialog-title">{"Depositing"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Before buying orders, make sure that you have made a sufficient deposit.
            </DialogContentText>
            {this.renderForm()}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.saveForm} color="primary" variant="raised" disabled={slider_value == 0}>
              Make a deposit
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

BalanceApprove.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
};

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
            onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
		})
	))
	(withMobileDialog()(withStyles(styles)(BalanceApprove)))
);
