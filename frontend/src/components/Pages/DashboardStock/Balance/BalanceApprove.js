import React from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';
// import NumberFormat from 'react-number-format';
import Input from 'material-ui/Input';
import Slider from 'react-toolbox/lib/slider';


import FormField from '../../../BlockForm/FormField'
import Preloader from '../../../Preloader'
import SnackMetaMask from '../SnackMetaMask'
import {web3balanceForToken, web3balanceOf, web3approve} from '../../../../utils_stock/getWeb3.js'
import {numConvert} from '../../../../utils/functions'
import theme from './theme.scss'


const styles = theme => ({
  buttonApproved: {
      width: '100%',
      display: 'block',
  },
});

class BalanceApprove extends React.Component {
  state = {
    open: false,
    balance_approved: 0,
    balance_token: 0,
    slider_value: 0,
    slider_minimum: 0,
    snackbar: false,
  };

  timeout = false
  clearTimer = () => {
      var that = this
      clearTimeout(that.timeout)
  }


  componentWillUnmount () {
      this.props.onRef(undefined)
      this.clearTimer()
  }

  componentDidMount() {
      this.props.onRef(this)
      this.getBalanceApproved()
  }

  handleChangeApprove = (event) => {
    this.setState({slider_value: event.target.value}, () => {});
  };

  handleChangeSlider = (value) => {
    this.setState({slider_value: value});
  };

  handleClickOpen = () => {
    this.setState({
        open: true,
        snackbar: false,
    });
  };

  handleClose = () => {
    this.setState({
        open: false,
    });
  };

  checkBalance = (amount) => {
      const {balance_approved} = this.state

      if(amount > balance_approved) {
          // alert('Difference is '+(amount - balance_approved))
          this.setState({
              slider_value: amount
          })
          this.handleClickOpen()
          return false
      } else {
          return true
      }
  }

  // approveExtra(value) {
  //
  //     this.setState({
  //         slider_value: value,
  //         slider_minimum: value,
  //         open: true,
  //     })
  // }

  saveForm = () => {
      let init = {
          token: this.props.tokenAddress,
          value: this.state.slider_value
      }

      web3approve(init).then((res) => {
          console.log(res)
      })

      this.setState({snackbar: true})

      this.handleClose()
      this.props.onToggleRightMenu(true)
  }



  async getBalanceApproved() {
      let user_address = this.props.userAddress
      let token_address = this.props.tokenAddress

      const { slider_value } = this.state

      if(token_address && token_address) {
          web3balanceForToken(token_address, user_address).then((res) => {
              this.setState({
                  balance_approved: res,
                  slider_value: slider_value == 0 ? res : slider_value
              })
          })

          web3balanceOf(token_address, user_address).then((res) => {
              // console.log('callTokenBalance()')
              // console.log(res)
              this.setState({balance_token: res})
          })
      }


      this.timeout = setTimeout(() => {
          this.getBalanceApproved()
      }, 3000)


  }

  renderForm() {
      const { classes, ticker } = this.props;
      const { slider_value, slider_minimum, balance_approved, balance_token } = this.state;

      return (
        <div>

            <ul className={theme.balanceData}>
                <li>
                    <label>Approved:</label>
                    <span>
                        <span dangerouslySetInnerHTML={{__html: numConvert(balance_approved)}} />
                        <span data-el="ticker">{ticker}</span>
                    </span>
                </li>
                <li>
                    <label>Available:</label>
                    <span>
                        <span dangerouslySetInnerHTML={{__html: numConvert(balance_token)}} />
                        <span data-el="ticker">{ticker}</span>
                    </span>
                </li>
            </ul>

            <FormField
                state_value={this.state.slider_value}
                state_name="slider_value"
                label="Changing approved balance"
                placeholder="Amount"
                type="money"
                required={false}
                disabled={false}
                error={false}
                onChange={(event) => this.handleChangeApprove(event)}
                autoFocus={true}
                 />

            <div>
                <Slider
                    min={0}
                    max={parseFloat(balance_token)}
                    value={parseFloat(slider_value)}
                    onChange={(value) => this.handleChangeSlider(value)} />
            </div>

      </div>
      )
  }

  render() {
    const { fullScreen, classes, ticker } = this.props;
    const { balance_approved, slider_value, snackbar } = this.state;

    return (
      <div>

          <span onClick={this.handleClickOpen} data-link>
              <span dangerouslySetInnerHTML={{__html: numConvert(balance_approved)}} /> {ticker}
          </span>

          {snackbar && <SnackMetaMask
              message="Confirm this action in MetaMask"
           />}

        <Dialog
          fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
          maxWidth="xs"
        >
          <DialogTitle id="responsive-dialog-title">{"Increase approved funds"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Specify the amount allowed for payment when executing the orders of this token.
            </DialogContentText>
            {this.renderForm()}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.saveForm} color="primary" variant="raised" disabled={balance_approved == slider_value}>
              Approve
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

BalanceApprove.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
};

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
            onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
		})
	))
	(withMobileDialog()(withStyles(styles)(BalanceApprove)))
);
