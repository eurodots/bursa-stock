import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';


import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import green from 'material-ui/colors/green';
import NumberFormat from 'react-number-format';
import {numConvert} from '../../../../utils/functions'
import dateFormat from 'dateformat';

import SnackMetaMask from '../SnackMetaMask'
import FormField from '../../../BlockForm/FormField'
import {web3orderSell, web3orderBuy} from '../../../../utils_stock/getWeb3'

import theme from './theme.scss'

const _ = require('lodash');




const mui = createMuiTheme({
  // palette: {
  //   primary: {
	// 	main: green[500],
	// 	contrastText: 'white',
	// }, // Purple and green play nicely together.
  //   // primary: { main: '#11cb5f' }, // This is just green.A700 as hex.
  // },
});

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
});

class AskBidForm extends React.Component {

    state = {
        amount: '',
        price: '',
        total_amount_sell: 0,
        submit_disabled: false,
		snackbar: false,
    }


    timeout = false
    clearTimer = () => {
        var that = this
        clearTimeout(that.timeout)
    }

    componentWillUnmount () {
        this.props.onRef(undefined)
        this.clearTimer()
    }

    componentDidMount() {
        this.props.onRef(this)
    }

    autocompleteByOrder = (init) => {
        let price = init.price.toPrecision(8)
        let amount = init.amount.toPrecision(8)
        this.setState({
            price: price,
            amount: amount,
        })
    }


	snackbarInit(type) {
		if(type) {
			this.setState({snackbar: false}, () => {
				this.setState({snackbar: true})
			})
		} else {
			this.setState({snackbar: false})
		}
	}


    initOrderPlace = (event, type) => {
		event.preventDefault()
		document.activeElement.blur()

        const {amount, price} = this.state
		const {data} = this.props

		if(type == 'sell') {
			if(this.props.refBalanceApprove.checkBalance(amount)) {

				this.props.onToggleRightMenu(true)

				this.snackbarInit(true)
				web3orderSell({
					amount: amount,
					price: price,
					stock_token: data.token_address,
                    user_address: data.user_address,
				}).then(response => {
					if(response) {
						this.setState({
							amount: '',
							price: '',
						})
						alert('Order was placed!')
					}
				})

			} else {
				// ...
			}

		} else if(type == 'buy') {
			if(this.props.refBalanceDeposit.checkBalance(amount * price)) {

				this.props.onToggleRightMenu(true)


				this.snackbarInit(true)
				web3orderBuy({
					amount: amount,
					price: price,
					stock_token: data.token_address,
                    user_address: data.user_address,
				}).then(response => {
					if(response) {
						this.setState({
							amount: '',
							price: '',
						})
						alert('Order was placed!')
					}
				})

			} else {
				// ...
			}

		}

    }


    handleChange = name => event => {
		let value = event.target.value
		if(value < 0) {
			value = 0
		}
        this.setState({[name]: value}, () => {})
    };


    pastRecommendPrice = (price) => {
        this.setState({price: price.toFixed(8)})
    }



    renderForm() {
        const { classes, data, ordersbook } = this.props;
        const {
        	amount,
        	price,
        	submit_disabled
        } = this.state;


        let total = amount * price


        // Get best price
        let bestPrice = data.type == 'sell' ? ordersbook.bestprice_ask : ordersbook.bestprice_bid


        return (
			<MuiThemeProvider theme={mui}>
				<div className={theme.AskBidForm}>

					<form onSubmit={(event) => this.initOrderPlace(event, data.type)}>
						<FormField
			                state_value={this.state.amount}
			                state_name="amount"
			                placeholder={`Amount ${data.coin_from}`}
							label={`Amount ${data.coin_from}`}
			                type="money"
			                required={true}
			                disabled={false}
			                error={false}
			                onChange={this.handleChange('amount')}
			                autoFocus={false}
			                 />

						 <FormField
							 state_value={this.state.price}
							 state_name="price"
							 placeholder={`Price ${data.coin_to}`}
							 label={`Price ${data.coin_to}`}
							 type="money"
							 required={true}
							 disabled={false}
							 error={false}
							 onChange={this.handleChange('price')}
							 autoFocus={false}
							  />

                          {bestPrice && <div className={theme.recommendPrice}>
                              Recommend: <span data-link onClick={() => this.pastRecommendPrice(bestPrice)} dangerouslySetInnerHTML={{__html: numConvert(bestPrice)}} />
                          </div>}


		                <Typography variant="subheading" className={theme.total}>
		                    <span data-el="totalLabel">Total:</span>
							<span data-el="totalNumber">
								≈ <span dangerouslySetInnerHTML={{__html: numConvert(total)}} /> {data.coin_to}
							</span>
		                </Typography>


		                {data.type == 'sell' &&
		                <Button type="submit" variant="raised" disabled={submit_disabled} size="large" color="secondary" className={theme.actionButton} >
		                  Sell
		                </Button>}

		                {data.type == 'buy' &&
		                <Button type="submit" variant="raised" disabled={submit_disabled} size="large" color="primary" className={theme.actionButton} >
		                  Buy
		                </Button>}
					</form>

				</div>
			</MuiThemeProvider>
		)
    }



	render() {
		const { snackbar } = this.state;

        return (
            <div>
				{this.renderForm()}

				{snackbar && <SnackMetaMask
	                message="Confirm this action in MetaMask"
	             />}
			 </div>
        )

	}
}

AskBidForm.propTypes = {
	classes: PropTypes.object.isRequired
};

// export default withStyles(styles)(AskBidForm);

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
            onSaveWeb3Transactions: (payload) => {
				dispatch({type: 'SAVE_WEB3_TRANSACTIONS', payload})
			},
		})
	))
	(withStyles(styles)(AskBidForm))
);
