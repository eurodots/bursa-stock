import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
// import SwipeableViews from 'react-swipeable-views';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Typography from 'material-ui/Typography'
import Icon from 'material-ui/Icon';

import AskBidForm from './AskBidForm';
import {confSite} from '../../../../config/init'
import style from './theme.scss'

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: '20px 0 0' }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired,
};

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    // width: 500,
  },
  appBar: {
      boxShadow: 'none',
      marginTop: '-10px',
  },
  // indicator: {
  //     height: 5,
  // },
  tab: {
      minWidth: 'inherit',
  }
});

class AskBidTabs extends React.Component {
  state = {
    value: 'sell',
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  // handleChangeIndex = index => {
  //   this.setState({ value: index });
  // };

  componentWillUnmount () {
      this.props.onRef(undefined)
  }

  componentDidMount() {
      this.props.onRef(this)
  }

  autocompleteByOrder = (init) => {
      this.refAskBidForm.autocompleteByOrder(init)
  }

  render() {
    const { classes, theme, coinFrom } = this.props;

    const { value } = this.state

    return (
      <div className={classes.root}>
        <AppBar position="static" color="inherit" className={classes.appBar}>
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            fullWidth
          >
            <Tab value="sell" label={`Sell ${coinFrom}`} className={classes.tab} />
            <Tab value="buy" label={`Buy ${coinFrom}`} className={classes.tab} />
          </Tabs>
        </AppBar>

        <TabContainer dir={theme.direction}>
            <AskBidForm
                onRef={ref => (this.refAskBidForm = ref)}
                refBalanceApprove={this.props.refBalanceApprove}
                refBalanceDeposit={this.props.refBalanceDeposit}
                ordersbook={this.props.ordersbook}
                data={{
                    token_address: this.props.tokenAddress,
                    user_address: this.props.userAddress,
                    type: value,
                    coin_from: coinFrom,
                    coin_to: 'ETH'
                }} />
        </TabContainer>

        <ul className={style.smartContractBlock}>
            <li>
                <Icon data-el="icon">verified_user</Icon>
            </li>
            <li>
                Security is provided by our Smart Contract & MetaMask.
                <p data-el="links">
                    <a href={confSite.smartcontract.etherscan} target="_blank" data-link="underlined">Check Contract</a>
                    <a href={confSite.smartcontract.github} target="_blank" data-link="underlined">Github</a>
                </p>
            </li>
        </ul>

      </div>
    );
  }
}

AskBidTabs.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(AskBidTabs);
