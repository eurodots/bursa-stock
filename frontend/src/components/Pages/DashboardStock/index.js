import React from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';


import TimeAgo from 'react-timeago';
import Preloader from '../../Preloader'

// import MetaMaskLogo from './MetaMaskLogo'
// import Pairs from './Pairs/index.js'
// import {getCurrentToken} from '../../../utils_stock/functions'
import {apiCoinsListCurrent} from '../../../utils/functions'
import {web3ordersBook} from '../../../utils_stock/getWeb3.js'

// import Details from './Details'
import Charts from './Charts'

import AskBidForm from './AskBidForm'
import OrdersBook from './OrdersBook'
import ContractBar from './ContractBar'
import BalanceApprove from './Balance/BalanceApprove'
import BalanceDeposit from './Balance/BalanceDeposit'

// ******** MY COMPONENTS ********** //
import ModalDetails from '../CoinsList/ModalDetails/'


// import {getCoinsArr} from '../../../utils_stock/functions'
const _ = require('lodash');

import theme from './theme.scss'

const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

class Dashboard extends React.Component {

	constructor (props) {
		super(props)
		this.state = {
			data: false,
            // MetaMaskLoaded: false,
            page_not_found: false,
            ordersbook: false,
            ordersbook_preloader: false,
		}
		// this.openDetails = this.openDetails.bind(this)
	}

    timeout = false
    clearTimer = () => {
        var that = this
        clearTimeout(that.timeout)
    }

    componentWillUnmount () {
        this.clearTimer()
    }

    initStockConfig() {
      // function getElementAttrs(el) {
      //   return [].slice.call(el.attributes).map((attr) => {
      //       return {name: attr.name, value: attr.value}
      //   });
      // }
      //
      // let el = document.getElementById('stock-widget')
      // let attrs = getElementAttrs(el)
      // let stock_name = _.find(attrs, { 'name': 'data-stock-name' })
      // let stock_token = _.find(attrs, { 'name': 'data-stock-token' })
      // let stock_private = _.find(attrs, { 'name': 'data-stock-private' })
      //
      // this.props.onStockInit({
      //     stock_name: stock_name ? stock_name.value : '',
      //     stock_token: stock_token ? stock_token.value : '',
      //     stock_private: stock_private ? stock_private.value : '',
      // })
    }

	componentDidMount() {

        this.loadData(this.props.match.params.coin_id)
        // this.getBalanceApproved()
		// this.props.onHideAllMenus()

        this.initStockConfig()
        // this.preloadData()

        console.log('<Dashboard /> componentDidMount()')
	}

	componentWillReceiveProps(nextProps) {

		const coin_next = nextProps.match.params.coin_id
		const coin_this = this.props.match.params.coin_id
		if(coin_next != coin_this) {
			this.setState({data: false})
			this.loadData(nextProps.match.params.coin_id)
		}
	}

	loadData(coin) {
		// const coin = this.props.match.params.coin_id
		const that = this
		// getCurrentToken(coin).then(function(response) {
        //     if(response) {
        //         that.setState({data: response.data}, function() {
        //             document.title = response.data.ethplorer.tokenInfo.name
        //         })
        //     }
		// })
        apiCoinsListCurrent(coin).then((response) => {

			if(response.status == 200) {
                console.log(response.data)
                if(response.data.error) {
                    this.setState({page_not_found: true})
                } else {
    				this.setState({
    					data: response.data.results,
    				})
                }

                this.getOrdersbook()
			}

            // console.log(this.state.data)
			// this.timeout = setTimeout(() => {
	        //     this.loadData()
	        // }, 10000)

		}, () => {})
	}

    // returnDataFromOrdersBook(type) {
    //     return this.refOrdersBook.returnDataToRef(type)
    // }



    // Fetching orders from WEB3JS
    async getOrdersbook() {

        // console.log('getOrdersData()')

        const {data} = this.state
        const user_address = this.props.stock.web3Metamask.user_address

        this.setState({ordersbook_preloader: true})
        await web3ordersBook(data.token).then(ordersbook => {


            // Get middle price of orderbook
            /*
            let ordersbook_middle_price = false
            let orders_sell_arr = _.filter(ordersbook, {type: 'sell'})
            let orders_buy_arr = _.filter(ordersbook, {type: 'buy'})
            if(orders_sell_arr.length > 0 && orders_buy_arr.length > 0) {
                let orders_sell = _.orderBy(orders_sell_arr, ['price'], 'asc');
                let orders_buy = _.orderBy(orders_buy_arr, ['price'], 'desc');
                ordersbook_middle_price = (orders_sell[0].price + orders_buy[0].price) / 2
            }
            */
            // console.log('/////-----')
            // console.log(ordersbook)

            this.setState({
                ordersbook: ordersbook,
                ordersbook_preloader: false,
            })
        })

        this.timeout = setTimeout(() => {
            this.getOrdersbook()
        }, 3000)

    }



    // Autocomplete price & amount into Order form, when clicking by the order
    autocompleteByOrder = (init) => {
        this.refAskBidForm.autocompleteByOrder(init)
    }




	renderBoard() {
		const { message } = this.props
		const {data, ordersbook, ordersbook_preloader} = this.state

        // const token_address = this.props.match.params.coin_id
        const user_address = this.props.stock.web3Metamask.user_address
        // let user_deposit = this.props.stock.web3Metamask.contract_balance
        //     user_deposit = `${user_deposit} ETH`

        if(!data) {
            return <Preloader />
        }

		return (
			<div>

				<ul className={theme.boardHeader}>
					<li>
						<img src={data.favicon} />
					</li>
					<li>
						<Typography variant="display1" className={theme.title}>
							{data.ticker} <span>& ETH</span>
						</Typography>
					</li>
				</ul>

				<div className={theme.wrapper}>
                    <div className={theme.container} data-el="header">
                        <div className={theme.content}>
                            <ContractBar data={data} tokenAddress={data.token} ordersbook={ordersbook}>
                                <ModalDetails request={data.token} />
                             </ContractBar>
                        </div>
                        <div className={theme.sidebar}>
                            <ul>
                                <li>
                                    <label>Approved:</label>
                                    <BalanceApprove
                                        onRef={ref => (this.refBalanceApprove = ref)}
                                        tokenAddress={data.token}
                                        userAddress={user_address}
                                        ticker={data.ticker} >
                                    </BalanceApprove>
                                </li>
                                <li>
                                    <label>Deposited:</label>
                                    <BalanceDeposit
                                        onRef={ref => (this.refBalanceDeposit = ref)}
                                        tokenAddress={data.token}
                                        userAddress={user_address}
                                        ticker={`ETH`} >
                                    </BalanceDeposit>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className={theme.container}>
                        <div className={theme.sidebar}>
                            {ordersbook && <OrdersBook
                                ordersbook={ordersbook}
                                ordersbookPreloader={ordersbook_preloader}
                                init={{
                                    coin_from: data.ticker,
                                    coin_to: 'ETH',
                                }}
                                autocompleteByOrder={this.autocompleteByOrder}
                                tokenAddress={data.token}
                                userAddress={user_address} />}
                        </div>
                        <div className={theme.content}>
                            <Charts tokenAddress={data.token} />
                        </div>
                        <div className={theme.sidebar}>
                            {ordersbook && <AskBidForm
                                onRef={ref => (this.refAskBidForm = ref)}
                                tokenAddress={data.token}
                                userAddress={user_address}
                                ordersbook={ordersbook}
                                refBalanceApprove={this.refBalanceApprove}
                                refBalanceDeposit={this.refBalanceDeposit}
                                coinFrom={data.ticker} />}
                        </div>
                    </div>
                </div>

                {this.renderTransactions()}

			</div>
		);
	}

	renderBoardMobile() {
		const {classes} = this.props

		return (
			<div className={classes.root}>
				<ExpansionPanel>
					<ExpansionPanelSummary expandIcon={<Icon>expand_more_icon</Icon>}>
						<Typography className={classes.heading}>Orders Book</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails>
						orders
					</ExpansionPanelDetails>
				</ExpansionPanel>
				<ExpansionPanel>
					<ExpansionPanelSummary expandIcon={<Icon>expand_more_icon</Icon>}>
						<Typography className={classes.heading}>Pairs Book</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails>
						pairs????
					</ExpansionPanelDetails>
				</ExpansionPanel>
				<ExpansionPanel defaultExpanded={false}>
					<ExpansionPanelSummary expandIcon={<Icon>expand_more_icon</Icon>}>
						<Typography className={classes.heading}>Chart</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails>
						chart
					</ExpansionPanelDetails>
				</ExpansionPanel>
			</div>
		)
	}

    renderTransactions() {

        const {data} = this.state

        let user_address = this.props.stock.web3Metamask.user_address
        let transactions = this.props.stock.web3Transactions
            transactions = transactions[user_address] ? transactions[user_address] : []

        return (
            <div className={theme.blockTransactions}>

                <Typography variant="headline" gutterBottom>
                    Your transactions for {this.state.data.coin}
                </Typography>

                <ul>
                    <li>
                        Wallet: {this.props.stock.web3Metamask.user_address}
                    </li>
                </ul>

                <div className={theme.transactionsList}>
                    {transactions.map((item, index) => {
                        if(item.coin == data.coin) {
                            let hash_url = `https://etherscan.io/tx/${item.hash}`
                            return (
                                <ul key={index}>
                                    <li data-el="coin">{item.coin}</li>
                                    <li data-el="value">{item.value} ETH</li>
                                    <li data-el="hash">
                                        <a href={hash_url} target="_blank">{item.hash}</a>
                                    </li>
                                    <li data-el="date">
                                        <TimeAgo date={item.date} /> ago
                                    </li>
                                </ul>
                            )
                        }
                    })}
                </div>
            </div>
        )
    }

	render() {

		const {data, page_not_found} = this.state
		const windowSize = this.props.stock.windowSize
		// console.log(this.props)

        if(page_not_found) {
            return (
                <div>
                    This token not found
                </div>
            )
        }
        return (
            <div>
                {this.renderBoard()}
            </div>
        )
        return (
			<div>
				{data
				?
					windowSize[0] > 940 ? this.renderBoard() : this.renderBoardMobile()
				:
					<Preloader />
				}
			</div>
		)
	}
}

Dashboard.propTypes = {
	classes: PropTypes.object.isRequired
};


export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
            // onHideAllMenus: (payload) => {
			// 	dispatch({type: 'HIDE_ALL_MENUS', payload})
			// },
            // onToggleLeftMenu: (payload) => {
			// 	dispatch({type: 'LEFT_MENU_TOGGLE', payload})
			// },
			// onToggleRightMenu: (payload) => {
			// 	dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			// },
            onPreloadCoins: (payload) => {
                dispatch({type: 'PRELOAD_COINS', payload})
            },
            onSelectCoins: (payload) => {
				dispatch({type: 'SELECT_COINS', payload})
			},
            onStockInit: (payload) => {
				dispatch({type: 'STOCK_INIT', payload})
			},
		})
	))
	(withStyles(styles)(Dashboard))
);
