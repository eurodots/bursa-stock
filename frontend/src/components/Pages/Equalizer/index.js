import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';

import TimeAgo from 'react-timeago';
import NumberFormat from 'react-number-format';
import {getEqualizer, numConvert} from '../../../utils/functions'

import Preloader from '../../Preloader';
import { LinearProgress } from 'material-ui/Progress';
import Filters from './Filters';

// import EqualizerJson from '../../assets/data/equalizer.json';

import theme from './theme.scss';

const _ = require('lodash');


class TransactionsList extends Component {

    render() {
        const t = this.props.transactions;

        return (
            <div>
                <ul className={theme.transactionsListHeader}>
                    <li>
                        <Typography variant="title">
                            {t.coin_pay} — <span data-color="danger">{t.coin_buy}</span> — {t.coin_get}
                        </Typography>
                    </li>
                    <li>
                        ...
                    </li>
                </ul>
                <ul className={theme.transactionsList}>
                    <li>
                        <ul data-el="rowHeader">
                            <li>Pay {t.coin_pay}</li>
                            <li>Buy {t.coin_buy}</li>
                            <li>Get {t.coin_get}</li>
                        </ul>
                    </li>
                    <li data-el="body">
                        {t.data.map((item, index) => {
                            return (
                                <div key={index} data-group>
                                    <ul key={index}
                                        onMouseEnter={() => this.props.callChecker(item.pay.index, item.get.index)}
                                        onMouseLeave={() => this.props.callChecker(-1, -1)} >
                                        <li title={`#${item.pay.index}`}>
                                            <span dangerouslySetInnerHTML={{__html: numConvert(item.pay.total)}} />
                                            <label>
                                                = BTC <span dangerouslySetInnerHTML={{__html: numConvert(item.pay.total_btc)}} />
                                            </label>
                                        </li>
                                        <li>
                                            <span dangerouslySetInnerHTML={{__html: numConvert(item.buy.volume)}} />
                                            <label>
                                                <NumberFormat value={item.buy.profit_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='$ ' />
                                                <span data-from>from</span>
                                                <NumberFormat value={item.buy.total_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='$ ' />
                                            </label>
                                        </li>
                                        <li title={`#${item.get.index}`}>
                                            <span dangerouslySetInnerHTML={{__html: numConvert(item.get.total)}} />
                                            <label>
                                                = BTC <span dangerouslySetInnerHTML={{__html: numConvert(item.get.total_btc)}} />
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            )
                        })}
                    </li>
                    <li>
                        <ul data-el="rowFooter">
                            <li>
                                <span dangerouslySetInnerHTML={{__html: numConvert(t.total_pay)}} />
                                <label>
                                    <NumberFormat value={t.total_pay_btc} displayType={'text'} decimalScale={8} thousandSeparator={true} prefix='BTC ' />
                                </label>
                            </li>
                            <li>
                                <span dangerouslySetInnerHTML={{__html: numConvert(t.total_volume)}} />
                                <label>
                                    <NumberFormat value={t.total_profit_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='$ ' />
                                    <span data-from>from</span>
                                    <NumberFormat value={t.total_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='$ ' />
                                </label>
                            </li>
                            <li>
                                <span dangerouslySetInnerHTML={{__html: numConvert(t.total_get)}} />
                                <label>
                                    <NumberFormat value={t.total_get_btc} displayType={'text'} decimalScale={8} thousandSeparator={true} prefix='BTC ' />
                                </label>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        )
    }
}


class OrdersList extends Component {

    state = {
        data: []
    }

    // componentWillUnmount () {
    //     this.props.onRef(undefined)
    // }

    componentWillMount() {
        // this.props.onRef(this)
        this.setState({data: this.props.data})
    }

    render() {
        const { type } = this.props;
        const { data } = this.state;

        // console.log(data.fastlink)

        return (
            <div data-type={type}>

                <ul className={theme.ordersListHeader}>
                    <li>
                        <Typography variant="title" data-el="title">
                            <a href={data.fastlink} target="_blank">
                                <span data-color="danger">{data.coin_from}</span>_{data.coin_to}
                            </a>
                        </Typography>
                    </li>
                    <li>
                        <Typography variant="body2">
                            {data.stock_name} #{data.pair_id}
                        </Typography>
                        <Typography variant="caption">
                            <TimeAgo date={data.updated_at} />
                        </Typography>
                    </li>
                </ul>

                <ul className={theme.ordersList}>
                    <li>
                        <ul data-el="rowHeader">
                            <li>Price ({data.coin_to})</li>
                            <li>Volume ({data.coin_from})</li>
                            <li>Total ({data.coin_to})</li>
                        </ul>
                    </li>
                    <li data-el="body">
                        {data.data.map((item, index) => {

                            let price = numConvert(item.price)
                            let volume = numConvert(item.volume)
                            let total = numConvert(item.total)


                            let data_volume = 0
                            if(item.total_usd < 1000) {
                                data_volume = (item.total_usd / 1000) * 100
                            } else {
                                data_volume = 100
                            }
                            data_volume = `${data_volume}%`

                            return (
                                <div key={index}>
                                    <div data-volume><div style={{width: data_volume}}></div></div>
                                    <ul data-el="row" data-hover={item.hover ? 'true' : 'false'}>
                                        <li data-li="price">
                                            <span dangerouslySetInnerHTML={{__html: price}} />
                                            <label>
                                                {data.coin_to != 'BTC' && `BTC ${item.price_btc}`}
                                            </label>
                                        </li>
                                        <li>
                                            <span dangerouslySetInnerHTML={{__html: volume}} />
                                            <label>
                                                <NumberFormat value={item.total_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='$ ' />
                                            </label>
                                        </li>
                                        <li>
                                            <span dangerouslySetInnerHTML={{__html: total}} />
                                            <label>
                                                {data.coin_to != 'BTC' && `= BTC ${item.total_btc}`}
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            )
                        })}
                    </li>
                    <li>
                        <ul data-el="rowFooter">
                            <li>
                                ...
                            </li>
                            <li>
                                ...
                            </li>
                            <li>
                                ...
                                <label>
                                    ...
                                </label>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        )
    }
}


class EqualizerInner extends Component {

    state = {
        item: false
    }

    handlePanel = () => {
        var newState = this.state.item
            newState.active = !this.state.item.active;
        this.setState({item: newState}, () => {});
    }


    componentWillMount() {
        this.setState({item: this.props.item})
    }

    callChecker = (index_ask, index_bid) => {

        // console.log(index_ask)
        // console.log(index_bid)

        let newState = this.state.item
        newState.asks.data.map((el, index) => {
            if(el.index_id == index_ask) {
                newState.asks.data[index].hover = true
            } else {
                newState.asks.data[index].hover = false
            }
        })
        newState.bids.data.map((el, index) => {
            if(el.index_id == index_bid) {
                newState.bids.data[index].hover = true
            } else {
                newState.bids.data[index].hover = false
            }
        })
        this.setState({item: newState})
    }


    render() {
        const { samecoins, index } = this.props;
        const { item } = this.state;

        if(!item) {
            return (
                <div>Loading...</div>
            )
        }

        if(samecoins && item.asks.coin_to != item.bids.coin_to) {
            return (
                <div></div>
            )
        }

        return (
            <ul data-expansion={item.active ? 'show' : 'hide'}>
                <li onClick={() => this.handlePanel()}>
                    <ul data-expansion-header>
                        <li data-container="title">
                            <Typography>
                                #{index} {item.asks.stock_name} — {item.bids.stock_name}
                            </Typography>
                            <Typography variant="button">
                                {item.transactions.coin_pay} — <span data-color="danger">{item.transactions.coin_buy}</span> — {item.transactions.coin_get}
                            </Typography>
                        </li>
                        <li data-container="data">
                            <ul>
                                <li data-li="volume">
                                    <span dangerouslySetInnerHTML={{__html: numConvert(item.transactions.total_volume)}} />
                                    <label>{item.asks.coin_from}</label>
                                </li>
                                <li data-li="profit">
                                    <strong>
                                        <NumberFormat value={item.transactions.total_profit_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='$ ' />
                                    </strong>
                                    <span data-span="from">from</span>
                                    <NumberFormat value={item.transactions.total_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix="$ " />
                                </li>
                                <li data-li="median">
                                    <NumberFormat value={0} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='+' /> %
                                </li>
                            </ul>
                        </li>
                        <li data-container="icon">
                            {item.active ? <Icon>keyboard_arrow_up</Icon> : <Icon>keyboard_arrow_down</Icon>}
                        </li>
                    </ul>
                </li>
                <li data-expansion-inner>
                    {item.active &&
                    <ul>
                        <li data-inner="orders">
                            <OrdersList type="sell" data={item.asks} />
                        </li>
                        <li data-inner="orders">
                            <OrdersList type="buy" data={item.bids} />
                        </li>
                        <li data-inner="transactions">
                            <TransactionsList transactions={item.transactions} callChecker={this.callChecker} />
                        </li>
                    </ul>}
                </li>
            </ul>
        )

    }
}


EqualizerInner.propTypes = {
  // classes: PropTypes.object.isRequired,
};


const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: '30px 0',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

class PageEqualizer extends Component {

    state = {
        data: false,
        data_preloader: 10,
        autorefresh: true,
        samecoins: false,
    }


    timeout = false
    clearTimer = () => {
        var that = this
        clearTimeout(that.timeout)
    }

    componentWillUnmount () {
        this.clearTimer()
    }

    componentWillMount() {
        this.loadData()
        this.loadDataTimer()
    }

    loadDataTimer = () => {
        const { data_preloader, autorefresh } = this.state

        if(autorefresh) {
            let load_timer = this.state.data_preloader - 1
            if(load_timer >= 0) {
                this.setState({data_preloader: load_timer}, () => {})
            } else if (load_timer == -1) {
                this.loadData()
            }
        }

        this.timeout = setTimeout(() => {
            this.loadDataTimer()
        }, 1000)

    }

    loadData = () => {
        this.setState({
            // data: [],
            autorefresh: false
        }, () => {})

        getEqualizer().then((response) => {
            this.setState({
                data: response.data.results,
                data_preloader: 10,
                autorefresh: true,
            }, () => {})
        })

        // this.setState({
        //     data: EqualizerJson
        // })
    }


    handleSwitcher = name => event => {
        this.setState({ [name]: event.target.checked }, () => {});
    }

    render() {
        const { classes } = this.props;
        const { data, samecoins, data_preloader } = this.state;

        // let progess_number = data_preloader * 10
        // <div className={theme.preloadBar}>
        //     <LinearProgress variant="determinate" color="primary" value={progess_number} />
        // </div>



        return (
          <div className={classes.root} data-content-inner>

              {data &&
                  <div>
                      <Filters
                          handleSwitcher={this.handleSwitcher}
                          state={this.state}
                           />
                  </div>
              }

              {!data || data_preloader == 0 ? <Preloader /> : ''}

               <div className={theme.expansionPanel}>
                   {data && data.map((item, index) => {
                       return <EqualizerInner key={index} item={item} index={index} samecoins={samecoins} />
                   })}

               </div>


           </div>
        );
    }

}

PageEqualizer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PageEqualizer);
