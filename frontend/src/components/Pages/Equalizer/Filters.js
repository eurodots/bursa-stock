import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import { FormGroup, FormControlLabel } from 'material-ui/Form';
import Switch from 'material-ui/Switch';

import theme from './theme.scss'

const styles = theme => ({
});

class Filters extends React.Component {


    renderToolbar() {
        const { state } = this.props;

        let autorefresh_label = `Autorefresh (${state.data_preloader})`


        let same_iter = 0
        this.props.state.data.map((item, index) => {
            if(item.asks.coin_to == item.bids.coin_to) {
                same_iter++
            }
        })
        let sameCoins_label = `Same coins (${same_iter})`

    	return (
            <div>
        		<FormGroup row>
        			<FormControlLabel
                        control={
                            <Switch checked={state.autorefresh}
                            color="primary"
                            onChange={this.props.handleSwitcher('autorefresh')}
                            value="autorefresh" />
                        }
                        label={autorefresh_label} />

                    <FormControlLabel
                        control={
                            <Switch checked={state.samecoins}
                            color="primary"
                            onChange={this.props.handleSwitcher('samecoins')}
                            value="samecoins" />
                        }
                        label={sameCoins_label} />
        		</FormGroup>
            </div>
    	);
    }

    render() {
        // const { state } = this.props
        return (
            <ul className={theme.filtersWrapper}>
                <li>{this.renderToolbar()}</li>
                <li>...</li>
            </ul>
        )
    }
}

Filters.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Filters);
