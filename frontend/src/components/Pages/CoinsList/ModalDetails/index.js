import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import SwipeableDrawer from 'material-ui/SwipeableDrawer';
import Button from 'material-ui/Button';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';
// import { mailFolderListItems, otherMailFolderListItems } from './tileData';


import ModalContent from './Content'


const styles = {
  // list: {
  //   width: 250,
  // },
  // fullList: {
  //   width: 'auto',
  // },
};

class SwipeableTemporaryDrawer extends React.Component {
  state = {
    open: false,
  };

  toggleDrawer = (open) => () => {
    this.setState({
      open: open,
    });
  };

  render() {
    const { classes } = this.props;



    return (
      <div>
        <Button variant="raised" color="primary" onClick={this.toggleDrawer(true)}>Details</Button>

        <SwipeableDrawer
          anchor="right"
          open={this.state.open}
          onClose={this.toggleDrawer(false)}
          onOpen={this.toggleDrawer(true)}
        >
          <div
            tabIndex={0}
            role="button"
          >
            <ModalContent request={this.props.request} />
          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

SwipeableTemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SwipeableTemporaryDrawer);
