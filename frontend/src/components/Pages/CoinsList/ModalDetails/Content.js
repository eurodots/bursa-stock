import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Typography from 'material-ui/Typography';


import NumberFormat from 'react-number-format';
import TimeAgo from 'react-timeago';
import Preloader from '../../../Preloader'
import HeikinAshi from '../../../ChartHeikinAshi'
import Calculator from '../../../Calculator'

import {apiCoinsListCurrent} from '../../../../utils/functions'

const _ = require('lodash');
import theme from './theme.scss'


class Content extends Component {

    state = {
        data: false,
    }

    componentDidMount() {
        this.loadData()
    }

    loadData() {
        const { request } = this.props
        apiCoinsListCurrent(request).then((response) => {

			if(response.status == 200) {
				this.setState({
					data: response.data.results,
				})
			}

            // console.log(this.state.data)
			// this.timeout = setTimeout(() => {
	        //     this.loadData()
	        // }, 10000)

		}, () => {})
    }

	render() {
        const {data} = this.state

        if(!data) {
            return (
                <div className={theme.contentWrapper}>
                    <Preloader />
                </div>
            )
        }
		return (
			<div className={theme.contentWrapper}>

                <div className={theme.wrapperTop}>
                    <ul data-ul="header">
                        {data.favicon && <li data-li="favicon">
                                <img src={data.favicon} />
                            </li>
                        }
                        <li data-li="name">
                            <Typography variant="display1" gutterBottom>
                                {data.ticker}
                            </Typography>
                            <div data-el="subname">
                                {data.name && data.name}
                                {data.platform && ` (${data.platform} platform)`}
                            </div>
                        </li>
                        <li data-li="updated">
                            Updated <TimeAgo date={data.updated_at} />
                        </li>
                    </ul>

                    <ul data-ul="links">
                        {data.links.map((item, index) => {
                            return (
                                <li key={index}>
                                    <a href={item.link} data-link target="_blank">{item.label}</a>
                                </li>
                            )
                        })}
                    </ul>
                </div>

                <div className={theme.wrapperCalculator}>
                    <Typography variant="title" gutterBottom>
                        Converter (Calculator)
                    </Typography>

                    <Calculator coinFrom={data.ticker} coinTo={'BTC'} />
                </div>

                <div className={theme.wrapperCoinmarketcap}>

                    {data.coinmarketcap ?
                        <div>
                            Coinmarketcap
                            <ul>
                                <li>
                                    {data.coinmarketcap.rank}
                                </li>
                                <li>
                                    {data.coinmarketcap.percent_change_1h}
                                </li>
                                <li>
                                    {data.coinmarketcap.percent_change_24h}
                                </li>
                                <li>
                                    {data.coinmarketcap.percent_change_7d}
                                </li>
                            </ul>
                        </div>
                    : ''}

                </div>
                <div className={theme.wrapperEthplorer}>
                    {data.ethplorer ? <HeikinAshi tokenAddress={data.token} /> : 'Transactions not found'}
                </div>
            </div>
		)
	}
}

Content.propTypes = {
  // title: PropTypes.string.isRequired,
};

export default Content
