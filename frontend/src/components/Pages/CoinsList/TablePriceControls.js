import React, {Component} from 'react';
import PropTypes from 'prop-types';

import theme from './theme.scss'

const styles = {
};

class TablePriceControls extends Component {

	state = {
		selected: 'BTC'
	}

	switchCurrency(selected) {
		this.setState({selected}, () => {})
		this.props.switchCurrency(selected)
	}

	render() {
		const {selected} = this.state

		return (
            <ul className={theme.tablePriceControls}>
    			<li onClick={() => this.switchCurrency('BTC')} data-selected={selected == 'BTC'}>BTC</li>
                <li onClick={() => this.switchCurrency('USD')} data-selected={selected == 'USD'}>USD</li>
                <li onClick={() => this.switchCurrency('CNY')} data-selected={selected == 'CNY'}>CNY</li>
                <li onClick={() => this.switchCurrency('ETH')} data-selected={selected == 'ETH'}>ETH</li>
                <li onClick={() => this.switchCurrency('RUB')} data-selected={selected == 'RUB'}>RUB</li>
            </ul>
		)
	}
}

TablePriceControls.propTypes = {
  // classes: PropTypes.object.isRequired,
};

export default TablePriceControls;
