import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom'
import {Link} from 'react-router-dom';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
// import Icon from 'material-ui/Icon';
import Table from './Table'



import Preloader from '../../Preloader'
import {apiCoinsList} from '../../../utils/functions'

import theme from './theme.scss'

const styles = {
};

class CoinsList extends Component {

    state = {
        data: false,
    }

    componentDidMount() {
        this.loadData()
    }

    loadData() {
        apiCoinsList().then((response) => {

			if(response.status == 200) {
				this.setState({
					data: response.data.results,
				})
			}

            console.log(this.state.data)
			// this.timeout = setTimeout(() => {
	        //     this.loadData()
	        // }, 10000)

		}, () => {})
    }


	render() {
        const {data} = this.state
        if(!data) {
            return <Preloader />
        }
		return (
            <div>
    			{data && <Table data={data} />}
            </div>
		)
	}
}

CoinsList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(CoinsList));
