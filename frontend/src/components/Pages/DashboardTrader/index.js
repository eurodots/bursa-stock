import React, {Component} from 'react';
import PropTypes from 'prop-types';

// import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';

import {numConvert} from '../../../utils/functions'
import {web3orderCancel} from '../../../utils_stock/getWeb3.js'
import {Web3MetamaskOrdersList} from '../../../utils_stock/functions'

import theme from './theme.scss'

class Dashboard extends Component {

	state = {
		data: false
	}

	componentDidMount() {
		this.loadData()
	}

	loadData = () => {
		Web3MetamaskOrdersList().then(res => {
			this.setState({data: res.data.results})
			console.log(res)
		})
	}

	cancelOrder(init) {

		console.log(init)

		web3orderCancel(init)
		// this.props.onToggleRightMenu('hideAccount')

		// console.log(init)

		// return false
	}

	render() {

		const {data} = this.state

		if (!data) {
			return (
				<div>Data not found</div>
			)
		}

		return (
			<div>
				<div onClick={this.loadData}>Refresh</div>

				{data.map((item, index) => {
					return (
						<div key={index}>
							<Typography variant="headline" gutterBottom>
								{item.stock_name}
							</Typography>
							{item.stock_token}

							{item.orders.map((order, i) => {
								return (
									<ul key={i}>
										<li>
											{order.type}
										</li>
										<li>
											<span dangerouslySetInnerHTML={{
												__html: numConvert(item.amount, 8)
											}}/>
										</li>
										<li>
											<span dangerouslySetInnerHTML={{
												__html: numConvert(item.price, 8)
											}}/>
										</li>
										<li>
											{order.created_at}
										</li>
										<li>
											{order.updated_at}
										</li>
										<li>
											{order.status}
											<span data-link onClick={() => this.cancelOrder(order)}>Cancel</span>
										</li>
										<hr/>
									</ul>
								)
							})}
						</div>
					)
				})}

			</div>
		)
	}
}

export default Dashboard
