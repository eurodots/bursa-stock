import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import ExpansionPanel, {ExpansionPanelDetails, ExpansionPanelSummary} from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';
import Switch from 'material-ui/Switch';
import { FormControlLabel } from 'material-ui/Form';


import TimeAgo from 'react-timeago';
import Preloader from '../../Preloader'
import FormField from '../../BlockForm/FormField'
import FormButton from '../../BlockForm/FormButton'
import ConnectionChecker from '../../ConnectionChecker'
import theme from './theme.scss'

import {apiFishrod, apiFishrodApplyNewPrice, numConvert} from '../../../utils/functions'

const _ = require('lodash');

const styles = theme => ({
	root: {
    	flexGrow: 1,
	},
	// heading: {
	// 	fontSize: theme.typography.pxToRem(20),
	// 	flexBasis: '33.33%',
	// 	flexShrink: 0,
	// 	textTransform: 'uppercase',
	// },
	// secondaryHeading: {
	// 	fontSize: theme.typography.pxToRem(15),
	// 	color: theme.palette.text.secondary
	// }
});

class PageFishrod extends Component {

	state = {
		data: false,
		expanded: 'panel-0',
		expanded_page: null,

		new_price: "",
		data_preloder: true,
	}

	timeout = false
    clearTimer = () => {
        var that = this
        clearTimeout(that.timeout)
    }

    componentWillUnmount () {
        this.clearTimer()
    }

	componentDidMount() {
		this.loadData()
	}


	loadData() {

		this.setState({data_preloder: true})
		apiFishrod().then((response) => {

			if(response.status == 200) {
				this.setState({
					data: response.data.results,
					data_preloder: false,
				})
			}
			this.timeout = setTimeout(() => {
	            this.loadData()
	        }, 10000)

		}, () => {})
	}

	submitNewPrice(event, type, order_id, new_price, price_border) {

		event.preventDefault()

		// Reset focus()
		document.activeElement.blur()
		this.setState({new_price: ''}, () => {})

		apiFishrodApplyNewPrice({order_id, new_price}).then((response) => {

			// console.log(response)

			if(response.status == 200) {
				if(response.data.error) {
					alert(response.data.message)
				} else {
					alert(response.data.message)
				}
				// this.setState({data: response.data.results})
			} else {
				alert('Unknown error!')
			}

			// this.loadData()

		}, () => {})
	}

	updateNewPrice(value) {
		let value_ = parseFloat(value).toFixed(8)
		this.setState({new_price: value_}, () => {
			console.log(this.state.new_price)
		})
	}

	handleChangeExpansion = panel => (event, expanded) => {
		this.setState({
			expanded: expanded ? panel : false
		});
	};

	handleChangeInput = (event, name) => {
        this.setState({
            [name]: event.target.value,
        });
    };

	renderOrders() {
		const {classes} = this.props;
		const {data, expanded, new_price} = this.state;


		return (
			<div className={classes.root}>
				{data.map((item, index) => {

					let expanded_id = `block-${index}`

					return (
						<ExpansionPanel
							key={index}
							expanded={expanded === expanded_id}
							onChange={this.handleChangeExpansion(expanded_id)}
							classes={{
								root: theme.expansionPanel,
							}}
							data-status={item.api_status}>
							<ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}
								classes={{
									root: theme.headerWrapper,
									content: theme.headerWrapperContent,
								}} >
								<ul>
									<li data-li="title">
										<Typography variant="title">
											<Icon data-icon="progress">update</Icon>
											<Icon data-icon="completed">check</Icon>

											<span data-color="mute">{item.stock_name}</span>:
											<nobr>
												{item.type}&nbsp;<span data-color="success">{item.total_usd.toFixed(0)} $</span>
											</nobr>
										</Typography>
									</li>
									<li data-li="errors">
										<ul data-color="danger">
											{item.errors.map((msg, i) => {
												return (
													<li key={i}>{msg}</li>
												)
											})}
										</ul>
									</li>
									<li data-li="autopilot">
										{item.autopilot &&
											<FormControlLabel
												disabled
												control={
													<Switch
														classes={{
															default: theme.autopilotSwitch
														}}
														value=""
														checked={item.autopilot}
														color="primary" />
												}
												label="Autopilot" />
										}
									</li>
									<li data-li="created">
										#{item.order_id}, Created <TimeAgo date={item.created_at} />
									</li>
								</ul>


							</ExpansionPanelSummary>
							<ExpansionPanelDetails>
								<div style={{width: '100%'}}>
									<div className={theme.fishrodDataWrapper}>
										<div>
											<ul className={theme.details}>
												<li>
													<label>Stock name:</label>
													<div>
														{item.stock_name}
													</div>
												</li>
												<li>
													<label>Pair:</label>
													<div>
														<a href={item.fastlink} target="_blank" data-link="underlined">
															{item.pair.join('_')}
														</a>
													</div>
												</li>
												<li data-li="divider"></li>
												<li>
													<label>Amount:</label>
													<div>
														{item.amount.toFixed(8)} {item.pair[0]}
													</div>
												</li>
												<li data-li="divider"></li>
												{item.market.pricebook.map((el, i) => {
													return (
														<li key={i}>
															<label>{el.stock_name}:</label>
															<div>
																<div>
																	<a href={el.fastlink} target="_blank" data-link="underlined">
																		{el.bestprice} {item.pair[1]}
																	</a>
																</div>
																<small><TimeAgo date={el.updated_at} /></small>
															</div>
														</li>
													)
												})}
												<li data-li="divider"></li>
												<li data-li="divider"></li>
												<li data-li="divider"></li>
												<li style={{opacity: .3}}>
													<label>
														<a href={item.market.coinmarketcap.fastlink} data-link="underlined" target="_blank">CoinMarketCap:</a>
													</label>
													<div>
														<ul>
															{_.keys(item.market.coinmarketcap.prices).map((key, i) => {
																return (
																	<li key={i}>
																		{key} = {item.market.coinmarketcap.prices[key]}
																	</li>
																)
															})}
														</ul>

														<small><TimeAgo date={item.market.coinmarketcap.updated_at} /></small>
													</div>
												</li>
											</ul>
										</div>
										<div>
											<ul className={theme.details}>
												<li>
													<label>Price now:</label>
													<div>
														<strong data-color="success">
															{item.price.toFixed(8)} {item.pair[1]}
														</strong>
														<div><small>
															{item.price_difference && `(${item.price_difference.percent}% / ${item.price_difference.usd} $)`}
														</small></div>
													</div>
												</li>
												<li>
													<label>Price source:</label>
													<div>
														<span data-link onClick={() => this.updateNewPrice(item.price_source)}>
															{item.price_source.toFixed(8)} {item.pair[1]}
														</span>
														<div><small>
															{item.price_source_difference && `(${item.price_source_difference.percent}% / ${item.price_source_difference.usd} $)`}
														</small></div>
													</div>
												</li>
												<li>
													<label>Price history:</label>
													<div>
														{item.price_history.toFixed(8)} {item.pair[1]}
													</div>
												</li>
												<li data-li="divider"></li>
												<li>
													<label>Total:</label>
													<div>
														<div>{item.total.toFixed(8)} {item.pair[1]}</div>
														<div><small>
															({item.total_usd} $)
														</small></div>
													</div>
												</li>
												<li data-li="divider"></li>
												<li>
													<label>Stop loss:</label>
													<div>
														{item.stop_loss_percent} %
													</div>
												</li>
												<li data-li="divider"></li>
												<li>
													<label>New price:</label>
													<div>
														<span data-link data-color="danger" onClick={() => this.updateNewPrice(item.new_price)}>
															{item.new_price ? `${item.new_price.toFixed(8)} ${item.pair[1]}` : '—'}
														</span>
														<div><small>
															{item.new_price_difference && `(${item.new_price_difference.percent}% / ${item.new_price_difference.usd} $)`}
														</small></div>
													</div>
												</li>
												<li>
													<label>Price border:</label>
													<div>
														<span data-link onClick={() => this.updateNewPrice(item.price_border)}>
															{item.price_border}
														</span>
													</div>
												</li>

											</ul>
										</div>
										<div className={theme.ordersBook}>
											<ul data-ul="header">
												<li>Price {item.pair[1]}</li>
												<li>Amount {item.pair[0]}</li>
												<li>Total {item.pair[1]}</li>
												<li></li>
											</ul>
											<div data-el="inner">
												{item.ordersbook.map((el, i) =>{
													return (
														<ul
															key={i}
															onClick={() => this.updateNewPrice(el.price)}
															data-selected={el.price == item.price ? 'true' : ''}
															>
															<li>
																<span dangerouslySetInnerHTML={{__html: numConvert(el.price)}} />
															</li>
															<li>
																<span dangerouslySetInnerHTML={{__html: numConvert(el.amount)}} />
															</li>
															<li>
																<span dangerouslySetInnerHTML={{__html: numConvert(el.total)}} />
															</li>
															<li>
																{el.difference} %
															</li>
														</ul>
													)
												})}
											</div>
											<div data-el="footer">
												<TimeAgo date={item.orderbook_updated} />
											</div>
										</div>
									</div>

									<div className={theme.middleBlock}>
										<ul className={theme.listMessages}>
											{item.messages.map((msg, i) => {
												return (
													<li key={i}>{msg}</li>
												)
											})}
										</ul>
										{!item.autopilot &&
											<form className={theme.formPrice}
												onSubmit={(event) => this.submitNewPrice(event, item.type, item.order_id, this.state.new_price, item.price_border)}>
												<ul>
													<li>
														<FormField
							                                state_value={this.state.new_price}
							                                state_name="new_price"
							                                placeholder="New price"
							                                type="number"
							                                required={true}
							                                disabled={false}
							                                error={false}
							                                onChange={this.handleChangeInput}
							                                 />
													</li>
													<li>
														<FormButton
				   		                                 disabled={false}
				   		                                 type="submit"
				   		                                 label="Apply"
				   		                                 fullWidth
				   		                                 />
													</li>
												</ul>
											</form>
										}
									</div>



									<ul className={theme.listBottom}>
										<li>
											<label>Status:</label>
											<div>
												{item.status}
											</div>
											<small><TimeAgo date={item.updated_at} /></small>
										</li>
										<li>
											<label>API status:</label>
											<div>
												{item.api_status}
											</div>
											<small>{item.api_updated ? <TimeAgo date={item.api_updated} /> : 'Never'}</small>
										</li>
										<li>
											<label>Account:</label>
											<small>
												{item.account_email}
											</small>
										</li>
										<li>
											<label>Admin:</label>
											<div>
												<a href={item.admin_url} target="_blank" data-link="underlined">Edit #{item.order_id}</a>
											</div>
										</li>
									</ul>

								</div>
							</ExpansionPanelDetails>
						</ExpansionPanel>
					)
				})}
			</div>
		)
	}

	render() {

		const {data, data_preloder} = this.state

		return (
			<div data-content-inner="fishrod">

				<ConnectionChecker />

				<div className={theme.dataPreloader}>
					{data_preloder && <Preloader size={30} />}
				</div>
				{data != false && this.renderOrders()}

            </div>
		)
	}
}


PageFishrod.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PageFishrod);
