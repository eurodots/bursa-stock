import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import Typography from 'material-ui/Typography';
import { MenuItem } from 'material-ui/Menu';
import Select from 'material-ui/Select';
import Input, { InputLabel } from 'material-ui/Input';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Icon from 'material-ui/Icon';

import {CopyToClipboard} from 'react-copy-to-clipboard';
import LinksPreviewer from '../../LinksPreviewer'

import BlockForm from '../../BlockForm'
import FormButton from '../../BlockForm/FormButton'
import FormField from '../../BlockForm/FormField'
import Preloader from '../../Preloader'

import {apiDomain} from '../../../config/init'
import {apiUserStocksGet, apiUserStocksUpdate} from '../../../utils/functions'

import theme from './theme.scss'



const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
      maxWidth: 250,
    },
  },
};

const styles = theme => ({
  // root: {
  //   // display: 'flex',
  //   // flexWrap: 'wrap',
  // },
  formControl: {
    // margin: theme.spacing.unit,
    minWidth: 120,
    width: '100%',
  },
  textField: {
    // marginLeft: theme.spacing.unit,
    // marginRight: theme.spacing.unit,
    // margin: theme.spacing.unit,
    width: '100%',
  },
  selectField: {
      width: '100%',
  },
});

class PageUserStocksAdd extends React.Component {

    state = {
        data_categories: [],
        data_industries: [],
        data_countries: [],
        data_platforms: [],
        data_statuses: [],
        data_tariffs: [],
        data_categories_active: false,
        data_industries_active: false,
        data_countries_active: false,
        data_platforms_active: false,
        data_statuses_active: false,
        data_tariffs_active: false,

        // token_category: [],
        // token_industry: [],
        // token_country: '',
        // token_platform: '',
        // token_status: '',
        //
        // token_name: '',
        // token_ticker: '',
        // token_address: '',
        // token_description: '',
        // token_links: [],

        stock_id: 0,
        token_tariff: '',
        token_category: [],
        token_industry: [],
        token_country: '',
        token_platform: '',
        token_status: '',

        token_wallet: '',
        token_domain: '',
        token_name: '',
        token_ticker: '',
        token_address: '',
        token_description: '',
        token_links: [],

        errors: {
            token_category: false,
            token_industry: false,
            token_country: false,
            token_platform: false,
            token_status: false,
            token_tariff: false,
            // owner_email: false,
            token_wallet: false,
            token_domain: false,
            token_name: false,
            token_ticker: false,
            token_address: false,
            token_description: false,
        },



        form_disabled: false,
        form_tariff: 'free',
        form_first_step: true,
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     if(this.state.data_categories.length > 0) {
    //         return false
    //     }
    //     return true
    // }


    componentDidMount() {

        // Get tariff from url
        const {match} = this.props
        let load_tariff = match.params.tariff ? match.params.tariff : false
        if(load_tariff == 'pro' || load_tariff == 'enterprise') {
            load_tariff = load_tariff
        } else {
            load_tariff = 'free'
        }
        this.setState({
            form_tariff: load_tariff,
            token_tariff: load_tariff,
        }, () => {})



        // Load fields data
        let stock_id = this.props.match.params.stock_id
            stock_id = stock_id == 'add' ? 0 : stock_id
        apiUserStocksGet(stock_id).then((response) => {
            let d = response.data
            this.setState({
                data_tariffs: d.data_tariffs,
                data_categories: d.data_categories,
                data_countries: d.data_countries,
                data_industries: d.data_industries,
                data_platforms: d.data_platforms,
                data_statuses: d.data_statuses,


                stock_id: d.stock_id,
                token_tariff: d.token_tariff,
                token_category: d.token_category,
                token_industry: d.token_industry,
                token_country: d.token_country,
                token_platform: d.token_platform,
                token_status: d.token_status,
                token_wallet: d.token_wallet,
                token_domain: d.token_domain,
                token_name: d.token_name,
                token_ticker: d.token_ticker,
                token_address: d.token_address,
                token_description: d.token_description,
                token_links: d.token_links,

                form_first_step: d.stock_id == 0 ? true : false,
            }, () => {})

            // console.log(this.state)
        })

    }


    formContinue() {
        this.setState({form_first_step: false})
    }


    formSubmit = (event) => {
        event.preventDefault()
        const {
            stock_id,
            token_tariff,
            token_category,
            token_industry,
            token_country,
            token_platform,
            token_status,
            token_wallet,
            token_domain,
            token_name,
            token_ticker,
            token_address,
            token_description,
            token_links
        } = this.state

        let categories = token_category.join(',')
            categories = `[${categories}]`

        let industries = token_industry.join(',')
            industries = `[${industries}]`

        let links = token_links.join('||')
            links = `${links}`

        const joinData = {
            stock_id: stock_id,
            token_tariff: token_tariff,
            token_category: categories,
            token_industry: industries,
            token_country: token_country,
            token_platform: token_platform,
            token_status: token_status,
            token_wallet: token_wallet,
            token_domain: token_domain,
            token_name: token_name,
            token_ticker: token_ticker,
            token_address: token_address,
            token_description: token_description,
            token_name: token_name,
            token_links: links,
        }

        // console.log( joinData )


        // this.setState({form_disabled: true})

        // prompt('joinData',JSON.stringify(joinData))

        apiUserStocksUpdate(joinData, stock_id).then((response) => {
            console.log('-----response------')
            console.log(response.data)

            const {errors} = this.state
            if(response.status == 200) {
                let d = response.data
                if(d.error) {
                    let newState = Object.assign({}, errors);
                    _.keys(newState).map((item, index) => {
                        newState[item] = false
                    })
                    _.keys(d.errors).map((item, index) => {
                        newState[item] = d.errors[item][0];
                    })
                    this.setState({errors: newState}, () => {});
                    console.log(d.errors)
                } else {
                    this.props.history.push(`/p/user/stocks`)
                }
                this.setState({form_disabled: false}, () => {});
                window.scrollTo(0,0);
            }
        })
    }

    handleChangeSelect = (event, active, status) => {
        this.setState({
            [event.target.name]: event.target.value,
            [active]: status,
        });
    };
    handleChangeSelectOpen = (active, status) => {
        this.setState({[active]: status})
    }


    handleChangeInput = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };


    renderSelectSingle(init) {
        const {classes} = this.props
        const {errors, form_disabled} = this.state;
        const error_id = `${init.id}-error`
        const list = this.state[init.state]

        return (
            <FormControl className={classes.formControl} error={errors[init.id] ? true : false} aria-describedby={error_id}>
              <InputLabel htmlFor={init.id}>{init.label}</InputLabel>
              <Select
                open={this.state[init.active]}
                value={this.state[init.id]}
                onChange={(event) => this.handleChangeSelect(event, init.active, false)}
                onOpen={() => this.handleChangeSelectOpen(init.active, true)}
                onClose={() => this.handleChangeSelectOpen(init.active, false)}
                inputProps={{
                  name: init.id,
                  id: init.id,
                }}
                className={classes.selectField}
                disabled={form_disabled}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {list.map((item, index) => {
                    return (
                        <MenuItem key={index} value={item.id}>{item.value}</MenuItem>
                    )
                })}
              </Select>
              <FormHelperText id={error_id}>{errors[init.id]}</FormHelperText>
            </FormControl>
        )
    }

    renderSelectMultiple(init) {
        const {classes} = this.props
        const {errors, form_disabled} = this.state;
        const error_id = `${init.id}-error`
        const list = this.state[init.state]

        let openLimiter = true
        if(this.state[init.id].length >= 1) {
            openLimiter = false
        }

        return (
            <FormControl className={classes.formControl} error={errors[init.id] ? true : false} aria-describedby={error_id}>
              <InputLabel htmlFor={init.id}>{init.label}</InputLabel>
              <Select
                multiple
                open={this.state[init.active]}
                value={this.state[init.id]}
                onChange={(event) => this.handleChangeSelect(event, init.active, openLimiter)}
                onOpen={() => this.handleChangeSelectOpen(init.active, true)}
                onClose={() => this.handleChangeSelectOpen(init.active, false)}
                input={<Input id={init.id} name={init.id} />}
                MenuProps={MenuProps}
                className={classes.selectField}
                disabled={form_disabled}
              >
                {list.map((item, index) => {
                    return (
                        <MenuItem
                          key={index}
                          value={item.id}
                        >
                          {item.value}
                        </MenuItem>
                    )
                })}
              </Select>
              <FormHelperText id={error_id}>{errors[init.id]}</FormHelperText>
            </FormControl>
        )
    }



    // style={{
    //     fontWeight:
    //       this.state[state_id].indexOf(item.value) === -1
    //         ? theme.typography.fontWeightRegular
    //         : theme.typography.fontWeightMedium,
    //   }}


    onSaveLinks = (links_arr) => {
        this.setState({token_links: links_arr}, () => {
            console.log(this.state.token_links)
        });
    }


    renderForm() {
        const {classes} = this.props
        const {errors, form_tariff, data_categories, form_disabled} = this.state


        if(data_categories.length == 0) {
            return (
                <BlockForm bgid={13} title="Adding new stock">
                    <Preloader />
                </BlockForm>
            )
        }


		return (
            <BlockForm bgid={13} title={`Adding new stock`}>
                <form onSubmit={(event) => this.formSubmit(event)} className={theme.formWrapper}>
                    <ul>
                        <li>
                            {this.renderSelectSingle({
                                state: 'data_tariffs',
                                id: 'token_tariff',
                                active: 'data_tariffs_active',
                                label: 'Tariff',
                            })}
                        </li>
                        <li>
                            <FormField
                                state_value={this.state.token_domain}
                                state_name="token_domain"
                                label="Domain"
                                placeholder="http://"
                                type="text"
                                required={true}
                                disabled={false}
                                error={errors.token_domain}
                                onChange={this.handleChangeInput('token_domain')}
                                autoFocus={false}
                                 />
                        </li>
                        <li>
                            <FormField
                                state_value={this.state.token_wallet}
                                state_name="token_wallet"
                                label="ETH wallet for money incoming"
                                placeholder=""
                                type="text"
                                required={true}
                                disabled={false}
                                error={errors.token_wallet}
                                onChange={this.handleChangeInput('token_wallet')}
                                autoFocus={false}
                                 />
                        </li>
                        <li>
                            <FormField
                                state_value={this.state.token_name}
                                state_name="token_name"
                                label="Token name"
                                placeholder="Full name"
                                type="text"
                                required={true}
                                disabled={false}
                                error={errors.token_name}
                                onChange={this.handleChangeInput('token_name')}
                                autoFocus={false}
                                 />
                        </li>
                        <li>
                            <FormField
                                state_value={this.state.token_ticker}
                                state_name="token_ticker"
                                label="Token ticker"
                                placeholder="Short name"
                                type="text"
                                required={true}
                                disabled={false}
                                error={errors.token_ticker}
                                onChange={this.handleChangeInput('token_ticker')}
                                autoFocus={false}
                                 />
                        </li>
                        <li>
                            <FormField
                                state_value={this.state.token_address}
                                state_name="token_address"
                                label="Token address"
                                placeholder=""
                                type="text"
                                required={true}
                                disabled={false}
                                error={errors.token_address}
                                onChange={this.handleChangeInput('token_address')}
                                autoFocus={false}
                                 />
                        </li>
                        <li>
                            <FormField
                                state_value={this.state.token_description}
                                state_name="token_description"
                                label="Token description"
                                placeholder=""
                                type="text"
                                required={true}
                                disabled={false}
                                error={errors.token_description}
                                onChange={this.handleChangeInput('token_description')}
                                autoFocus={false}
                                multiline={true}
                                 />
                        </li>
                        <li>
                            {this.renderSelectMultiple({
                                state: 'data_categories',
                                id: 'token_category',
                                active: 'data_categories_active',
                                label: 'Project categories',
                            })}
                        </li>
                        <li>
                            {this.renderSelectMultiple({
                                state: 'data_industries',
                                id: 'token_industry',
                                active: 'data_industries_active',
                                label: 'Project industries',
                            })}
                        </li>
                        <li>
                            {this.renderSelectSingle({
                                state: 'data_countries',
                                id: 'token_country',
                                active: 'data_countries_active',
                                label: 'Project country',
                            })}
                        </li>
                        <li>
                            {this.renderSelectSingle({
                                state: 'data_platforms',
                                id: 'token_platform',
                                active: 'data_platforms_active',
                                label: 'Token platform',
                            })}
                        </li>
                        <li>
                            {this.renderSelectSingle({
                                state: 'data_statuses',
                                id: 'token_status',
                                active: 'data_statuses_active',
                                label: 'Project status',
                            })}
                        </li>
                        <li>
                            <LinksPreviewer onSaveLinks={this.onSaveLinks} data={this.state.token_links} />
                        </li>
                        <li>
                            <FormButton
                                disabled={form_disabled}
                                type="submit"
                                label="Save"
                                fullWidth
                                />
                        </li>
                    </ul>
                </form>
            </BlockForm>
		);
    }


	render() {
        const {form_first_step} = this.state
        //
        if(form_first_step) {
            return (
                <BlockForm bgid={13} title="Do you have a token?">
                    <form onSubmit={(event) => this.formContinue(event)} className={theme.formWrapper}>
                        <ul>
                            <li data-li="token-creation">
                                If not, we recommend that you create a token.
                            </li>
                            <li>
                                <FormButton
                                    disabled={false}
                                    type="button"
                                    variant="default"
                                    label="No, create new token"
                                    onClick={() => window.open('https://contracts.mywish.io/dashboard/create')}
                                    fullWidth
                                    />
                            </li>
                            <li>
                                <FormButton
                                    disabled={false}
                                    type="submit"
                                    label="Yes, I have a token"
                                    fullWidth
                                    />
                            </li>
                        </ul>
                    </form>
                </BlockForm>
            )
        }
        return (
            <div>
                {this.renderForm()}
            </div>
        )
	}
}


PageUserStocksAdd.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};


export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			// onSelectCoins: (payload) => {
			// 	dispatch({type: 'SELECT_COINS', payload})
			// },
		})
	))
	(withStyles(styles, { withTheme: true })(PageUserStocksAdd))
);
