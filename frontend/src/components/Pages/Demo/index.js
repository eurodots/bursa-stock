import React, {Component} from 'react';
import PropTypes from 'prop-types';
// import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import {Link} from 'react-router-dom';

import BlockPreview from '../../BlockPreview';
import BlockHeaderBg from '../../BlockHeaderBg';

import {confSite} from '../../../config/init';


import theme from './theme.scss'


class PageDemo extends Component {

	render() {

		return (
			<div>
				<BlockHeaderBg
					bgid={30}
					title='Demo'
					description={`Choose the mode of the script.`} />

				<div className={theme.demoWrapper} data-content-inner>
	                <ul>
						<li>
							<BlockPreview
								title="Single-Mode"
								dataImage="dashboard1.png"
								onClick={() => console.log('Click')} />

							<div className={theme.actions}>
								<Link to={`/p/stock/${confSite.stock_token}`} data-link-button>
									<Button variant="raised" size="large" color="primary">
			                            Single mode
			                        </Button>
								</Link>
							</div>

						</li>
						<li>
							<BlockPreview
								title="Multi-Mode"
								dataImage="dashboard2.png" />

							<div className={theme.actions}>
								<Link to={`/p/coinslist`} data-link-button>
									<Button variant="raised" size="large" color="primary">
			                            Multi-mode
			                        </Button>
								</Link>
							</div>
						</li>
					</ul>
				</div>
            </div>
		)
	}
}

export default PageDemo
