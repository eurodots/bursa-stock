import React from 'react';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import Tooltip from 'material-ui/Tooltip';


import theme from './theme.scss'

import {apiEndpoint} from '../../../config/init'
import axios, {post} from 'axios';


class SimpleReactFileUpload extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			file: null,
			file_src: false,
			favicon_src: false
		}
		this.onChange = this.onChange.bind(this)
		this.fileUpload = this.fileUpload.bind(this)
	}

	componentDidMount() {
		this.setState({favicon_src: this.props.faviconSrc})
	}

	onChange(e) {

        if(e.target.files[0]) {
            this.setState({
                file: e.target.files[0],
            }, () => {
                this.fileUpload(this.state.file).then((response) => {
                    if(response.data.favicon != 'undefined') {
                        this.setState({favicon_src: response.data.favicon})
                    }
                })
            })
        }

	}
	fileUpload(file) {

		const url = `${apiEndpoint}/user/stocks/favicon/${this.props.stockId}/`;
		const formData = new FormData();
		formData.append('name', 'favicon')
		formData.append('file', file)
		const config = {
			headers: {
				'content-type': 'multipart/form-data'
			}
		}
		return post(url, formData, config)
	}

	renderFaviconUploader() {
		const {file_src, favicon_src} = this.state

		let init = {
			id: `favicon-${this.props.stockId}`
		}

		return (
			<ul className={theme.faviconUploader}>
				<li>
					<input accept="image/*" id={init.id} multiple={false} type="file" onChange={this.onChange} style={{
						display: 'none'
					}}/>

                <Tooltip title="Upload logotype">
                        <label htmlFor={init.id} data-el="favicon">
    						{favicon_src ? <img src={favicon_src}/> : <Icon>file_upload</Icon>}
    					</label>
                    </Tooltip>
				</li>
			</ul>
		)
	}

	render() {
		return (
			<div className={theme.fileuploadWrapper}>
				{this.renderFaviconUploader()}
			</div>
		)
	}
}

export default SimpleReactFileUpload
