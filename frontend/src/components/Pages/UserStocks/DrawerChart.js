import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import Button from 'material-ui/Button';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';

import HeikinAshi from '../../ChartHeikinAshi'

const styles = {
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
};

class TemporaryDrawer extends React.Component {
  state = {
    bottom: false,
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };

  render() {
    const { classes, tokenAddress } = this.props;


    return (
      <div>
        <Button
            size="small"
            variant="raised"
            onClick={this.toggleDrawer('bottom', true)}
            >
            Trading history
        </Button>

        <Drawer
          anchor="bottom"
          open={this.state.bottom}
          onClose={this.toggleDrawer('bottom', false)}
        >
          <div
            tabIndex={0}
            role="button"
          >
            <HeikinAshi tokenAddress={tokenAddress} />
          </div>
        </Drawer>

      </div>
    );
  }
}

TemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  tokenAddress: PropTypes.string.isRequired,
};

export default withStyles(styles)(TemporaryDrawer);
