import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import {Link} from 'react-router-dom';

// import Typography from 'material-ui/Typography';
// import Button from 'material-ui/Button';
import FormButton from '../../../BlockForm/FormButton'
import Preloader from '../../../Preloader'
import {apiPricelist} from '../../../../utils/functions'


import theme from './theme.scss'


class BlockPricing extends Component {

    state = {
        data: false
    }


    componentDidMount() {
		this.loadPricelist()
	}

	loadPricelist = () => {
		apiPricelist().then((response) => {
			this.setState({data: response.data.results}, () => {})
		})
	}


    renderPrices() {
        // const {classes} = this.props;
        const {data} = this.state;

        if(!data) {
            return (
                <Preloader />
            )
        }

        return (
            <div className={theme.pricingList} {...this.props}>
                {data.map((item, index) => {
                    return (
                        <ul key={index}>
                            <li data-li="plan">
                                {item.plan}
                            </li>
                            <li data-li="price">
                                {item.price}
                            </li>
                            <li data-li="list">
                                <ul>
                                    {item.list.map((list, i) => {
                                        return (
                                            <li key={i}>
                                                {list.label}
                                            </li>
                                        )
                                    })}
                                </ul>
                            </li>
                            <li data-li="button">
                                <Link to={item.url}>
                                    <FormButton
                                        disabled={false}
                                        type="button"
                                        label="Choose this plan"
                                        variant={item.priority ? 'colored' : 'colored-outline'}
                                        />
                                </Link>
                            </li>
                        </ul>
                    )
                })}
            </div>
        )
    }

	render() {


		return (
			<div className={theme.wrapper}>
                {this.renderPrices()}
            </div>
		)
	}
}


BlockPricing.propTypes = {
  children: PropTypes.node,
  // classes: PropTypes.object.isRequired,
};

export default BlockPricing;
