import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Typography from 'material-ui/Typography';

import ReactJson from 'react-json-view'

import axios from 'axios';
// const _ = require('lodash');
import {apiEndpoint} from '../../../../config/init'
import FormField from '../../../BlockForm/FormField'
import FormButton from '../../../BlockForm/FormButton'
import Preloader from '../../../Preloader'

import theme from './theme.scss'


class Content extends Component {

    state = {
        json_data: false,
		json_url: '',
    }

    componentDidMount() {
        this.setState({
            json_url: `${apiEndpoint}${this.props.request}`,
        }, () => {
            this.loadApiDocsJson()
        })
    }

    handleChangeInput = (event, name) => {
        this.setState({
            [name]: event.target.value,
        });
    };

	loadApiDocsJson(folder=false) {
	    const url = this.state.json_url
	    console.log(url)

		this.setState({
			json_data: false,
		}, () => {})

		axios.get(url).then(response => {
			let d = response.data
			this.setState({
				json_data: response.data
			}, () => {})
		})

	}

	submitApiRequest = (event) => {
		event.preventDefault()
		event.stopPropagation()

		this.loadApiDocsJson()
	}

	render() {
        const {json_data} = this.state

		return (
			<div className={theme.contentWrapper}>
                <div className={theme.requestApiWrapper}>
                    <form onSubmit={(event) => this.submitApiRequest(event)}>
                        <ul>
                            <li>
                                <FormField
                                    state_value={this.state.json_url}
                                    state_name="json_url"
                                    placeholder="API url"
                                    type="text"
                                    required={false}
                                    disabled={false}
                                    error={false}
                                    onChange={this.handleChangeInput}
                                     />
                            </li>
                            <li data-li="button">
                                <FormButton
                                    disabled={false}
                                    type="submit"
                                    label="Check"
                                    fullWidth
                                    />
                            </li>
                        </ul>
                    </form>

                    {json_data ? <ReactJson src={json_data} /> : <Preloader />}

                </div>
            </div>
		)
	}
}

Content.propTypes = {
  // title: PropTypes.string.isRequired,
};

export default Content
