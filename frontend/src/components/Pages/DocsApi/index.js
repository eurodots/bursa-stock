import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import ExpansionPanel, {ExpansionPanelDetails, ExpansionPanelSummary} from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';
import Collapse from 'material-ui/transitions/Collapse';

const _ = require('lodash');
import {apiPages} from '../../../utils/functions'
import {apiEndpoint} from '../../../config/init'

import BlockHeaderBg from '../../BlockHeaderBg';
import Preloader from '../../Preloader'
import ModalApiRequest from './ModalApiRequest/'
import theme from './theme.scss'

const styles = theme => ({
	// root: {
	// 	flexGrow: 1,
	// 	paddingBottom: 50,
	// },
	// headerWrapper: {
	// 	padding: '50px auto !important',
	// 	display: 'flex',
	// 	alignItems: 'center',
	// 	flexWrap: 'wrap',
	// },
	heading: {
		fontSize: theme.typography.pxToRem(20),
		flexBasis: '33.33%',
		flexShrink: 0
	},
	secondaryHeading: {
		fontSize: theme.typography.pxToRem(15),
		color: theme.palette.text.secondary
	}
});

class ControlledExpansionPanels extends React.Component {

	state = {
		data: false,
		// expanded: null,
		expanded: 'panel-0',
		expanded_page: null,
	}

	componentDidMount() {
		apiPages('apidocs').then((response) => {
			let d = response.data
			this.setState({
				data: _.values(d.results)
			}, () => {})
		})
	}

	handleChangeExpansion = panel => (event, expanded) => {
		this.setState({
			expanded: expanded ? panel : false,
			expanded_page: null,
		});
	};
	handleChangeExpansionPage = (expanded_id) => {
		this.setState({
			expanded_page: expanded_id
				? expanded_id
				: false
		});
	};




	renderPagesInner(pages) {
		const {expanded_page} = this.state;

		return (
			<ul className={theme.pagesList}>
				{pages.map((item, index) => {
					let expanded_id = `page-${index}`

					let autoExpand = false
					if(pages.length == 1) {
						autoExpand = true
					}
					return (
						<li key={index}>
							<ul data-ul={expanded_page === expanded_id || autoExpand ? 'titleActive' : 'title'}
								onClick={() => this.handleChangeExpansionPage(expanded_id)}
								>
								<li data-li="icon">
									{expanded_page === expanded_id || autoExpand ? <Icon>remove</Icon> : <Icon>add</Icon>}
								</li>
								<li data-li="text">
									{item.title}
								</li>
							</ul>
							<Collapse in={expanded_page === expanded_id || autoExpand}>
								<div data-el="description">
									<p dangerouslySetInnerHTML={{__html: item.description}} />

									{item.request &&
										<div>
											<div data-el="apiButton">
												<a href={`${apiEndpoint}${item.request}`} target="_blank" data-link="underlined">{`${apiEndpoint}${item.request}`}</a>
												<ModalApiRequest request={item.request} />
											</div>
										</div>
									}
								</div>

							</Collapse>
						</li>
					)
				})}
			</ul>
		)
	}

	renderDocsList() {
		const {classes} = this.props;
		const {data, expanded} = this.state;

		return (
			<div>
				{data.map((item, index) => {
					let expanded_id = `panel-${index}`

					if(item.pages.length > 0) {
						return (
							<ExpansionPanel key={index} expanded={expanded === expanded_id} onChange={this.handleChangeExpansion(expanded_id)}>
								<ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}
									classes={{
										root: theme.headerWrapper,
										content: theme.headerWrapperContent,
									}} >
									<Typography className={classes.heading}>{item.category_name} {`(${item.pages.length})`}</Typography>
									<Typography className={classes.secondaryHeading}>{item.category_description}</Typography>
								</ExpansionPanelSummary>
								<ExpansionPanelDetails>

									{this.renderPagesInner(item.pages)}

								</ExpansionPanelDetails>
							</ExpansionPanel>
						)
					}
				})}
			</div>
		)
	}

	render() {

		const {data} = this.state

		return (
			<div>
				<BlockHeaderBg
					bgid={31}
					title='API KUPINET'
					description={'Documentation for working with our API'} />

				<div data-content-inner className={theme.docsWrapper}>
					{!data ?
						<div data-box style={{padding: 20}}>
							<Preloader />
						</div>
						: this.renderDocsList()
					}
				</div>
			</div>
		);
	}
}

ControlledExpansionPanels.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ControlledExpansionPanels);
