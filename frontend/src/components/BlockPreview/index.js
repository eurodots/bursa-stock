import React, {Component} from 'react';
import PropTypes from 'prop-types';
// import Icon from 'material-ui/Icon';
// import Typography from 'material-ui/Typography';

import theme from './theme.scss'
import {apiDomain} from '../../config/init'

class BlockPreview extends Component {

    clickFunc = () => {
        const {onClick} = this.props
        if(onClick) {
            this.props.onClick()
        }
    }

    renderPreview() {
        const {dataImage, onClick, title} = this.props

        return (
            <ul data-block="preview">
                <li data-li="header">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                    {title && <h5>{title}</h5>}
                </li>
                <li data-bg={onClick ? 'overlay' : ''} onClick={onClick ? this.clickFunc : false}>
                    <img src={`${apiDomain}/media/landing/${dataImage}`} />
                </li>
            </ul>

        )
    }


	render() {

		return (
			<div className={theme.wrapper}>
                {this.renderPreview()}
            </div>
		)
	}
}

BlockPreview.propTypes = {
  dataImage: PropTypes.string.isRequired,
  // onClick: PropTypes.func.isRequired,
};

export default BlockPreview
