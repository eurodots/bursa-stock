import React, {Component} from 'react';

import {Link} from 'react-router-dom';
// import Typography from 'material-ui/Typography';
import {confSite} from '../../../config/init.js'

import theme from './theme.scss'

export default class Footer extends Component {

    state = {}

	render() {
		return (
			<div className={theme.wrapper}>
                <div data-content-inner>
                    <ul>
                        <li data-li="logotype">
                            <h3>
                                {confSite.name}
                            </h3>
                            <p>{confSite.slogan}</p>
                            <p>
                                <Link to="/icosignup">Download for free →</Link>
                            </p>
                        </li>
                        <li data-li="menu">
                            <ul>
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/p/demo">Demo</Link></li>
                                <li><Link to="/p/pricing">Pricing</Link></li>
                                <li><Link to="/p/tokens">Tokens</Link></li>
                            </ul>
                            <ul>
                                <li><Link to="/p/metamask">MetaMask</Link></li>
                                <li><Link to="/p/docs">Docs</Link></li>
                                <li><Link to="/p/support">Support</Link></li>
                            </ul>
                            <ul>
                                <li><a href={confSite.links.facebook} target="_blank">Facebook</a></li>
                                <li><a href={confSite.links.bitcointalk} target="_blank">Bitcoin Talk</a></li>
                            </ul>
                            <div>
                                <div data-el="gototop" onClick={() => window.scrollTo(0,0)}>
                                    Go to top
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div data-el="copyright">
                        Copyright © 2018 All rights reserved. {confSite.name}
                    </div>
                </div>
            </div>
		)
	}
}
