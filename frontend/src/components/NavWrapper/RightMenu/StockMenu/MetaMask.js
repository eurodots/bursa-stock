import React from 'react';
import {Link} from 'react-router-dom';
import Button from 'material-ui/Button';

import SvgIcon from '../../../SvgIcon';
import {confSite} from '../../../../config/init'
import theme from './theme.scss'

class Application extends React.Component {


    renderInstall() {

        return (
            <div className={theme.blockInstall}>
                <ul className={theme.listMetamask}>
                    <li className={theme.li_icon}>
                        <SvgIcon name="metamask" gradient={false} />
                    </li>
                    <li className={theme.li_hint}>
                        For more convenient work, use our modification of the MetaMask plugin.
                    </li>
                    <li className={theme.li_install}>
                        <Button onClick={() => window.open(confSite.metamask_plugin.chrome)} color="primary" variant="raised">
                            Get Chrome Extension
                        </Button>
                    </li>
                </ul>
                <ul className={theme.listLinks}>
                    <li>
                        <span onClick={() => window.open(confSite.metamask_plugin.github)} data-link="underlined">Github</span>
                    </li>
                    <li>
                        <Link to="/p/metamask" data-link>Description</Link>
                    </li>
                </ul>
            </div>
        )
    }

  render() {
        // console.log(appendElementContainer)

        // let test = appendElementContainer.toString()

        return (
          <div style={{height: '100%'}}>
              <div data-el="MetaMask">
                  <div id="MetaMaskIframe">
                      {this.renderInstall()}
                  </div>
                  <div id="MetaMaskDuplicate" data-el="duplicate" className={theme.MetaMaskDuplicateHide}>
                      The MetaMask extension for BursaDex can not be displayed, because you have the original MetaMask extension installed.
                  </div>
              </div>
          </div>
         );
  }
}

export default Application
// ReactDOM.render(<Application />, document.querySelector('.react-render-element'));
