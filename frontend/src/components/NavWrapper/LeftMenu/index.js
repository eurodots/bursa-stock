import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import ListSubheader from 'material-ui/List/ListSubheader';
import List, {ListItem, ListItemIcon, ListItemText} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Icon from 'material-ui/Icon';


const _ = require('lodash');

import theme from './theme.scss'



class MenuGenerator extends React.Component {


	render() {
		const { data, label } = this.props

		return (
			<List
				component="nav"
				subheader={<ListSubheader component="div" data-menu-label>{label}</ListSubheader>}
				>

				{data.map((item, index) => {
					if(item.type == 'divider') {
						return (
							<Divider key={index} />
						)
					}
					return (
						<Link to={item.link} key={index}>
							<ListItem button data-menulist-item>
								<ListItemIcon>
									<Icon>{item.icon}</Icon>
								</ListItemIcon>
								<ListItemText primary={item.label}/>
							</ListItem>
						</Link>
					)
				})}

			</List>
		)
	}
}



// import style from './theme';
const styles = theme => ({
	root: {
		width: '100%',
		maxWidth: 360,
		background: theme.palette.background.paper
	},
	nested: {
		paddingLeft: theme.spacing.unit * 4
	},
});

class MenuComponent extends React.Component {

	state = {};


	render() {

		const {user} = this.props

		const menu_public = [
			{
				type: 'menu',
				icon: 'home',
				link: '/',
				label: 'Home',
			},
			{
				type: 'divider',
			},
			{
				type: 'menu',
				icon: 'compare_arrows',
				link: '/p/calc',
				label: 'Calculator',
			},
			{
				type: 'menu',
				icon: 'monetization_on',
				link: '/p/coinslist',
				label: 'Coins list',
			},
			{
				type: 'menu',
				icon: 'show_chart',
				link: '/p/pairs',
				label: 'Orders & Pairs',
			},
			{
				type: 'divider',
			},
			{
				type: 'menu',
				icon: 'code',
				link: '/p/docs-api',
				label: 'API docs',
			},
			{
				type: 'menu',
				icon: 'help',
				link: '/p/support',
				label: 'Support',
			},
		]
		const menu_public_extra = [
			{
				type: 'menu',
				icon: 'monetization_on',
				link: '/p/pricing',
				label: 'Pricing',
			},
			{
				type: 'menu',
				icon: 'https',
				link: '/p/demo',
				label: 'Demo',
			},
			{
				type: 'menu',
				icon: 'info',
				link: '/p/docs',
				label: 'Docs',
			},
		]
		const menu_admin = [
			{
				type: 'menu',
				icon: 'local_dining',
				link: '/p/fishrod',
				label: 'Fishrod',
			},
			{
				type: 'menu',
				icon: 'equalizer',
				link: '/p/equalizer',
				label: 'Equalizer',
			},
			{
				type: 'divider',
			},
		]
		const menu_private = [
			{
				type: 'menu',
				icon: 'insert_chart',
				link: '/p/user/stocks',
				label: 'My stocks',
			},
			{
				type: 'menu',
				icon: 'people',
				link: '/',
				label: 'Affiliates',
			},
			{
				type: 'menu',
				icon: 'account_box',
				link: '/',
				label: 'Profile',
			},
			{
				type: 'divider',
			},
		]

		return (
			<div className={theme.leftMenu}>

				{user.data && user.data.isAdmin ?
					<div>
						<MenuGenerator data={menu_admin} label="Admin menu" />
					</div>
				: ''}
				{user.data ?
					<div>
						<MenuGenerator data={menu_private} label="Account menu" />
					</div>
				: ''}

				<MenuGenerator data={menu_public_extra} label="Extra menu" />
				<MenuGenerator data={menu_public} label="Public menu" />
			</div>
		);
	}
}

MenuComponent.propTypes = {
	classes: PropTypes.object.isRequired
};

// export default withStyles(styles)(LeftMenu);


export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			// onSelectCoins: (payload) => {
			// 	dispatch({type: 'SELECT_COINS', payload})
			// },
		})
	))
	(withStyles(styles)(MenuComponent))
);
