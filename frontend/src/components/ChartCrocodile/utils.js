import {web3ordersBook} from '../../utils_stock/getWeb3.js'



export async function getDataChart(tokenAddress) {
	let getOrders = await web3ordersBook(tokenAddress).then((res) => {
		return res
	})

	let asks = []
	let bids = []

	getOrders.map((item, index) => {

		if(item.type == 'sell') {
			asks.push({
				x: item.price,
				y: item.total,
				z: 0,
			})
		} else {
			bids.push({
				x: item.price,
				y: 0,
				z: item.total,
			})
		}


	})
	asks = _.orderBy(asks, ['total'], ['asc']);
	bids = _.orderBy(bids, ['total'], ['desc']);

	let together = []
	asks.map((item, index) => { together.push(item) })
	bids.map((item, index) => { together.push(item) })

	console.log('----')
	console.log(together)

	return together

}
