import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Typography from 'material-ui/Typography';


import theme from './theme.scss'
import {confSite} from '../../config/init'
const _ = require('lodash');

class BlockDonation extends Component {


	render() {

		return (
			<div className={theme.donationsWrapper} {...this.props}>
                <Typography variant="title" gutterBottom>
                    Thanks for donations!
                </Typography>
                <ul className={theme.donationsList}>
                    {_.keys(confSite.donations).map((key, index) => {
                        return (
                            <li key={index}>
                                <label>{confSite.donations[key].name}</label>
                                <span>{confSite.donations[key].wallet}</span>
                            </li>
                        )
                    })}
                </ul>
            </div>
		)
	}
}

BlockDonation.propTypes = {
  // dataImage: PropTypes.string.isRequired,
  // onClick: PropTypes.func.isRequired,
};

export default BlockDonation
