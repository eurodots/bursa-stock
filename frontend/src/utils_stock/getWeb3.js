"use strict";

// import CircularJSON from 'circular-json';
import axios from 'axios';
import {Web3BackendOrders, Web3MetamaskAuth, Web3MetamaskSaveOrder, Web3MetamaskCancelOrder} from './functions'
import {cookie} from '../utils/functions'

import Web3 from 'web3';
import BursaContract from './contract.js';

let bursaInstance = false
// if(typeof web3 !== 'undefined') {
//     bursaInstance = web3.eth.contract(BursaContract.abi).at(BursaContract.address)
// }
async function loadInstance() {
    if(window.web3 != 'undefined' && window.web3.eth != 'undefined') {
        bursaInstance = window.web3.eth.contract(BursaContract.abi).at(BursaContract.address)
    } else {
        setTimeout(() => {
            loadInstance()
        }, 1000)
    }
}
window.onload = (e) => {
    loadInstance()
}




// INIT WEB3 WITH CONTRACT
async function initWeb3() {
    let data = {
        web3: false,
        status: false,
    }

    let web3 = false
    if(window.web3 == 'undefined') {
        window.addEventListener('load', () => {

            // Fallback to localhost if no web3 injection. We've configured this to
            // use the development console's port by default.
            web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"))
            data.web3 = false
            data.status = 'No web3 instance injected, using Local web3.'

            cookie('delete','web3_wallet')
        })
    } else {
        let web3 = window.web3
        web3 = new Web3(web3.currentProvider)
        data.web3 = true
        data.status = 'MetaMask activated!'

        // alert('!')
        // console.log('------------')
        window.web3.eth.getAccounts((error, accounts) => {
            cookie('set',['web3_wallet',accounts[0]])
            // axios.defaults.headers.common['Authorization'] = `Web3Token ${accounts[0]}}`;
            Web3MetamaskAuth().then(response => {})
        })

    }


    return data
}



export async function getWeb3() {

    if(bursaInstance) {
        let response = await new Promise((resolve, reject) => {

            let web3_data = initWeb3()
            let web3_user_address = false
            let web3_user_balance = false
            let web3_contract_balance = false

            // RETRIEVE DATA...

                // USER ADDRESS
                web3.eth.getAccounts((error, accounts) => {
                    web3_user_address = accounts[0]


                    // USER BALANCE
                    if(web3_user_address) {
                        web3.eth.getBalance(web3_user_address, async (err, res) => {
                          if (!err) {
                              web3_user_balance = fromWei(res)


                              // USER BALANCE AR CONTRACT
                              // if(typeof(bursaInstance.balanceOf(web3_user_address, () => {})) !== 'function') {
                              //     return false
                              // }

                              await bursaInstance.balanceOf(web3_user_address, async (err, res) => {
                                  if(!err) {
                                      web3_contract_balance = fromWei(res)
                                      // console.log( data )

                                      // let url_ethplorer = 'https://api.ethplorer.io/getAddressInfo/'+web3_user_address+'?apiKey=freekey'
                                      // console.log( url_ethplorer )
                                      //
                                      // axios.get(url_ethplorer).then((res) => {
                                      //     // let json = CircularJSON.stringify(res);
                                      //     // json = CircularJSON.parse(json)
                                      //     return res.data;
                                      // }).then((ethplorer) => {
                                      //
                                      //     let responseData = {
                                      //         status: web3_data.status,
                                      //         web3: web3_data.web3,
                                      //         user_address: web3_user_address,
                                      //         user_balance: web3_user_balance,
                                      //         contract_balance: web3_contract_balance,
                                      //         ethplorer: ethplorer
                                      //     }
                                      //     // console.log(responseData)
                                      //     resolve(responseData)
                                      //
                                      // }).catch(err => { throw err });


                                      let responseData = {
                                          status: web3_data.status,
                                          web3: web3_data.web3,
                                          user_address: web3_user_address,
                                          user_balance: web3_user_balance,
                                          contract_balance: web3_contract_balance,
                                      }

                                      resolve(responseData)


                                  } else {
                                      console.error(error)
                                  }
                              })


                          } else {
                            console.log(err);
                          }
                        })
                    }

                })

        })

        return response
    }

}


// USER DEPOSIT
export async function depositWeb3(value) {

    let response = await new Promise((resolve, reject) => {
        bursaInstance.deposit({value: toWei(value)}, (err, res) => {
            if(!err) {
                resolve(res)
                return res
            } else {
                console.log(err)
            }
        })
    })

    Divider('DEPOSIT')
    console.log(response)
    return response

}



// USER WITHDRAWAL
export async function withdrawalWeb3(value) {

    let response = await new Promise((resolve, reject) => {
        bursaInstance.withdraw(toWei(value), (err, res) => {
            if(!err) {
                resolve(res)
                return res
            } else {
                console.log(err)
            }
        })
    })

    Divider('WITHDRAWAL')
    console.log(response)
    return response

}





// WILL SELL INFO (SELL/ASKS)
export async function web3ordersBook(token_address) {



    let orders_arr = []

    // FETCHING EXTRA ORDERS =)))
    await Web3BackendOrders(token_address).then(response => {
        orders_arr = response
        console.log(orders_arr)
    })

    return orders_arr

    /*
    let data = initWeb3()
    function arrPrepare(spot_id, type, res) {

        let wei_price = res[1]
        let wei_amount = res[2]

        if(wei_price && wei_amount) {

            let price = fromWei(res[1], 18)
            let amount = fromWei(res[2], 18)
            let total = Number(price) * Number(amount)

            if(price > 0 && amount > 0) {
                let item = {
                    type: type,
                    user_address: res[0],
                    price: price,
                    amount: amount,
                    total: total,
                    spot_id: spot_id,

                    wei_amount: wei_amount,
                    wei_price: wei_price,
                }
                // console.log(item)
                orders.push(item)

            }
        }

        // console.log(orders)

    }

    async function fetchOrderBySpot(spot_id, type) {
        // console.log(spot_id)
        try {
            if(type == 'sell') {
                await bursaInstance.willsellInfo(token_address, spot_id, (err, res) => {
                    if(!err) arrPrepare(spot_id, type, res)
                    else console.log(err)
                })
            } else if(type == 'buy') {
                await bursaInstance.willbuyInfo(token_address, spot_id, (err, res) => {
                    if(!err) arrPrepare(spot_id, type, res)
                    else console.log(err)
                })
            }
        } catch (e) {
            console.log(e)
          // return 'caught';
        }
    }

    for (let i = 0; i < 30; i++) {
        await new Promise(resolve => setTimeout(resolve, Math.random() * 100));
        // console.log(i);
        if(!type) {
            fetchOrderBySpot(i, 'sell');
            fetchOrderBySpot(i, 'buy');
        } else {
            if(type=='sell') fetchOrderBySpot(i, 'sell');
            else if(type=='buy') fetchOrderBySpot(i, 'buy');
        }

    }




    if(orders.length > 0) {

        let orders_sort = []
        orders.map((item, index) => {
            let p = item
                p.price = parseFloat(p.price)
                p.amount = parseFloat(p.amount)
                orders_sort.push(p)
        })

        orders = _.orderBy(orders_sort, ['price'], ['desc']);
        return orders
    } else {
        return false
    }
    */

}




var defaultTokenAbi = [{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"from","type":"address"},{"name":"to","type":"address"},{"name":"value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"who","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"to","type":"address"},{"name":"value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"owner","type":"address"},{"name":"spender","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint2html56"}],"name":"Transfer","type":"event"}]


// let testTokenInstance = false
function initDefaultToken(token) {
    if(typeof web3 !== 'undefined') {
        return web3.eth.contract(defaultTokenAbi).at(token)
    }
}


// Разрешаем бирже списывать наши токены с аккаунта (если не указано второе значение, то разрешаем все токены)
export async function web3approve(init) {

    let defaultTokenInstance = initDefaultToken(init.token)

    // нужно апрувить ровно столько сколько есть на балансе, не больше
    let response = await new Promise((resolve, reject) => {
        defaultTokenInstance.approve(BursaContract.address, toWei(init.value), (err, res) => {
          if(!err) {
              resolve(res)
          } else {
              console.log(err)
          }
        })
    })

    return response
}


export async function web3orderSell(init) {

    if(init.price <= 0 || init.amount <= 0) {
        alert('Price or Amount can\'t be null')
        return false
    }

    let response = new Promise((resolve, reject) => {

        let question = confirm('web3placeAll() ?');
        if(question) {
            web3placeAll(init, 'sell').then((res,err) => {
                // alert('web3placeAll()')
                console.log(res)
            })
        } else {
            web3findEmptySpot({type: 'sell', token: init.stock_token}).then( async (empty_spot) => {
                await bursaInstance.willsell(toWei(init.amount), init.stock_token, toWei(init.price), empty_spot, (err, res) => {
                    if(!err) {

                        let store_order = init
                            store_order.hash = res
                            store_order.type = 'sell'
                            store_order.spot_id = empty_spot

                        Web3MetamaskSaveOrder(store_order)
                        // console.log(store_order)

                        resolve(res)
                    } else {
                        console.log(err)
                    }
                })
            })
        }
    })

    return response
}

export async function web3orderBuy(init) {

    if(init.price <= 0 || init.amount <= 0) {
        alert('Price or Amount can\'t be null')
        return false
    }

    let response = new Promise((resolve, reject) => {

        let question = confirm('web3placeAll() ?');
        if(question) {
            web3placeAll(init, 'buy').then((res,err) => {
                // alert('web3placeAll()')
                console.log(res)
            })
        } else {
            web3findEmptySpot({type: 'buy', token: init.stock_token}).then( async (empty_spot) => {
                await bursaInstance.willbuy(toWei(init.amount), init.stock_token, toWei(init.price), empty_spot, (err, res) => {
                    if(!err) {
                        let store_order = init
                            store_order.hash = res
                            store_order.type = 'buy'
                            store_order.spot_id = empty_spot

                        Web3MetamaskSaveOrder(store_order)
                        // console.log(store_order)
                        resolve(res)
                    } else {
                        console.log(err)
                    }
                })
            })
        }
    })

    return response
}


export async function web3findEmptySpot(init) {

    let response = new Promise((resolve, reject) => {
        if(init.type == 'sell') {
            bursaInstance.willsellFindSpot(init.token, (err, res) => {
                console.log('SPOT SELL')
                console.log(res)
                resolve(parseFloat(res))
            })
        } else if(init.type == 'buy') {
            bursaInstance.willbuyFindSpot(init.token, (err, res) => {
                console.log('SPOT BUY')
                console.log(res)
                resolve(parseFloat(res))
            })
        }
    })

    return response
}


// Check orders before willbuy & trying to buy available spots
// buy(amount, token, min_price_each, order, frontend_refund)
export async function web3placeAll(init, type) {

    let response = new Promise(function(resolve, reject) {

        let ordersbook_type = type == 'buy' ? 'arr_asks' : 'arr_bids'
        web3ordersBook(init.token).then(ordersbook => {

            // console.log(ordersbook)

            // Prepare spots_arr by available orders with price
            let ordersbook_arr = ordersbook[ordersbook_type]
            alert('Check console.log')
            console.log('-------')
            console.log(ordersbook_arr)
            let spots_arr = []
            ordersbook_arr.map((order, index) => {
                if(type == 'sell' && order.price >= parseFloat(init.price)) {
                    spots_arr.push(order.spot_id)
                } else if(type == 'buy' && order.price <= parseFloat(init.price)) {
                    spots_arr.push(order.spot_id)
                }
            })

            // let type_sort = type == 'buy' ? 'asc' : 'desc'
            // let ordersbook_sort = _.orderBy(ordersbook, ['price'], [type_sort]);

            // ordersbook_sort.map((order, index) => {
            //     if(type == 'sell' && order.price >= parseFloat(init.price)) {
            //         spots_arr.push(order.spot_id)
            //     } else if(type == 'buy' && order.price <= parseFloat(init.price)) {
            //         spots_arr.push(order.spot_id)
            //     }
            // })

            console.log(spots_arr)

            if(type == 'buy') {
                bursaInstance.buyAll(toWei(init.amount), init.token, toWei(init.price), spots_arr, 0, (err, res) => {
                    alert('buyAll()')
                    console.log(res)
                    resolve(res)
                })
            } else if(type == 'sell') {
                bursaInstance.sellAll(toWei(init.amount), init.token, toWei(init.price), spots_arr, 0, (err, res) => {
                    alert('sellAll()')
                    // console.log(res)

                    // let t = window.web3.eth.contract(BursaContract.abi).at(BursaContract.address)
                    // bursaInstance.allEvents((err, res) => {
                    //     console.log('--events--')
                    //     console.log(err)
                    //     console.log(res)
                    // })

                    resolve(res)
                })
            }

        })
    })

    return response
}


// Узнаем сколько токенов на балансе у аккаунта
export async function web3balanceOf(token_address, user_address) {
    let defaultTokenInstance = initDefaultToken(token_address)

    let response = await new Promise((resolve, reject) => {
        defaultTokenInstance.balanceOf(user_address, (err, res) => {
          if(!err) {
              let balance = ( ((parseFloat(res)).toPrecision(50))/toWei(1) )
              resolve(balance)
              return balance
          } else {
              console.log(err)
          }
        })
    })

    return response
}

export async function web3balanceForToken(token_address, user_address) {

    let defaultTokenInstance = initDefaultToken(token_address)

    let response = await new Promise((resolve, reject) => {

        if(bursaInstance) {
            bursaInstance.balanceApprovedForToken(token_address, user_address, (err, res) => {
              if(!err) {
                  let result = res / toWei(1)
                  // result = result.toPrecision(8)
                  // console.log( fromWei(parseFloat(res)) )
                  // console.log(Web3.utils.fromWei(parseFloat(res), "ether"))
                  resolve(result)
                  return result
              } else {
                  resolve(0)
                  console.log(err)
              }
            })
        } else {
            resolve(0)
        }

    })

    return response
}



export async function web3orderCancel(init) {

    loadInstance()

    let response = await new Promise((resolve, reject) => {

        if(init.type == 'sell') {

            bursaInstance.buy(toWei(init.amount), init.stock_token, 0, init.spot_id, 0, (err, res) => {
                alert('Canceled')
                console.log('SELL')
                console.log( toWei(init.amount) )
                console.log(init)
                console.log(res)
                Web3MetamaskCancelOrder(init)
                resolve(res)
                return res
            })
        } else if(init.type == 'buy') {
            console.log('BUY')
            console.log(init)
            bursaInstance.sell(toWei(init.amount), init.stock_token, 0, init.spot_id, 0, (err, res) => {
                alert('Canceled')
                Web3MetamaskCancelOrder(init)
                resolve(res)
                return res
            })
        }
    })
    return response
}



// async function collectOrders(block) {
// 	var blockNumber = await web3.eth.getBlockNumber()
// 	var orders = await bursaInstance.getPastEvents('Order', {
// 		fromBlock: block,
// 		toBlock: blockNumber
// 	}, async function(e, order) {});
//
// 	// tmp TODO REMOVE
// 	data.orders = [];
//
// 	var i = 0;
// 	while (i < orders.length) {
// 		var o = orders[i].returnValues;
// 		var amountLeft = await bursaInstance.methods.amountLeft(o.tokenGet, o.amountGet, o.tokenGive, o.amountGive, o.block, o.user).call({from: acc0});
// 		if (amountLeft > 0) {
// 			var ord = await getOrderData(o);
// 			data.orders.push(ord);
// 		}
// 		++i;
// 	}
// 	data.block = blockNumber;
//
// 	data.orders = data.orders.sort(function(a, b) {
// 		if (a.priceOfTokenGet == b.priceOfTokenGet) {
// 			return a.event.block - b.event.block;
// 		}
// 		return b.priceOfTokenGet - a.priceOfTokenGet;
// 	});
//
// 	await saveData();
// }



/*
instantiateContract() {

  const contract = require('truffle-contract')
  const simpleStorage = contract(SimpleStorageContract)
  simpleStorage.setProvider(this.state.web3.currentProvider)

  // Declaring this for later so we can chain functions on SimpleStorage.
  var simpleStorageInstance

  // Get accounts.
  this.state.web3.eth.getAccounts((error, accounts) => {
    simpleStorage.deployed().then((instance) => {
      simpleStorageInstance = instance

      // Stores a given value, 5 by default.
      return simpleStorageInstance.set(5, {from: accounts[0]})
    }).then((result) => {
      // Get the value from the contract to prove it worked.
      return simpleStorageInstance.get.call(accounts[0])
    }).then((result) => {
      // Update state with the result.
      return this.setState({ storageValue: result.c[0] })
    })
  })
}
*/



// export default getWeb3




// HELP FUNCTIONS

    function Divider(value='Web3') {
        let d = '-----------'
        console.log(d+value+d)
    }
    // Divider()



    function toWei(price) {
        // let value = parseFloat(price).toPrecision(30).substr(0,18)
        // value = Web3.utils.toWei(value, 'ether');
        // return value
        return window.web3.toWei(price.toString(), 'ether');
    }
    function fromWei(price, size=18) {
        // price = 1e+21
        // 1e21 = 100000.000000000000000.0000000000

        let value = parseFloat(price).toPrecision(30)
        value = value.split('.')
        value = value[0]
        value = window.web3.fromWei(value.toString(), 'ether');
        // value = Number(value).toPrecision(30)
        // value = Number(value)
        return value
      // return Web3.utils.fromWei(price.toString(), 'ether');
    }





    function scientificToDecimal(num) {
        //if the number is in scientific notation remove it
        if(/\d+\.?\d*e[\+\-]*\d+/i.test(num)) {
            var zero = '0',
                parts = String(num).toLowerCase().split('e'), //split into coeff and exponent
                e = parts.pop(),//store the exponential part
                l = Math.abs(e), //get the number of zeros
                sign = e/l,
                coeff_array = parts[0].split('.');
            if(sign === -1) {
                num = zero + '.' + new Array(l).join(zero) + coeff_array.join('');
            }
            else {
                var dec = coeff_array[1];
                if(dec) l = l - dec.length;
                num = coeff_array.join('') + new Array(l+1).join(zero);
            }
        }

        // num = num.substr(0,18)

        return num;
    };
