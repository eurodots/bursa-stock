import {apiEndpoint} from '../config/init'
import {cookie} from '../utils/functions'

import axios from 'axios';
const _ = require('lodash');





// FETCHING METAMASK API DATA
export function Web3BackendOrders(token) {

    const url = `${apiEndpoint}/web3/orderbook/${token}/`
    console.log(url)

	const data = axios.get(url).then((response) => {
        if(!response.data.error) {
            return response.data.results
        } else {
            return []
        }

	}).catch((error) => {
		console.log(error);
	})

	return data
}




// METAMASK
export function Web3MetamaskAuth() {

    let web3_wallet = cookie('get','web3_wallet')

    const url = `${apiEndpoint}/web3/metamask/${web3_wallet}/auth/`
    console.log(url)

	const data = axios.get(url).then((response) => {
        // if(!response.data.error) {
        //     return response.data.results
        // } else {
        //     return []
        // }
        return response

	}).catch((error) => {
		console.log(error);
	})

	return data
}



export function Web3MetamaskSaveOrder(request) {

    let web3_wallet = cookie('get','web3_wallet')

    const url = `${apiEndpoint}/web3/metamask/${web3_wallet}/orders/save/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
        return response

	}).catch((error) => {
		console.log(error);
	})

	return data
}

export function Web3MetamaskCancelOrder(request) {

    let web3_wallet = cookie('get','web3_wallet')

    const url = `${apiEndpoint}/web3/metamask/${web3_wallet}/orders/cancel/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
        return response

	}).catch((error) => {
		console.log(error);
	})

	return data
}


export function Web3MetamaskOrdersList() {

    let web3_wallet = cookie('get','web3_wallet')

    const url = `${apiEndpoint}/web3/metamask/${web3_wallet}/orders/list/`
    console.log(url)

	const data = axios.get(url).then((response) => {
        if(!response.data.error) {
            return response
        } else {
            return []
        }

	}).catch((error) => {
		console.log(error);
	})

	return data
}
