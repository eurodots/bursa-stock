import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cssModules from 'react-css-modules';
import styles from '../style/index.scss';

import NavWrapper from '../components/NavWrapper'
import Preloader from '../components/Preloader'

import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {isBrowser, isMobile} from 'react-device-detect';
const _ = require('lodash');

import {apiTokenAuth, pageTitle, cookie} from '../utils/functions'

@cssModules(styles)
class App extends Component {


  constructor(props) {
    super(props)

    this.state = {}
  }

  componentDidMount() {

      this.titleChecker()
      // if(isBrowser) {
      //     this.props.onToggleLeftMenu(true)
      // }

      apiTokenAuth().then((user) => {
          console.log(user)
          if(user) {
              this.props.onLoginByToken({
                  token: user.token,
                  name: user.email,
                  email: user.email,
                  isAdmin: user.is_superuser,
              })
          } else {
              console.log('Something wrong with auth...')
          }
      })

  }


  componentWillReceiveProps(nextProps) {

    // SCROLL TO TOP IF PAGE CHANGED
    let nextpage = nextProps.routing.location.pathname
    if (this.props.routing.location.pathname != nextpage) {
    	window.scrollTo(0, 0);
    	this.titleChecker(nextpage)
        if(isMobile) {
            this.props.onToggleLeftMenu(false)
        }
    }
  }

  titleChecker(nextpage=false) {
      if(!nextpage) {
          nextpage = this.props.routing.location.pathname
          // pageTitle()
      }

      let titles_arr = [
          {
              title: 'Home',
              path: '/',
              exact: true,
          },
          {
              title: 'Auth',
              path: '/p/login',
              exact: false,
          },
          {
              title: 'SingUp',
              path: '/p/signup',
              exact: false,
          },
      ]

      for (let i=0; i < titles_arr.length; i++) {
          if(!titles_arr[i].exact && nextpage.indexOf(titles_arr[i].path) !=-1) {
              document.title = titles_arr[i].title
              break
          } else if(titles_arr[i].exact && nextpage == titles_arr[i].path) {
              document.title = titles_arr[i].title
          }
      }

  }

  renderWrapper() {
      const { children, styles } = this.props;

      return (
        <div className={styles.container}>
            <NavWrapper {...this.props}>

                {children}

            </NavWrapper>
        </div>
      );
  }


  render() {

      return (
          <div>

              {this.renderWrapper()}

          </div>
      )
  }
}


App.propTypes = {
    children: PropTypes.any.isRequired,
    styles: PropTypes.object
};

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onToggleLeftMenu: (payload) => {
				dispatch({type: 'LEFT_MENU_TOGGLE', payload})
			},
			onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
            onLoginByToken: (payload) => {
				dispatch({type: 'USER_LOGGED_IN', payload})
			},
		})
	))
	(App)
);
