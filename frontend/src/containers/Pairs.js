import React, {Component} from 'react';
import PagePairs from '../components/Pages/Pairs'

export default class Pairs extends Component {
	render() {
		return (
			<div data-content>
				<PagePairs />
			</div>
		)
	}
}
