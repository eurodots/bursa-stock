import React, {Component} from 'react';
import PageCoinsList from '../components/Pages/CoinsList'

export default class CoinsList extends Component {
	render() {
		return (
			<div data-content>
				<PageCoinsList />
			</div>
		)
	}
}
