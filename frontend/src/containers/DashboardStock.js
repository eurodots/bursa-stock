import React, {Component} from 'react';
import PageDashboardStock from '../components/Pages/DashboardStock'

export default class DashboardStock extends Component {

	render() {
		return (
			<div data-content>
				<PageDashboardStock />
			</div>
		)
	}
}
