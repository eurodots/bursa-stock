import React, {Component} from 'react';
import PageDashboardTrader from '../components/Pages/DashboardTrader'

export default class DashboardTrader extends Component {

	render() {
		return (
			<div data-content>
				<PageDashboardTrader />
			</div>
		)
	}
}
