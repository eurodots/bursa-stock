import cookielib from 'react-cookies';
import axios from 'axios';
const _ = require('lodash');

import {apiDomain, apiEndpoint, confSite} from '../config/init'



// PAGE TITLE FUNC()
export function pageTitle(title=false) {
    if(!title) {
        document.title = confSite.name
    } else {
        document.title = `${title} — ${confSite.name}`
    }
    return true
}


// USER STOCKS SETTINGS
export function apiUserStocksList() {

    const url = `${apiEndpoint}/user/stocks/list/`
    console.log(url)

	const data = axios.get(url).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

export function apiUserStocksDelete(id) {

    let request = {
        'stock_id': id,
    }
    const url = `${apiEndpoint}/user/stocks/delete/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

export function apiUserStocksGet(stock_id) {
    const url = `${apiEndpoint}/user/stocks/edit/${stock_id}/`
    console.log(url)

	const data = axios.get(url).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

export function apiUserStocksUpdate(postData, stock_id) {
    const url = `${apiEndpoint}/user/stocks/edit/${stock_id}/`
    console.log(url)

	const data = axios.post(url, postData).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

// LINK PREVIEWER
export function apiLinkPreview(postData) {
    const url = `${apiEndpoint}/linkpreview/`
    console.log(url)

	const data = axios.post(url, postData).then((response) => {
		return response
	}).catch((error) => {
		console.log(error);
	})

	return data
}

// CONTENT PAGES
export function apiPages(folder=false) {
    const url = `${apiEndpoint}/pages/${folder}/`
    console.log(url)

	const data = axios.get(url).then(response => {
		return response
	})

	return data
}

export function apiPricelist() {
    const url = `${apiEndpoint}/datalist/pricelist/`
    console.log(url)

	const data = axios.get(url).then(response => {
		return response
	})

	return data
}


// CALCULATOR
export function apiCalculatorMath(coin_from, coin_to, amount) {
    const url = `${apiEndpoint}/v1/freeApi/calc/math/${coin_from}/${coin_to}/${amount}/`
    console.log(url)

	const data = axios.get(url).then(response => {
		return response
    })

    return data
}
export function apiCalculatorData() {
    const url = `${apiEndpoint}/v1/freeApi/calc/data/`
    console.log(url)

	const data = axios.get(url).then(response => {
		return response
    })

    return data
}


// COINS LIST
export function apiCoinsListCurrent(coin) {
    const url = `${apiEndpoint}/datalist/coinslist-current/${coin}/`
    console.log(url)

	const data = axios.get(url).then(response => {
		return response
    })

    return data
}

export function apiCoinsList() {
    const url = `${apiEndpoint}/datalist/coinslist/`
    console.log(url)

	const data = axios.get(url).then(response => {
		return response
    })

    return data
}


// FISHROD
export function apiFishrodApplyNewPrice(request) {
    const url = `${apiEndpoint}/tasks/fishrod-apply-price/`
    console.log(url)
    console.log(request)

	const data = axios.post(url, request).then((response) => {
		return response
    })

    return data
}

export function apiFishrod() {
    const url = `${apiEndpoint}/tasks/fishrod/`
    console.log(url)

	const data = axios.get(url).then(response => {
		return response
    })

    return data
}


// SUPPORT
export function apiContactForm(request) {
    const url = `${apiEndpoint}/support/contactform/`
    console.log(url)

	const data = axios.post(url, request).then(response => {
		return response
	})

	return data
}

// AUTH
export function apiTokenAuth() {

    const url = `${apiEndpoint}/accounts/user-profile/`
    console.log(url)

    const token = cookie('get', 'token')
    const data = axios.get(url).then((response) => {
        if(response.status == 200) {
            let r = response.data.user
            r['token'] = token
            return r
        } else {
            cookie('delete', 'token')
            return false
        }
    })

    return data
}


export function apiPasswordSubmit(uid, token, request) {
    const url = `${apiEndpoint}/accounts/reset/${uid}/${token}/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
    })

    return data
}


export function apiPasswordReset(request) {
    const url = `${apiEndpoint}/accounts/password_reset/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
    })

    return data
}

export function apiSignupVerify(token) {
    const url = `${apiEndpoint}/accounts/verify/${token}/`
    console.log(url)

	const data = axios.get(url).then((response) => {
		return response
    })

    return data
}

export function apiSignup(request) {
    const url = `${apiEndpoint}/accounts/register/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
    })

    return data
}

export function apiSignin(request) {
    const url = `${apiEndpoint}/accounts/login/`
    console.log(url)

	const data = axios.post(url, request).then((response) => {
		return response
    })

    return data
}



// EQUALIZER
export async function getEqualizer() {
    const url = `${apiEndpoint}/equalizer/list/`
    console.log(url)

	const data = await axios.get(url).then((response) => {
		return response
    })

    return data
}




// Colorized zeros in number
export function numConvert(value, fix=8) {

    if(typeof value === "undefined") {
        return ''
    }

    if(value.toString().length > 0 && (typeof(value) == 'string' || typeof(value) == 'number')) {
        let n = isNaN(value) ? 0 : value

            n = parseFloat(n).toPrecision(8)
            // n = n.split('e')[0]
            n = parseFloat(n).toFixed(fix)

        let b = n
        let c = n.split('.')[1].split('').reverse()
        let iter = ""
        for(let i=0; i<c.length; i++) {
            if(c[i] == 0) {
                iter += "0"
                b = b.slice(0, -1)
            } else {
                break
            }
        }
        let resp = n
        if(iter.length > 1) {
            resp = `${b}<u data-zero>${iter}</u>`
        }
        // return `<strong>${n}</strong>`
        return resp

    } else {
        return ''
    }

}



export function cookie(type, payload) {
    switch (type) {
    	case 'get':
            let value = cookielib.load(payload)
            if(typeof(value) === 'undefined' || value === 'undefined') {
                value = false
            }
    		return value
        case 'set':
            cookielib.save(payload[0], payload[1], {path: '/'})

            // Hack for authorization
            if(payload[0] == 'token') {
                axios.defaults.headers.common['Authorization'] = `Token ${payload[1]}`;
            }

            return true
        case 'delete':
            cookielib.remove(payload, {path: '/'})
            return true
    	default:
    		return false
    }
}




let CURRENT_AUDIO = false
export function soundsCollection(song, play) {
    let sound_url = `${apiDomain}/media/sounds/${song}.mp3`

    if(CURRENT_AUDIO.paused || CURRENT_AUDIO.currentTime >= 0) {
        CURRENT_AUDIO.pause()
    } else {
        CURRENT_AUDIO = new Audio(sound_url)
    }
    if(play) {
        // CURRENT_AUDIO.play()
    } else {
        CURRENT_AUDIO.pause()
        CURRENT_AUDIO = false
    }
}



// Token for API Authorization
export let GLOBAL_RESPONSE_STATUS = true
function applyAxiosSettings() {
    axios.defaults.headers.common['Authorization'] = `Token ${cookie('get', 'token')}`;
    axios.defaults.xsrfCookieName = 'csrftoken';
    axios.defaults.xsrfHeaderName = 'X-CSRFToken';

    axios.interceptors.response.use(response => {
        soundsCollection('connection_error', false)

        GLOBAL_RESPONSE_STATUS = true
        return response;
    }, error => {

        soundsCollection('connection_error', true)

        if (!error.response) {
            // console.log('Network error!')
            GLOBAL_RESPONSE_STATUS = 'Network error!'
        } else {
            console.log(error.response.status)
            GLOBAL_RESPONSE_STATUS = error.response.status
            return error.response
        }
        return Promise.reject(error.response);
    })


} applyAxiosSettings()
