// console.log( process.env )

export const ENV = process.env.NODE_ENV || 'development';
export const isProduction = ENV === 'production';

export const apiDomain = isProduction ? 'http://api.stock.kupi.net' : 'http://127.0.0.1:8000';
// export const apiDomain = isProduction ? 'http://127.0.0.1:8000' : 'http://127.0.0.1:8000';
// export const apiDomain = isProduction ? 'http://bursadex.com:8888' : 'http://bursadex.com:8888';
export const apiEndpoint = `${apiDomain}/api`


export const apiEtherscan = 'VJT69AJD19KQDUBKSJKAEBEQRA21WJBIPC'

export const confSite = {
    name: 'Kupi.net',
    version: '1.0',
    slogan: 'Do what you do',
    links: {
        facebook: 'http://facebook.com/',
        bitcointalk: 'http://bitcointalk.com/',
    },
    smartcontract: {
        etherscan: 'https://etherscan.io/address/kupinet.eth',
        github: 'https://github.com/Kupinet/our-smart-contract'
    },
    metamask_plugin: {
        'chrome': 'https://chrome.google.com/webstore/detail/metamask-by-kupinet/lpodkilpfnkllpeiebmmkalohjkdnghm',
        'github': 'https://github.com/Kupinet/metamask-extension',
    },
    stock_token: '0xafe5a978c593fe440d0c5e4854c5bd8511e770a4/',
    donations: {
        ETH: {
            'name': 'Ethereum',
            'wallet': '0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0',
        },
        BTC: {
            'name': 'BitCoin',
            'wallet': '0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0',
        },
    }
}
