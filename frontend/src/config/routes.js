import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from '../containers/Home';
import Fishrod from '../containers/Fishrod';
import Equalizer from '../containers/Equalizer';

import CoinsList from '../containers/CoinsList';
import Pairs from '../containers/Pairs';
import Calculator from '../containers/Calculator';
import Support from '../containers/Support';
import Demo from '../containers/Demo';
import Pricing from '../containers/Pricing';
import Docs from '../containers/Docs';
import DocsApi from '../containers/DocsApi';
import MetaMask from '../containers/MetaMask';

import DashboardStock from '../containers/DashboardStock';
import DashboardTrader from '../containers/DashboardTrader';


import UserStocks from '../containers/UserStocks';
import UserStocksAdd from '../containers/UserStocksAdd';

import NotFoundPage from '../containers/NotFoundPage';

import AdminComponent from '../containers/Auth/Admin'
import LoginComponent from '../containers/Auth/Login'
import Verify from '../containers/Auth/Verify'


import { userIsAuthenticatedRedir, userIsNotAuthenticatedRedir, userIsAdminRedir,
         userIsAuthenticated, userIsNotAuthenticated } from '../auth'


// Need to apply the hocs here to avoid applying them inside the render method
const Login = userIsNotAuthenticatedRedir(LoginComponent)
const Admin = userIsAuthenticatedRedir(userIsAdminRedir(AdminComponent))

const ProtectedUserStocks = userIsAuthenticatedRedir(UserStocks)
const ProtectedUserStocksAdd = userIsAuthenticatedRedir(UserStocksAdd)


export default (
  <Switch>
    <Route exact path="/" component={Home}/>
    <Route exact path="/p/fishrod" component={Fishrod}/>
    <Route exact path="/p/equalizer" component={Equalizer}/>
    <Route exact path="/p/coinslist" component={CoinsList}/>
    <Route exact path="/p/pairs" component={Pairs}/>
    <Route exact path="/p/calc" component={Calculator}/>
    <Route exact path="/p/support" component={Support}/>
    <Route exact path="/p/demo" component={Demo}/>
    <Route exact path="/p/pricing" component={Pricing}/>
    <Route exact path="/p/docs" component={Docs}/>
    <Route exact path="/p/docs-api" component={DocsApi}/>
    <Route exact path="/p/metamask" component={MetaMask}/>


    <Route path="/p/dashboard/stock/:coin_id" component={DashboardStock} />
    <Route path="/p/dashboard/trader" component={DashboardTrader} />


    <Route exact path="/p/login" component={Login}/>
    <Route exact path="/p/signup" component={Login}/>
    <Route path="/p/login/reset/:password_reset_uid/:password_reset_token" component={Login}/>
    <Route path="/p/verify/:token" component={Verify}/>

    <Route exact path="/p/user/stocks" component={ProtectedUserStocks}/>

    <Route path="/p/user/stocks/:stock_id" component={ProtectedUserStocksAdd}/>
    <Route path="/p/user/stocks/add/:tariff" component={ProtectedUserStocksAdd}/>

    <Route component={NotFoundPage} />

  </Switch>
);
