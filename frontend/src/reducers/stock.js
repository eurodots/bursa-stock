import * as constants from '../constants'
const _ = require('lodash');

const initialState = {
  coins_data: [],
  coins_arr: [],
  selected_coins: ['RHOC','TRX','VEN'],
  web3Metamask: {
      contract_balance: 0,
      user_balance: 0,
      user_address: '',
      status: false,
      web3: false,
  },
  web3Transactions: [],
  tokenStockInit: {
    stock_name: "Kupinet",
    stock_token: "0xd26114cd6EE289AccF82350c8d8487fedB8A0C07",
    stock_private: false,
  }
}


export default function stockUpdate(state = initialState, { type, payload }) {

  // console.log('-------------//')
  // console.log(type)
  // console.log(payload)

  switch (type) {

    case 'SAVE_WEB3_METAMASK':
        state.web3Metamask = payload
        // console.log(payload)
        return {...state}

    case 'SAVE_WEB3_TRANSACTIONS':
        let data = state.web3Transactions
        let user_address = payload.user_address
        let transactions = payload.transactions
        if(!data[user_address]) {
            data[user_address] = []
        }
        data[user_address].push(transactions)
        state.web3Transactions = data

        return {...state}



    case 'PRELOAD_COINS':
        state.coins_arr = _.values(payload.coins)
        state.coins_data = payload.data
        return {...state}

    case 'SELECT_COINS':

        let coins_arr = state.coins_arr
        let selected_coins = state.selected_coins

        if(payload.type) {
            payload.array.map((item, index) => {
                let coin = item.toUpperCase()
                if(coins_arr.includes(coin)) {
                    selected_coins.push(coin)
                    state.selected_coins = Array.from(new Set(selected_coins))
                }
            })
        } else {
            payload.array.map((item, index) => {
                state.selected_coins = selected_coins.filter(e => e !== item)
            })
        }

        return {...state}


    case 'STOCK_INIT':
        state.tokenStockInit = payload
        return {...state}

    default:
      return state
  }
}
