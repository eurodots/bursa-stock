import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import counter from './counter';
import user from './user';
import config from './config';
import stock from './stock';

const rootReducer = combineReducers({
  counter,
  user,
  config,
  routing,
  stock
});

export default rootReducer;
