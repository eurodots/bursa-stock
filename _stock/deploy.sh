#!/bin/bash

SYSTEM="/home/admin/web/_stock"
GIT=$SYSTEM/GIT
FRONTEND="/home/admin/web/stock.kupi.net"

BACKEND="/home/admin/web/api.stock.kupi.net"
BACKEND_PORT=7700
FRONTEND_PORT=7701

# A menu driven shell script sample template
## ----------------------------------
# Step #1: Define variables
# ----------------------------------
EDITOR=vim
PASSWD=/etc/passwd
RED='\033[0;41;30m'
STD='\033[0;0;39m'

# ----------------------------------
# Step #2: User defined function
# ----------------------------------
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

frontend(){

  cd $GIT && git pull

  rm -rf $FRONTEND/public_html
  cp -r $GIT/frontend $FRONTEND/public_html

  cd $FRONTEND/public_html
  ln -s $SYSTEM/FRONTEND/node_modules/ node_modules

  # yarn build
  # ln -s build $BACKEND/public_html/media/
  # rm -rf .next
  # unzip .next.zip
  # npm run build

  echo "DONE"
  pause
}

frontendNpmUpdate(){
  cd $FRONTEND/public_html
  yarn install
  # nohup npm run start &
}

# do something in two()
backend(){

  cd $GIT && git pull
  rm -rf $BACKEND/public_html
  cp -r $GIT/backend $BACKEND/public_html

  rm $BACKEND/public_html/DEFAULT/settings_local.py
  mv $BACKEND/public_html/DEFAULT/settings_public.py $BACKEND/public_html/DEFAULT/settings_local.py


  # connect backup
  cd $BACKEND/public_html

  rm -rf $BACKEND/public_html/media
  rm $BACKEND/public_html/db.sqlite3
  ln -s $SYSTEM/BACKEND/media media
  ln -s $SYSTEM/BACKEND/db.sqlite3 db.sqlite3



  # VIRTUAL ENV
  # /usr/local/bin/virtualenv -p python3.5 ENV
  # source ENV/bin/activate
  # pip3 install -r requirements.txt
  # python manage.py runserver
  ln -s $SYSTEM/BACKEND/ENV ENV
  source ENV/bin/activate


  python3 manage.py makemigrations
  python3 manage.py migrate
  python3 manage.py collectstatic

  echo "BACKEND STARTED!"
  pause

  # nohup uwsgi --http-keepalive --http 10000 --module DEFAULT.wsgi &

}

nginxFrontend(){
  NGINX_NAME="stock.kupi.net.nginx.conf"
  cd /home/admin/conf/web
  sudo rm $NGINX_NAME
  sudo unlink $NGINX_NAME
  sudo ln -s $SYSTEM/nginx_frontend.conf $NGINX_NAME
  sudo cat $NGINX_NAME
  /bin/sleep 3
  sudo /etc/init.d/nginx restart

  # sudo pkill -f :$FRONTEND_PORT & /bin/sleep 2
  # sudo pkill -f node


  cd $FRONTEND/public_html
  nohup yarn start &
}
nginxBackend(){
  NGINX_NAME="api.stock.kupi.net.nginx.conf"
  cd /home/admin/conf/web
  sudo rm $NGINX_NAME
  sudo unlink $NGINX_NAME
  sudo ln -s $SYSTEM/nginx_backend.conf $NGINX_NAME
  sudo cat $NGINX_NAME

  # sudo pkill -f :$BACKEND_PORT & /bin/sleep 2

  sudo /etc/init.d/nginx restart
  cd $BACKEND/public_html/
  nohup uwsgi --http-keepalive --http :$BACKEND_PORT --module DEFAULT.wsgi &
  # nohup uwsgi --http-keepalive --http :7000 --module DEFAULT.wsgi &
  # uwsgi --http :7700 --module DEFAULT.wsgi &
  # uwsgi --http-keepalive --http 7000 --module DEFAULT.wsgi &
  # uwsgi --http :7700 --module DEFAULT.wsgi

}

nginx(){
  nginxBackend
  nginxFrontend
  exit
}


# function to display menus
show_menus() {
  clear
  echo "~~~~~~~~~~~~~~~~~~~~~"
  echo " M A I N - M E N U"
  echo "~~~~~~~~~~~~~~~~~~~~~"
  echo "1. FRONTEND"
  echo "2. FRONTEND: NPM UPDATE"
  echo "3. BACKEND"
  echo "4. Restart NGINX"
  echo "6. Exit"
}
# read input from the keyboard and take a action
# invoke the one() when the user select 1 from the menu option.
# invoke the two() when the user select 2 from the menu option.
# Exit when user the user select 3 form the menu option.
read_options(){
  local choice
  read -p "Enter choice [ 1 - 3] " choice
  case $choice in
    1) frontend ;;
    2) frontendNpmUpdate ;;
    3) backend ;;
    4) nginx ;;
    6) exit 0;;
    *) echo -e "${RED}Error...${STD}" && sleep 2
  esac
}


if [ $1 = "start" ]; then
   echo "START!"
   nginx
else
  # ----------------------------------------------
  # Step #3: Trap CTRL+C, CTRL+Z and quit singles
  # ----------------------------------------------

  trap '' SIGINT SIGQUIT SIGTSTP

  # -----------------------------------
  # Step #4: Main logic - infinite loop
  # ------------------------------------
  while true
  do

    show_menus
    read_options
  done
fi
